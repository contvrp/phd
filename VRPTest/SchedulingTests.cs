﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DynamicVehicleRouting;
using ParticleSwarmOptimization;

namespace VRPTest
{
    [TestClass]
    public class SchedulingTests
    {
        [TestMethod]
        public void BenchmarkTest()
        {
            var vehicles = new List<Vehicle>();
            var clients = new List<Client>();
            Depot depot = null;
            decimal timeStep = 0.025M;
            var fileName = "c120D.vrp.010-0,575.txt";
            var currentDayPercent = LoadScheduleFromFile(vehicles, clients, ref depot, fileName);
            var distances = ComputeDistances(depot, clients.ToArray());
            Assert.AreEqual(120, clients.Count);
            while (currentDayPercent <= 1.0M)
            {
                var value = 0.0;
                foreach (Vehicle vehicle in vehicles)
                {
                    vehicle.SetEarliestLeaveTime((currentDayPercent + timeStep) * depot.EndAvailable);
                    vehicle.Distances = distances;
                    vehicle.ignoreTimeBounds = false;

                    double[] center = vehicle.Encode();
                    value += vehicle.CommitVehicles(center, 1.6M, timeStep, false); 
                    Assert.IsTrue(value < double.MaxValue);
                }
                Assert.AreEqual(false, clients.Where(
        clnt => clnt.StartAvailable <= currentDayPercent * depot.EndAvailable && vehicles.Sum(vhcl => vhcl.assignedClients.Count(clnt2 => clnt2.Id == clnt.Id) + vhcl.clientsToAssign.Count(clnt2 => clnt2.Id == clnt.Id)) == 0
        ).Any());
                currentDayPercent += timeStep;
            }
            Assert.AreEqual(120, vehicles.Sum(vhcl => vhcl.assignedClients.Where(clnt => !(clnt is Depot)).Count()));

        }

        [TestMethod]
        public void RouteEncoding()
        {
            Random random = new Random();
            var vehicles = new List<Vehicle>();
            var clients = new List<Client>();
            Depot depot = null;
            decimal timeStep = 0.025M;
            var fileName = "c120D.vrp.010-0,575.txt";
            var currentDayPercent = LoadScheduleFromFile(vehicles, clients, ref depot, fileName);
            var distances = ComputeDistances(depot, clients.ToArray());
            vehicles[0].Distances = distances;
            vehicles[0].SetDepot(depot);
            vehicles[0].SetEarliestLeaveTime((currentDayPercent + timeStep) * depot.EndAvailable);
            vehicles[0].clientsToAssign = vehicles[0].clientsToAssign.Where(clnt => !(clnt is Depot)).OrderBy(clnt => clnt.Rank).ToList();
            var route = vehicles[0].Encode();
            var value = vehicles[0].Value(route);
            vehicles[0].clientsToAssign = vehicles[0].clientsToAssign.Where(clnt => !(clnt is Depot)).OrderBy(clnt => clnt.Id).ToList();
            Assert.AreEqual(value, vehicles[0].Value(vehicles[0].Encode()));
            Assert.AreEqual(value, vehicles[0].Value(route));
            vehicles[0].clientsToAssign = vehicles[0].clientsToAssign.Where(clnt => !(clnt is Depot)).OrderBy(clnt => random.Next()).ToList();
            Assert.AreEqual(value, vehicles[0].Value(vehicles[0].Encode()));
            Assert.AreEqual(value, vehicles[0].Value(route));
        }

        private static decimal LoadScheduleFromFile(List<Vehicle> vehicles, List<Client> clients, ref Depot depot, string fileName)
        {
            string[] lines = System.IO.File.ReadAllLines(fileName);
            string[] firstLine = lines[0].Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);
            Assert.AreEqual("200", firstLine[3]);
            var currentDayPercent = decimal.Parse(firstLine[2]);
            var capacity = int.Parse(firstLine[3]);
            decimal rank = 0.0M;
            for (int lineNo = 2; lineNo < lines.Length; lineNo++)
            {
                var timeEntry = lines[lineNo].Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);
                var vehicleId = int.Parse(timeEntry[0]);
                var clientId = int.Parse(timeEntry[1]);
                var x = int.Parse(timeEntry[2]);
                var y = int.Parse(timeEntry[3]);
                var endTime = int.Parse(timeEntry[5]);
                var visitTime = decimal.Parse(timeEntry[6]);
                var need = -int.Parse(timeEntry[7]);
                var assigned = bool.Parse(timeEntry[8]);
                var unloadTime = int.Parse(timeEntry[9]);
                if (depot == null && clientId == 0)
                {
                    depot = new Depot()
                    {
                        X = x,
                        Y = y,
                        Id = clientId,
                        EndAvailable = endTime
                    };
                }
                Vehicle current;
                if (!vehicles.Any(vhcl => vhcl.Id == vehicleId))
                {
                    var vehicle = new Vehicle()
                    {
                        Available = true,
                        Capacity = capacity,
                        Cargo = 0,
                        clientsToAssign = new List<Client>(),
                        assignedClients = new List<Client>(),
                        Id = vehicleId,
                    };
                    vehicle.SetDepot((Depot)depot.Clone());
                    current = vehicle;
                    vehicles.Add(vehicle);
                }
                else
                {
                    current = vehicles.FirstOrDefault(vhcl => vhcl.Id == vehicleId);
                }
                if (clientId > 0)
                {
                    var client = new Client()
                    {
                        X = x,
                        Y = y,
                        Id = clientId,
                        Need = need,
                        VisitTime = assigned ? visitTime : 0,
                        Rank = rank,
                        TimeToUnload = unloadTime,
                        FakeVehicleId = current.Id,
                    };
                    rank += 1.0M / (decimal)lines.Length;
                    clients.Add(client);
                    if (assigned)
                    {
                        current.assignedClients.Add((Client)client.Clone());
                    }
                    else
                    {
                        current.clientsToAssign.Add((Client)client.Clone());
                    }
                }
                else
                {
                    if (assigned)
                    {
                        current.assignedClients.Add((Depot)depot.Clone());
                        current.assignedClients[current.assignedClients.Count - 1].VisitTime = visitTime;
                    }
                }
            }
            return currentDayPercent;
        }

        [TestMethod]
        public void VehicleToStringTest()
        {
            Vehicle vehicle;
            Depot depot;
            double[,] distances;
            SetupInstanceAndVehicle(out vehicle, out depot, out distances);
            Assert.AreEqual("5 [] [1,2,3]", vehicle.ToString());

        }

        [TestMethod]
        public void JustInTimeScheduleTest()
        {
            decimal timeStep = 0.125M;
            Vehicle vehicle;
            Depot depot;
            double[,] distances;
            SetupInstanceAndVehicle(out vehicle, out depot, out distances);
            for (decimal planningTime = timeStep; planningTime <= 1.0M; planningTime += timeStep)
            {
                double length = vehicle.GetRouteLength(new Depot[] { depot }, 1, distances);
                Assert.AreEqual(4, length);
                vehicle.SetEarliestLeaveTime(planningTime * depot.EndAvailable);
                vehicle.ignoreTimeBounds = false;
                vehicle.Distances = distances;
                vehicle.SetDepot(depot);
                vehicle.CommitVehicles(
                    vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).OrderBy(clnt => clnt.Id).Select(clnt => (double)clnt.Rank).ToArray(),
                    0,
                    timeStep,
                    false
                    );
            }
        }

        [TestMethod]
        public void TwoOPTRandomRouteTest()
        {
            Vehicle vehicle;
            Depot depot;
            double[,] distances;
            for (int seed = 1; seed < 21; seed++)
            {
                foreach (int size in new int[] { 10, 50, 100 })
                {
                    SetupRandomInstanceAndVehicle(size, seed, out vehicle, out depot, out distances);
                    var ranks = vehicle.Encode();
                    var value = vehicle.Value(ranks);
                    Assert.IsTrue(value >= vehicle.Value(vehicle.EnhanceWith2Opt(vehicle.Encode(), new Depot[] { depot }, distances)));
                }
            }
        }


        [TestMethod]
        public void LateScheduleTest()
        {
            Vehicle vehicle;
            Depot depot;
            double[,] distances;
            SetupInstanceAndVehicle(out vehicle, out depot, out distances);
            double length = vehicle.GetRouteLength(new Depot[] { depot }, 2, distances);
            Assert.AreEqual(double.MaxValue, length);
            Assert.AreEqual(false, vehicle.clientsToAssign.Any(clnt => clnt.Id == 3));
        }

        private static void SetupInstanceAndVehicle(out Vehicle vehicle, out Depot depot, out double[,] distances)
        {
            vehicle = new Vehicle()
            {
                Available = true,
                Capacity = 1000,
                Cargo = 0,
                clientsToAssign = new List<Client>(),
                assignedClients = new List<Client>(),
                Id = 5,
            };
            depot = new Depot()
            {
                X = 0,
                Y = 0,
                EndAvailable = 8,
                Id = 0,
            };
            Client[] clients = new Client[]
            {
                new Client()
                {
                    Id = 1,
                    Need = -1,
                    TimeToUnload = 1,
                    StartAvailable = 0,
                    X = 0,
                    Y = 1,
                    Rank = 0.0M,
                },
                new Client()
                {
                    Id = 2,
                    Need = -1,
                    TimeToUnload = 1,
                    StartAvailable = 0,
                    X = 1,
                    Y = 1,
                    Rank = 0.5M,
                },
                new Client()
                {
                    Id = 3,
                    Need = -1,
                    TimeToUnload = 1,
                    StartAvailable = 0,
                    X = 1,
                    Y = 0,
                    Rank = 1.0M,
                },
            };
            distances = ComputeDistances(depot, clients);
            vehicle.clientsToAssign.Add(clients[0]);
            vehicle.clientsToAssign.Add(clients[1]);
            vehicle.clientsToAssign.Add(clients[2]);
            vehicle.SetDepot(depot);
        }

        private static void SetupRandomInstanceAndVehicle(int clientsNo, int randomSeed, out Vehicle vehicle, out Depot depot, out double[,] distances)
        {
            Random random = new Random(randomSeed);
            vehicle = new Vehicle()
            {
                Available = true,
                Capacity = clientsNo,
                Cargo = 0,
                clientsToAssign = new List<Client>(),
                assignedClients = new List<Client>(),
                Id = 5,
            };
            depot = new Depot()
            {
                X = 0,
                Y = 0,
                EndAvailable = int.MaxValue,
                Id = 0,
            };
            Client[] clients = new Client[clientsNo];
            var rank = 0.0M;
            for (int c = 0; c < clientsNo; c++)
            {
                clients[c] = new Client()
                {
                    Id = c+1,
                    Need = -1,
                    TimeToUnload = 1,
                    StartAvailable = 0,
                    X = random.Next(100) - 50,
                    Y = random.Next(100) - 50,
                    Rank = rank,
                };
                rank += 1.0M / clientsNo;
                vehicle.clientsToAssign.Add(clients[c]);
            }
            distances = ComputeDistances(depot, clients);
            vehicle.SetDepot(depot);
            vehicle.Distances = distances;
            vehicle.SetEarliestLeaveTime(0.0M);
        }

        private static double[,] ComputeDistances(Depot depot, Client[] clients)
        {
            double[,] distances;
            distances = new double[clients.Length + 1, clients.Length + 1];
            var locations = clients.Concat(new Client[] { depot });
            foreach (Client client1 in locations)
            {
                foreach (Client client2 in locations)
                {
                    distances[client1.Id, client2.Id] = Utils.Instance.EuclideanDistance(new double[] { client1.X, client1.Y }, new double[] { client2.X, client2.Y });
                }
            }
            return distances;
        }
    }
}
