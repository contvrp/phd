﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace BenchmarkProject
{
    class DBConnector
    {
        private static string connStr = "Server=194.29.178.211;Database=DataAnalyzer;User Id=dvrp;Password=dvrp;";
        public static object connLocker = new object();
        public static List<SqlCommand> commands = new List<SqlCommand>();
        public const double EPS = 0.05;

        public static void InsertNewResult(DateTime timestamp, string name, double result, decimal dod, int iterations, double bestKnownResult, double resultToBest, string type, double computationsTime)
        {
            string sql = "INSERT INTO [dbo].[PSO_DVRP_LIMITED_AREAS_PHASE] ( " +
                           "[Timestamp]  " +
                           ",[Name] " +
                           ",[Result] " +
                           ",[Dod] " +
                           ",[Iterations] " +
                           ",[BestKnownResult] " +
                           ",[ResultToBest] " +
                           ",[Type] " +
                           ",[ComputationsTime] " +
                           ",[Machine]) " +
                     "VALUES " +
                           "(@timestamp " +
                           ",@name " +
                           ",@result " +
                           ",@dod " +
                           ",@iterations " +
                           ",@bestKnownResult " +
                           ",@resultToBest " +
                           ",@type " +
                           ",@time " +
                           ",@machine)";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.Parameters.AddWithValue("@timestamp", timestamp);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@result", result);
            cmd.Parameters.AddWithValue("@dod", dod);
            cmd.Parameters.AddWithValue("@iterations", iterations);
            cmd.Parameters.AddWithValue("@bestKnownResult", bestKnownResult);
            cmd.Parameters.AddWithValue("@resultToBest", resultToBest);
            cmd.Parameters.AddWithValue("@type", type);
            cmd.Parameters.AddWithValue("@time", computationsTime);
            cmd.Parameters.AddWithValue("@machine", Environment.MachineName);
            cmd.CommandType = CommandType.Text;
            lock (connLocker)
            {
                commands.Add(cmd);
            }
            ExecuteInsertResults();
        }

        public static void ExecuteInsertResults()
        {
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                try
                {
                    lock (connLocker)
                    {
                        connection.Open();
                        while (commands.Any())
                        {
                            commands[0].Connection = connection;
                            commands[0].ExecuteNonQuery();
                            commands.RemoveAt(0);
                        }
                    }
                }
                catch (SqlException)
                {
                }
            }
        }

        public static int CheckNumberOfExperiments(string name, string type)
        {
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                try
                {
                    lock (connLocker)
                    {
                        connection.Open();
                        string sql = "SELECT count(*) FROM [dbo].[PSO_DVRP_LIMITED_AREAS_PHASE] " +
                            "WHERE Type = @type AND Name = @name";
                        SqlCommand cmd = new SqlCommand(sql);
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@name", name);
                        cmd.Parameters.AddWithValue("@type", type);
                        return (int)cmd.ExecuteScalar();
                    }
                }
                catch (SqlException)
                {
                    return 0;
                }
            }
        }

        public static double GetMinValueForExperiment(string name)
        {
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                try
                {
                    lock (connLocker)
                    {
                        connection.Open();
                        string sql = "select min(Result) from PSO_DVRP_LIMITED_AREAS_PHASE " +
                            "where Type IN ( " +
                            "'2MPSO 20v3x3+4 8/007/044/0(0.025)', " +
                            "'2MPSO 20v3x3+4 8/070/443/0(0.025)', " +
                            "'2MPSO_RSv3x3+4 2OPT 8/22/140(0.025)', " +
                            "'2MPSOv2x1+50 2OPT 8/040/250/800', " +
                            "'2MPSOv2x1+50 2OPT 8/100/1000/100', " +
                            "'2MPSOv2x3+4 2OPT 8/040/250/800', " +
                            "'2MPSOv2x3+4 2OPT 8/100/1000/100', " +
                            "'DE 260v3x3+4 8/010/031/0C0.90(0.025)', " +
                            "'DE 260v3x3+4 8/016/195/0C0.90(0.025)', " +
                            "'DE 260v3x3+4 8/051/617/0C0.90(0.025)', " +
                            "'DE 260v3x3+4 8/160/1950/0C0.90(0.025)', " +
                            "'PSO 16v3x3+4 8/022/140/0(0.025)', " +
                            "'PSO 20v3x3+4 8/020/127/0(0.025)', " +
                            "'PSO 20v3x3+4 8/020/127/27(0.025)', " +
                            "'PSO 20v3x3+4 8/220/1400/0(0.025)', " +
                            "'PSO NoHistory 20v3x3+4 8/022/140/0(0.025)' " +
                            ") and Name LIKE @name " +
                            "group by Name " +
                            "order by Name";
                        SqlCommand cmd = new SqlCommand(sql);
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@name", name);
                        return (double)(decimal)cmd.ExecuteScalar();
                    }
                }
                catch (SqlException)
                {
                    return double.MaxValue;
                }
            }
        }


    }
}
