﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using DynamicVehicleRouting;
using ParticleSwarmOptimization;
using System.Drawing;
using System.Net.Mail;
using System.Net;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace BenchmarkProject
{
    class Benchmark
    {
        static List<Process> computingServices;
        public static Dictionary<int, bool> busyServices;
        static bool breakKiller = false;
        static Dictionary<Thread, DateTime> threadsCalls = new Dictionary<Thread, DateTime>();
        static readonly object threadlock = new object();
        static DateTime lastMessage = DateTime.Now;
        static MailMessage message = null;
        static Semaphore semaphore = new Semaphore(
            Properties.Settings.Default.ThreadCount == -1 ? Environment.ProcessorCount : Properties.Settings.Default.ThreadCount,
            Properties.Settings.Default.ThreadCount == -1 ? Environment.ProcessorCount : Properties.Settings.Default.ThreadCount);
        public static Semaphore serviceAccess = new Semaphore(Environment.ProcessorCount, Environment.ProcessorCount);

        static string path = Properties.Settings.Default.InitialDir;
        static readonly object resultslock = new object();
        private static int expCount = 1;

        static bool ConsoleEventCallback(int eventType)
        {
            if (message != null)
            {
                TrySendMail();
            }
            return false;
        }

        static ConsoleEventDelegate handler;   // Keeps it from getting garbage collected
        // Pinvoke
        private delegate bool ConsoleEventDelegate(int eventType);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetConsoleCtrlHandler(ConsoleEventDelegate callback, bool add);

        static void Main(string[] args)
        {
            handler = new ConsoleEventDelegate(ConsoleEventCallback);
            SetConsoleCtrlHandler(handler, true);
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);
            Thread killer = new Thread(ThreadKiller);
            killer.Start();
            SortedSet<string> benchmarks = new SortedSet<string>();
            foreach (string filter in Properties.Settings.Default.FileFilter.Split(';'))
            {
                DirectoryInfo info = new DirectoryInfo(path);
                foreach (string file in info.GetFiles(filter, SearchOption.AllDirectories).Select(file => file.FullName))
                    benchmarks.Add(file);
            }
            StartServices();
            for (int i = 0; i < Properties.Settings.Default.ExperimentsForFile; i++)
            {
                List<string> benchmarksList = benchmarks.ToList();
                if (Properties.Settings.Default.RandomOrderOfInstances)
                    benchmarksList = benchmarks.OrderBy(bnch => Utils.Instance.random.Next()).ToList();
                foreach (string filepath in benchmarksList)
                {
                    Thread t = new Thread(RunBenchmark);
                    semaphore.WaitOne();
                    t.Start(filepath);
                    lock (client)
                    {
                        if (message != null && (DateTime.Now - lastMessage).TotalMinutes > (new Random()).Next(10) + 25)
                        {
                            TrySendMail();
                        }
                    }
                }
            }
            for (int i = 0; i < (Properties.Settings.Default.ThreadCount == -1 ? Environment.ProcessorCount : Properties.Settings.Default.ThreadCount); i++)
                semaphore.WaitOne();
            lock (client)
            {
                if (message != null)
                    TrySendMail();
            }
            StopServices();

            Console.WriteLine("Skończyłem.");
            breakKiller = true;
            killer.Interrupt();
            killer.Join();
            Console.ReadKey();
        }

        public static void StopServices()
        {
            foreach (var p in computingServices)
            {
                try
                {
                    p.Kill();
                }
                catch
                {
                    //Does not metter
                }
            }
        }

        public static void StartServices()
        {
            computingServices = new List<Process>();
            busyServices = new Dictionary<int, bool>();
            int port = 6540;
            for (int services = 0; services < Environment.ProcessorCount; ++services)
            {
                if (Properties.Settings.Default.UseProcessesNotThreads)
                {
                    Process p = Process.Start(new ProcessStartInfo("BenchmarkProjectComputingService.exe", port.ToString())
                    {
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        WindowStyle = ProcessWindowStyle.Hidden
                    });
                    computingServices.Add(p);
                    busyServices.Add(port, false);
                }
                port++;
            }
        }

        static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            StopServices();
            if (message != null)
            {
                TrySendMail();
            }
        }

        private static void TrySendMail()
        {
            lastMessage = DateTime.Now;
            if (Properties.Settings.Default.SendEmail)
                try
                {
                    client.Send(message);
                    message = null;
                }
                catch
                {
                    //Wysłanie maila nie jest super ważne
                }
        }

        static SmtpClient client;
        static Benchmark()
        {
            client= new SmtpClient("smtp.gmail.com", 587);
            client.Credentials = new NetworkCredential("optymalizacja.rojowa@gmail.com", "eberhart1");
            client.EnableSsl = true;
        }

        private static void RunBenchmark(object filepath)
        {
            try
            {
                lock (threadlock)
                {
                    threadsCalls.Add(Thread.CurrentThread, DateTime.Now);
                }
                int expNo = 0;
                lock (semaphore)
                {
                    expNo = expCount++;
                }
                List<DVRPThreadSolveWrapper> functions = new List<DVRPThreadSolveWrapper>();
                for (int i = 0; i < Properties.Settings.Default.ParallelInstances; i++)
                {
                    functions.Add(new DVRPThreadSolveWrapper()
                    {
                        function = new DVRPInstance(
                        (string)filepath,
                        Properties.Settings.Default.TimeStep,
                        Properties.PSO.Default.SwarmSize,
                        Properties.PSO.Default.SwarmIterations,
                        Properties.PSO.Default.RouteSwarmIterations,
                        Properties.PSO.Default.NeighbourhoodProb1,
                        Properties.PSO.Default.InventorsRatio1,
                        Properties.PSO.Default.NeighbourhoodProb1,
                        Properties.PSO.Default.NeighbourhoodProb2,
                        Properties.Historical.Default.InitialTime,
                        (DVRPInstance.DegreeOfDynamicity)Properties.Historical.Default.DOD,
                        Properties.Settings.Default.Optimizer,
                        Properties.Historical.Default.InitialCapacityPart,
                        Properties.Historical.Default.CapacitySlope,
                        Properties.Historical.Default.TraceDetailedInfo,
                        Properties.Historical.Default.DistanceWeightPower,
                        Properties.Historical.Default.UsePreviousSolution,
                        Properties.Heuristics.Default.Use2Opt,
                        Properties.PSO.Default.ClassesForVehicle,
                        Properties.PSO.Default.ReservoirVehiclesCount,
                        Properties.MC.Default.InitialMonteCarloPartOfTheDay,
                        Properties.MC.Default.InitialMonteCarloProposals,
                        Properties.MC.Default.AdditionalMonteCarloSimulations,
                        Properties.MC.Default.GenerateDataFromModel,
                        Properties.MC.Default.UseUCB,
                        Properties.MC.Default.RememberBandits,
                        Properties.MC.Default.UseLambdaInterchangeInMC,
                        Properties.Historical.Default.UseGreedyInsteadOfRandom,
                        (DVRPInstance.Clustering)Properties.Heuristics.Default.ClusteringType,
                        Properties.Heuristics.Default.UseLambdaInterchange,
                        Properties.Settings.Default.AdvanceCommitTimeSteps,
                        Properties.MC.Default.C,
                        Properties.MC.Default.FakeSolutionsBufferSize
                        ),
                        processesNotThreads = Properties.Settings.Default.UseProcessesNotThreads
                });
                }
                DVRPInstance function = functions[0].function;
                /*
                string name = new FileInfo(filepath.ToString()).Name;
                File.WriteAllLines(
                    name.Split('.')[0] + ".txt",
                    function.Clients.Select(clnt => String.Format("{0:0} {1:0} {2:0} {3:0} {4:0} {5:0} {6:0}" ,clnt.Id, clnt.X, clnt.Y, clnt.StartAvailable, clnt.Need, function.Vehicles[0].Capacity, function.Depots[0].EndAvailable )));
                return;
                 * */
                /*
                List<double[]> points = function.Clients.Select(clnt => new double[] { clnt.X, clnt.Y, clnt.StartAvailable }).ToList();
                name = new FileInfo(filepath.ToString()).Name;
                double[,,] matrix1 = new double[5,5,5], matrix2 = new double[5,5,5];
                double minX = points.Min(pnt => pnt[0]);
                double minY = points.Min(pnt => pnt[1]);
                double minT = points.Min(pnt => pnt[2]);
                double maxX = points.Max(pnt => pnt[0]) + 1.0;
                double maxY = points.Max(pnt => pnt[1]) + 1.0;
                double maxT = points.Max(pnt => pnt[2]) + 1.0;
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        for (int k = 0; k < 5; k++)
                        {
                            matrix1[i, j, k] = 1.0 + ((double)points.Count(pnt => (pnt[2] - minT) >= (maxT - minT) * ((double)k) / 5.0 && (pnt[2] - minT) < (maxT - minT) * ((double)k + 1.0) / 5.0)) / 25.0;
                            matrix2[i, j, k] = 1.0 + points.Count(pnt =>
                                (pnt[0] - minX) >= (maxX - minX) * ((double)i) / 5.0 &&
                                (pnt[1] - minY) >= (maxY - minY) * ((double)j) / 5.0 &&
                                (pnt[2] - minT) >= (maxT - minT) * ((double)k) / 5.0 &&
                                (pnt[0] - minX) < (maxX - minX) * ((double)i + 1.0) / 5.0 &&
                                (pnt[1] - minY) < (maxY - minY) * ((double)j + 1.0) / 5.0 &&
                                (pnt[2] - minT) < (maxT - minT) * ((double)k + 1.0) / 5.0
                                );
                        }
                    }
                }
                File.AppendAllText("data.csv", name+Environment.NewLine);
                for (int k = 0; k < 5; k++)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        for (int j = 0; j < 5; j++)
                        {
                            File.AppendAllText("data.csv", matrix1[i, j, k] + " ");
                        }
                        for (int j = 0; j < 5; j++)
                        {
                            File.AppendAllText("data.csv", matrix2[i, j, k] + " ");
                        }
                        File.AppendAllText("data.csv", Environment.NewLine);
                    }
                }
                return;
                */
                /*
                string name = new FileInfo(filepath.ToString()).Name;
                int[] capacities = function.Vehicles.Select(vhcl => vhcl.Capacity).Distinct().ToArray();
                int[] needs = function.Clients.Select(clnt => clnt.Need).ToArray();

                File.AppendAllText("capacities.csv", name + ";" + Math.Ceiling(((double)needs.Sum()) / -capacities.Sum()) + Environment.NewLine);

                File.AppendAllText("capacities.csv", "Capacities;" + capacities.Sum() + Environment.NewLine);
                foreach (int capacity in capacities)
                {
                    File.AppendAllText("capacities.csv", capacity + Environment.NewLine);
                }
                File.AppendAllText("capacities.csv", "Needs;" + needs.Sum() + Environment.NewLine);
                foreach (int need in needs)
                {
                    File.AppendAllText("capacities.csv", need + Environment.NewLine);
                }
                return;
                */
                double routeLength = double.MaxValue;
                string lastImage = "";
                while (function.CurrentTime <= 1.0M)
                {
                    List<Thread> threads = new List<Thread>();
                    foreach (DVRPThreadSolveWrapper wrapper in functions)
                    {
                        if (wrapper.function.bandits != null)
                            function.listOfBandits.Add(wrapper.function.bandits);
                        if (wrapper.function != function)
                        {
                            wrapper.function = function.Clone();
                            wrapper.function.SetSwarms(Properties.PSO.Default.SwarmSize,
                                Properties.PSO.Default.RouteSwarmIterations,
                                Properties.PSO.Default.SwarmIterations);
                        
                        }
                            //Jeżeli jest więcej optymalizatorów to najlepszy jest zamieniany w szybkie MST z małą poprawką
                        else if (functions.Count > 1 && Properties.Historical.Default.AdditionalTreeOptimizer)
                        {
                            wrapper.function.SetSwarms(10,
                                10,
                                10);
                        }
                    }
                    foreach (DVRPThreadSolveWrapper wrapper in functions)
                    {
                        serviceAccess.WaitOne();
                        lock (serviceAccess)
                        {
                            if (busyServices.Any())
                            {
                                wrapper.port = busyServices.Where(pair => !pair.Value).Min(pair => pair.Key);
                                busyServices[wrapper.port] = true;
                            }
                        }
                        threads.Add(new Thread(wrapper.Solve));
                        threads.Last().Start();
                    }
                    Exception exception = new Exception();
                    foreach (Thread thread in threads)
                    {
                        thread.Join();
                        if (functions[threads.IndexOf(thread)].exception != null)
                        {
                            functions[threads.IndexOf(thread)].result = double.MaxValue;
                            if (exception != null)
                                exception = functions[threads.IndexOf(thread)].exception;
                        }
                        else
                        {
                            exception = null;
                        }
                        Console.WriteLine("{0}\t{1}\t{2:0.00}\t{3:0.00}\t{4}", DateTime.Now, new FileInfo(filepath.ToString()).Name, functions[threads.IndexOf(thread)].function.CurrentTime, functions[threads.IndexOf(thread)].result, string.Join(";", functions[threads.IndexOf(thread)].function.Vehicles.Where(vhcl1 => vhcl1.clientsToAssign.Count + vhcl1.assignedClients.Count > 0).Select(vhcl => vhcl.Id.ToString() + ":" + "(" + string.Join(",", (vhcl.assignedClients.Union(vhcl.clientsToAssign)).Select(clnt => clnt.Id.ToString())) + ")")));
                    }
                    if (exception != null)
                        throw exception;
                    routeLength = functions.Min(fctn2 => fctn2.result);
                    function = functions.Where(fctn => fctn.result == routeLength).Select(fctn => fctn.function).First();
                    lock (threadlock)
                    {
                        threadsCalls[Thread.CurrentThread] = DateTime.Now;
                    }
                    if (Properties.Settings.Default.DrawEveryImage)
                    {
                        lastImage = DrawImageAndSaveState(filepath, expNo, lastImage, function);
                    }
                    //Utils.LogInfo("log2.txt", "{0}\t{1}\t{2:0.00}\t{3:0.00}", DateTime.Now, new FileInfo(filepath.ToString()).Name, function.CurrentTime, routeLength);
                    if (function.ClientRequestsCount != function.ClientsInVehiclesCount)
                        throw new Exception("Not all clients have been assigned!");
                    function.UpdateTime();
                }
                function.CacheDistances();
                Utils.Instance.LogInfo("wyniki.txt", "{10}{11} {12}/{13:000}/{14:000}/{15:000}\t{0}\t{1}\t{2:0.00}\t{3:0.00}\t{4:0.00}\t{5}\t{6}\t{7}\t{8}\t{9}",
                    DateTime.Now,
                    (new FileInfo(filepath.ToString())).Name,
                    routeLength,
                    function.DOD,
                    function.Completness,
                    function.EvaluationCount,
                    function.bestKnown,
                    routeLength / function.bestKnown,
                    DateTime.Now - functions.Where(fctn => fctn.result == routeLength).First().startTime,
                    function.Vehicles.Count(vhcl => vhcl.clientsToAssign.Count + vhcl.assignedClients.Count > 0),
                    Properties.Settings.Default.Comment,
                    Properties.PSO.Default.NeighbourhoodProb1,
                    Properties.Settings.Default.ParallelInstances,
                    Properties.PSO.Default.SwarmSize,
                    Properties.PSO.Default.SwarmIterations * 0.04M / Properties.Settings.Default.TimeStep,
                    Properties.PSO.Default.RouteSwarmIterations * 0.04M / Properties.Settings.Default.TimeStep);
                /*
                Utils.LogInfo("log2.txt",
                                "{9} Parametry: (DODType:{0} Swarm:{1} Iter:{2} Neighb1:{3} Inv1:{4} Neighb2:{5} Inv2:{6} InitCap:{7:0.00} Slope:{8:0.00} Optimizer:{9})",
                                Properties.Settings.Default.DOD,
                                Properties.Settings.Default.SwarmSize,
                                Properties.Settings.Default.SwarmIterations,
                                Properties.Settings.Default.NeighbourhoodProb1,
                                Properties.Settings.Default.InventorsRatio1,
                                Properties.Settings.Default.NeighbourhoodProb2,
                                Properties.Settings.Default.InventorsRatio2,
                                Properties.Settings.Default.InitialCapacityPart,
                                Properties.Settings.Default.CapacitySlope,
                                Properties.Settings.Default.Optimizer,
                                DateTime.Now
                );
                */
                Console.WriteLine("{0}\t{1}\t{2:0.00}\t{3:0.00}\t{4:0.00}\t{5}\t{6}\t{7}", DateTime.Now, (new FileInfo(filepath.ToString())).Name, routeLength, function.DOD, function.Completness, function.EvaluationCount, function.bestKnown, routeLength / function.bestKnown);
                if (!Properties.Settings.Default.DrawEveryImage)
                {
                    lastImage = DrawImageAndSaveState(filepath, expNo, lastImage, function);
                }
                lock (client)
                {
                    if (message == null)
                    {
                        message = new MailMessage(
                            "optymalizacja.rojowa@gmail.com",
                            "okulewicz.michal@gmail.com",
                            string.Format(Properties.Settings.Default.Comment + "({10}) Parametry: (DODType:{0} Swarm:{1} Iter:{2} Neighb1:{3} Inv1:{4} Neighb2:{5} Inv2:{6} InitCap:{7:0.00} Slope:{8:0.00} Optimizer:{9} History: {11} 2Opt: {12})",
                                Properties.Historical.Default.DOD,
                                Properties.PSO.Default.SwarmSize,
                                Properties.PSO.Default.SwarmIterations + "/" + Properties.PSO.Default.RouteSwarmIterations,
                                Properties.PSO.Default.NeighbourhoodProb1,
                                Properties.PSO.Default.InventorsRatio1,
                                Properties.PSO.Default.NeighbourhoodProb2,
                                Properties.PSO.Default.InventorsRatio2,
                                Properties.Historical.Default.InitialCapacityPart,
                                Properties.Historical.Default.CapacitySlope,
                                Properties.Settings.Default.Optimizer,
                                Properties.Settings.Default.ParallelInstances,
                                Properties.Historical.Default.UsePreviousSolution,
                                Properties.Heuristics.Default.Use2Opt),
                            string.Format("{0} {1} {2}", Environment.MachineName, Environment.OSVersion, Environment.ProcessorCount) +
                            Environment.NewLine +  string.Format("{0}\t{1}\t{2:0.00}\t{3:0.00}\t{4:0.00}\t{5}\t{6}\t{7}", "Godzina", "Test", "Trasa", "dod", "Ukończono", "Obliczeń", "Znany", "Jakość"));
                        message.Bcc.Add("optymalizacja.rojowa@gmail.com");
                    }
                    message.Body += Environment.NewLine;
                    message.Body += string.Format("\"{0}\"\t{1}\t{2:0.00}\t{3:0.00}\t{5}\t{6}\t{7}", DateTime.Now, (new FileInfo(filepath.ToString())).Name, routeLength, function.DOD, function.Completness, function.EvaluationCount, function.bestKnown, routeLength / function.bestKnown);
                    message.Attachments.Add(new Attachment(lastImage));
                    message.Attachments.Add(new Attachment(lastImage.Replace(".png",".txt")));
                }
                }
            catch (Exception ex)
            {
                Utils.Instance.LogInfo("log2.txt", "{0}", ex.Message);
                Utils.Instance.LogInfo("exceptions.txt", ex.GetType().ToString());
                Utils.Instance.LogInfo("exceptions.txt", ex.Message);
                Utils.Instance.LogInfo("exceptions.txt", ex.StackTrace);
                string prefix = "";
                while (ex.InnerException != null)
                {
                    prefix += ">";
                    Utils.Instance.LogInfo("exceptions.txt", prefix + ex.InnerException.GetType().ToString());
                    ex = ex.InnerException;
                }
            }
            finally
            {
                lock (threadlock)
                {
                    threadsCalls.Remove(Thread.CurrentThread);
                }
                semaphore.Release();
            }
        }

        private static string DrawImageAndSaveState(object filepath, int expNo, string lastImage, DVRPInstance function)
        {
            Image img = new Bitmap(1024, 768);
            DynamicVehicleRouting.Visualize.Visualizer.DrawVehicleAssignment(function, Graphics.FromImage(img));
            img.Save(lastImage = string.Format("{0}.{3:000}-{1}.{2}", new FileInfo(filepath.ToString()).Name, function.CurrentTime, "png", expNo), System.Drawing.Imaging.ImageFormat.Png);

            //Generate instances with only one vehicle
            if (Properties.Historical.Default.SeparateImagesForRoutes)
            {
                foreach (Vehicle vehicle in function.Vehicles)
                {
                    if (vehicle.clientsToAssign.Count() + vehicle.assignedClients.Count() > 0)
                    {
                        DVRPInstance oneVehicleFunction = function.Clone();
                        foreach (Vehicle vehicle2 in oneVehicleFunction.Vehicles)
                        {
                            if (vehicle2.Id != vehicle.Id)
                            {
                                vehicle2.assignedClients.Clear();
                                vehicle2.clientsToAssign.Clear();
                            }
                        }
                        img = new Bitmap(1024, 768);
                        DynamicVehicleRouting.Visualize.Visualizer.DrawVehicleAssignment(oneVehicleFunction, Graphics.FromImage(img));
                        img.Save(lastImage = string.Format("{0}.{3:000}-{1}-{4:00}.{2}", new FileInfo(filepath.ToString()).Name, function.CurrentTime, "png", expNo, vehicle.Id), System.Drawing.Imaging.ImageFormat.Png);
                    }
                }
            }

            string csvFile = string.Format("{0}.{3:000}-{1}.{2}", new FileInfo(filepath.ToString()).Name, function.CurrentTime, "txt", expNo);
            if (File.Exists(csvFile))
                File.Delete(csvFile);
            File.AppendAllText(csvFile, new FileInfo(filepath.ToString()).Name + "\t" + function.bestKnown + "\t" + function.CurrentTime + "\t" + function.capacity  + Environment.NewLine);
            File.AppendAllText(csvFile, string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t", "VehicleId", "ClientId", "X", "Y", "StartTime", "EndTime", "ArriveTime", "Need") + Environment.NewLine);

            foreach (Vehicle v in function.Vehicles)
            {
                foreach (Client c in v.assignedClients)
                {
                    File.AppendAllText(csvFile, string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t", v.Id, c.Id, c.X, c.Y, c.StartAvailable, c.EndAvailable, c.VisitTime, -c.Need) + Environment.NewLine);
                }
                foreach (Client c in v.clientsToAssign)
                {
                    File.AppendAllText(csvFile, string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t", v.Id, c.Id, c.X, c.Y, c.StartAvailable, c.EndAvailable, c.VisitTime, -c.Need) + Environment.NewLine);
                }
            }

            return lastImage;
        }

        static void ThreadKiller()
        {
            while (true)
            {
                try
                {
                    try
                    {
                             Thread.Sleep(DVRPInstance.timeout);
                    }
                    catch (ThreadInterruptedException)
                    {
                        
                    }
                    if (breakKiller)
                        return;
                    lock (threadlock)
                    {
                        foreach (Thread t in threadsCalls.Keys)
                            if (DateTime.Now - threadsCalls[t] > DVRPInstance.timeout)
                                t.Abort();
                    }
                }
                catch (ThreadAbortException)
                {
                    break;
                }
            }
        }

    }
}
