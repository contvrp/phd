﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BenchmarkProject.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0")]
    internal sealed partial class Heuristics : global::System.Configuration.ApplicationSettingsBase {
        
        private static Heuristics defaultInstance = ((Heuristics)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Heuristics())));
        
        public static Heuristics Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public int ClusteringType {
            get {
                return ((int)(this["ClusteringType"]));
            }
            set {
                this["ClusteringType"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool Use2Opt {
            get {
                return ((bool)(this["Use2Opt"]));
            }
            set {
                this["Use2Opt"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool UseLambdaInterchange {
            get {
                return ((bool)(this["UseLambdaInterchange"]));
            }
            set {
                this["UseLambdaInterchange"] = value;
            }
        }
    }
}
