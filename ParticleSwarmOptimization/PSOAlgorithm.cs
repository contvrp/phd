﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace ParticleSwarmOptimization
{
    public class PSOAlgorithm: IOptimizer
    {
        private Particle[] particles;

        public Particle[] Particles
        {
            get { return particles; }
        }

        public double SwarmDiameter
        {
            get { return particles.Max(particle => particles.Max(particle2 => Utils.Instance.EuclideanDistance(particle2.x, particle.x))); }
        }

        public double WorstValue
        {
            get { return particles.Max(particle => particle.bestValue); }
        }

        public double BestValue
        {
            get
            {
                return particles.Min(particle => particle.bestValue);
            }
        }

        public double[] Best
        {
            get
            {
                return particles.First(particle => particle.bestValue == BestValue).best;
            }
        }

        public double AvarageSpeedValue
        {
            get
            {
                return particles.Average(particle => Utils.Instance.EuclideanDistance(particle.v, new double[particle.v.Length]));
            }
        }

        public int Iterations = 0;
        private IFunction function;
        private double radius;
        private double inventorsRatio;
        private bool log = false;

        /// <summary>
        /// Liczba obliczeń funkcji
        /// </summary>
        public int ValueCount
        {
            get { return function.ValueCount; }
        }

        public PSOAlgorithm(IFunction function, int swarmSize, double radius, Dictionary<double[],int> seedsDictionary, double neighbourhood, int maxNeighbourhoodSize, double inventorsRatio, bool log = false)
        {
            this.log = log;
            this.function = function;
            this.inventorsRatio = inventorsRatio;
            this.radius = radius;
            particles = new Particle[swarmSize];
            InitializeSwarm(function, swarmSize, radius, inventorsRatio, seedsDictionary);
            for (int i = 0; i < swarmSize; ++i)
            {
                List<int> randomToss = new List<int>();
                List<int> urn = new List<int>();
                for (int a = 0; a < swarmSize; ++a)
                {
                    urn.Add(a);
                }
                while (randomToss.Count < Math.Min(maxNeighbourhoodSize, swarmSize))
                {
                    int choice = Utils.Instance.random.Next(urn.Count);
                    randomToss.Add(urn[choice]);
                    urn.RemoveAt(choice);
                }
                for (int j = 0; j < randomToss.Count; j++)
                {
                    if ((particles[i] is Inventor || Utils.Instance.random.NextDouble() < neighbourhood) && i != j)
                        particles[i].neighbours.Add(particles[randomToss[j]]);
                }
                particles[i].InitializeVelocity();
            }
            /*
            if (log)
                File.WriteAllText(string.Format("pso-{0}.log", Thread.CurrentThread.ManagedThreadId), "");
            */

        }

        private void InitializeSwarm(IFunction function, int swarmSize, double radius, double inventorsRatio, Dictionary<double[],int> seedsDictionary)
        {
            //Utils.random = new Random((int)(DateTime.Now.Ticks % int.MaxValue));
            do
            {
#warning take out population initialization
                /*
             * Ideas: random in each swarm - pass best solutions
             * Generation: number bound vs. capacity bound (stop after first larger than prediction)
             * */
                var populationPositions = Optimizer.GenerateInitialPopulationLocations(swarmSize, radius, seedsDictionary);
                for (int i = 0; i < swarmSize; ++i)
                {
                    if (Utils.Instance.random.NextDouble() >= inventorsRatio)
                        particles[i] = new Particle(function);
                    else
                        particles[i] = new Inventor(function);
                    particles[i].neighbours.Clear();
                    particles[i].InitializePosition(populationPositions[i], 1e-300);
                }
            }
            while (SwarmDiameter == 0 && function.Dimension > 1 && radius > 1e-2 && swarmSize > 1);
        }

        public void Step()
        {
            ++Iterations;
            for (int i = 0; i < particles.Length; i += 1)
            {
                particles[i].Move();
                if (particles[i].x.Any(xx => double.IsInfinity(xx) || decimal.ToDouble(decimal.MaxValue) < xx || decimal.ToDouble(decimal.MinValue) > xx))
                {
                    particles[i].InitializePosition(this.Best, radius);
                    particles[i].InitializeVelocity();
                }
                particles[i].UpdateBest();
            }
            for (int i = 0; i < particles.Length; i += 1)
                particles[i].UpdateVelocity();
            if (Iterations % 50 == 0 && SwarmDiameter <1e-6 && AvarageSpeedValue <1e-3)
            {
                this.radius *= 1.1;
                var currentBestSeedDictionary = Optimizer.CreatePopulationSeedsFromLastAndHeuristic(particles.Length, this.Best, null, null);
                InitializeSwarm(function, particles.Length, radius, inventorsRatio, currentBestSeedDictionary);
            }
            /*
            if (log)
                File.AppendAllLines(string.Format("pso-{0}.log", Thread.CurrentThread.Name), new string[] {string.Format("{0:0.00}",this.BestValue)});
            */
        }

    }
}
