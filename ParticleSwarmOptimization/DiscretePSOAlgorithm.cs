﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DiscreteParticleSwarmOptimization
{
    public class PSOAlgorithm
    {
        private Particle[] particles;

        public Particle[] Particles
        {
            get { return particles; }
        }

        public double SwarmDiameter
        {
            get { return particles.Max(particle => particles.Max(particle2 => ParticleSwarmOptimization.Utils.Instance.EuclideanDistance(particle2.x, particle.x))); }
        }

        public double WorstValue
        {
            get { return particles.Max(particle => particle.bestValue); }
        }

        public double BestValue
        {
            get
            {
                return particles.Min(particle => particle.bestValue);
            }
        }

        public int[] Best
        {
            get
            {
                return particles.First(particle => particle.bestValue == BestValue).best;
            }
        }

        public double AvarageSpeedValue
        {
            get
            {
                return particles.Average(particle => ParticleSwarmOptimization.Utils.Instance.EuclideanDistance(particle.v, new int[particle.v.Length]));
            }
        }

        public int Iterations = 0;
        private IFunction function;
        private int radius;
        private double inventorsRatio;

        /// <summary>
        /// Liczba obliczeń funkcji
        /// </summary>
        public int ValueCount
        {
            get { return function.ValueCount; }
        }

        public PSOAlgorithm(IFunction function, int swarmSize, int[] center, int radius, double neighbourhood, double inventorsRatio = 0.0)
        {
            this.function = function;
            this.inventorsRatio = inventorsRatio;
            this.radius = radius;
            particles = new Particle[swarmSize];
            InitializeSwarm(function, swarmSize, center, radius, inventorsRatio);
            for (int i = 0; i < swarmSize; ++i)
            {
                for (int j = 0; j < swarmSize; j++)
                {
                    if ((particles[i] is Inventor || ParticleSwarmOptimization.Utils.Instance.random.NextDouble() < neighbourhood) && i != j)
                        particles[i].neighbours.Add(particles[j]);
                }
                particles[i].InitializeVelocity();
            }
        }

        private void InitializeSwarm(IFunction function, int swarmSize, int[] center, int radius, double inventorsRatio)
        {
            //ParticleSwarmOptimization.Utils.random = new Random((int)(DateTime.Now.Ticks % int.MaxValue));
            do
            {
                for (int i = 0; i < swarmSize; ++i)
                {
                    if (ParticleSwarmOptimization.Utils.Instance.random.NextDouble() >= inventorsRatio)
                        particles[i] = new Particle(function);
                    else
                        particles[i] = new Inventor(function);
                    if (i % 10 == 0)
                    {
                        particles[i].InitializePosition(center, 1);
                    }
                    else
                    {
                        particles[i].InitializePosition(center, radius);
                    }
                }
                radius *= 2;
            }
            while (SwarmDiameter == 0 && particles.Length > 1 && function.DiscreteDimension > 1 && radius > 0.0);
        }

        public void Step()
        {
            //Console.WriteLine(this.function.Value(this.Best));
            //Console.WriteLine(this.BestValue);
            ++Iterations;
            for (int i = 0; i < particles.Length; i += 1)
                particles[i].Move();
            for (int i = 0; i < particles.Length; i += 1)
                particles[i].UpdateVelocity();
            if (Iterations % 50 == 0 && SwarmDiameter <1e-6 && AvarageSpeedValue <1e-3)
            {
                this.radius *= 2;
                InitializeSwarm(function, particles.Length, this.Best, radius, inventorsRatio);
            }
        }

    }
}
