﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParticleSwarmOptimization
{
    interface IKnownFunction: IFunction
    {
        double[] Optimum { get; }
        double OptimumValue { get; }
    }
}
