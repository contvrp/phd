﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParticleSwarmOptimization
{
    public interface IFunction
    {
        double Value(double[] x);
        int Dimension { get; }
        int ValueCount { get; }
    }
}
