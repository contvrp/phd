﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParticleSwarmOptimization
{
    abstract public class Optimizer: IOptimizer
    {
        protected IFunction _objectiveFunction;
        protected int _populationSize;
        protected double[][] _population;
        protected double[] _values;
        protected int[] failCounter;

        public Optimizer(int populationSize, IFunction objectiveFunction, double radius, Dictionary<double[],int> seedsDictionary)
        {
#warning integrate population initializer from PSO
                        BestValue = double.MaxValue;
            _populationSize = populationSize;
            _objectiveFunction = objectiveFunction;
            _population = new double[_populationSize][];
            failCounter = new int[_populationSize];
            _values = new double[_populationSize];
            _population = Optimizer.GenerateInitialPopulationLocations(_populationSize, radius, seedsDictionary);
            for (int specimenIndex = 0; specimenIndex < _populationSize; specimenIndex++)
            {
                _values[specimenIndex] = _objectiveFunction.Value(_population[specimenIndex]);
                TryUpdateBest(specimenIndex);
            }

        }

        public double BestValue { get; protected set; }
        public double[] Best { get; protected set; }

        public void Step()
        {
            for (int specimenIndex = 0; specimenIndex < _populationSize; specimenIndex++)
            {
                double[] move = MoveSpecimen(specimenIndex);
                double tempValue = _objectiveFunction.Value(move);
                if (tempValue < _values[specimenIndex])
                {
                    _values[specimenIndex] = tempValue;
                    _population[specimenIndex] = move;
                    TryUpdateBest(specimenIndex);
                    failCounter[specimenIndex] = 0;
                }
                else
                {
                    ++failCounter[specimenIndex];
                }
            }
        }

        protected void TryUpdateBest(int specimenIndex)
        {
            if (_values[specimenIndex] < BestValue)
            {
                if (Best == null)
                    Best = new double[_population[specimenIndex].Length];
                BestValue = _values[specimenIndex];
                Array.Copy(_population[specimenIndex], Best, _population[specimenIndex].Length);
            }
        }

        protected abstract double[] MoveSpecimen(int specimenIndex);

        public static Dictionary<double[], int> CreatePopulationSeedsFromLastAndHeuristic(int swarmSize, double[] center, double[] discretizedCenter, double[] heuristicSolution)
        {
            var seedsDictionary = new Dictionary<double[], int>();
            seedsDictionary.Add(center, Math.Max(1, swarmSize / 10));
            if (heuristicSolution != null)
                seedsDictionary.Add(heuristicSolution, 1);
            if (discretizedCenter != null)
                seedsDictionary.Add(discretizedCenter, 1);
            seedsDictionary.Add((double[])center.Clone(), int.MaxValue);
            return seedsDictionary;
        }

        public static Dictionary<double[], int> CreatePopulationSeedsFromList(int swarmSize, List<double[]> centers)
        {
            var seedsDictionary = new Dictionary<double[], int>();
            seedsDictionary.Add(centers[0], Math.Max(1, swarmSize / 10));
            for (int i = 1; i < centers.Count; i++)
            {
                if (centers[i] != null)
                {
                    seedsDictionary.Add(centers[i], 1);
                }
            }
            seedsDictionary.Add((double[])centers[0].Clone(), int.MaxValue);
            return seedsDictionary;
        }

        public static double[][] GenerateInitialPopulationLocations(int populationSize, double radius, Dictionary<double[], int> positionSeeds)
        {
            double[][] populationPositions = new double[populationSize][];
            int seedCounter = 0;
            var seedEnumerator = positionSeeds.GetEnumerator();
            seedEnumerator.MoveNext();
            foreach (var key in populationPositions.Select((el, idx) => idx).OrderBy(el => Utils.Instance.random.Next()))
            {
                if (seedEnumerator.Current.Value == int.MaxValue)
                {
                    populationPositions[key] = Utils.Instance.GetRandomInZeroBasedNSphere(seedEnumerator.Current.Key.Length, radius)
                        .Select((val, idx) => val + seedEnumerator.Current.Key[idx])
                        .ToArray();
                }
                else
                {
                    populationPositions[key] = new double[seedEnumerator.Current.Key.Length];
                    Array.Copy(seedEnumerator.Current.Key, populationPositions[key], populationPositions[key].Length);
                }
                seedCounter++;
                if (seedCounter >= seedEnumerator.Current.Value)
                {
                    seedCounter = 0;
                    seedEnumerator.MoveNext();
                }
            }
            return populationPositions;
        }
         
    }
}
