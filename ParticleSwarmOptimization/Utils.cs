﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;

namespace ParticleSwarmOptimization
{
    public class Utils
    {
        private Random _random;
        private static List<int> seeds = new List<int>();
        private static object seedsLock = new object();

        [ThreadStatic]
        private static Utils _instance;

        public static Utils Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Utils();
                }
                return _instance;
            }
        }

        public Utils()
        {
#if DEBUG
            var seed = (int)Thread.CurrentThread.ManagedThreadId;
            //var seed = (int)((DateTime.Now.ToFileTimeUtc() + Thread.CurrentThread.ManagedThreadId) % int.MaxValue); 
#else
            var seed = (int)((DateTime.Now.ToFileTimeUtc() + Thread.CurrentThread.ManagedThreadId) % int.MaxValue); 
#endif
            lock (seedsLock)
            {
                seeds.Add(seed);
            }
            _random = new Random(seed);
        }

        public Random random
        {
            get
            {
                return _random;
            }
            set
            {
                _random = value;
            }
        }


        private static readonly object locklog = new object();

        static Utils()
        {
            //random = new Random((int)(DateTime.Now.Ticks % int.MaxValue));
            //_random = new Random(100);
        }

        public double[] GetRandomInZeroBasedNSphere(int n, double radius = 1.0)
        {
            double[] x = new double[n];
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < 100; j++)
                {
                    x[i] += random.NextDouble() - 0.5;
                }
                x[i] /= 100;
            }
            double randomRadius = random.NextDouble() * radius;//(new ContinuousUniform(0.0, radius)).Sample();
            double normalFactor = EuclideanDistance(x, new double[n]);
            for (int i = 0; i < n; ++i)
                x[i] *= randomRadius / normalFactor;
            return x;
        }

        double[,] distances;

        public double EuclideanDistance(double[] x, double[] y, int id1 = -1, int id2 = -1)
        {
            if (distances == null)
                distances = new double[500, 500];
            if (id1 > -1 && id2 > -1 && distances[id1, id2] > 0.0)
                return (double)distances[id1, id2];
            double dist = 0.0;
            for (int i = 0; i < x.Length && i < y.Length; ++i)
                dist += (x[i] - y[i]) * (x[i] - y[i]);
            if (id1 > -1 && id2 > -1)
            {
                return (distances[id1, id2] = Math.Sqrt(dist));
            }
            return Math.Sqrt(dist);
        }


        public double EuclideanDistance(int[] x, int[] y)
        {
            double dist = 0.0;
            for (int i = 0; i < x.Length && i < y.Length; ++i)
                dist += (x[i] - y[i]) * (x[i] - y[i]);
            return Math.Sqrt(dist);
        }

        public void LogInfo(string logname, string format, params object[] args)
        {
            lock (locklog)
            {
                try
                {
                    StreamWriter sw = new StreamWriter(logname, true);
                    sw.WriteLine(format, args);
                    sw.Close();
                    sw.Dispose();
                }
                catch
                {
                }
            }
        }

    }
}
