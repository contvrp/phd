﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParticleSwarmOptimization
{
    public class DifferentialEvolution : Optimizer
    {
        private double maxF;
        private double minF;
        private double crossProb;
        private bool separateFForEachDim;

        public DifferentialEvolution(int populationSize, IFunction objectiveFunction, double radius, Dictionary<double[], int> seedsDictionary,
            double maxF = 0.75, double minF = 0.25, double crossProb = 0.95, bool separateFForEachDim = false)
            : base(populationSize, objectiveFunction, radius, seedsDictionary)
        {
#if DEBUG
            Console.WriteLine("{0}-{1}", minF, maxF);
            Console.WriteLine(crossProb);
#endif
            this.maxF = maxF;
            this.minF = minF;
            this.crossProb = crossProb;
            this.separateFForEachDim = separateFForEachDim;
            
        }

        protected override double[] MoveSpecimen(int specimenIndex)
        {
            HashSet<int> parentsSet = new HashSet<int>() { specimenIndex };
            while (parentsSet.Count < 4)
            {
                parentsSet.Add(Utils.Instance.random.Next(_populationSize));
            }
            int[] parents = parentsSet.ToArray();
            double[] move = new double[_objectiveFunction.Dimension];
            double FFact = Utils.Instance.random.NextDouble() * (maxF - minF) + minF;
            for (int dimIdx = 0; dimIdx < _objectiveFunction.Dimension; dimIdx++)
            {
                if (Utils.Instance.random.NextDouble() < crossProb)
                {
                    if (separateFForEachDim)
                    {
                        FFact = Utils.Instance.random.NextDouble() * (maxF - minF) + minF;
                    }
                    move[dimIdx] = _population[parents[1]][dimIdx] + FFact * (_population[parents[2]][dimIdx] - _population[parents[3]][dimIdx]);
                }
                else
                {
                    move[dimIdx] = _population[parents[0]][dimIdx];
                }
            }
            return move;
        }

    }
}
