﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParticleSwarmOptimization;

namespace DynamicVehicleRoutingProblem
{
    class DVRPClusterAnalysis: DVRPInstance, IFunction
    {
        /*
        class ClusterFunction : IFunction
        {
            private List<double[]> points;
            private int dimension;
            private int valueCount;

            public ClusterFunction(List<double[]> points, int dimension)
            {
                this.points = points;
                this.dimension = dimension;
            }

            public double Value(double[] x)
            {
                valueCount++;
                foreach (double[] point in points)
                {

                }
            }

            public int Dimension
            {
                get { return 2 * dimension; }
            }

            public int ValueCount
            {
                get { return valueCount; }
            }
        }
        */

        public DVRPClusterAnalysis(string filename, decimal timeStep, decimal currentTime, Dictionary<decimal, Vehicle[]> bestVehicles, List<Client> knownClients)
            :base(filename, timeStep, 1, 1, currentTime)
        {
            this.BestVehicles = bestVehicles;
            this.knownClients = knownClients;
            if (this.BestVehicles.ContainsKey(currentTime))
            {
                for (int i = 0; i < Vehicles.Length; i++)
                    Vehicles[i].Unavailable = BestVehicles[currentTime][i].Unavailable;
            }

        }


        double IFunction.Value(double[] x)
        {
            base.CopyClientsFromPreviousBest();
            base.AssignClientsToVehicles(x);
            double sum = 0.0;
            foreach (Vehicle vehicle in Vehicles)
            {
                double dist = 0.0;
                decimal time = 0.0M;
                if (vehicle.clientsToServe.Count > 0)
                {
                    for (int i = 1; i < vehicle.clientsToServe.Count; ++i)
                    {
                        if (vehicle.clientsToServe[i - 1] is Depot)
                            vehicle.Cargo = vehicle.Capacity;
                        dist += Utils.EuclideanDistance(
                            new double[] { vehicle.clientsToServe[i].X, vehicle.clientsToServe[i].Y },
                            new double[] { vehicle.clientsToServe[i - 1].X, vehicle.clientsToServe[i - 1].Y },
                            vehicle.clientsToServe[i].Id,
                            vehicle.clientsToServe[i - 1].Id
                            );
                        vehicle.Cargo += vehicle.clientsToServe[i].Need;
                        time = vehicle.clientsToServe[i].VisitTime + vehicle.clientsToServe[i].ServiceTime;
                    }
                }
                else
                {
                    if (vehicle.clients.Count > 0)
                    {
                        dist += Depots.Min(dpt =>
                            Utils.EuclideanDistance(
                                new double[] { vehicle.X, vehicle.Y },
                                new double[] { dpt.X, dpt.Y }
                                ));
                        time = vehicle.clients.Last().VisitTime + vehicle.clients.Last().ServiceTime;
                        time += (decimal)Depots.Min(dpt =>
                            Utils.EuclideanDistance(
                                new double[] { vehicle.X, vehicle.Y },
                                new double[] { dpt.X, dpt.Y }
                                )) / 2;
                    }
                    vehicle.Cargo = vehicle.Capacity;
                }
                foreach (Client client in vehicle.clients)
                {
                    if (vehicle.Cargo + client.Need < 0)
                    {
                        dist += 2 * Depots.Min(dpt =>
                            Utils.EuclideanDistance(
                                new double[] { vehicle.X, vehicle.Y },
                                new double[] { dpt.X, dpt.Y }
                                ));
                        time += (decimal)Depots.Min(dpt =>
                            Utils.EuclideanDistance(
                                new double[] { vehicle.X, vehicle.Y },
                                new double[] { dpt.X, dpt.Y }
                                ));
                    }
                    dist += Utils.EuclideanDistance(
                        new double[] { vehicle.X, vehicle.Y },
                        new double[] { client.X, client.Y }
                        );
                    time += (decimal)Utils.EuclideanDistance(
                        new double[] { vehicle.X, vehicle.Y },
                        new double[] { client.X, client.Y }
                        ) / 2;
                    vehicle.Cargo += client.Need;
                    time += client.ServiceTime;
                }
                if (vehicle.clients.Count + vehicle.clientsToServe.Count > 0)
                    dist += Depots.Min(dpt =>
                    Utils.EuclideanDistance(
                        new double[] { vehicle.X, vehicle.Y },
                        new double[] { dpt.X, dpt.Y }
                        ));
                vehicle.Cargo = 0;
                sum += dist;
                if (time > Depots.Max(dpt => dpt.EndAvailable))
                    sum += ((double)time - Depots.Max(dpt => dpt.EndAvailable));
            }
            return sum;
        }

    }
}
