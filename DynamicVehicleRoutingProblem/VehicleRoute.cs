﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParticleSwarmOptimization;

namespace DynamicVehicleRoutingProblem
{
    public class VehicleRoute : IFunction
    {
        Depot[] depots;
        Client[] clients;
        private Client[] setClients;
        decimal capacity;
        decimal currentTime;
        int valueCount = 0;

        public List<Client> Route;
        public List<Client> UnassignedClients;

        public VehicleRoute(Depot[] depots, Client[] clients, Client[] setClients, decimal capacity, decimal currentTime)
        {
            this.depots = depots;
            this.clients = clients;
            this.setClients = setClients;
            this.capacity = capacity;
            this.currentTime = currentTime;
        }

        public double Value(double[] x)
        {
            ++valueCount;
            decimal cargo = 0.0M;
            decimal dist = 0.0M;
            decimal time = 0.0M;
            for (int i = 0; i < clients.Length && i < x.Length; i++)
                clients[i].Rank = x[i];
            Route = new List<Client>();
            UnassignedClients = new List<Client>();
            Route.AddRange(setClients);
            foreach (Client c in clients.Where(clnt => !(clnt is Depot)).OrderBy(clnt => clnt.Rank))
            {
                Route.Add((Client)c.Clone());
            }
            for (int i = 0; i < Route.Count; i++)
            {
                if (Route[i].Need + cargo < 0)
                    Route.Insert(i, depots[0]);
                if (Route[i] is Depot)
                    cargo = capacity;
                else
                    cargo += Route[i].Need;
                int j = (i + 1) % Route.Count;
                decimal currentDist = (decimal)Utils.EuclideanDistance(new double[] { Route[i].X, Route[i].Y }, new double[] { Route[j].X, Route[j].Y },
                    Route[i].Id, Route[j].Id);
                dist += currentDist;
                time += Route[i].ServiceTime + currentDist;
                if (j > 0 && !(setClients.Contains(Route[j])))
                    Route[j].VisitTime = Math.Max(currentTime, Route[i].VisitTime + Route[i].ServiceTime) + currentDist;

            }
            for (int i = 1; i < Route.Count; i++)
            {
                decimal distToStart = (decimal)Utils.EuclideanDistance(new double[] { Route[i].X, Route[i].Y }, new double[] { Route[0].X, Route[0].Y },
                    Route[i].Id, Route[0].Id);
                decimal finishTime = Math.Max(setClients.Contains(Route[i]) ? 0 : currentTime, Route[i].VisitTime + Route[i].ServiceTime) + distToStart;
                if (finishTime > depots.Max(dpt => dpt.EndAvailable))
                {
                    UnassignedClients = Route.GetRange(i, Route.Count - i);
                    Route.RemoveRange(i, Route.Count - i);
                    break;
                }
            }
            Route.RemoveRange(0, setClients.Length);
            return (double)dist;
        }

        public int Dimension
        {
            get { return clients.Length; }
        }

        public int ValueCount
        {
            get { return valueCount; }
        }
    }
}
