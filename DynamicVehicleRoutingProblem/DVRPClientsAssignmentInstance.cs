﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRoutingProblem
{
    public class DVRPClientsAssignmentInstance: DVRPInstance
    {
        public override int Dimension
        {
            get
            {
                return KnownClients.Count;
            }
        }

        protected override void AssignClientsToVehicles(double[] x)
        {
            for (int i = 0; i < KnownClients.Count; i++)
            {
                Vehicles[Math.Abs(((int)Math.Round(x[i] * Vehicles.Length)) % Vehicles.Length)].clients.Add(KnownClients[i]);
            }
        }

        public DVRPClientsAssignmentInstance(string filename, decimal timeStep, int internalSwarmSize, int internalSwarmIterations, decimal currentTime)
            : base(filename, timeStep, internalSwarmSize, internalSwarmIterations, currentTime)
        {
        }

    }
}
