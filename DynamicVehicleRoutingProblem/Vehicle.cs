﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParticleSwarmOptimization;

namespace DynamicVehicleRoutingProblem
{
    public class Vehicle: SpatialPoint
    {
        public int Cargo;
        public int Capacity;
        public List<Client> clients;
        public List<Client> clientsToServe;
        public bool Unavailable;
        public int GetId
        {
            get
            {
                return Id;
            }
        }

        public int CountClients
        {
            get
            {
                return clients.Count;
            }
        }

        public int CountCommited
        {
            get
            {
                return clientsToServe.Count;
            }
        }

        public int Need
        {
            get
            {
				return (clients.Count > 0 ? clients.Sum(clnt => clnt.Need) : 0) +
					   (clientsToServe.Count > 0 ? clientsToServe.Sum(clnt => clnt.Need) : 0);
            }
        }


        public decimal ClientsRouteTime
        {
            get
            {
                decimal time = 0.0M;
                for (int i = 0; i < clients.Count; i++)
                {
                    int j = (i + 1) % clients.Count;
                    decimal currentDist = (decimal)Utils.EuclideanDistance(new double[] { clients[i].X, clients[i].Y }, new double[] { clients[j].X, clients[j].Y },
                        clients[i].Id, clients[j].Id);
                    time += clients[i].ServiceTime + currentDist;
                }
                return time;

            }
        }

        public decimal FinishTime
        {
            get
            {
                    if (clients.Count > 0)
                    {
                        Client start = clientsToServe.Count > 0 ? clientsToServe.First() : clients.OrderBy(clnt => clnt.VisitTime).First();
                        Client finish = clients.OrderBy(clnt => clnt.VisitTime).Last();
                        double dist = Utils.EuclideanDistance(
                            new double[] {start.X, start.Y},
                            new double[] { finish.X, finish.Y },
                            start.Id,
                            finish.Id
                            );
                        return finish.VisitTime + finish.ServiceTime + (decimal)dist;
                    }
                    else if (clientsToServe.Count > 0)
                    {
                        Client start = clientsToServe.First();
                        Client finish = clientsToServe.OrderBy(clnt => clnt.VisitTime).Last();
                        double dist = Utils.EuclideanDistance(
                            new double[] { start.X, start.Y },
                            new double[] { finish.X, finish.Y },
                            start.Id,
                            finish.Id
                            );
                        return finish.VisitTime + finish.ServiceTime + (decimal)dist;
                    }
                    else
                        return 0.0M;
            }
        }
    }
}
