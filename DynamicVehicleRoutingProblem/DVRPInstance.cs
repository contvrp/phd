﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParticleSwarmOptimization;
using System.IO;
using System.Threading;

namespace DynamicVehicleRoutingProblem
{
    public class DVRPInstance: IFunction
    {
        public delegate void UpdateBestEvent();
        int valueCount = 0;
        protected decimal currentTime;
        protected decimal availableCapacity;
        private decimal timeStep;
        public event UpdateBestEvent UpdatedBest;
        static readonly object loggerLocker = new object();

        public void UpdateTime()
        {
            CurrentTime += timeStep;
        }

        public decimal Time
        {
            get
            {
                return currentTime;
            }
        }

        private decimal CurrentTime
        {
            get
            {
                return currentTime;
            }
            set
            {
                currentTime = value;
                CopyClientsFromPreviousBest();
                if (Clients.Length > 0 && Vehicles.Length > 0 && Depots.Length > 0)
                {

                    knownClients = Clients.Where(
                        clnt => clnt.StartAvailable <= (decimal)Depots.Max(dpt => dpt.EndAvailable) * this.CurrentTime &&
                            Vehicles.Sum(vhcl => vhcl.clientsToServe.Count(clnt2 => clnt2.Id == clnt.Id)) == 0
                        ).ToList();

                    availableCapacity =
                        //        (decimal)Vehicles.Max(vhcl => vhcl.Capacity);

                Math.Min(
                Math.Max(KnownClients.Count > 0 ? KnownClients.Max(clnt => -clnt.Need) : 0.0M,
                (decimal)Vehicles.Max(vhcl => vhcl.Capacity) * currentTime * 1.2M),
                (decimal)Vehicles.Max(vhcl => vhcl.Capacity));
                }
                else
                {
                    knownClients = new List<Client>();
                }
                foreach (Vehicle vehicle in Vehicles)
                    vehicle.Unavailable = false;
                bestValue = Double.MaxValue;
            }
        }

        public decimal NextTime
        {
            get
            {
                return currentTime + timeStep;
            }
        }

        public double MaxTime
        {
            get { return Depots.Max(depot => depot.EndAvailable); }
        }

        public Vehicle[] Vehicles;
        public Depot[] Depots;
        public Client[] Clients;

        public Dictionary<decimal,Vehicle[]> BestVehicles;
        private double bestValue = Double.MaxValue;

        protected int internalSwarmSize;
        protected int internalSwarmIterations;
        protected string filename;

        public DVRPInstance(string filename, decimal timeStep, int internalSwarmSize, int internalSwarmIterations, decimal currentTime)
        {
            this.filename = filename;
            this.timeStep = timeStep;
            this.internalSwarmSize = internalSwarmSize;
            this.internalSwarmIterations = internalSwarmIterations;
            StreamReader reader = new StreamReader(filename);
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (line.StartsWith("NUM_DEPOTS"))
                {
                    Depots = new Depot[GetNumberFromLine(line)];
                }
                else if (line.StartsWith("NUM_CAPACITIES"))
                {
                    if (GetNumberFromLine(line) != 1)
                        throw new Exception("Nie obsługuję różnych typów towarów");
                }
                else if (line.StartsWith("NUM_VISITS"))
                {
                    Clients = new Client[GetNumberFromLine(line)];
                }
                else if (line.StartsWith("NUM_LOCATIONS"))
                {
                    Depots = new Depot[GetNumberFromLine(line) - Clients.Length];
                }
                else if (line.StartsWith("NUM_VEHICLES"))
                {
                    Vehicles = new Vehicle[GetNumberFromLine(line)];
                    BestVehicles = new Dictionary<decimal,Vehicle[]>();
                    BestVehicles[currentTime] = new Vehicle[Vehicles.Length];
                }
                else if (line.StartsWith("CAPACITIES"))
                {
                    int capacity = GetNumberFromLine(line);
                    for (int i = 0; i < Vehicles.Length; ++i)
                    {
                        Vehicles[i] = new Vehicle();
                        Vehicles[i].clientsToServe = new List<Client>();
                        BestVehicles[currentTime][i] = new Vehicle();
                        BestVehicles[currentTime][i].Id = Vehicles[i].Id = Clients.Length + Depots.Length + i;
                        BestVehicles[currentTime][i].Capacity = Vehicles[i].Capacity = capacity;
                    }
                }
                else if (line.StartsWith("DEPOTS"))
                {
                    for (int i = 0; i < Depots.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Depots[i] = new Depot();
                        Depots[i].Id = numbers[0];
                    }
                }
                else if (line.StartsWith("DEMAND_SECTION"))
                {
                    for (int i = 0; i < Clients.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Clients[i] = new Client();
                        Clients[i].Id = numbers[0];
                        Clients[i].Need = numbers[1];
                    }
                }
                else if (line.StartsWith("LOCATION_COORD_SECTION"))
                {
                    for (int i = 0; i < Clients.Length + Depots.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Client client = null;
                        if (Clients.Count(clnt => clnt.Id == numbers[0]) > 0)
                        {
                            client = Clients.First(clnt => clnt.Id == numbers[0]);
                        }
                        else
                        {
                            client = Depots.First(dpt => dpt.Id == numbers[0]);
                            foreach (Vehicle bestVehicle in BestVehicles[currentTime])
                            {
                                bestVehicle.X = numbers[1];
                                bestVehicle.Y = numbers[2];
                            }
                        }
                        client.X = numbers[1];
                        client.Y = numbers[2];
                    }
                }
                else if (line.StartsWith("DURATION_SECTION"))
                {
                    for (int i = 0; i < Clients.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Client client = Clients.First(clnt => clnt.Id == numbers[0]);
                        if (client != null)
                            client.ServiceTime = numbers[1];
                    }
                }
                else if (line.StartsWith("DEPOT_TIME_WINDOW_SECTION"))
                {
                    for (int i = 0; i < Depots.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Depot depot = Depots.First(dpt => dpt.Id == numbers[0]);
                        if (depot != null)
                        {
                            depot.StartAvailable = numbers[1];
                            depot.EndAvailable = numbers[2];
                        }
                    }
                }
                else if (line.StartsWith("TIME_AVAIL_SECTION"))
                {
                    for (int i = 0; i < Clients.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Client client = Clients.First(clnt => clnt.Id == numbers[0]);
                        if (client != null)
                            client.StartAvailable = numbers[1];
                    }
                }
            }
            reader.Close();
            reader.Dispose();
            distances = new double[Clients.Length + Depots.Length, Clients.Length + Depots.Length];
            foreach (Depot d1 in Depots)
            {
                foreach (Depot d2 in Depots)
                {
                    distances[d1.Id, d2.Id] = Utils.EuclideanDistance(new double[] { d1.X, d1.Y }, new double[] { d2.X, d2.Y });
                }
                foreach (Client c in Clients)
                {
                    distances[d1.Id, c.Id] = Utils.EuclideanDistance(new double[] { d1.X, d1.Y }, new double[] { c.X, c.Y });
                }
            }
            foreach (Client c1 in Clients)
            {
                foreach (Depot d in Depots)
                {
                    distances[c1.Id, d.Id] = Utils.EuclideanDistance(new double[] { c1.X, c1.Y }, new double[] { d.X, d.Y });
                }
                foreach (Client c2 in Clients)
                {
                    distances[c1.Id, c2.Id] = Utils.EuclideanDistance(new double[] { c1.X, c1.Y }, new double[] { c2.X, c2.Y });
                }
            }
            this.CurrentTime = currentTime;
        }

        private double[,] distances;

        private static int GetNumberFromLine(string line)
        {
            return int.Parse(line.Trim().Split(':')[1].Trim());
        }

        private static int[] GetNumbersFromLine(string line)
        {
            string[] data = line.Trim().Split(' ');
            int[] numbers = new int[data.Length];
            for (int i = 0; i < data.Length; ++i)
                numbers[i] = int.Parse(data[i].Trim());
            return numbers;
        }

        public double Value(double[] x)
        {
            CopyClientsFromPreviousBest();
            while (true)
            {
                this.bestValue = double.MaxValue;
                double bestValue = double.MaxValue;
                if (knownClients.Count > 0)
                {
                    x = new double[Dimension];
                    DVRPClusterAnalysis cluster = new DVRPClusterAnalysis(filename, timeStep, currentTime, BestVehicles, knownClients);
                    double minX = knownClients.Min(clnt => clnt.X);
                    double minY = knownClients.Min(clnt => clnt.Y);
                    double maxX = knownClients.Max(clnt => clnt.X);
                    double maxY = knownClients.Max(clnt => clnt.Y);
                    Vehicle[] availableVehicles = BestVehicles[BestVehicles.Where(kvp1 => kvp1.Key < cluster.Time).Max(kvp => kvp.Key)].ToArray();
                    if (BestVehicles.ContainsKey(currentTime))
                        availableVehicles = BestVehicles[currentTime].Where(vhcl => !vhcl.Unavailable).ToArray();
                    for (int i = 0; i < x.Length; i += 2)
                    {
                        x[i] = availableVehicles[i/2].X;
                        x[i + 1] = availableVehicles[i/2].Y;
                    }
                    PSOAlgorithm pso = new PSOAlgorithm(cluster, internalSwarmSize, x, Math.Max(maxY - minY, maxX - minX) * Math.Sqrt(x.Length), 0.5);
                    for (int i = 0; i < internalSwarmIterations; i++)
                    {
                        if (pso.AvarageSpeedValue < 1e-6)
                            pso = new PSOAlgorithm(cluster, internalSwarmSize, x, Math.Max(maxY - minY, maxX - minX) * Math.Sqrt(x.Length), 0.5);
                        if (bestValue > pso.BestValue)
                        {
                            bestValue = pso.BestValue;
                            x = pso.Best;
                        }
                        pso.Step();
                    }
                    Utils.LogInfo("log.txt", "[{2:yyyy-MM-dd HH:mm:ss}];{3}(Przybliżony);{0:0.00};{1:0.00}", currentTime, bestValue, DateTime.Now, new FileInfo(filename).Name);
                }
                AssignClientsToVehicles(x);
                /*
                if (Vehicles.Sum(vhcl => vhcl.clients.Count) != KnownClients.Count)
                    throw new Exception(string.Format("Liczba klientów {0} jest różna od liczby klientów przypisanych do pojazdów {1}!", KnownClients.Count, Vehicles.Sum(vhcl => vhcl.clients.Count)));
                */
                double value = OptimizeRoutes();
                UpdateBestRoutes(value);
                if (knownClients.Count + ClientsInVehiclesCount != ClientRequestsCount)
                    throw new Exception("Liczby zamówień się nie zgadzają");
                if (knownClients.Sum(knwclnt => Vehicles.Sum(vhcl => vhcl.clientsToServe.Where(clnt => clnt.Id == knwclnt.Id).Count())) > 0)
                {
                    throw new Exception(string.Format("Istnieją podwójni klienci"));
                }
                if (knownClients.Sum(knwclnt => Vehicles.Sum(vhcl => vhcl.clients.Where(clnt => clnt.Id == knwclnt.Id).Count())) > 0)
                {
                    throw new Exception(string.Format("Istnieją podwójni klienci"));
                }
                if (AvailableVehicles.Count == 0)
                {
                    for (int i = 0; i < Vehicles.Length; i++)
                    {
                        Vehicles[i].Unavailable = false;
                        BestVehicles[currentTime][i].Unavailable = false;
                    }
                    //this.RestartTime();
                    throw new Exception(string.Format("Nie mam już komu przydzielać zamówień"));
                }
                if (knownClients.Count == 0)
                    return value;
            }
        }

        private int ClientRequestsCount
        {
            get
            {
                return Clients.Where(clnt => clnt.StartAvailable <= currentTime * Depots.Max(dpt => dpt.EndAvailable)).Count();
            }
        }

        private int ClientsInVehiclesCount
        {
            get
            {
                return Vehicles.Sum(vhcl => vhcl.clients.Where(clnt => !(clnt is Depot)).Count() + vhcl.clientsToServe.Where(clnt => !(clnt is Depot)).Count());
            }
        }

        protected void CopyClientsFromPreviousBest()
        {
            for (int i = 0; i < Vehicles.Length; ++i)
            {
                decimal v = -1;
                if (BestVehicles.Count > 0 && BestVehicles.Keys.Count(key => key < currentTime) > 0)
                    v = BestVehicles.Keys.Where(key => key < currentTime).Max();
                if (BestVehicles.ContainsKey(v))
                {
                    if (BestVehicles[v][i].clientsToServe != null && BestVehicles[v][i].clientsToServe.Count > 0)
                        Vehicles[i].clientsToServe = BestVehicles[v][i].clientsToServe;
                }
                if (BestVehicles.ContainsKey(currentTime))
                {
                    Vehicles[i].Unavailable = BestVehicles[currentTime][i].Unavailable;
                    Vehicles[i].X = BestVehicles[currentTime][i].X;
                    Vehicles[i].Y = BestVehicles[currentTime][i].Y;

                    if (BestVehicles[currentTime][i].clients != null && BestVehicles[currentTime][i].clientsToServe != null)
                    {
                        Vehicles[i].clients = new List<Client>();
                        Vehicles[i].clientsToServe = new List<Client>();
                        Vehicles[i].clients.AddRange(BestVehicles[currentTime][i].clients);
                        Vehicles[i].clientsToServe.AddRange(BestVehicles[currentTime][i].clientsToServe);
                    }
                    else
                    {
                        Vehicles[i].clients = new List<Client>();
                    }
                }
                else
                {
                    Vehicles[i].clients = new List<Client>();
                }
            }
        }

        protected List<Client> knownClients;

        public List<Client> KnownClients
        {
            get
            {
                return knownClients;
            }
        }

        public List<Vehicle> AvailableVehicles
        {
            get
            {
                return Vehicles.Where(vhcl => !vhcl.Unavailable).ToList();
            }
        }

        private void UpdateBestRoutes(double value)
        {
            if (bestValue >= value)
            {
                bestValue = value;
                BestVehicles[currentTime] = new Vehicle[Vehicles.Length];
                for (int i = 0; i < Vehicles.Length; ++i)
                {
                    BestVehicles[currentTime][i] = new Vehicle();
                    BestVehicles[currentTime][i].Capacity = Vehicles[i].Capacity;
                    BestVehicles[currentTime][i].Id = Vehicles[i].Id;
                    BestVehicles[currentTime][i].X = Vehicles[i].X;
                    BestVehicles[currentTime][i].Y = Vehicles[i].Y;
                    BestVehicles[currentTime][i].clients = new List<Client>();
                    BestVehicles[currentTime][i].clients.AddRange(Vehicles[i].clients);
                    BestVehicles[currentTime][i].clientsToServe = new List<Client>();
                    BestVehicles[currentTime][i].clientsToServe.AddRange(Vehicles[i].clientsToServe);
                    BestVehicles[currentTime][i].Unavailable = Vehicles[i].Unavailable;

                    if (BestVehicles[currentTime][i].clients.Count > 0 && BestVehicles[currentTime][i].FinishTime + timeStep * Depots.Max(dpt => dpt.EndAvailable) > Depots.Max(dpt => dpt.EndAvailable))
                    {
                        while (BestVehicles[currentTime][i].clients.Count > 0 &&
                            (BestVehicles[currentTime][i].clients[0].VisitTime < NextTime * Depots.Max(dpt => dpt.EndAvailable)))
                        {
                            /*
                            if (BestVehicles[currentTime][i].clients[0].VisitTime > Depots.Max(dpt => dpt.EndAvailable))
                                break; // spóźniony nie może być zatwierdzony
                            */
                            BestVehicles[currentTime][i].clientsToServe.Add(BestVehicles[currentTime][i].clients[0]);
                            BestVehicles[currentTime][i].clients.RemoveAt(0);
                        }
                    }
                    if (BestVehicles[currentTime][i].clientsToServe.Count == 1)
                    {
                        BestVehicles[currentTime][i].clients.Insert(0, BestVehicles[currentTime][i].clientsToServe[0]);
                        BestVehicles[currentTime][i].clientsToServe.RemoveAt(0);
                    }
                    if (BestVehicles[currentTime][i].clientsToServe.Intersect(BestVehicles[currentTime][i].clients.Where(clnt => !(clnt is Depot))).Count() > 0)
                    {
                        throw new Exception("Nie przerzucono wszystkich z ClientsToServe");
                    }
                    if (BestVehicles[currentTime][i].Need + (BestVehicles[currentTime][i].clientsToServe.Count(clnt => clnt is Depot) + BestVehicles[currentTime][i].clients.Count(clnt => clnt is Depot)) * BestVehicles[currentTime][i].Capacity < 0)
                        throw new Exception("Samochód nie odwiedza dostatecznie wielu zajezdni");
                }
            }
            if (UpdatedBest != null)
                UpdatedBest();
        }

        protected virtual double OptimizeRoutes()
        {
            double val = 0.0;
            foreach (Vehicle vehicle in Vehicles.Where(vhcl => vhcl.clients.Count + vhcl.clientsToServe.Count > 0))
            {
                val += GetOptimumRouteForVehicle(vehicle);
            }
            Utils.LogInfo("log.txt","[{2:yyyy-MM-dd HH:mm:ss}];{3};{0:0.00};{1:0.00}", currentTime, val, DateTime.Now, new FileInfo(filename).Name);
            return val;
        }

        private double GetOptimumRouteForVehicle(object obj)
        {
            Vehicle vehicle = (Vehicle)obj;
            List<Client> tempClients = new List<Client>();
            if (vehicle.Unavailable)
            {
                vehicle.clientsToServe = vehicle.clientsToServe.Union(vehicle.clients).ToList();
                tempClients.AddRange(vehicle.clients);
                vehicle.clients.Clear();
            }
            VehicleRoute function = new VehicleRoute(
                Depots,
                vehicle.clients.ToArray(),
                vehicle.clientsToServe.ToArray(),
                vehicle.Capacity,
                currentTime * Depots.Max(dpt => dpt.EndAvailable));
            double bestValue = double.MaxValue;
            double[] best = new double[function.Dimension];
            if (function.Dimension > 0)
            {
                PSOAlgorithm pso = new PSOAlgorithm(function, internalSwarmSize, new double[vehicle.clients.Count], 1.0, 0.5);
                for (int i = 0; i < internalSwarmIterations; i++)
                    while (pso.Iterations < internalSwarmIterations)
                    {
                        pso.Step();
                        if (pso.BestValue < bestValue)
                        {
                            bestValue = pso.BestValue;
                            best = pso.Best;
                        }
                        if (function.Dimension == 1)
                            break;
                        if (pso.AvarageSpeedValue < 1e-6)
                            pso = new PSOAlgorithm(function, internalSwarmSize, new double[vehicle.clients.Count], 1.0, 0.5);
                    }
            }
            valueCount += function.ValueCount;
            bestValue = function.Value(best);
            vehicle.clients = function.Route;
            knownClients.RemoveAll(clnt => function.Route.Count(clnt2 => clnt2.Id == clnt.Id) > 0);
            if (vehicle.Unavailable)
            {
                vehicle.clients = tempClients;
                vehicle.clientsToServe.RemoveRange(vehicle.clientsToServe.Count - tempClients.Count, tempClients.Count);
            }
            if (function.UnassignedClients.Count > 0)
            {
                vehicle.Unavailable = true;
                knownClients.AddRange(function.UnassignedClients.Where(clnt => knownClients.Count(clnt2 => clnt2.Id == clnt.Id) == 0 && !(clnt is Depot)));
            }

            return bestValue;
        }

        private struct ClientVehicleEdge: IComparable<ClientVehicleEdge>
        {
            public Client client;
            public Vehicle vehicle;
            private double distance;

            public double Distance
            {
                get
                {
                    return distance;
                }
            }

            public ClientVehicleEdge(Client client, Vehicle vehicle)
            {
                this.client = client;
                this.vehicle = vehicle;
                distance = Utils.EuclideanDistance(
                        new double[] { client.X, client.Y },
                        new double[] { vehicle.X, vehicle.Y }
                        );
            }

            public int CompareTo(ClientVehicleEdge other)
            {
                int comp = this.distance.CompareTo(other.distance);
                if (comp == 0)
                    comp = this.vehicle.Id.CompareTo(other.vehicle.Id);
                if (comp == 0)
                    comp = this.client.Id.CompareTo(other.client.Id);
                return comp;
            }

            public override bool Equals(object obj)
            {
                if (obj is ClientVehicleEdge)
                    return this.CompareTo((ClientVehicleEdge)obj) == 0;
                return false;
            }

            public override int GetHashCode()
            {
                return (int)distance;
            }
        }

        protected virtual void AssignClientsToVehicles(double[] x)
        {
            int k = 0;
            SortedSet<ClientVehicleEdge> edges = new SortedSet<ClientVehicleEdge>();
            for (int i = 0; i < Vehicles.Length; ++i)
            {
                if (!Vehicles[i].Unavailable)
                {
                        Vehicles[i].X = x[2 * k];
                        Vehicles[i].Y = x[2 * k + 1];
                        ++k;
                }
                //else
                //{
                //    Vehicles[i].X = Vehicles[i].clientsToServe[Vehicles[i].clientsToServe.Count - 1].X;
                //    Vehicles[i].Y = Vehicles[i].clientsToServe[Vehicles[i].clientsToServe.Count - 1].Y;
                //}
                if (!Vehicles[i].Unavailable)
                    for (int j = 0; j < KnownClients.Count; ++j)
                    {
                        edges.Add(new ClientVehicleEdge(KnownClients[j], Vehicles[i]));
                    }
            }
            //edges = edges.OrderBy(edg => edg.Distance).ToList();
            List<ClientVehicleEdge> edgesSet = new List<ClientVehicleEdge>(edges);
            for (int i = 0; i < edgesSet.Count; i++)
            {
                Client clientToRemove = edgesSet[i].client;
                edgesSet[i].vehicle.clients.Add(edgesSet[i].client);
                edgesSet[i].client.VisitTime = currentTime * Depots.Max(dpt => dpt.EndAvailable);
                edgesSet.RemoveAll(edg => edg.client == clientToRemove);
                i--;
            }
            while (edgesSet.Count > 0)
            {
                Client clientToRemove = edgesSet[0].client;
                edgesSet[0].vehicle.clients.Add(edgesSet[0].client);
                edgesSet[0].client.VisitTime = currentTime * Depots.Max(dpt => dpt.EndAvailable);
                edgesSet.RemoveAll(edg => edg.client == clientToRemove);
            }
        }

        public virtual int Dimension
        {
            get { return Vehicles.Count(vhcl => /* vhcl.clientsToServe.Count == 0 &&*/ !vhcl.Unavailable) * 2; }
        }

        public int ValueCount
        {
            get { return valueCount; }
        }

        public void RestartTime()
        {
            currentTime = 0;
            foreach (Vehicle vehicle in Vehicles)
            {
                vehicle.clientsToServe.Clear();
            }
            BestVehicles = new Dictionary<decimal, Vehicle[]>();
        }

        public override string ToString()
        {
            return base.ToString() + new FileInfo(filename).Name;
        }
    }
}
