﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRoutingProblem
{
    public class Client: SpatialPoint, IComparable<Client>, ICloneable
    {
        public int StartAvailable;
        public int EndAvailable;
        public int ServiceTime;
        public int Need;
        public double Rank;
        public Vehicle Vehicle;
        public decimal VisitTime;

        public int CompareTo(Client other)
        {
            int rankComp = Rank.CompareTo(other.Rank);
            if (rankComp != 0)
                return rankComp;
            else
                return Id.CompareTo(Id);
        }

        public object Clone()
        {
            Client newClient = new Client();
            newClient.EndAvailable = this.EndAvailable;
            newClient.Id = this.Id;
            newClient.Need = this.Need;
            newClient.Rank = this.Rank;
            newClient.ServiceTime = this.ServiceTime;
            newClient.StartAvailable = this.StartAvailable;
            newClient.VisitTime = this.VisitTime;
            newClient.Vehicle = this.Vehicle;
            newClient.X = this.X;
            newClient.Y = this.Y;
            return newClient;
        }
    }
}
