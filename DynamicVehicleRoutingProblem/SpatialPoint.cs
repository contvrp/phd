﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRoutingProblem
{
    public class SpatialPoint
    {
        public SpatialPoint Next;
        public SpatialPoint Prev;
        public double X;
        public double Y;
        public int Id;
    }
}
