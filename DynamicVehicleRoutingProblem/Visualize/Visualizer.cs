﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace DynamicVehicleRoutingProblem.Visualize
{
    public class Visualizer
    {
        public static void DrawVehicleAssignment(DVRPInstance function, Graphics graphics)
        {
            if (function == null)
                return;
            lock (function)
            {
                if (!function.BestVehicles.ContainsKey(function.Time))
                    return;
                Graphics g = graphics;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                try
                {
                    if (function.BestVehicles[function.Time].Where(vhcl => vhcl.clients.Count + vhcl.clientsToServe.Count > 0).ToList().Count == 0)
                        return;
                    double minX = Math.Min(function.Clients.Min(clt => clt.X), function.BestVehicles[function.Time].Where(vhcl => vhcl.clients.Count + vhcl.clientsToServe.Count > 0).Min(vhcl => vhcl.X));
                    double minY = Math.Min(function.Clients.Min(clt => clt.Y), function.BestVehicles[function.Time].Where(vhcl => vhcl.clients.Count + vhcl.clientsToServe.Count > 0).Min(vhcl => vhcl.Y));
                    double maxX = Math.Max(function.Clients.Max(clt => clt.X), function.BestVehicles[function.Time].Where(vhcl => vhcl.clients.Count + vhcl.clientsToServe.Count > 0).Max(vhcl => vhcl.X));
                    double maxY = Math.Max(function.Clients.Max(clt => clt.Y), function.BestVehicles[function.Time].Where(vhcl => vhcl.clients.Count + vhcl.clientsToServe.Count > 0).Max(vhcl => vhcl.Y));
                    Random r = new Random();
                    g.Clear(Color.LightGray);
                    foreach (Vehicle v in function.BestVehicles[function.Time])
                    {
                        if (v.clients.Count + v.clientsToServe.Count == 0)
                            continue;
                        Pen colorPen = new Pen(Color.FromArgb(r.Next(164), r.Next(164), r.Next(164)), 2);
                        Pen pen = new Pen(Color.FromArgb((int)(colorPen.Color.R * 1.2M), (int)(colorPen.Color.G * 1.2M), (int)(colorPen.Color.B * 1.2M)), 2);
                        g.DrawImage(
                            Properties.Resources.Truck_48,
                            (float)((v.X - minX) * ((graphics.VisibleClipBounds.Width - 40) / (maxX - minX)) - 4),
                                (float)((v.Y - minY) * ((graphics.VisibleClipBounds.Height - 40) / (maxY - minY)) - 4));
                        List<Client> clients = new List<Client>();
                        clients.AddRange(v.clientsToServe);
                        clients.AddRange(v.clients);
                        for (int i = 0; i < clients.Count; ++i)
                        {
                            if (i >= v.clientsToServe.Count)
                                pen = colorPen;
                            int j = (i + 1) % clients.Count;
                            g.DrawString(
                                clients[i].Id.ToString() + ":" + clients[i].VisitTime.ToString("0"),
                                new Font("Calibri", 14),
                                new SolidBrush(pen.Color),
                                (float)((clients[i].X - minX) * ((graphics.VisibleClipBounds.Width - 40) / (maxX - minX)) + 20),
                                (float)((clients[i].Y - minY) * ((graphics.VisibleClipBounds.Height - 40) / (maxY - minY)) + 20));
                            g.DrawLine(
                                pen,
                                (float)((clients[i].X - minX) * ((graphics.VisibleClipBounds.Width - 40) / (maxX - minX)) + 20),
                                (float)((clients[i].Y - minY) * ((graphics.VisibleClipBounds.Height - 40) / (maxY - minY)) + 20),
                                (float)((clients[j].X - minX) * ((graphics.VisibleClipBounds.Width - 40) / (maxX - minX)) + 20),
                                (float)((clients[j].Y - minY) * ((graphics.VisibleClipBounds.Height - 40) / (maxY - minY)) + 20)
                                );
                        }
                    }
                }
                catch
                {
                    return;
                }

            }
        }


    }
}
