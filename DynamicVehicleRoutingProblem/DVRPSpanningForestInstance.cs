﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParticleSwarmOptimization;

namespace DynamicVehicleRoutingProblem
{
    public class DVRPSpanningForestInstance: DVRPInstance
    {
        class Edge
        {
            public Client end1;
            public Client end2;
            public double Length;

            public Edge(Client end1, Client end2)
            {
                this.end1 = end1;
                this.end2 = end2;
                Length = Utils.EuclideanDistance(
                    new double[] { end1.X, end1.Y },
                    new double[] { end2.X, end2.Y },
                    end1.Id,
                    end2.Id);
            }
        }

        protected override void AssignClientsToVehicles(double[] x)
        {
            base.AssignClientsToVehicles(x);
            List<Edge> edges = new List<Edge>();
            int i = 0;
            foreach (Vehicle vehicle in Vehicles)
            {
                vehicle.clients = new List<Client>();
            }
            foreach (Client client1 in KnownClients)
            {
                if (i < Vehicles.Length)
                {
                    Vehicles[i].clients.Add(client1);
                    client1.Vehicle = Vehicles[i];
                    ++i;
                }
                else
                {
                    Vehicle vehicle = new Vehicle();
                    vehicle.clients = new List<Client>();
                    vehicle.clients.Add(client1);
                    client1.Vehicle = vehicle;
                }
                foreach (Client client2 in KnownClients)
                {
                    if (client1 != client2)
                    {
                        edges.Add(new Edge(client1, client2));
                    }
                }
            }
            foreach (Edge edge in edges.OrderBy(edg => edg.Length))
            {
                if (edge.end1.Vehicle != edge.end2.Vehicle &&
                    -(edge.end1.Vehicle.Need + edge.end2.Vehicle.Need) <= availableCapacity)
                {
                    if (Vehicles.Count(vhcl => vhcl.Id == edge.end1.Vehicle.Id) > 0)
                    {
                        edge.end1.Vehicle.clients.AddRange(edge.end2.Vehicle.clients);
                        edge.end2.Vehicle.clients.Clear();
                        edge.end2.Vehicle = edge.end1.Vehicle;
                    }
                    else
                    {
                        edge.end2.Vehicle.clients.AddRange(edge.end1.Vehicle.clients);
                        edge.end1.Vehicle.clients.Clear();
                        edge.end1.Vehicle = edge.end2.Vehicle;
                    }
                }
                else if (edge.end1.Vehicle != edge.end2.Vehicle &&
                    -(edge.end1.Vehicle.Need + edge.end2.Vehicle.Need) <= 1.8M * availableCapacity)
                {
                    Vehicle v1 = edge.end1.Vehicle;
                    Vehicle v2 = edge.end2.Vehicle;
                    Client c1 = edge.end1.Vehicle.clients[0];
                    Client c2 = edge.end2.Vehicle.clients[0];
                    double dist = Utils.EuclideanDistance(
                        new double[] {c1.X, c1.Y},
                        new double[] {c2.X, c2.Y},
                        c1.Id,
                        c2.Id
                        );
                    foreach (Client tempc1 in edge.end1.Vehicle.clients)
                        foreach (Client tempc2 in edge.end2.Vehicle.clients)
                        {
                            double tempDist = Utils.EuclideanDistance(
                        new double[] { tempc1.X, tempc1.Y },
                        new double[] { tempc2.X, tempc2.Y },
                        tempc1.Id,
                        tempc2.Id
                        );
                            if (tempDist > dist)
                            {
                                c1 = tempc1;
                                c2 = tempc2;
                                dist = tempDist;
                            }
                        }
                    List<Client> clients = new List<Client>();
                    clients.AddRange(v1.clients);
                    clients.AddRange(v2.clients);
                    IOrderedEnumerable<Client> orderedClients = clients.OrderBy(clnt => Utils.EuclideanDistance(
                        new double[] { clnt.X, clnt.Y },
                        new double[] { c1.X, c1.Y },
                        clnt.Id,
                        c1.Id
                        ));
                    v1.clients.Clear();
                    v2.clients.Clear();
                    Vehicle vhcl = v1;
                    foreach (Client client in orderedClients)
                    {
                        if (-(vhcl.Need + client.Need) > availableCapacity)
                            vhcl = v2;
                        vhcl.clients.Add(client);
                        client.Vehicle = vhcl;
                    }
                }
            }
            foreach (Vehicle vehicle in Vehicles)
            {
                if (vehicle.clients.Count > 0)
                {
                    vehicle.X = vehicle.clients.Average(clnt => clnt.X);
                    vehicle.Y = vehicle.clients.Average(clnt => clnt.Y);
                }
            }
        }

        public DVRPSpanningForestInstance(string filename, decimal timeStep, int  internalSwarmSize, int internalSwarmIterations, decimal currentTime)
            : base(filename, timeStep, internalSwarmSize, internalSwarmIterations, currentTime)
        {
        }
    }
}
