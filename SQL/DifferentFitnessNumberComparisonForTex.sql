select
replace(mpsov3_250000.name,'D.vrp','') + ' & ' +
(case when mpsov3_250.minresult < mpsov3.minresult and  mpsov3_250.minresult < mpsov3_250000.minresult and  mpsov3_250.minresult < mpsov3_2500.minresult 
then '\textbf{' else '' end) + 
cast (cast (mpsov3_250.minresult as numeric (8,2)) AS varchar) +
(case when mpsov3_250.minresult < mpsov3.minresult and  mpsov3_250.minresult < mpsov3_250000.minresult and mpsov3_250.minresult < mpsov3_2500.minresult 
then '}' else '' end) + 
' & ' +
(case when mpsov3_250.avgresult < mpsov3.avgresult and  mpsov3_250.avgresult < mpsov3_250000.avgresult and mpsov3_250.avgresult < mpsov3_2500.avgresult 
then '\textbf{' else '' end) + 
cast (cast (mpsov3_250.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov3_250.avgresult < mpsov3.avgresult and  mpsov3_250.avgresult < mpsov3_250000.avgresult and mpsov3_250.avgresult < mpsov3_2500.avgresult 
then '}' else '' end) + 
' & ' +
(case when mpsov3_2500.minresult < mpsov3.minresult and  mpsov3_2500.minresult < mpsov3_250000.minresult and mpsov3_250.minresult > mpsov3_2500.minresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3_2500.minresult as numeric (8,2)) AS varchar) +
(case when mpsov3_2500.minresult < mpsov3.minresult and  mpsov3_2500.minresult < mpsov3_250000.minresult and mpsov3_250.minresult > mpsov3_2500.minresult
then '}' else '' end) + 
' & ' +
(case when mpsov3_2500.avgresult < mpsov3.avgresult and  mpsov3_2500.avgresult < mpsov3_250000.avgresult and mpsov3_250.avgresult > mpsov3_2500.avgresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3_2500.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov3_2500.avgresult < mpsov3.avgresult and  mpsov3_2500.avgresult < mpsov3_250000.avgresult and mpsov3_250.avgresult > mpsov3_2500.avgresult
then '}' else '' end) + 
' & ' +
(case when mpsov3_2500.minresult > mpsov3.minresult and  mpsov3.minresult < mpsov3_250000.minresult and mpsov3_250.minresult > mpsov3.minresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3.minresult as numeric (8,2)) AS varchar)+
(case when mpsov3_2500.minresult > mpsov3.minresult and  mpsov3.minresult < mpsov3_250000.minresult and mpsov3_250.minresult > mpsov3.minresult
then '}' else '' end) + 
' & ' +
(case when mpsov3_2500.avgresult > mpsov3.avgresult and  mpsov3.avgresult < mpsov3_250000.avgresult and mpsov3_250.avgresult > mpsov3.avgresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov3_2500.avgresult > mpsov3.avgresult and  mpsov3.avgresult < mpsov3_250000.avgresult and mpsov3_250.avgresult > mpsov3.avgresult
then '}' else '' end) + 
' & ' +
(case when mpsov3_250000.minresult < mpsov3.minresult and  mpsov3_2500.minresult > mpsov3_250000.minresult and mpsov3_250.minresult > mpsov3_250000.minresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3_250000.minresult as numeric (8,2)) AS varchar)+
(case when mpsov3_250000.minresult < mpsov3.minresult and  mpsov3_2500.minresult > mpsov3_250000.minresult and mpsov3_250.minresult > mpsov3_250000.minresult
then '}' else '' end) + 
' & ' +
(case when mpsov3_250000.avgresult < mpsov3.avgresult and  mpsov3_2500.avgresult > mpsov3_250000.avgresult and mpsov3_250.avgresult > mpsov3_250000.avgresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3_250000.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov3_250000.avgresult < mpsov3.avgresult and  mpsov3_2500.avgresult > mpsov3_250000.avgresult and mpsov3_250.avgresult > mpsov3_250000.avgresult
then '}' else '' end) + 
' \\ \hline '
from 
(
--wyniki mpsov3_250000
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].[MPSO3_8SWARM_50TS_250000FFE]
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].[MPSO3_8SWARM_50TS_250000FFE]) as tab
) as mpsov3_250000
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].MPSO3_8SWARM_50TS_250FFE
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].MPSO3_8SWARM_50TS_250FFE) as tab
) as mpsov3_250 on mpsov3_250.name = mpsov3_250000.name
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].[MPSO3_8SWARM_50TS_2500FFE]
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].[MPSO3_8SWARM_50TS_2500FFE]) as tab
) as mpsov3_2500 on mpsov3_2500.name  = mpsov3_250000.name
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].[MPSO3_8SWARM_50TS_25000FFE]
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].[MPSO3_8SWARM_50TS_25000FFE]) as tab
) as mpsov3 on mpsov3.name = mpsov3_250000.name
order by CASE mpsov3_250000.name
WHEN 'c50' THEN 1
WHEN 'c75' THEN 2
WHEN 'c100b' THEN 4
WHEN 'c100' THEN 3
WHEN 'c120' THEN 5
WHEN 'c150' THEN 6
WHEN 'c199' THEN 7
WHEN 'f71' THEN 8
WHEN 'f134' THEN 9
WHEN 'tai75a' THEN 10
WHEN 'tai75b' THEN 11
WHEN 'tai75c' THEN 12
WHEN 'tai75d' THEN 13
WHEN 'tai100a' THEN 14
WHEN 'tai100b' THEN 15
WHEN 'tai100c' THEN 16
WHEN 'tai100d' THEN 17
WHEN 'tai150a' THEN 18
WHEN 'tai150b' THEN 19
WHEN 'tai150c' THEN 20
WHEN 'tai150d' THEN 21
ELSE 22 END
go
