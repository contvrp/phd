select 
  m,
  requests,
  spatial,
  avg(result_to_best)
from
(
select
	name,
	type,
	min_result_to_best,
	result_to_best,
	norm_stdev_result
		,(CASE
		WHEN type LIKE '%v2%' THEN 2
		ELSE 1
	END) as ver
	,(CASE
		WHEN type LIKE '%x1+%' THEN 1
		WHEN type LIKE '%3+%' THEN 3
		ELSE 0
	END) as cluster		
	,(CASE
		WHEN type LIKE '%v1%' AND type LIKE '%x1+%' THEN '2MPSO_1'
		WHEN type LIKE '%v2%' AND type LIKE '%x1+%' THEN '2MPSO_2'
		WHEN type LIKE '%v2%' AND type LIKE '%x3+%' THEN '2MPSO_3'
		ELSE ''
	END) as m		
	,(CASE
		WHEN name LIKE '%_more%' THEN 'moreskewed'
		WHEN name LIKE '%_skewed%' THEN 'skewed'
		WHEN name LIKE '%_normal%' THEN 'normal'
		WHEN name LIKE '%_uniform%' THEN 'uniform'
		ELSE ''
	END) as requests		
	,(CASE
		WHEN name LIKE '%pseudo%' THEN 'pseudoclustered'
		WHEN name LIKE '%really%' THEN 'reallyclusterd'
		WHEN name LIKE '%uniform_D%' THEN 'uniform'
		WHEN name LIKE '%structured%' THEN 'structured'
		ELSE ''
	END) as spatial		

	from
	(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name
	,[Type] as type
	,count(*) as expno
	,MIN(result) as min_result
	,AVG(result) as avg_result
	,STDEV(result) as stdev_result
	,STDEV(result) / AVG(result) as norm_stdev_result
	,MIN(resulttobest) as min_result_to_best
	,AVG(resulttobest) as result_to_best
	,count(result) as experiments
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where [Name] LIKE 'okul_%0%b%' AND
	[Type] NOT LIKE '2MPSOv2x1+50 2OPT 8/040/250/100(0.01)' AND
	[Type] LIKE '%(0.01)'
group by
	[Name]
	,[Type]
	) as res1
	) detailed
	group by
	m,
	spatial,
	requests
order by m,
(CASE
  WHEN requests = 'uniform' THEN 1
  WHEN requests = 'normal' THEN 0
  WHEN requests = 'skewed' THEN 2
  WHEN requests = 'moreskewed' THEN 3
END),
(CASE
  WHEN spatial = 'uniform' THEN 1
  WHEN spatial = 'reallyclusterd' THEN 0
  WHEN spatial = 'pseudoclustered' THEN 2
  WHEN spatial = 'structured' THEN 3
END)