select
replace(fourth_column.name,'D.vrp','') + ' & ' +
(case when first_column.minresult < third_column.minresult and  first_column.minresult < fourth_column.minresult and  first_column.minresult < second_column.minresult 
then '\textbf{' else '' end) + 
cast (cast (first_column.minresult as numeric (8,2)) AS varchar) +
(case when first_column.minresult < third_column.minresult and  first_column.minresult < fourth_column.minresult and first_column.minresult < second_column.minresult 
then '}' else '' end) + 
' & ' +
(case when first_column.avgresult < third_column.avgresult and  first_column.avgresult < fourth_column.avgresult and first_column.avgresult < second_column.avgresult 
then '\textbf{' else '' end) + 
cast (cast (first_column.avgresult as numeric (8,2)) AS varchar)+
(case when first_column.avgresult < third_column.avgresult and  first_column.avgresult < fourth_column.avgresult and first_column.avgresult < second_column.avgresult 
then '}' else '' end) + 
' & ' +
(case when second_column.minresult < third_column.minresult and  second_column.minresult < fourth_column.minresult and first_column.minresult > second_column.minresult
then '\textbf{' else '' end) + 
cast (cast (second_column.minresult as numeric (8,2)) AS varchar) +
(case when second_column.minresult < third_column.minresult and  second_column.minresult < fourth_column.minresult and first_column.minresult > second_column.minresult
then '}' else '' end) + 
' & ' +
(case when second_column.avgresult < third_column.avgresult and  second_column.avgresult < fourth_column.avgresult and first_column.avgresult > second_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (second_column.avgresult as numeric (8,2)) AS varchar)+
(case when second_column.avgresult < third_column.avgresult and  second_column.avgresult < fourth_column.avgresult and first_column.avgresult > second_column.avgresult
then '}' else '' end) + 
' & ' +
(case when second_column.minresult > third_column.minresult and  third_column.minresult < fourth_column.minresult and first_column.minresult > third_column.minresult
then '\textbf{' else '' end) + 
cast (cast (third_column.minresult as numeric (8,2)) AS varchar)+
(case when second_column.minresult > third_column.minresult and  third_column.minresult < fourth_column.minresult and first_column.minresult > third_column.minresult
then '}' else '' end) + 
' & ' +
(case when second_column.avgresult > third_column.avgresult and  third_column.avgresult < fourth_column.avgresult and first_column.avgresult > third_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (third_column.avgresult as numeric (8,2)) AS varchar)+
(case when second_column.avgresult > third_column.avgresult and  third_column.avgresult < fourth_column.avgresult and first_column.avgresult > third_column.avgresult
then '}' else '' end) + 
' & ' +
(case when fourth_column.minresult < third_column.minresult and  second_column.minresult > fourth_column.minresult and first_column.minresult > fourth_column.minresult
then '\textbf{' else '' end) + 
cast (cast (fourth_column.minresult as numeric (8,2)) AS varchar)+
(case when fourth_column.minresult < third_column.minresult and  second_column.minresult > fourth_column.minresult and first_column.minresult > fourth_column.minresult
then '}' else '' end) + 
' & ' +
(case when fourth_column.avgresult < third_column.avgresult and  second_column.avgresult > fourth_column.avgresult and first_column.avgresult > fourth_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (fourth_column.avgresult as numeric (8,2)) AS varchar)+
(case when fourth_column.avgresult < third_column.avgresult and  second_column.avgresult > fourth_column.avgresult and first_column.avgresult > fourth_column.avgresult
then '}' else '' end) + 
' \\ \hline '
from 
(
--wyniki fourth_column
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].MCTREE_PSO_343_8SWARM_40TS
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].MCTREE_PSO_343_8SWARM_40TS) as tab
) as fourth_column
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
) as third_column on fourth_column.name = replace(third_column.name,'D.vrp','')
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA2014'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA2014'
) as first_column on fourth_column.name = replace(first_column.name,'D.vrp','')
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'ACOLNS'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'ACOLNS'
) as second_column on fourth_column.name = replace(second_column.name,'D.vrp','')
order by CASE fourth_column.name
WHEN 'c50' THEN 1
WHEN 'c75' THEN 2
WHEN 'c100b' THEN 4
WHEN 'c100' THEN 3
WHEN 'c120' THEN 5
WHEN 'c150' THEN 6
WHEN 'c199' THEN 7
WHEN 'f71' THEN 8
WHEN 'f134' THEN 9
WHEN 'tai75a' THEN 10
WHEN 'tai75b' THEN 11
WHEN 'tai75c' THEN 12
WHEN 'tai75d' THEN 13
WHEN 'tai100a' THEN 14
WHEN 'tai100b' THEN 15
WHEN 'tai100c' THEN 16
WHEN 'tai100d' THEN 17
WHEN 'tai150a' THEN 18
WHEN 'tai150b' THEN 19
WHEN 'tai150c' THEN 20
WHEN 'tai150d' THEN 21
WHEN 'big_tai385' THEN 22
ELSE 23 END
go

