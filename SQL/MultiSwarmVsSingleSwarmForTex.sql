select
replace(mpsov2large.name,'','') + ' & ' +
(case when mpsov2large.minresult < mpsov3.minresult and  mpsov2large.minresult < mpsov3large.minresult and mpsov2large.minresult < mpsov2.minresult 
then '\textbf{' else '' end) + 
cast (cast (mpsov2large.minresult as numeric (8,2)) AS varchar) +
(case when mpsov2large.minresult < mpsov3.minresult and  mpsov2large.minresult < mpsov3large.minresult and mpsov2large.minresult < mpsov2.minresult 
then '}' else '' end) + 
' & ' +
(case when mpsov2large.avgresult < mpsov3.avgresult and mpsov2large.avgresult < mpsov3large.avgresult and mpsov2large.avgresult < mpsov2.avgresult 
then '\textbf{' else '' end) + 
cast (cast (mpsov2large.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov2large.avgresult < mpsov3.avgresult and mpsov2large.avgresult < mpsov3large.avgresult and mpsov2large.avgresult < mpsov2.avgresult 
then '}' else '' end) + 
' & ' +
(case when mpsov2.minresult < mpsov3.minresult and  mpsov2.minresult < mpsov3large.minresult and mpsov2large.minresult > mpsov2.minresult
then '\textbf{' else '' end) + 
cast (cast (mpsov2.minresult as numeric (8,2)) AS varchar) +
(case when mpsov2.minresult < mpsov3.minresult and  mpsov2.minresult < mpsov3large.minresult and mpsov2large.minresult > mpsov2.minresult
then '}' else '' end) + 
' & ' +
(case when mpsov2.avgresult < mpsov3.avgresult and  mpsov2.avgresult < mpsov3large.avgresult and mpsov2large.avgresult > mpsov2.avgresult
then '\textbf{' else '' end) + 
cast (cast (mpsov2.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov2.avgresult < mpsov3.avgresult and  mpsov2.avgresult < mpsov3large.avgresult and mpsov2large.avgresult > mpsov2.avgresult
then '}' else '' end) + 
' & ' +
(case when mpsov3large.minresult < mpsov3.minresult and  mpsov2.minresult > mpsov3large.minresult and mpsov2large.minresult > mpsov3large.minresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3large.minresult as numeric (8,2)) AS varchar)+
(case when mpsov3large.minresult < mpsov3.minresult and  mpsov2.minresult > mpsov3large.minresult and mpsov2large.minresult > mpsov3large.minresult
then '}' else '' end) + 
' & ' +
(case when mpsov3large.avgresult < mpsov3.avgresult and  mpsov2.avgresult > mpsov3large.avgresult and mpsov2large.avgresult > mpsov3large.avgresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3large.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov3large.avgresult < mpsov3.avgresult and  mpsov2.avgresult > mpsov3large.avgresult and mpsov2large.avgresult > mpsov3large.avgresult
then '}' else '' end) + 
' & ' +
(case when mpsov2.minresult > mpsov3.minresult and  mpsov3.minresult < mpsov3large.minresult and mpsov2large.minresult > mpsov3.minresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3.minresult as numeric (8,2)) AS varchar)+
(case when mpsov2.minresult > mpsov3.minresult and  mpsov3.minresult < mpsov3large.minresult and mpsov2large.minresult > mpsov3.minresult
then '}' else '' end) + 
' & ' +
(case when mpsov2.avgresult > mpsov3.avgresult and  mpsov3.avgresult < mpsov3large.avgresult and mpsov2large.avgresult > mpsov3.avgresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov2.avgresult > mpsov3.avgresult and  mpsov3.avgresult < mpsov3large.avgresult and mpsov2large.avgresult > mpsov3.avgresult
then '}' else '' end) + 
' \\ \hline '
from 
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].MPSO2_1SWARM_50TS_20000FFE
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].MPSO2_1SWARM_50TS_20000FFE) as tab
) as mpsov2large
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].[MPSO2_8SWARM_50TS_2500FFE]
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].[MPSO2_8SWARM_50TS_2500FFE]) as tab
) as mpsov2 on mpsov2.name = mpsov2large.name
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].MPSO3_1SWARM_50TS_20000FFE
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].MPSO3_1SWARM_50TS_20000FFE) as tab
) as mpsov3large on mpsov3large.name = mpsov2large.name
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].[MPSO3_8SWARM_50TS_2500FFE]
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].[MPSO3_8SWARM_50TS_2500FFE]) as tab
) as mpsov3 on mpsov3.name = mpsov2large.name
order by CASE mpsov2large.name
WHEN 'c50' THEN 1
WHEN 'c75' THEN 2
WHEN 'c100b' THEN 4
WHEN 'c100' THEN 3
WHEN 'c120' THEN 5
WHEN 'c150' THEN 6
WHEN 'c199' THEN 7
WHEN 'f71' THEN 8
WHEN 'f134' THEN 9
WHEN 'tai75a' THEN 10
WHEN 'tai75b' THEN 11
WHEN 'tai75c' THEN 12
WHEN 'tai75d' THEN 13
WHEN 'tai100a' THEN 14
WHEN 'tai100b' THEN 15
WHEN 'tai100c' THEN 16
WHEN 'tai100d' THEN 17
WHEN 'tai150a' THEN 18
WHEN 'tai150b' THEN 19
WHEN 'tai150c' THEN 20
WHEN 'tai150d' THEN 21
ELSE 22 END
go
