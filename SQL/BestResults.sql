SELECT rs.[Type],REPLACE(rs.[Name],'D.vrp','')as [Name], round(MIN(result),2) as min_result
    FROM (
        SELECT [Type],[Name], result, Rank() 
          over (Partition BY [Name]
                ORDER BY result ) AS Rank
        FROM 
        (
        SELECT  [Type],[Name], result FROM
        (SELECT [Type],[Name], result, Rank()
        over (Partition BY [Name],[Type]
                ORDER BY id ) AS Rank
        FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
        WHERE [TYPE] LIKE '2MPSO%' and [Name] NOT LIKE 'okul%'
        AND
        [Type] NOT IN
			('2MPSOv2x3+4 2OPT 8/040/100/025(0.02) Ordered Dict',
			'2MPSOv2x3+4 2OPT 8/040/150/000(0.02) No2ndPhase',
			'2MPSOv2x1+50 2OPT 8/040/280/200(0.01)',
			'2MPSOv2x3+4 2OPT 8/040/250/100(0.01)')
		) as rs1 WHERE Rank <= 30
		
        UNION
        SELECT [Algorithm] as [Type], [Name], [MAPSOMinResult] as result
		FROM [DataAnalyzer].[dbo].[ExternalResults]) as res
        
         
        ) rs WHERE Rank = 1
GROUP BY rs.[Type],rs.[Name]
order by CASE rs.[Name]
WHEN 'c50' THEN 1
WHEN 'c75' THEN 2
WHEN 'c100b' THEN 4
WHEN 'c100' THEN 3
WHEN 'c120' THEN 5
WHEN 'c150' THEN 6
WHEN 'c199' THEN 7
WHEN 'f71' THEN 8
WHEN 'f134' THEN 9
WHEN 'tai75a' THEN 10
WHEN 'tai75b' THEN 11
WHEN 'tai75c' THEN 12
WHEN 'tai75d' THEN 13
WHEN 'tai100a' THEN 14
WHEN 'tai100b' THEN 15
WHEN 'tai100c' THEN 16
WHEN 'tai100d' THEN 17
WHEN 'tai150a' THEN 18
WHEN 'tai150b' THEN 19
WHEN 'tai150c' THEN 20
WHEN 'tai150d' THEN 21
ELSE 22 END
go