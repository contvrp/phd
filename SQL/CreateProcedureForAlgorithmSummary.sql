DROP PROCEDURE spSelectSummary
go

CREATE PROCEDURE spSelectSummary (@Experiment varchar(2047)) 
AS 
select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE @Experiment and namemin.name not like 'okul%' and ExternalResults.Algorithm = 'MEMSO(A)'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select @Experiment,'avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),AVG(minmapsoratio),AVG(avgmapsoratio)
from
(select
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE @Experiment and namemin.name not like 'okul%' and ExternalResults.Algorithm = 'MEMSO(A)'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name) as tab
go