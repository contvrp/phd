select
replace(mapso.name,'D.vrp','') + ' & ' +
(case when mpsov1.minresult < mpsov3.minresult and  mpsov1.minresult < mapso.minresult and mpsov1.minresult < memso.minresult and mpsov1.minresult < ga.minresult and mpsov1.minresult < tabu.minresult and mpsov1.minresult < mpsov2.minresult 
then '\textbf{' else '' end) + 
cast (cast (mpsov1.minresult as numeric (8,2)) AS varchar) +
(case when mpsov1.minresult < mpsov3.minresult and  mpsov1.minresult < mapso.minresult and mpsov1.minresult < memso.minresult and mpsov1.minresult < ga.minresult and mpsov1.minresult < tabu.minresult and mpsov1.minresult < mpsov2.minresult 
then '}' else '' end) + 
' & ' +
(case when mpsov1.avgresult < mpsov3.avgresult and  mpsov1.avgresult < mapso.avgresult and mpsov1.avgresult < memso.avgresult and mpsov1.avgresult < ga.avgresult and mpsov1.avgresult < tabu.avgresult and mpsov1.avgresult < mpsov2.avgresult 
then '\textbf{' else '' end) + 
cast (cast (mpsov1.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov1.avgresult < mpsov3.avgresult and  mpsov1.avgresult < mapso.avgresult and mpsov1.avgresult < memso.avgresult and mpsov1.avgresult < ga.avgresult and mpsov1.avgresult < tabu.avgresult and mpsov1.avgresult < mpsov2.avgresult 
then '}' else '' end) + 
' & ' +
(case when mpsov2.minresult < mpsov3.minresult and  mpsov2.minresult < mapso.minresult and mpsov2.minresult < memso.minresult and mpsov2.minresult < ga.minresult and mpsov2.minresult < tabu.minresult and mpsov1.minresult > mpsov2.minresult
then '\textbf{' else '' end) + 
cast (cast (mpsov2.minresult as numeric (8,2)) AS varchar) +
(case when mpsov2.minresult < mpsov3.minresult and  mpsov2.minresult < mapso.minresult and mpsov2.minresult < memso.minresult and mpsov2.minresult < ga.minresult and mpsov2.minresult < tabu.minresult and mpsov1.minresult > mpsov2.minresult
then '}' else '' end) + 
' & ' +
(case when mpsov2.avgresult < mpsov3.avgresult and  mpsov2.avgresult < mapso.avgresult and mpsov2.avgresult < memso.avgresult and mpsov2.avgresult < ga.avgresult and mpsov2.avgresult < tabu.avgresult and mpsov1.avgresult > mpsov2.avgresult
then '\textbf{' else '' end) + 
cast (cast (mpsov2.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov2.avgresult < mpsov3.avgresult and  mpsov2.avgresult < mapso.avgresult and mpsov2.avgresult < memso.avgresult and mpsov2.avgresult < ga.avgresult and mpsov2.avgresult < tabu.avgresult and mpsov1.avgresult > mpsov2.avgresult
then '}' else '' end) + 
' & ' +
(case when mpsov2.minresult > mpsov3.minresult and  mpsov3.minresult < mapso.minresult and mpsov3.minresult < memso.minresult and mpsov3.minresult < ga.minresult and mpsov3.minresult < tabu.minresult and mpsov1.minresult > mpsov3.minresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3.minresult as numeric (8,2)) AS varchar)+
(case when mpsov2.minresult > mpsov3.minresult and  mpsov3.minresult < mapso.minresult and mpsov3.minresult < memso.minresult and mpsov3.minresult < ga.minresult and mpsov3.minresult < tabu.minresult and mpsov1.minresult > mpsov3.minresult
then '}' else '' end) + 
' & ' +
(case when mpsov2.avgresult > mpsov3.avgresult and  mpsov3.avgresult < mapso.avgresult and mpsov3.avgresult < memso.avgresult and mpsov3.avgresult < ga.avgresult and mpsov3.avgresult < tabu.avgresult and mpsov1.avgresult > mpsov3.avgresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov2.avgresult > mpsov3.avgresult and  mpsov3.avgresult < mapso.avgresult and mpsov3.avgresult < memso.avgresult and mpsov3.avgresult < ga.avgresult and mpsov3.avgresult < tabu.avgresult and mpsov1.avgresult > mpsov3.avgresult
then '}' else '' end) + 
' & ' +
(case when mapso.minresult < mpsov3.minresult and  mpsov2.minresult > mapso.minresult and mapso.minresult < memso.minresult and mapso.minresult < ga.minresult and mapso.minresult < tabu.minresult and mpsov1.minresult > mapso.minresult
then '\textbf{' else '' end) + 
cast (cast (mapso.minresult as numeric (8,2)) AS varchar)+
(case when mapso.minresult < mpsov3.minresult and  mpsov2.minresult > mapso.minresult and mapso.minresult < memso.minresult and mapso.minresult < ga.minresult and mapso.minresult < tabu.minresult and mpsov1.minresult > mapso.minresult
then '}' else '' end) + 
' & ' +
(case when mapso.avgresult < mpsov3.avgresult and  mpsov2.avgresult > mapso.avgresult and mapso.avgresult < memso.avgresult and mapso.avgresult < ga.avgresult and mapso.avgresult < tabu.avgresult and mpsov1.avgresult > mapso.avgresult
then '\textbf{' else '' end) + 
cast (cast (mapso.avgresult as numeric (8,2)) AS varchar)+
(case when mapso.avgresult < mpsov3.avgresult and  mpsov2.avgresult > mapso.avgresult and mapso.avgresult < memso.avgresult and mapso.avgresult < ga.avgresult and mapso.avgresult < tabu.avgresult and mpsov1.avgresult > mapso.avgresult
then '}' else '' end) + 
' & ' +
(case when memso.minresult < mpsov3.minresult and  memso.minresult < mapso.minresult and mpsov2.minresult > memso.minresult and memso.minresult < ga.minresult and memso.minresult < tabu.minresult and mpsov1.minresult > memso.minresult
then '\textbf{' else '' end) + 
cast (cast (memso.minresult as numeric (8,2)) AS varchar)+
(case when memso.minresult < mpsov3.minresult and  memso.minresult < mapso.minresult and mpsov2.minresult > memso.minresult and memso.minresult < ga.minresult and memso.minresult < tabu.minresult and mpsov1.minresult > memso.minresult
then '}' else '' end) + 
' & ' +
(case when memso.avgresult < mpsov3.avgresult and  memso.avgresult < mapso.avgresult and mpsov2.avgresult > memso.avgresult and memso.avgresult < ga.avgresult and memso.avgresult < tabu.avgresult and mpsov1.avgresult > memso.avgresult
then '\textbf{' else '' end) + 
cast (cast (memso.avgresult as numeric (8,2)) AS varchar)+
(case when memso.avgresult < mpsov3.avgresult and  memso.avgresult < mapso.avgresult and mpsov2.avgresult > memso.avgresult and memso.avgresult < ga.avgresult and memso.avgresult < tabu.avgresult and mpsov1.avgresult > memso.avgresult
then '}' else '' end) + 
' & ' +
(case when ga.minresult < mpsov3.minresult and  ga.minresult < mapso.minresult and mpsov2.minresult > ga.minresult and memso.minresult > ga.minresult and ga.minresult < tabu.minresult and mpsov1.minresult > ga.minresult
then '\textbf{' else '' end) + 
cast (cast (ga.minresult as numeric (8,2)) AS varchar)+
(case when ga.minresult < mpsov3.minresult and  ga.minresult < mapso.minresult and mpsov2.minresult > ga.minresult and memso.minresult > ga.minresult and ga.minresult < tabu.minresult and mpsov1.minresult > ga.minresult
then '}' else '' end) + 
' & ' +
(case when ga.avgresult < mpsov3.avgresult and  ga.avgresult < mapso.avgresult and mpsov2.avgresult > ga.avgresult and memso.avgresult > ga.avgresult and ga.avgresult < tabu.avgresult and mpsov1.avgresult > ga.avgresult
then '\textbf{' else '' end) + 
cast (cast (ga.avgresult as numeric (8,2)) AS varchar)+
(case when ga.avgresult < mpsov3.avgresult and  ga.avgresult < mapso.avgresult and mpsov2.avgresult > ga.avgresult and memso.avgresult > ga.avgresult and ga.avgresult < tabu.avgresult and mpsov1.avgresult > ga.avgresult
then '}' else '' end) + 
' & ' +
(case when tabu.minresult < mpsov3.minresult and  tabu.minresult < mapso.minresult and mpsov2.minresult > tabu.minresult and memso.minresult > tabu.minresult and ga.minresult > tabu.minresult and mpsov1.minresult > tabu.minresult
then '\textbf{' else '' end) + 
cast (cast (tabu.minresult as numeric (8,2)) AS varchar)+
(case when tabu.minresult < mpsov3.minresult and  tabu.minresult < mapso.minresult and mpsov2.minresult > tabu.minresult and memso.minresult > tabu.minresult and ga.minresult > tabu.minresult and mpsov1.minresult > tabu.minresult
then '}' else '' end) + 
' & ' +
(case when tabu.avgresult < mpsov3.avgresult and  tabu.avgresult < mapso.avgresult and mpsov2.avgresult > tabu.avgresult and memso.avgresult > tabu.avgresult and ga.avgresult > tabu.avgresult and mpsov1.avgresult > tabu.avgresult
then '\textbf{' else '' end) + 
cast (cast (tabu.avgresult as numeric (8,2)) AS varchar)+
(case when tabu.avgresult < mpsov3.avgresult and  tabu.avgresult < mapso.avgresult and mpsov2.avgresult > tabu.avgresult and memso.avgresult > tabu.avgresult and ga.avgresult > tabu.avgresult and mpsov1.avgresult > tabu.avgresult
then '}' else '' end) + 
' \\ \hline '
from 
(
--wyniki mapso
SELECT  
	'sum' as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
) as mapso
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
) as memso on mapso.name = memso.name
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
) as ga on mapso.name = ga.name
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'Tabu'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'Tabu'
) as tabu on mapso.name = tabu.name
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].[MPSO1_8SWARM_50TS_3000FFE]
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].[MPSO1_8SWARM_50TS_3000FFE]) as tab
) as mpsov1 on mpsov1.name = replace(mapso.name,'D.vrp','')
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].[MPSO2_8SWARM_50TS_2500FFE]
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].[MPSO2_8SWARM_50TS_2500FFE]) as tab
) as mpsov2 on mpsov2.name = replace(mapso.name,'D.vrp','')
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].[MPSO3_8SWARM_50TS_2500FFE]
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].[MPSO3_8SWARM_50TS_2500FFE]) as tab
) as mpsov3 on mpsov3.name = replace(mapso.name,'D.vrp','')
order by CASE mapso.name
WHEN 'c50D.vrp' THEN 1
WHEN 'c75D.vrp' THEN 2
WHEN 'c100bD.vrp' THEN 4
WHEN 'c100D.vrp' THEN 3
WHEN 'c120D.vrp' THEN 5
WHEN 'c150D.vrp' THEN 6
WHEN 'c199D.vrp' THEN 7
WHEN 'f71D.vrp' THEN 8
WHEN 'f134D.vrp' THEN 9
WHEN 'tai75aD.vrp' THEN 10
WHEN 'tai75bD.vrp' THEN 11
WHEN 'tai75cD.vrp' THEN 12
WHEN 'tai75dD.vrp' THEN 13
WHEN 'tai100aD.vrp' THEN 14
WHEN 'tai100bD.vrp' THEN 15
WHEN 'tai100cD.vrp' THEN 16
WHEN 'tai100dD.vrp' THEN 17
WHEN 'tai150aD.vrp' THEN 18
WHEN 'tai150bD.vrp' THEN 19
WHEN 'tai150cD.vrp' THEN 20
WHEN 'tai150dD.vrp' THEN 21
ELSE 22 END
go
