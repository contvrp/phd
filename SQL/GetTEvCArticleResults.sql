/*
SELECT [Type],[Name], count(*),min(result), avg(result)
  FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
  where [Type] LIKE '2MPSOv2x3+4 2OPT 8/040/100/000(0.02) No 2OPT' OR [Type] LIKE '2MPSOv2x3+4 2OPT 8/040/100/025(0.02) No Tree'
   OR [Type] LIKE '2MPSOv2x3+4 2OPT 8/040/100/025(0.02)'
  group by [Type],[Name]
  order by [Name],[Type]
GO


SELECT distinct([Type])
  FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
  order by [Type]
GO
*/

--SELECT distinct([Type]) FROM  [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
--        WHERE [TYPE] LIKE       '%(0.025)'

if OBJECT_ID('tempdb..#tmpTEvC','U') is not null
	DROP TABLE  #tmpTEvC
CREATE TABLE #tmpTEvC
(
   Type varchar(1024),
   Name varchar(1024),
   Min numeric(18,2),
   Avg numeric(18,2),
   Count INT
)


INSERT INTO #tmpTEvC 
SELECT rs.[Type],REPLACE(rs.[Name],'D.vrp','')as [Name], round(MIN(result),2), CAST( round(AVG(result),2) as decimal(10,2)), COUNT(*)
    FROM (
        SELECT [Type],[Name], result, Rank() 
          over (Partition BY [Type],[Name]
                ORDER BY [Timestamp] ) AS Rank
        FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
        WHERE [TYPE] IN
        ('2MPSOv1x1+4 2OPT 8/040/100/050(0.02)',
        '2MPSOv2x1+4 2OPT 1/320/100/025(0.02)(40)',
        '2MPSOv2x1+4 2OPT 8/001/001/001(0.02) Tree2OPT',
        --'2MPSOv2x1+4 2OPT 8/020/020/005(0.02)',
        --'2MPSOv2x1+4 2OPT 8/040/100/000(0.02) No 2OPT',
        '2MPSOv2x1+4 2OPT 8/040/100/025(0.02)',
        --'2MPSOv2x1+4 2OPT 8/040/100/025(0.02) No Tree',
        '2MPSOv2x3+4 2OPT 1/320/100/025(0.02)(40)',
        '2MPSOv2x3+4 2OPT 8/020/020/005(0.02)',
        '2MPSOv2x3+4 2OPT 8/040/100/000(0.02) No 2OPT',
        '2MPSOv2x3+4 2OPT 8/040/100/025(0.02)',
        '2MPSOv2x3+4 2OPT 8/040/100/025(0.02) No Tree',
        --'2MPSOv2x3+4 2OPT 8/100/400/100(0.02)',
        'PSO 20v3x3+4 8/220/1400/0(0.025)',

		'PSO 20v3x3+4 8/070/443/0(0.025)',
		'2MPSO 20v3x3+4 8/007/044/0(0.025)',
		'2MPSO 20v3x3+4 8/070/443/0(0.025)',
		'2MPSO 20v3x3+4 8/220/443/0(0.025)',
		'2MPSO_RSv3x3+4 2OPT 8/22/140(0.025)',
		'DE 260v3x3+4 8/160/1950/0C0.90(0.025)',
		'DE 260v3x3+4 8/051/617/0C0.90(0.025)',
		'DE 260v3x3+4 8/018/173/0C0.90(0.025)',
		--2MPSO+2nd
		'PSO 20v3x3+4 8/020/127/27(0.025)'

        )
        ) rs WHERE Rank <= 30
GROUP BY rs.[Type],rs.[Name]
order by CASE rs.[Name]
WHEN 'c50D.vrp' THEN 1
WHEN 'c75D.vrp' THEN 2
WHEN 'c100bD.vrp' THEN 4
WHEN 'c100D.vrp' THEN 3
WHEN 'c120D.vrp' THEN 5
WHEN 'c150D.vrp' THEN 6
WHEN 'c199D.vrp' THEN 7
WHEN 'f71D.vrp' THEN 8
WHEN 'f134D.vrp' THEN 9
WHEN 'tai75aD.vrp' THEN 10
WHEN 'tai75bD.vrp' THEN 11
WHEN 'tai75cD.vrp' THEN 12
WHEN 'tai75dD.vrp' THEN 13
WHEN 'tai100aD.vrp' THEN 14
WHEN 'tai100bD.vrp' THEN 15
WHEN 'tai100cD.vrp' THEN 16
WHEN 'tai100dD.vrp' THEN 17
WHEN 'tai150aD.vrp' THEN 18
WHEN 'tai150bD.vrp' THEN 19
WHEN 'tai150cD.vrp' THEN 20
WHEN 'tai150dD.vrp' THEN 21
ELSE 22 END


GO

SELECT [Type],[Name],[Min],[Rank] FROM (
SELECT [Type],[Name],[Min], RANK() over (Partition BY [Name]
                ORDER BY [Min],[Avg] ) AS Rank
 FROM #tmpTEvC) as TempTab
 WHERE Rank <=1 
 
 order by CASE [Name]
WHEN 'c50' THEN 1
WHEN 'c75' THEN 2
WHEN 'c100b' THEN 4
WHEN 'c100' THEN 3
WHEN 'c120' THEN 5
WHEN 'c150' THEN 6
WHEN 'c199' THEN 7
WHEN 'f71' THEN 8
WHEN 'f134' THEN 9
WHEN 'tai75a' THEN 10
WHEN 'tai75b' THEN 11
WHEN 'tai75c' THEN 12
WHEN 'tai75d' THEN 13
WHEN 'tai100a' THEN 14
WHEN 'tai100b' THEN 15
WHEN 'tai100c' THEN 16
WHEN 'tai100d' THEN 17
WHEN 'tai150a' THEN 18
WHEN 'tai150b' THEN 19
WHEN 'tai150c' THEN 20
WHEN 'tai150d' THEN 21
ELSE 22 END

/*
select * from [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
where result >= 1684.95-0.01 and result <= 1684.95+0.01
*/
/*
select * from [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
where type = 'DE 260v3x3+4 8/160/1950/0C0.90(0.025)' and name like 'tai75c%'
order by result
*/
/*
delete from DataAnalyzer.dbo.PSO_DVRP_LIMITED_AREAS_PHASE
where id = 106920
*/

/*
select * from [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
where type like '%8/022/140/0(0.025)'
order by result
*/

exec spSelectSummary @Experiment = 'PSODR3 20v3OrgnlDSharex1+4 8/022/2.34sec(0.025)'
go

exec spSelectSummary @Experiment = 'VehNumPen 20v3OrgnlDSharex1+4 8/022/2.34sec(0.025)'
go

exec spSelectTimeAllSummary @Experiment = 'PSODR3 20v3OrgnlDSharex3+4 8/022/140(0.025)', @Machine = '%'
go

exec spSelectSummary @Experiment = 'PSODR3 20v3Dscrtx1+4 8/022/1.88sec(0.025)'
go

exec spSelectSummary @Experiment = 'PSODR3 20v3Dscrtx2+4 8/022/1.88sec(0.025)'
go

exec spSelectSummary @Experiment = 'PSODR3 20v3Dscrtx3+4 8/022/1.88sec(0.025)'
go

exec spSelectTimeAllSummary @Experiment = 'PSODR3 4100v3OrgnlDSharex1+4 8/022/1.88sec(0.025)', @Machine = '%'
exec spSelectTimeAllSummary @Experiment = 'PSODR3 20v3OrgnlDSharex1+4 8/022/1.88sec(0.025)', @Machine = '%'
exec spSelectTimeAllSummary @Experiment = 'PSODR3 20v3Dscrtx1+4 8/022/1.88sec(0.025)', @Machine = '%'
exec spSelectTimeAllSummary @Experiment = 'DEDR3 260v3Dscrtx1+4 8/016/1.88secC0.90(0.025)', @Machine = '%'
exec spSelectTimeAllSummary @Experiment = 'PSODR3 20v3Dscrtx2+4 8/022/1.88sec(0.025)', @Machine = '%'
exec spSelectTimeAllSummary @Experiment = 'DEDR3 260v3Dscrtx2+4 8/016/1.88secC0.90(0.025)', @Machine = '%'
exec spSelectTimeAllSummary @Experiment = 'PSODR3 20v3Dscrtx3+4 8/022/1.88sec(0.025)', @Machine = '%'
exec spSelectTimeAllSummary @Experiment = 'DEDR3 260v3Dscrtx3+4 8/016/1.88secC0.90(0.025)', @Machine = '%'

exec spSelectTimeAllSummary @Experiment = '2MPSO_RSv3x3+4 2OPT 8/22/140(0.025)', @Machine = '%'


exec spSelectCustomSummary @Experiment ='PSODR3 20v3Dscrtx1+4 8/022/1.88sec(0.025)', @External='GA'
go

exec spSelectCustomSummary @Experiment ='PSODR3 20v3OrgnlDSharex1+4 8/022/1.88sec(0.025)', @External='GA'
go

exec spSelectCustomSummary @Experiment ='PSODR3 20v3Dscrtx1+4 8/022/1.88sec(0.025)', @External='GA'
exec spSelectCustomSummary @Experiment ='PSODR3 20v3Dscrtx1+4 8/022/1.88sec(0.025)', @External='ACOLNS'
exec spSelectCustomSummary @Experiment ='DEDR3 260v3Dscrtx1+4 8/016/1.88secC0.90(0.025)', @External='GA'
exec spSelectCustomSummary @Experiment ='DEDR3 260v3Dscrtx1+4 8/016/1.88secC0.90(0.025)', @External='ACOLNS'
go

select * from PSO_DVRP_LIMITED_AREAS_PHASE
where type LIKE '%DR3 %v3Dscrtx%+4 8/%/1.88sec%(0.025)'
order by ComputationsTime desc


--SWEVO results
exec spSelectCustomSummary @Experiment ='DR5 20 16v3OrgnlDSharex2+4 8/022/140/0(0.025)', @External='MEMSO(A)'
exec spSelectCustomSummary @Experiment ='DR5 Log4 8192v3OrgnlDSharex2+4 8/022/140/0(0.025)', @External='MEMSO(A)'
exec spSelectCustomSummary @Experiment ='DR5 Log4 8192v3OrgnlDSharex2+4 8/022/140/0(0.025)', @External='GA'
exec spSelectCustomSummary @Experiment ='DR5 20 16v3OrgnlDSharex2+4 8/022/140/0(0.025)', @External='AAC-DVRP-2OPT'
exec spSelectCustomSummary @Experiment ='DR5 20 16v3OrgnlDSharex2+4 8/022/140/0(0.025)', @External='ACOLNS'
exec spSelectCustomSummary @Experiment ='DR5 20 16v3OrgnlDSharex2+4 8/022/140/0(0.025)', @External='ACS'
exec spSelectCustomSummary @Experiment ='DR5 Log4 8192v3OrgnlDSharex2+4 8/022/140/0(0.025)', @External='AAC-DVRP-2OPT'
