drop table #tmpResults;
CREATE TABLE #tmpResults
(
   Type varchar(1024),
   Name varchar(1024),
   Instances INT,
   MinResult numeric(18,2),
   AvgResult numeric(18,2),
   MaxResult numeric(18,2),
   StdevResult numeric(18,2),
   NormStdevResult numeric(18,4),
   Sigma numeric(18,2),
   Min_1_05 numeric(18,2),
   Stable varchar(1024),
   BestRatio numeric(18,4),
   AvgRatio numeric(18,4),
   MaxRatio numeric(18,4),
   MinMAPSORatio numeric(18,4),
   AvgMAPSORatio numeric(18,4)
)

--basic 2MPSO results after tuning parameters
DELETE FROM #tmpResults
WHERE [Type] = '2MPSO_RSv3x3+4 2OPT 8/22/140(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = '2MPSO_RSv3x3+4 2OPT 8/22/140(0.025)'
go

--basic real two-phse 2MPSO results after tuning parameters
DELETE FROM #tmpResults
WHERE [Type] = 'PSO 20v3x3+4 8/020/127/27(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = 'PSO 20v3x3+4 8/020/127/27(0.025)'
go

--basic 2MPSO without 2nd phase results after tuning parameters
DELETE FROM #tmpResults
WHERE [Type] = 'PSO 20v3x3+4 8/020/127/0(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = 'PSO 20v3x3+4 8/020/127/0(0.025)'
go

--basic 2MPSO results after tuning parameters and no knowledge transfer
DELETE FROM #tmpResults
WHERE [Type] = 'PSO NoHistory 20v3x3+4 8/022/140/0(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = 'PSO NoHistory 20v3x3+4 8/022/140/0(0.025)'
go

--basic 2MPSO results after tuning parameters and no heuristic algorithm
DELETE FROM #tmpResults
WHERE [Type] = 'PSO 16v3x3+4 8/022/140/0(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = 'PSO 16v3x3+4 8/022/140/0(0.025)'
go


--just tree and 2opt
DELETE FROM #tmpResults
WHERE [Type] = 'Tree 4 8(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = 'Tree 4 8(0.025)'
go


--2MPSO small number of FFEs
DELETE FROM #tmpResults
WHERE [Type] = '2MPSO 20v3x3+4 8/007/044/0(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = '2MPSO 20v3x3+4 8/007/044/0(0.025)'
go

--2MPSO large number of FFEs
DELETE FROM #tmpResults
WHERE [Type] = '2MPSO 20v3x3+4 8/070/443/0(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = '2MPSO 20v3x3+4 8/070/443/0(0.025)'
go

--2MPSO very large number of FFEs
DELETE FROM #tmpResults
WHERE [Type] = 'PSO 20v3x3+4 8/220/1400/0(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = 'PSO 20v3x3+4 8/220/1400/0(0.025)'
go

--basic DE results after tuning parameters
DELETE FROM #tmpResults
WHERE [Type] = 'DE 260v3x3+4 8/016/195/0C0.90(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = 'DE 260v3x3+4 8/016/195/0C0.90(0.025)'
go

--basic DE results - fast experiment
DELETE FROM #tmpResults
WHERE [Type] = 'DE 260v3x3+4 8/010/031/0C0.90(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = 'DE 260v3x3+4 8/010/031/0C0.90(0.025)'
go

--basic DE results - large experiment
DELETE FROM #tmpResults
WHERE [Type] = 'DE 260v3x3+4 8/051/617/0C0.90(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = 'DE 260v3x3+4 8/051/617/0C0.90(0.025)'
go

DELETE FROM #tmpResults
WHERE [Type] = 'DE 260v3x3+4 8/160/1950/0C0.90(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = 'DE 260v3x3+4 8/160/1950/0C0.90(0.025)'
go

--single swarm 1st config.
DELETE FROM #tmpResults
WHERE [Type] = '2MPSO 20v3x3+4 1/062/396/0(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = '2MPSO 20v3x3+4 1/062/396/0(0.025)'
go

--single swarm 2nd config.
DELETE FROM #tmpResults
WHERE [Type] = '2MPSO 20v3x3+4 1/176/140/0(0.025)'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = '2MPSO 20v3x3+4 1/176/140/0(0.025)'
go

--Orlando, FL SSCI2014 results
DELETE FROM #tmpResults
WHERE [Type] = '2MPSOv2x3+4 2OPT 8/040/250/800'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/040/250/800'
go

DELETE FROM #tmpResults
WHERE [Type] = '2MPSOv2x3+4 2OPT 8/100/1000/100'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/100/1000/100'
go

DELETE FROM #tmpResults
WHERE [Type] = '2MPSOv2x1+50 2OPT 8/040/250/800'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = '2MPSOv2x1+50 2OPT 8/040/250/800'
go

DELETE FROM #tmpResults
WHERE [Type] = '2MPSOv2x1+50 2OPT 8/100/1000/100'
INSERT INTO #tmpResults
exec spSelectSummary @Experiment = '2MPSOv2x1+50 2OPT 8/100/1000/100'
go

select
replace(fourth_column.name,'D.vrp','') + ' & ' +
(case when first_column.minresult <= third_column.minresult and  first_column.minresult <= fourth_column.minresult and  first_column.minresult <= second_column.minresult and first_column.minresult <= fifth_column.minresult and  first_column.minresult <= sixth_column.minresult 
then '\textbf{' else '' end) + 
cast (cast (first_column.minresult as numeric (8,2)) AS varchar) +
(case when first_column.minresult <= third_column.minresult and  first_column.minresult <= fourth_column.minresult and first_column.minresult <= second_column.minresult  and first_column.minresult <= fifth_column.minresult and  first_column.minresult <= sixth_column.minresult 
then '}' else '' end) + 
' & ' +
(case when first_column.avgresult <= third_column.avgresult and  first_column.avgresult <= fourth_column.avgresult and first_column.avgresult <= second_column.avgresult  and first_column.avgresult <= fifth_column.avgresult and  first_column.avgresult <= sixth_column.avgresult 
then '\textbf{' else '' end) + 
cast (cast (first_column.avgresult as numeric (8,2)) AS varchar)+
(case when first_column.avgresult <= third_column.avgresult and  first_column.avgresult <= fourth_column.avgresult and first_column.avgresult <= second_column.avgresult  and first_column.avgresult <= fifth_column.avgresult and  first_column.avgresult <= sixth_column.avgresult 
then '}' else '' end) + 
' & ' +
(case when second_column.minresult <= third_column.minresult and  second_column.minresult <= fourth_column.minresult and first_column.minresult >= second_column.minresult and  second_column.minresult <= fifth_column.minresult and  second_column.minresult <= sixth_column.minresult
then '\textbf{' else '' end) + 
cast (cast (second_column.minresult as numeric (8,2)) AS varchar) +
(case when second_column.minresult <= third_column.minresult and  second_column.minresult <= fourth_column.minresult and first_column.minresult >= second_column.minresult and  second_column.minresult <= fifth_column.minresult and  second_column.minresult <= sixth_column.minresult
then '}' else '' end) + 
' & ' +
(case when second_column.avgresult <= third_column.avgresult and  second_column.avgresult <= fourth_column.avgresult and first_column.avgresult >= second_column.avgresult and  second_column.avgresult <= fifth_column.avgresult and  second_column.avgresult <= sixth_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (second_column.avgresult as numeric (8,2)) AS varchar)+
(case when second_column.avgresult <= third_column.avgresult and  second_column.avgresult <= fourth_column.avgresult and first_column.avgresult >= second_column.avgresult and  second_column.avgresult <= fifth_column.avgresult and  second_column.avgresult <= sixth_column.avgresult
then '}' else '' end) + 
' & ' +
(case when second_column.minresult >= third_column.minresult and  third_column.minresult <= fourth_column.minresult and first_column.minresult >= third_column.minresult and  third_column.minresult <= fifth_column.minresult and  third_column.minresult <= sixth_column.minresult
then '\textbf{' else '' end) + 
cast (cast (third_column.minresult as numeric (8,2)) AS varchar)+
(case when second_column.minresult >= third_column.minresult and  third_column.minresult <= fourth_column.minresult and first_column.minresult >= third_column.minresult and  third_column.minresult <= fifth_column.minresult and  third_column.minresult <= sixth_column.minresult
then '}' else '' end) + 
' & ' +
(case when second_column.avgresult >= third_column.avgresult and  third_column.avgresult <= fourth_column.avgresult and first_column.avgresult >= third_column.avgresult and  third_column.avgresult <= fifth_column.avgresult and  third_column.avgresult <= sixth_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (third_column.avgresult as numeric (8,2)) AS varchar)+
(case when second_column.avgresult >= third_column.avgresult and  third_column.avgresult <= fourth_column.avgresult and first_column.avgresult >= third_column.avgresult and  third_column.avgresult <= fifth_column.avgresult and  third_column.avgresult <= sixth_column.avgresult
then '}' else '' end) + 
' & ' +
(case when fourth_column.minresult <= third_column.minresult and  second_column.minresult >= fourth_column.minresult and first_column.minresult >= fourth_column.minresult and  fourth_column.minresult <= fifth_column.minresult and  fourth_column.minresult <= sixth_column.minresult
then '\textbf{' else '' end) + 
cast (cast (fourth_column.minresult as numeric (8,2)) AS varchar)+
(case when fourth_column.minresult <= third_column.minresult and  second_column.minresult >= fourth_column.minresult and first_column.minresult >= fourth_column.minresult and  fourth_column.minresult <= fifth_column.minresult and  fourth_column.minresult <= sixth_column.minresult
then '}' else '' end) + 
' & ' +
(case when fourth_column.avgresult <= third_column.avgresult and  second_column.avgresult >= fourth_column.avgresult and first_column.avgresult >= fourth_column.avgresult and  fourth_column.avgresult <= fifth_column.avgresult and  fourth_column.avgresult <= sixth_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (fourth_column.avgresult as numeric (8,2)) AS varchar)+
(case when fourth_column.avgresult <= third_column.avgresult and  second_column.avgresult >= fourth_column.avgresult and first_column.avgresult >= fourth_column.avgresult and  fourth_column.avgresult <= fifth_column.avgresult and  fourth_column.avgresult <= sixth_column.avgresult
then '}' else '' end) + 
' & ' +
(case when fifth_column.minresult <= third_column.minresult and  second_column.minresult >= fifth_column.minresult and first_column.minresult >= fifth_column.minresult and  fourth_column.minresult >= fifth_column.minresult and  fifth_column.minresult <= sixth_column.minresult
then '\textbf{' else '' end) + 
cast (cast (fifth_column.minresult as numeric (8,2)) AS varchar)+
(case when fifth_column.minresult <= third_column.minresult and  second_column.minresult >= fifth_column.minresult and first_column.minresult >= fifth_column.minresult and  fourth_column.minresult >= fifth_column.minresult and  fifth_column.minresult <= sixth_column.minresult
then '}' else '' end) + 
' & ' +
(case when fifth_column.avgresult <= third_column.avgresult and  second_column.avgresult >= fifth_column.avgresult and first_column.avgresult >= fifth_column.avgresult and  fourth_column.avgresult >= fifth_column.avgresult and  fifth_column.avgresult <= sixth_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (fifth_column.avgresult as numeric (8,2)) AS varchar)+
(case when fifth_column.avgresult <= third_column.avgresult and  second_column.avgresult >= fifth_column.avgresult and first_column.avgresult >= fifth_column.avgresult and  fourth_column.avgresult >= fifth_column.avgresult and  fifth_column.avgresult <= sixth_column.avgresult
then '}' else '' end) + 
' & ' +
(case when sixth_column.minresult <= third_column.minresult and  second_column.minresult >= sixth_column.minresult and first_column.minresult >= sixth_column.minresult and  sixth_column.minresult <= fifth_column.minresult and  fourth_column.minresult >= sixth_column.minresult
then '\textbf{' else '' end) + 
cast (cast (sixth_column.minresult as numeric (8,2)) AS varchar)+
(case when sixth_column.minresult <= third_column.minresult and  second_column.minresult >= sixth_column.minresult and first_column.minresult >= sixth_column.minresult and  sixth_column.minresult <= fifth_column.minresult and  fourth_column.minresult >= sixth_column.minresult
then '}' else '' end) + 
' & ' +
(case when sixth_column.avgresult <= third_column.avgresult and  second_column.avgresult >= sixth_column.avgresult and first_column.avgresult >= sixth_column.avgresult and  sixth_column.avgresult <= fifth_column.avgresult and  fourth_column.avgresult >= sixth_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (sixth_column.avgresult as numeric (8,2)) AS varchar)+
(case when sixth_column.avgresult <= third_column.avgresult and  second_column.avgresult >= sixth_column.avgresult and first_column.avgresult >= sixth_column.avgresult and  sixth_column.avgresult <= fifth_column.avgresult and  fourth_column.avgresult >= sixth_column.avgresult
then '}' else '' end) + 
' \\ \hline '
from 
(
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = 'DE 260v3x3+4 8/016/195/0C0.90(0.025)'
) as fifth_column
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult,
	  'MEMSO(A)' as Type
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult],
  'MEMSO(A)' as Type
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
) as fourth_column on fifth_column.name = replace(fourth_column.name,'D.vrp','')
join
(
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = '2MPSO_RSv3x3+4 2OPT 8/22/140(0.025)'
) as sixth_column on fifth_column.name = sixth_column.name
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult,
	  'MAPSO' as Type
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult],
  'MAPSO' as Type
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
) as third_column on fifth_column.name = replace(third_column.name,'D.vrp','')
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult,
	  'GA' as Type
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA' and name != 'big_tai385'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult],
  'GA' as Type
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA' and name != 'big_tai385'
) as second_column on fifth_column.name = replace(second_column.name,'D.vrp','')
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult,
	  'TABU' as Type
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'TABU' and name != 'big_tai385'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult],
  'TABU' as Type
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'TABU' and name != 'big_tai385'
) as first_column on fifth_column.name = replace(first_column.name,'D.vrp','')

order by CASE fifth_column.name
WHEN 'c50' THEN 1
WHEN 'c75' THEN 2
WHEN 'c100b' THEN 4
WHEN 'c100' THEN 3
WHEN 'c120' THEN 5
WHEN 'c150' THEN 6
WHEN 'c199' THEN 7
WHEN 'f71' THEN 8
WHEN 'f134' THEN 9
WHEN 'tai75a' THEN 10
WHEN 'tai75b' THEN 11
WHEN 'tai75c' THEN 12
WHEN 'tai75d' THEN 13
WHEN 'tai100a' THEN 14
WHEN 'tai100b' THEN 15
WHEN 'tai100c' THEN 16
WHEN 'tai100d' THEN 17
WHEN 'tai150a' THEN 18
WHEN 'tai150b' THEN 19
WHEN 'tai150c' THEN 20
WHEN 'tai150d' THEN 21
WHEN 'big_tai385' THEN 22
ELSE 23 END
go

select
replace(first_column.name,'D.vrp','') + ' & ' +
(case when first_column.minresult <= third_column.minresult and  first_column.minresult <= second_column.minresult 
then '\textbf{' else '' end) + 
cast (cast (first_column.minresult as numeric (8,2)) AS varchar) +
(case when first_column.minresult <= third_column.minresult and  first_column.minresult <= second_column.minresult 
then '}' else '' end) + 
' & ' +
(case when first_column.avgresult <= third_column.avgresult and  first_column.avgresult <= second_column.avgresult 
then '\textbf{' else '' end) + 
cast (cast (first_column.avgresult as numeric (8,2)) AS varchar)+
(case when first_column.avgresult <= third_column.avgresult and  first_column.avgresult <= second_column.avgresult 
then '}' else '' end) + 
' & ' +
(case when second_column.minresult <= third_column.minresult and  first_column.minresult >= second_column.minresult
then '\textbf{' else '' end) + 
cast (cast (second_column.minresult as numeric (8,2)) AS varchar) +
(case when second_column.minresult <= third_column.minresult and  first_column.minresult >= second_column.minresult
then '}' else '' end) + 
' & ' +
(case when second_column.avgresult <= third_column.avgresult and  first_column.avgresult >= second_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (second_column.avgresult as numeric (8,2)) AS varchar)+
(case when second_column.avgresult <= third_column.avgresult and  first_column.avgresult >= second_column.avgresult
then '}' else '' end) + 
' & ' +
(case when second_column.minresult >= third_column.minresult and  first_column.minresult >= third_column.minresult
then '\textbf{' else '' end) + 
cast (cast (third_column.minresult as numeric (8,2)) AS varchar)+
(case when second_column.minresult >= third_column.minresult and  first_column.minresult >= third_column.minresult
then '}' else '' end) + 
' & ' +
(case when second_column.avgresult >= third_column.avgresult and  first_column.avgresult >= third_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (third_column.avgresult as numeric (8,2)) AS varchar)+
(case when second_column.avgresult >= third_column.avgresult and first_column.avgresult >= third_column.avgresult
then '}' else '' end) + 
' \\ \cline{1-7} '
from 
(
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = '2MPSO 20v3x3+4 8/070/443/0(0.025)'
) as third_column
join
(
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = '2MPSO 20v3x3+4 8/007/044/0(0.025)'
) as first_column on third_column.name = replace(first_column.name,'D.vrp','')
join
(
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = '2MPSO_RSv3x3+4 2OPT 8/22/140(0.025)'
) as second_column on third_column.name = replace(second_column.name,'D.vrp','')
order by CASE first_column.name
WHEN 'c50' THEN 1
WHEN 'c75' THEN 2
WHEN 'c100b' THEN 4
WHEN 'c100' THEN 3
WHEN 'c120' THEN 5
WHEN 'c150' THEN 6
WHEN 'c199' THEN 7
WHEN 'f71' THEN 8
WHEN 'f134' THEN 9
WHEN 'tai75a' THEN 10
WHEN 'tai75b' THEN 11
WHEN 'tai75c' THEN 12
WHEN 'tai75d' THEN 13
WHEN 'tai100a' THEN 14
WHEN 'tai100b' THEN 15
WHEN 'tai100c' THEN 16
WHEN 'tai100d' THEN 17
WHEN 'tai150a' THEN 18
WHEN 'tai150b' THEN 19
WHEN 'tai150c' THEN 20
WHEN 'tai150d' THEN 21
WHEN 'big_tai385' THEN 22
ELSE 23 END
go

select
replace(first_column.name,'D.vrp','') + ' & ' +
(case when first_column.minresult <= third_column.minresult and  first_column.minresult <= second_column.minresult 
then '\textbf{' else '' end) + 
cast (cast (first_column.minresult as numeric (8,2)) AS varchar) +
(case when first_column.minresult <= third_column.minresult and  first_column.minresult <= second_column.minresult 
then '}' else '' end) + 
' & ' +
(case when first_column.avgresult <= third_column.avgresult and  first_column.avgresult <= second_column.avgresult 
then '\textbf{' else '' end) + 
cast (cast (first_column.avgresult as numeric (8,2)) AS varchar)+
(case when first_column.avgresult <= third_column.avgresult and  first_column.avgresult <= second_column.avgresult 
then '}' else '' end) + 
' & ' +
(case when second_column.minresult <= third_column.minresult and  first_column.minresult >= second_column.minresult
then '\textbf{' else '' end) + 
cast (cast (second_column.minresult as numeric (8,2)) AS varchar) +
(case when second_column.minresult <= third_column.minresult and  first_column.minresult >= second_column.minresult
then '}' else '' end) + 
' & ' +
(case when second_column.avgresult <= third_column.avgresult and  first_column.avgresult >= second_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (second_column.avgresult as numeric (8,2)) AS varchar)+
(case when second_column.avgresult <= third_column.avgresult and  first_column.avgresult >= second_column.avgresult
then '}' else '' end) + 
' & ' +
(case when second_column.minresult >= third_column.minresult and  first_column.minresult >= third_column.minresult
then '\textbf{' else '' end) + 
cast (cast (third_column.minresult as numeric (8,2)) AS varchar)+
(case when second_column.minresult >= third_column.minresult and  first_column.minresult >= third_column.minresult
then '}' else '' end) + 
' & ' +
(case when second_column.avgresult >= third_column.avgresult and  first_column.avgresult >= third_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (third_column.avgresult as numeric (8,2)) AS varchar)+
(case when second_column.avgresult >= third_column.avgresult and first_column.avgresult >= third_column.avgresult
then '}' else '' end) + 
' \\ \cline{1-7} '
from 
(
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = '2MPSO 20v3x3+4 1/062/396/0(0.025)'
) as third_column
join
(
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = '2MPSO_RSv3x3+4 2OPT 8/22/140(0.025)'
) as first_column on third_column.name = replace(first_column.name,'D.vrp','')
join
(
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = '2MPSO 20v3x3+4 1/176/140/0(0.025)'
) as second_column on third_column.name = replace(second_column.name,'D.vrp','')
order by CASE first_column.name
WHEN 'c50' THEN 1
WHEN 'c75' THEN 2
WHEN 'c100b' THEN 4
WHEN 'c100' THEN 3
WHEN 'c120' THEN 5
WHEN 'c150' THEN 6
WHEN 'c199' THEN 7
WHEN 'f71' THEN 8
WHEN 'f134' THEN 9
WHEN 'tai75a' THEN 10
WHEN 'tai75b' THEN 11
WHEN 'tai75c' THEN 12
WHEN 'tai75d' THEN 13
WHEN 'tai100a' THEN 14
WHEN 'tai100b' THEN 15
WHEN 'tai100c' THEN 16
WHEN 'tai100d' THEN 17
WHEN 'tai150a' THEN 18
WHEN 'tai150b' THEN 19
WHEN 'tai150c' THEN 20
WHEN 'tai150d' THEN 21
WHEN 'big_tai385' THEN 22
ELSE 23 END
go

select
replace(first_column.name,'D.vrp','') + ' & ' +
(case when first_column.minresult <= third_column.minresult and  first_column.minresult <= second_column.minresult 
then '\textbf{' else '' end) + 
cast (cast (first_column.minresult as numeric (8,2)) AS varchar) +
(case when first_column.minresult <= third_column.minresult and  first_column.minresult <= second_column.minresult 
then '}' else '' end) + 
' & ' +
(case when first_column.avgresult <= third_column.avgresult and  first_column.avgresult <= second_column.avgresult 
then '\textbf{' else '' end) + 
cast (cast (first_column.avgresult as numeric (8,2)) AS varchar)+
(case when first_column.avgresult <= third_column.avgresult and  first_column.avgresult <= second_column.avgresult 
then '}' else '' end) + 
' & ' +
(case when second_column.minresult <= third_column.minresult and  first_column.minresult >= second_column.minresult
then '\textbf{' else '' end) + 
cast (cast (second_column.minresult as numeric (8,2)) AS varchar) +
(case when second_column.minresult <= third_column.minresult and  first_column.minresult >= second_column.minresult
then '}' else '' end) + 
' & ' +
(case when second_column.avgresult <= third_column.avgresult and  first_column.avgresult >= second_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (second_column.avgresult as numeric (8,2)) AS varchar)+
(case when second_column.avgresult <= third_column.avgresult and  first_column.avgresult >= second_column.avgresult
then '}' else '' end) + 
' & ' +
(case when second_column.minresult >= third_column.minresult and  first_column.minresult >= third_column.minresult
then '\textbf{' else '' end) + 
cast (cast (third_column.minresult as numeric (8,2)) AS varchar)+
(case when second_column.minresult >= third_column.minresult and  first_column.minresult >= third_column.minresult
then '}' else '' end) + 
' & ' +
(case when second_column.avgresult >= third_column.avgresult and  first_column.avgresult >= third_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (third_column.avgresult as numeric (8,2)) AS varchar)+
(case when second_column.avgresult >= third_column.avgresult and first_column.avgresult >= third_column.avgresult
then '}' else '' end) + 
' \\ \cline{1-7} ' as info
from 
(
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = 'DE 260v3x3+4 8/051/617/0C0.90(0.025)'
) as third_column
join
(
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = 'DE 260v3x3+4 8/010/031/0C0.90(0.025)'
) as first_column on third_column.name = replace(first_column.name,'D.vrp','')
join
(
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = 'DE 260v3x3+4 8/016/195/0C0.90(0.025)'
) as second_column on third_column.name = replace(second_column.name,'D.vrp','')
order by CASE first_column.name
WHEN 'c50' THEN 1
WHEN 'c75' THEN 2
WHEN 'c100b' THEN 4
WHEN 'c100' THEN 3
WHEN 'c120' THEN 5
WHEN 'c150' THEN 6
WHEN 'c199' THEN 7
WHEN 'f71' THEN 8
WHEN 'f134' THEN 9
WHEN 'tai75a' THEN 10
WHEN 'tai75b' THEN 11
WHEN 'tai75c' THEN 12
WHEN 'tai75d' THEN 13
WHEN 'tai100a' THEN 14
WHEN 'tai100b' THEN 15
WHEN 'tai100c' THEN 16
WHEN 'tai100d' THEN 17
WHEN 'tai150a' THEN 18
WHEN 'tai150b' THEN 19
WHEN 'tai150c' THEN 20
WHEN 'tai150d' THEN 21
WHEN 'big_tai385' THEN 22
ELSE 23 END
go


select tab1.name, tab2.Type, tab1.minresult from (
	select name as name,
	min(minresult) as minresult from #tmpResults
	where [Type] not like '% 1/%'
	group by name
	) as tab1
join  #tmpResults as tab2 on tab1.name = tab2.name and tab1.minresult = tab2.MinResult


--elements of the algorithm switched off
select
replace(fourth_column.name,'D.vrp','') + ' & ' +
--(case when first_column.minresult <= third_column.minresult and  first_column.minresult <= fourth_column.minresult and  first_column.minresult <= second_column.minresult and first_column.minresult <= fifth_column.minresult and  first_column.minresult <= sixth_column.minresult 
--then '\textbf{' else '' end) + 
--cast (cast (first_column.minresult as numeric (8,2)) AS varchar) +
--(case when first_column.minresult <= third_column.minresult and  first_column.minresult <= fourth_column.minresult and first_column.minresult <= second_column.minresult  and first_column.minresult <= fifth_column.minresult and  first_column.minresult <= sixth_column.minresult 
--then '}' else '' end) + 
--' & ' +
(case when first_column.avgresult <= third_column.avgresult and  first_column.avgresult <= fourth_column.avgresult and first_column.avgresult <= second_column.avgresult  and first_column.avgresult <= fifth_column.avgresult and  first_column.avgresult <= sixth_column.avgresult 
then '\textbf{' else '' end) + 
cast (cast (first_column.avgresult as numeric (8,2)) AS varchar)+
(case when first_column.avgresult <= third_column.avgresult and  first_column.avgresult <= fourth_column.avgresult and first_column.avgresult <= second_column.avgresult  and first_column.avgresult <= fifth_column.avgresult and  first_column.avgresult <= sixth_column.avgresult 
then '}' else '' end) + 
' & ' +
--(case when second_column.minresult <= third_column.minresult and  second_column.minresult <= fourth_column.minresult and first_column.minresult >= second_column.minresult and  second_column.minresult <= fifth_column.minresult and  second_column.minresult <= sixth_column.minresult
--then '\textbf{' else '' end) + 
--cast (cast (second_column.minresult as numeric (8,2)) AS varchar) +
--(case when second_column.minresult <= third_column.minresult and  second_column.minresult <= fourth_column.minresult and first_column.minresult >= second_column.minresult and  second_column.minresult <= fifth_column.minresult and  second_column.minresult <= sixth_column.minresult
--then '}' else '' end) + 
--' & ' +
(case when second_column.avgresult <= third_column.avgresult and  second_column.avgresult <= fourth_column.avgresult and first_column.avgresult >= second_column.avgresult and  second_column.avgresult <= fifth_column.avgresult and  second_column.avgresult <= sixth_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (second_column.avgresult as numeric (8,2)) AS varchar)+
(case when second_column.avgresult <= third_column.avgresult and  second_column.avgresult <= fourth_column.avgresult and first_column.avgresult >= second_column.avgresult and  second_column.avgresult <= fifth_column.avgresult and  second_column.avgresult <= sixth_column.avgresult
then '}' else '' end) + 
' & ' +
--(case when second_column.minresult >= third_column.minresult and  third_column.minresult <= fourth_column.minresult and first_column.minresult >= third_column.minresult and  third_column.minresult <= fifth_column.minresult and  third_column.minresult <= sixth_column.minresult
--then '\textbf{' else '' end) + 
--cast (cast (third_column.minresult as numeric (8,2)) AS varchar)+
--(case when second_column.minresult >= third_column.minresult and  third_column.minresult <= fourth_column.minresult and first_column.minresult >= third_column.minresult and  third_column.minresult <= fifth_column.minresult and  third_column.minresult <= sixth_column.minresult
--then '}' else '' end) + 
--' & ' +
(case when second_column.avgresult >= third_column.avgresult and  third_column.avgresult <= fourth_column.avgresult and first_column.avgresult >= third_column.avgresult and  third_column.avgresult <= fifth_column.avgresult and  third_column.avgresult <= sixth_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (third_column.avgresult as numeric (8,2)) AS varchar)+
(case when second_column.avgresult >= third_column.avgresult and  third_column.avgresult <= fourth_column.avgresult and first_column.avgresult >= third_column.avgresult and  third_column.avgresult <= fifth_column.avgresult and  third_column.avgresult <= sixth_column.avgresult
then '}' else '' end) + 
' & ' +
--(case when fourth_column.minresult <= third_column.minresult and  second_column.minresult >= fourth_column.minresult and first_column.minresult >= fourth_column.minresult and  fourth_column.minresult <= fifth_column.minresult and  fourth_column.minresult <= sixth_column.minresult
--then '\textbf{' else '' end) + 
--cast (cast (fourth_column.minresult as numeric (8,2)) AS varchar)+
--(case when fourth_column.minresult <= third_column.minresult and  second_column.minresult >= fourth_column.minresult and first_column.minresult >= fourth_column.minresult and  fourth_column.minresult <= fifth_column.minresult and  fourth_column.minresult <= sixth_column.minresult
--then '}' else '' end) + 
--' & ' +
(case when fourth_column.avgresult <= third_column.avgresult and  second_column.avgresult >= fourth_column.avgresult and first_column.avgresult >= fourth_column.avgresult and  fourth_column.avgresult <= fifth_column.avgresult and  fourth_column.avgresult <= sixth_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (fourth_column.avgresult as numeric (8,2)) AS varchar)+
(case when fourth_column.avgresult <= third_column.avgresult and  second_column.avgresult >= fourth_column.avgresult and first_column.avgresult >= fourth_column.avgresult and  fourth_column.avgresult <= fifth_column.avgresult and  fourth_column.avgresult <= sixth_column.avgresult
then '}' else '' end) + 
' & ' +
--(case when fifth_column.minresult <= third_column.minresult and  second_column.minresult >= fifth_column.minresult and first_column.minresult >= fifth_column.minresult and  fourth_column.minresult >= fifth_column.minresult and  fifth_column.minresult <= sixth_column.minresult
--then '\textbf{' else '' end) + 
--cast (cast (fifth_column.minresult as numeric (8,2)) AS varchar)+
--(case when fifth_column.minresult <= third_column.minresult and  second_column.minresult >= fifth_column.minresult and first_column.minresult >= fifth_column.minresult and  fourth_column.minresult >= fifth_column.minresult and  fifth_column.minresult <= sixth_column.minresult
--then '}' else '' end) + 
--' & ' +
(case when fifth_column.avgresult <= third_column.avgresult and  second_column.avgresult >= fifth_column.avgresult and first_column.avgresult >= fifth_column.avgresult and  fourth_column.avgresult >= fifth_column.avgresult and  fifth_column.avgresult <= sixth_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (fifth_column.avgresult as numeric (8,2)) AS varchar)+
(case when fifth_column.avgresult <= third_column.avgresult and  second_column.avgresult >= fifth_column.avgresult and first_column.avgresult >= fifth_column.avgresult and  fourth_column.avgresult >= fifth_column.avgresult and  fifth_column.avgresult <= sixth_column.avgresult
then '}' else '' end) + 
' & ' +
--(case when sixth_column.minresult <= third_column.minresult and  second_column.minresult >= sixth_column.minresult and first_column.minresult >= sixth_column.minresult and  sixth_column.minresult <= fifth_column.minresult and  fourth_column.minresult >= sixth_column.minresult
--then '\textbf{' else '' end) + 
--cast (cast (sixth_column.minresult as numeric (8,2)) AS varchar)+
--(case when sixth_column.minresult <= third_column.minresult and  second_column.minresult >= sixth_column.minresult and first_column.minresult >= sixth_column.minresult and  sixth_column.minresult <= fifth_column.minresult and  fourth_column.minresult >= sixth_column.minresult
--then '}' else '' end) + 
--' & ' +
(case when sixth_column.avgresult <= third_column.avgresult and  second_column.avgresult >= sixth_column.avgresult and first_column.avgresult >= sixth_column.avgresult and  sixth_column.avgresult <= fifth_column.avgresult and  fourth_column.avgresult >= sixth_column.avgresult
then '\textbf{' else '' end) + 
cast (cast (sixth_column.avgresult as numeric (8,2)) AS varchar)+
(case when sixth_column.avgresult <= third_column.avgresult and  second_column.avgresult >= sixth_column.avgresult and first_column.avgresult >= sixth_column.avgresult and  sixth_column.avgresult <= fifth_column.avgresult and  fourth_column.avgresult >= sixth_column.avgresult
then '}' else '' end) + 
' \\ \cline{1-7} '
from 
(
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = 'PSO NoHistory 20v3x3+4 8/022/140/0(0.025)'
) as fifth_column
join (
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = 'PSO 16v3x3+4 8/022/140/0(0.025)'
) as fourth_column on fifth_column.name = fourth_column.name
join
(
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = 'Tree 4 8(0.025)'
) as sixth_column on fifth_column.name = sixth_column.name
join (
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = 'PSO 20v3x3+4 8/020/127/0(0.025)'
) as third_column on fifth_column.name = third_column.name
join (
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = 'PSO 20v3x3+4 8/020/127/27(0.025)'
) as second_column on fifth_column.name = second_column.name
join (
select 
	CASE WHEN name = 'avarage' THEN 'sum' ELSE REPLACE([Name],'D.vrp','') END as name,
	minresult,
	avgresult,
	[Type]
from #tmpResults
where [Type] = '2MPSO_RSv3x3+4 2OPT 8/22/140(0.025)'
) as first_column on fifth_column.name = first_column.name

order by CASE fifth_column.name
WHEN 'c50' THEN 1
WHEN 'c75' THEN 2
WHEN 'c100b' THEN 4
WHEN 'c100' THEN 3
WHEN 'c120' THEN 5
WHEN 'c150' THEN 6
WHEN 'c199' THEN 7
WHEN 'f71' THEN 8
WHEN 'f134' THEN 9
WHEN 'tai75a' THEN 10
WHEN 'tai75b' THEN 11
WHEN 'tai75c' THEN 12
WHEN 'tai75d' THEN 13
WHEN 'tai100a' THEN 14
WHEN 'tai100b' THEN 15
WHEN 'tai100c' THEN 16
WHEN 'tai100d' THEN 17
WHEN 'tai150a' THEN 18
WHEN 'tai150b' THEN 19
WHEN 'tai150c' THEN 20
WHEN 'tai150d' THEN 21
WHEN 'big_tai385' THEN 22
ELSE 23 END
go


select * from #tmpResults