SELECT avg(best)
FROM(
SELECT name, min(avg_result) as best
FROM
(
SELECT results.name, type, avg(result/min_result) as avg_result FROM 
[PSO_DVRP_LIMITED_AREAS_PHASE] as results

join 
(
SELECT name, min(min_result) as min_result
FROM
(
SELECT name, min(result) as min_result FROM 
[PSO_DVRP_LIMITED_AREAS_PHASE]
WHERE type like '2MPSO%8%'
and name not like 'okul%' 
group by name
UNION select name, min([MAPSOMinResult]) as min_result
FROM [ExternalResults]
group by name
) interexter
group by name
) as min_results
on min_results.name = results.name

WHERE type in ('2MPSOv1x1+4 2OPT 8/040/100/050(0.02)'
,'2MPSOv2x1+4 2OPT 8/040/100/025(0.02)'
,'2MPSOv2x3+4 2OPT 8/040/100/025(0.02)')
and results.name not like 'okul%' 
group by results.name, type
UNION select min_results.name, Algorithm as type, [MAPSOAvgResult]/min_result as avg_result
FROM [ExternalResults]
join 
(
SELECT name, min(min_result) as min_result
FROM
(
SELECT name, min(result) as min_result FROM 
[PSO_DVRP_LIMITED_AREAS_PHASE]
WHERE type like '2MPSO%8%'
and name not like 'okul%' 
group by name
UNION select name, min([MAPSOMinResult]) as min_result
FROM [ExternalResults]
group by name
) interexter
group by name
) as min_results
on min_results.name = ExternalResults.name

) normresults
group by name
) final