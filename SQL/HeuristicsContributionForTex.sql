select
replace(mpsov2large.name,'','') + ' & ' +
(case when mpsov2large.minresult < mpsov3.minresult and  mpsov2large.minresult < mpsov3large.minresult and mpsov2large.minresult < mpsov2.minresult 
then '\textbf{' else '' end) + 
cast (cast (mpsov2large.minresult as numeric (8,2)) AS varchar) +
(case when mpsov2large.minresult < mpsov3.minresult and  mpsov2large.minresult < mpsov3large.minresult and mpsov2large.minresult < mpsov2.minresult 
then '}' else '' end) + 
' & ' +
(case when mpsov2large.avgresult < mpsov3.avgresult and mpsov2large.avgresult < mpsov3large.avgresult and mpsov2large.avgresult < mpsov2.avgresult 
then '\textbf{' else '' end) + 
cast (cast (mpsov2large.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov2large.avgresult < mpsov3.avgresult and mpsov2large.avgresult < mpsov3large.avgresult and mpsov2large.avgresult < mpsov2.avgresult 
then '}' else '' end) + 
' & ' +
(case when mpsov2.minresult < mpsov3.minresult and  mpsov2.minresult < mpsov3large.minresult and mpsov2large.minresult > mpsov2.minresult
then '\textbf{' else '' end) + 
cast (cast (mpsov2.minresult as numeric (8,2)) AS varchar) +
(case when mpsov2.minresult < mpsov3.minresult and  mpsov2.minresult < mpsov3large.minresult and mpsov2large.minresult > mpsov2.minresult
then '}' else '' end) + 
' & ' +
(case when mpsov2.avgresult < mpsov3.avgresult and  mpsov2.avgresult < mpsov3large.avgresult and mpsov2large.avgresult > mpsov2.avgresult
then '\textbf{' else '' end) + 
cast (cast (mpsov2.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov2.avgresult < mpsov3.avgresult and  mpsov2.avgresult < mpsov3large.avgresult and mpsov2large.avgresult > mpsov2.avgresult
then '}' else '' end) + 
' & ' +
(case when mpsov3large.minresult < mpsov3.minresult and  mpsov2.minresult > mpsov3large.minresult and mpsov2large.minresult > mpsov3large.minresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3large.minresult as numeric (8,2)) AS varchar)+
(case when mpsov3large.minresult < mpsov3.minresult and  mpsov2.minresult > mpsov3large.minresult and mpsov2large.minresult > mpsov3large.minresult
then '}' else '' end) + 
' & ' +
(case when mpsov3large.avgresult < mpsov3.avgresult and  mpsov2.avgresult > mpsov3large.avgresult and mpsov2large.avgresult > mpsov3large.avgresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3large.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov3large.avgresult < mpsov3.avgresult and  mpsov2.avgresult > mpsov3large.avgresult and mpsov2large.avgresult > mpsov3large.avgresult
then '}' else '' end) + 
' & ' +
(case when mpsov2.minresult > mpsov3.minresult and  mpsov3.minresult < mpsov3large.minresult and mpsov2large.minresult > mpsov3.minresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3.minresult as numeric (8,2)) AS varchar)+
(case when mpsov2.minresult > mpsov3.minresult and  mpsov3.minresult < mpsov3large.minresult and mpsov2large.minresult > mpsov3.minresult
then '}' else '' end) + 
' & ' +
(case when mpsov2.avgresult > mpsov3.avgresult and  mpsov3.avgresult < mpsov3large.avgresult and mpsov2large.avgresult > mpsov3.avgresult
then '\textbf{' else '' end) + 
cast (cast (mpsov3.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov2.avgresult > mpsov3.avgresult and  mpsov3.avgresult < mpsov3large.avgresult and mpsov2large.avgresult > mpsov3.avgresult
then '}' else '' end) + 
' \\ \hline '
from 
(
--wyniki mapso
SELECT  
	'sum' as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
) as mapso
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
) as ga on mapso.name = ga.name
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'Tabu'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'Tabu'
) as tabu on mapso.name = tabu.name
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].[MPSO3_8SWARM_50TS_2500FFE]
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].[MPSO3_8SWARM_50TS_2500FFE]) as tab
) as mpsov2large on mpsov2large.name = replace(mapso.name,'D.vrp','')
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].MPSO3_8SWARM_50TS_2000FFE_No2ndPhase
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].MPSO3_8SWARM_50TS_2000FFE_No2ndPhase) as tab
) as mpsov2 on mpsov2.name = replace(mapso.name,'D.vrp','')
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].MPSO3_8SWARM_50TS_2500FFE_NoTree
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].MPSO3_8SWARM_50TS_2500FFE_NoTree) as tab
) as mpsov3large on mpsov3large.name = replace(mapso.name,'D.vrp','')
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].TREE2OPT_8TIMES_50TS
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].TREE2OPT_8TIMES_50TS) as tab
) as mpsov3 on mpsov3.name = replace(mapso.name,'D.vrp','')
order by CASE mapso.name
WHEN 'c50D.vrp' THEN 1
WHEN 'c75D.vrp' THEN 2
WHEN 'c100bD.vrp' THEN 4
WHEN 'c100D.vrp' THEN 3
WHEN 'c120D.vrp' THEN 5
WHEN 'c150D.vrp' THEN 6
WHEN 'c199D.vrp' THEN 7
WHEN 'f71D.vrp' THEN 8
WHEN 'f134D.vrp' THEN 9
WHEN 'tai75aD.vrp' THEN 10
WHEN 'tai75bD.vrp' THEN 11
WHEN 'tai75cD.vrp' THEN 12
WHEN 'tai75dD.vrp' THEN 13
WHEN 'tai100aD.vrp' THEN 14
WHEN 'tai100bD.vrp' THEN 15
WHEN 'tai100cD.vrp' THEN 16
WHEN 'tai100dD.vrp' THEN 17
WHEN 'tai150aD.vrp' THEN 18
WHEN 'tai150bD.vrp' THEN 19
WHEN 'tai150cD.vrp' THEN 20
WHEN 'tai150dD.vrp' THEN 21
ELSE 22 END
go
