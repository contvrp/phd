drop table #tmpPultusk
CREATE TABLE #tmpPultusk
(
   Type varchar(1024),
   Benchmarks INT,
   Instances INT,
   MaxTime numeric(18,2),
   AvgTime numeric(18,2),
   MinTime numeric(18,2),
   memso numeric(18,4),
   ga numeric(18,4),
   ga2014 numeric(18,4),
   ts numeric(18,4),
   acolns numeric(18,4),
   dapso numeric(18,4),
   vns numeric(18,4)
)

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = 'Tree% 4 8(0.025)', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = 'MCTree% 64 8x1+0(0.025)', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = 'MCTree% 64 8x1+0(0.02)', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = 'MCTree% 64 8x1+0(0.01)', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = '2MPSO% 20v3x3+4 8/002/002/0(0.025)', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = '2MPSO% 20v3x3+4 8/002/001/0(0.025)', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = '2MPSO% 20v3x3+4 8/001/001/0(0.025)', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = 'MCTree PSO % 80 8x1+0v3x3+4 8/002/001/0(0.02%', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = 'MCTree PSO % 80 8x1+0v3x3+4 8/002/002/0(0.02%', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = 'MCTree PSO % 80 8x1+0v3x3+4 8/002/014/0(0.02%', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = 'MCTree% 64 8x1+0(0.005)', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = 'MCTree PSO % 80 8x1+0v3x3+4 8/003/021/0(0.02%', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = 'MCTree PSO % 80 8x1+0v3x3+4 8/006/042/0(0.02%', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = 'MCTree PSO % 80 8x1+0v3x3+4 8/007/049/0(0.02%', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = '2MPSO% 20v3x3+4 8/002/014/0(0.025)', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = '2MPSO% 20v3x3+4 8/003/021/0(0.025)', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = '2MPSO% 20v3x3+4 8/004/028/0(0.025)', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = '2MPSO% 20v3x3+4 8/005/035/0(0.025)', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = '2MPSO% 20v3x3+4 8/006/042/0(0.025)', @Machine = 'P52601'
go

INSERT INTO #tmpPultusk
exec spSelectTimeAllSummary @Experiment = 'MCTree PSO % 80 8x1+0v3x3+4 8/001/001/0(0.02%', @Machine = 'P52601'
go

select * from #tmpPultusk
order by AvgTime desc
