select
replace(mapso.name,'D.vrp','') + ' & ' +
(case when mpsov3.minresult < dev3c95.minresult and  mpsov3.minresult < mapso.minresult and mpsov3.minresult < memso.minresult and mpsov3.minresult < ga.minresult and mpsov3.minresult < tabu.minresult and mpsov3.minresult < mpsov5.minresult 
then '\textbf{' else '' end) + 
cast (cast (mpsov3.minresult as numeric (8,2)) AS varchar) +
(case when mpsov3.minresult < dev3c95.minresult and  mpsov3.minresult < mapso.minresult and mpsov3.minresult < memso.minresult and mpsov3.minresult < ga.minresult and mpsov3.minresult < tabu.minresult and mpsov3.minresult < mpsov5.minresult 
then '}' else '' end) + 
' & ' +
(case when mpsov3.avgresult < dev3c95.avgresult and  mpsov3.avgresult < mapso.avgresult and mpsov3.avgresult < memso.avgresult and mpsov3.avgresult < ga.avgresult and mpsov3.avgresult < tabu.avgresult and mpsov3.avgresult < mpsov5.avgresult 
then '\textbf{' else '' end) + 
cast (cast (mpsov3.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov3.avgresult < dev3c95.avgresult and  mpsov3.avgresult < mapso.avgresult and mpsov3.avgresult < memso.avgresult and mpsov3.avgresult < ga.avgresult and mpsov3.avgresult < tabu.avgresult and mpsov3.avgresult < mpsov5.avgresult 
then '}' else '' end) + 
' & ' +
(case when mpsov5.minresult < dev3c95.minresult and  mpsov5.minresult < mapso.minresult and mpsov5.minresult < memso.minresult and mpsov5.minresult < ga.minresult and mpsov5.minresult < tabu.minresult and mpsov3.minresult > mpsov5.minresult
then '\textbf{' else '' end) + 
cast (cast (mpsov5.minresult as numeric (8,2)) AS varchar) +
(case when mpsov5.minresult < dev3c95.minresult and  mpsov5.minresult < mapso.minresult and mpsov5.minresult < memso.minresult and mpsov5.minresult < ga.minresult and mpsov5.minresult < tabu.minresult and mpsov3.minresult > mpsov5.minresult
then '}' else '' end) + 
' & ' +
(case when mpsov5.avgresult < dev3c95.avgresult and  mpsov5.avgresult < mapso.avgresult and mpsov5.avgresult < memso.avgresult and mpsov5.avgresult < ga.avgresult and mpsov5.avgresult < tabu.avgresult and mpsov3.avgresult > mpsov5.avgresult
then '\textbf{' else '' end) + 
cast (cast (mpsov5.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov5.avgresult < dev3c95.avgresult and  mpsov5.avgresult < mapso.avgresult and mpsov5.avgresult < memso.avgresult and mpsov5.avgresult < ga.avgresult and mpsov5.avgresult < tabu.avgresult and mpsov3.avgresult > mpsov5.avgresult
then '}' else '' end) + 
' & ' +
(case when mpsov5.minresult > dev3c95.minresult and  dev3c95.minresult < mapso.minresult and dev3c95.minresult < memso.minresult and dev3c95.minresult < ga.minresult and dev3c95.minresult < tabu.minresult and mpsov3.minresult > dev3c95.minresult
then '\textbf{' else '' end) + 
cast (cast (dev3c95.minresult as numeric (8,2)) AS varchar)+
(case when mpsov5.minresult > dev3c95.minresult and  dev3c95.minresult < mapso.minresult and dev3c95.minresult < memso.minresult and dev3c95.minresult < ga.minresult and dev3c95.minresult < tabu.minresult and mpsov3.minresult > dev3c95.minresult
then '}' else '' end) + 
' & ' +
(case when mpsov5.avgresult > dev3c95.avgresult and  dev3c95.avgresult < mapso.avgresult and dev3c95.avgresult < memso.avgresult and dev3c95.avgresult < ga.avgresult and dev3c95.avgresult < tabu.avgresult and mpsov3.avgresult > dev3c95.avgresult
then '\textbf{' else '' end) + 
cast (cast (dev3c95.avgresult as numeric (8,2)) AS varchar)+
(case when mpsov5.avgresult > dev3c95.avgresult and  dev3c95.avgresult < mapso.avgresult and dev3c95.avgresult < memso.avgresult and dev3c95.avgresult < ga.avgresult and dev3c95.avgresult < tabu.avgresult and mpsov3.avgresult > dev3c95.avgresult
then '}' else '' end) + 
' & ' +
(case when mapso.minresult < dev3c95.minresult and  mpsov5.minresult > mapso.minresult and mapso.minresult < memso.minresult and mapso.minresult < ga.minresult and mapso.minresult < tabu.minresult and mpsov3.minresult > mapso.minresult
then '\textbf{' else '' end) + 
cast (cast (mapso.minresult as numeric (8,2)) AS varchar)+
(case when mapso.minresult < dev3c95.minresult and  mpsov5.minresult > mapso.minresult and mapso.minresult < memso.minresult and mapso.minresult < ga.minresult and mapso.minresult < tabu.minresult and mpsov3.minresult > mapso.minresult
then '}' else '' end) + 
' & ' +
(case when mapso.avgresult < dev3c95.avgresult and  mpsov5.avgresult > mapso.avgresult and mapso.avgresult < memso.avgresult and mapso.avgresult < ga.avgresult and mapso.avgresult < tabu.avgresult and mpsov3.avgresult > mapso.avgresult
then '\textbf{' else '' end) + 
cast (cast (mapso.avgresult as numeric (8,2)) AS varchar)+
(case when mapso.avgresult < dev3c95.avgresult and  mpsov5.avgresult > mapso.avgresult and mapso.avgresult < memso.avgresult and mapso.avgresult < ga.avgresult and mapso.avgresult < tabu.avgresult and mpsov3.avgresult > mapso.avgresult
then '}' else '' end) + 
' & ' +
(case when memso.minresult < dev3c95.minresult and  memso.minresult < mapso.minresult and mpsov5.minresult > memso.minresult and memso.minresult < ga.minresult and memso.minresult < tabu.minresult and mpsov3.minresult > memso.minresult
then '\textbf{' else '' end) + 
cast (cast (memso.minresult as numeric (8,2)) AS varchar)+
(case when memso.minresult < dev3c95.minresult and  memso.minresult < mapso.minresult and mpsov5.minresult > memso.minresult and memso.minresult < ga.minresult and memso.minresult < tabu.minresult and mpsov3.minresult > memso.minresult
then '}' else '' end) + 
' & ' +
(case when memso.avgresult < dev3c95.avgresult and  memso.avgresult < mapso.avgresult and mpsov5.avgresult > memso.avgresult and memso.avgresult < ga.avgresult and memso.avgresult < tabu.avgresult and mpsov3.avgresult > memso.avgresult
then '\textbf{' else '' end) + 
cast (cast (memso.avgresult as numeric (8,2)) AS varchar)+
(case when memso.avgresult < dev3c95.avgresult and  memso.avgresult < mapso.avgresult and mpsov5.avgresult > memso.avgresult and memso.avgresult < ga.avgresult and memso.avgresult < tabu.avgresult and mpsov3.avgresult > memso.avgresult
then '}' else '' end) + 
' & ' +
(case when ga.minresult < dev3c95.minresult and  ga.minresult < mapso.minresult and mpsov5.minresult > ga.minresult and memso.minresult > ga.minresult and ga.minresult < tabu.minresult and mpsov3.minresult > ga.minresult
then '\textbf{' else '' end) + 
cast (cast (ga.minresult as numeric (8,2)) AS varchar)+
(case when ga.minresult < dev3c95.minresult and  ga.minresult < mapso.minresult and mpsov5.minresult > ga.minresult and memso.minresult > ga.minresult and ga.minresult < tabu.minresult and mpsov3.minresult > ga.minresult
then '}' else '' end) + 
' & ' +
(case when ga.avgresult < dev3c95.avgresult and  ga.avgresult < mapso.avgresult and mpsov5.avgresult > ga.avgresult and memso.avgresult > ga.avgresult and ga.avgresult < tabu.avgresult and mpsov3.avgresult > ga.avgresult
then '\textbf{' else '' end) + 
cast (cast (ga.avgresult as numeric (8,2)) AS varchar)+
(case when ga.avgresult < dev3c95.avgresult and  ga.avgresult < mapso.avgresult and mpsov5.avgresult > ga.avgresult and memso.avgresult > ga.avgresult and ga.avgresult < tabu.avgresult and mpsov3.avgresult > ga.avgresult
then '}' else '' end) + 
' & ' +
(case when tabu.minresult < dev3c95.minresult and  tabu.minresult < mapso.minresult and mpsov5.minresult > tabu.minresult and memso.minresult > tabu.minresult and ga.minresult > tabu.minresult and mpsov3.minresult > tabu.minresult
then '\textbf{' else '' end) + 
cast (cast (tabu.minresult as numeric (8,2)) AS varchar)+
(case when tabu.minresult < dev3c95.minresult and  tabu.minresult < mapso.minresult and mpsov5.minresult > tabu.minresult and memso.minresult > tabu.minresult and ga.minresult > tabu.minresult and mpsov3.minresult > tabu.minresult
then '}' else '' end) + 
' & ' +
(case when tabu.avgresult < dev3c95.avgresult and  tabu.avgresult < mapso.avgresult and mpsov5.avgresult > tabu.avgresult and memso.avgresult > tabu.avgresult and ga.avgresult > tabu.avgresult and mpsov3.avgresult > tabu.avgresult
then '\textbf{' else '' end) + 
cast (cast (tabu.avgresult as numeric (8,2)) AS varchar)+
(case when tabu.avgresult < dev3c95.avgresult and  tabu.avgresult < mapso.avgresult and mpsov5.avgresult > tabu.avgresult and memso.avgresult > tabu.avgresult and ga.avgresult > tabu.avgresult and mpsov3.avgresult > tabu.avgresult
then '}' else '' end) + 
' \\ \hline '
from 
(
--wyniki mapso
SELECT  
	'sum' as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
) as mapso
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
) as memso on mapso.name = memso.name
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
) as ga on mapso.name = ga.name
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'Tabu'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'Tabu'
) as tabu on mapso.name = tabu.name
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].[MPSO3_8SWARM_50TS_2500FFE]
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].[MPSO3_8SWARM_50TS_2500FFE]) as tab
) as mpsov3 on mpsov3.name = replace(mapso.name,'D.vrp','')
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].[MPSO5_8SWARM_50TS_2500FFE]
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].[MPSO5_8SWARM_50TS_2500FFE]) as tab
) as mpsov5 on mpsov5.name = replace(mapso.name,'D.vrp','')
join
(
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].[DEV3_C95_8SWARM_50TS_2500FFE]
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].[DEV3_C95_8SWARM_50TS_2500FFE]) as tab
) as dev3c95 on dev3c95.name = replace(mapso.name,'D.vrp','')
order by CASE mapso.name
WHEN 'c50D.vrp' THEN 1
WHEN 'c75D.vrp' THEN 2
WHEN 'c100bD.vrp' THEN 4
WHEN 'c100D.vrp' THEN 3
WHEN 'c120D.vrp' THEN 5
WHEN 'c150D.vrp' THEN 6
WHEN 'c199D.vrp' THEN 7
WHEN 'f71D.vrp' THEN 8
WHEN 'f134D.vrp' THEN 9
WHEN 'tai75aD.vrp' THEN 10
WHEN 'tai75bD.vrp' THEN 11
WHEN 'tai75cD.vrp' THEN 12
WHEN 'tai75dD.vrp' THEN 13
WHEN 'tai100aD.vrp' THEN 14
WHEN 'tai100bD.vrp' THEN 15
WHEN 'tai100cD.vrp' THEN 16
WHEN 'tai100dD.vrp' THEN 17
WHEN 'tai150aD.vrp' THEN 18
WHEN 'tai150bD.vrp' THEN 19
WHEN 'tai150cD.vrp' THEN 20
WHEN 'tai150dD.vrp' THEN 21
ELSE 22 END
go
