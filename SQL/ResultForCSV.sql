use DataAnalyzer
go

select
mapso.name,
cast (cast (mpsov1.minresult as numeric (8,2)) AS varchar) as mpso1_min,
cast (cast (mpsov1.avgresult as numeric (8,2)) AS varchar) as mpso1_avg,
cast (cast (mpsov2.minresult as numeric (8,2)) AS varchar) as mpso2_min,
cast (cast (mpsov2.avgresult as numeric (8,2)) AS varchar) as mpso2_avg,
cast (cast (mpsov3.minresult as numeric (8,2)) AS varchar) as mpso3_min,
cast (cast (mpsov3.avgresult as numeric (8,2)) AS varchar) as mpso3_avg,
cast (cast (mapso.minresult as numeric (8,2)) AS varchar) as mapso_min,
cast (cast (mapso.avgresult as numeric (8,2)) AS varchar) as mapso_avg,
cast (cast (memso.minresult as numeric (8,2)) AS varchar) as memso_min,
cast (cast (memso.avgresult as numeric (8,2)) AS varchar) as memso_min,
cast (cast (ga.minresult as numeric (8,2)) AS varchar) as ga_min,
cast (cast (ga.avgresult as numeric (8,2)) AS varchar) as ga_avg,
cast (cast (tabu.minresult as numeric (8,2)) AS varchar) as tabu_min,
cast (cast (tabu.avgresult as numeric (8,2)) AS varchar) as tabu_avg
from 
(
--wyniki mapso
SELECT  
	'sum' as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
) as mapso
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
) as memso on mapso.name = memso.name
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
) as ga on mapso.name = ga.name
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'Tabu'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'Tabu'
) as tabu on mapso.name = tabu.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv1x1+4 2OPT 8/040/100/050(0.02)' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv1x1+4 2OPT 8/040/100/050(0.02)' and name not like 'okul%'
group by name) as tab
) as mpsov1 on mpsov1.name = mapso.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x3+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x3+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by name) as tab
) as mpsov3 on mpsov3.name = mapso.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by name) as tab
)as mpsov2 on mpsov2.name = mapso.name
order by CASE mapso.name
WHEN 'c50D.vrp' THEN 1
WHEN 'c75D.vrp' THEN 2
WHEN 'c100bD.vrp' THEN 4
WHEN 'c100D.vrp' THEN 3
WHEN 'c120D.vrp' THEN 5
WHEN 'c150D.vrp' THEN 6
WHEN 'c199D.vrp' THEN 7
WHEN 'f71D.vrp' THEN 8
WHEN 'f134D.vrp' THEN 9
WHEN 'tai75aD.vrp' THEN 10
WHEN 'tai75bD.vrp' THEN 11
WHEN 'tai75cD.vrp' THEN 12
WHEN 'tai75dD.vrp' THEN 13
WHEN 'tai100aD.vrp' THEN 14
WHEN 'tai100bD.vrp' THEN 15
WHEN 'tai100cD.vrp' THEN 16
WHEN 'tai100dD.vrp' THEN 17
WHEN 'tai150aD.vrp' THEN 18
WHEN 'tai150bD.vrp' THEN 19
WHEN 'tai150cD.vrp' THEN 20
WHEN 'tai150dD.vrp' THEN 21
ELSE 22 END
go


--Comparison of algorithms od plain v2x1, v2x1 without Tree, v2x1 without second phase, only tree and 2OPT
use DataAnalyzer
go

select
mpsov1.name,
cast (cast (mpsov1.minresult as numeric (8,2)) AS varchar) as mpso2_min,
cast (cast (mpsov1.avgresult as numeric (8,2)) AS varchar) as mpso2_avg,
cast (cast (mpsov2.minresult as numeric (8,2)) AS varchar) as mpso2_no2nd_min,
cast (cast (mpsov2.avgresult as numeric (8,2)) AS varchar) as mpso2_no2nd_avg,
cast (cast (mpsov3.minresult as numeric (8,2)) AS varchar) as mpso2_notree_min,
cast (cast (mpsov3.avgresult as numeric (8,2)) AS varchar) as mpso2_notree_avg,
cast (cast (mpsov4.minresult as numeric (8,2)) AS varchar) as tree_2opt_min,
cast (cast (mpsov4.avgresult as numeric (8,2)) AS varchar) as tree_2opt_avg
from 
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by name) as tab
) as mpsov1
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/040/100/000(0.02) No 2OPT' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/040/100/000(0.02) No 2OPT' and name not like 'okul%'
group by name) as tab
)as mpsov2 on mpsov2.name = mpsov1.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/040/100/025(0.02) No Tree' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/040/100/025(0.02) No Tree' and name not like 'okul%'
group by name) as tab
) as mpsov3 on mpsov3.name = mpsov1.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/001/001/001(0.02) Tree2OPT' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/001/001/001(0.02) Tree2OPT' and name not like 'okul%'
group by name) as tab
) as mpsov4 on mpsov4.name = mpsov1.name
order by CASE mpsov1.name
WHEN 'c50D.vrp' THEN 1
WHEN 'c75D.vrp' THEN 2
WHEN 'c100bD.vrp' THEN 4
WHEN 'c100D.vrp' THEN 3
WHEN 'c120D.vrp' THEN 5
WHEN 'c150D.vrp' THEN 6
WHEN 'c199D.vrp' THEN 7
WHEN 'f71D.vrp' THEN 8
WHEN 'f134D.vrp' THEN 9
WHEN 'tai75aD.vrp' THEN 10
WHEN 'tai75bD.vrp' THEN 11
WHEN 'tai75cD.vrp' THEN 12
WHEN 'tai75dD.vrp' THEN 13
WHEN 'tai100aD.vrp' THEN 14
WHEN 'tai100bD.vrp' THEN 15
WHEN 'tai100cD.vrp' THEN 16
WHEN 'tai100dD.vrp' THEN 17
WHEN 'tai150aD.vrp' THEN 18
WHEN 'tai150bD.vrp' THEN 19
WHEN 'tai150cD.vrp' THEN 20
WHEN 'tai150dD.vrp' THEN 21
ELSE 22 END
go


--Comparison of algorithms of plain v2x1, woth larger single swarm
use DataAnalyzer
go

select
mpsov1.name,
cast (cast (mpsov1.minresult as numeric (8,2)) AS varchar) as mpso2_min,
cast (cast (mpsov1.avgresult as numeric (8,2)) AS varchar) as mpso2_avg,
cast (cast (mpsov2.minresult as numeric (8,2)) AS varchar) as pso2_320_min,
cast (cast (mpsov2.avgresult as numeric (8,2)) AS varchar) as pso2_320_avg,
cast (cast (mpsov3.minresult as numeric (8,2)) AS varchar) as mpso3_min,
cast (cast (mpsov3.avgresult as numeric (8,2)) AS varchar) as mpso3_avg,
cast (cast (mpsov4.minresult as numeric (8,2)) AS varchar) as pso3_320_min,
cast (cast (mpsov4.avgresult as numeric (8,2)) AS varchar) as pso3_320_avg
from 
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'

group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 1/320/100/025(0.02)' and name not like 'okul%'
group by name) as tab
) as mpsov1
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 1/320/100/025(0.02)(40)' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 1/320/100/025(0.02)(40)' and name not like 'okul%'
group by name) as tab
)as mpsov2 on mpsov2.name = mpsov1.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x3+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x3+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by name) as tab
) as mpsov3 on mpsov3.name = mpsov1.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x3+4 2OPT 1/320/100/025(0.02)(40)' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x3+4 2OPT 1/320/100/025(0.02)(40)' and name not like 'okul%'
group by name) as tab
) as mpsov4 on mpsov4.name = mpsov1.name
order by CASE mpsov1.name
WHEN 'c50D.vrp' THEN 1
WHEN 'c75D.vrp' THEN 2
WHEN 'c100bD.vrp' THEN 4
WHEN 'c100D.vrp' THEN 3
WHEN 'c120D.vrp' THEN 5
WHEN 'c150D.vrp' THEN 6
WHEN 'c199D.vrp' THEN 7
WHEN 'f71D.vrp' THEN 8
WHEN 'f134D.vrp' THEN 9
WHEN 'tai75aD.vrp' THEN 10
WHEN 'tai75bD.vrp' THEN 11
WHEN 'tai75cD.vrp' THEN 12
WHEN 'tai75dD.vrp' THEN 13
WHEN 'tai100aD.vrp' THEN 14
WHEN 'tai100bD.vrp' THEN 15
WHEN 'tai100cD.vrp' THEN 16
WHEN 'tai100dD.vrp' THEN 17
WHEN 'tai150aD.vrp' THEN 18
WHEN 'tai150bD.vrp' THEN 19
WHEN 'tai150cD.vrp' THEN 20
WHEN 'tai150dD.vrp' THEN 21
ELSE 22 END
go


--for larger number of iterations
select
mapso.name,
cast (cast (mpsov1.minresult as numeric (8,2)) AS varchar) as mpso1_min,
cast (cast (mpsov1.avgresult as numeric (8,2)) AS varchar) as mpso1_avg,
cast (cast (mpsov2.minresult as numeric (8,2)) AS varchar) as mpso2_min,
cast (cast (mpsov2.avgresult as numeric (8,2)) AS varchar) as mpso2_avg,
cast (cast (mpsov3.minresult as numeric (8,2)) AS varchar) as mpso3_min,
cast (cast (mpsov3.avgresult as numeric (8,2)) AS varchar) as mpso3_avg,
cast (cast (mapso.minresult as numeric (8,2)) AS varchar) as mapso_min,
cast (cast (mapso.avgresult as numeric (8,2)) AS varchar) as mapso_avg,
cast (cast (memso.minresult as numeric (8,2)) AS varchar) as memso_min,
cast (cast (memso.avgresult as numeric (8,2)) AS varchar) as memso_min,
cast (cast (ga.minresult as numeric (8,2)) AS varchar) as ga_min,
cast (cast (ga.avgresult as numeric (8,2)) AS varchar) as ga_avg,
cast (cast (tabu.minresult as numeric (8,2)) AS varchar) as tabu_min,
cast (cast (tabu.avgresult as numeric (8,2)) AS varchar) as tabu_avg
from 
(
--wyniki mapso
SELECT  
	'sum' as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
) as mapso
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
) as memso on mapso.name = memso.name
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
) as ga on mapso.name = ga.name
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'Tabu'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'Tabu'
) as tabu on mapso.name = tabu.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv1x1+4 2OPT 8/040/250/800' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv1x1+4 2OPT 8/040/250/800' and name not like 'okul%'
group by name) as tab
) as mpsov1 on mpsov1.name = mapso.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x3+4 2OPT 8/100/1000/100' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x3+4 2OPT 8/100/1000/100' and name not like 'okul%'
group by name) as tab
) as mpsov3 on mpsov3.name = mapso.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+50 2OPT 8/100/1000/100' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+50 2OPT 8/100/1000/100' and name not like 'okul%'
group by name) as tab
)as mpsov2 on mpsov2.name = mapso.name
order by CASE mapso.name
WHEN 'c50D.vrp' THEN 1
WHEN 'c75D.vrp' THEN 2
WHEN 'c100bD.vrp' THEN 4
WHEN 'c100D.vrp' THEN 3
WHEN 'c120D.vrp' THEN 5
WHEN 'c150D.vrp' THEN 6
WHEN 'c199D.vrp' THEN 7
WHEN 'f71D.vrp' THEN 8
WHEN 'f134D.vrp' THEN 9
WHEN 'tai75aD.vrp' THEN 10
WHEN 'tai75bD.vrp' THEN 11
WHEN 'tai75cD.vrp' THEN 12
WHEN 'tai75dD.vrp' THEN 13
WHEN 'tai100aD.vrp' THEN 14
WHEN 'tai100bD.vrp' THEN 15
WHEN 'tai100cD.vrp' THEN 16
WHEN 'tai100dD.vrp' THEN 17
WHEN 'tai150aD.vrp' THEN 18
WHEN 'tai150bD.vrp' THEN 19
WHEN 'tai150cD.vrp' THEN 20
WHEN 'tai150dD.vrp' THEN 21
ELSE 22 END
go

select
mapso.name,
(case when
	mpsov1.avgresult + mpsov1.sdresult < mapso.avgresult
	and mpsov1.avgresult + mpsov1.sdresult < memso.avgresult
	and mpsov1.avgresult + mpsov1.sdresult < ga.avgresult
	and mpsov1.avgresult + mpsov1.sdresult < tabu.avgresult
then '+'
when mpsov1.avgresult - mpsov1.sdresult > mapso.avgresult
	or mpsov1.avgresult - mpsov1.sdresult > memso.avgresult
	or mpsov1.avgresult - mpsov1.sdresult > ga.avgresult
	or mpsov1.avgresult - mpsov1.sdresult > tabu.avgresult
then '-' else '.' end) as mpsov1,

(case when
	mpsov2.avgresult + mpsov2.sdresult < mapso.avgresult
	and mpsov2.avgresult + mpsov2.sdresult < memso.avgresult
	and mpsov2.avgresult + mpsov2.sdresult < ga.avgresult
	and mpsov2.avgresult + mpsov2.sdresult < tabu.avgresult
then '+'
when mpsov2.avgresult - mpsov2.sdresult > mapso.avgresult
	or mpsov2.avgresult - mpsov2.sdresult > memso.avgresult
	or mpsov2.avgresult - mpsov2.sdresult > ga.avgresult
	or mpsov2.avgresult - mpsov2.sdresult > tabu.avgresult
then '-' else '.' end) as mpsov2,

(case when
	mpsov3.avgresult + mpsov3.sdresult < mapso.avgresult
	and mpsov3.avgresult + mpsov3.sdresult < memso.avgresult
	and mpsov3.avgresult + mpsov3.sdresult < ga.avgresult
	and mpsov3.avgresult + mpsov3.sdresult < tabu.avgresult
then '+'
when mpsov3.avgresult - mpsov3.sdresult > mapso.avgresult
	or mpsov3.avgresult - mpsov3.sdresult > memso.avgresult
	or mpsov3.avgresult - mpsov3.sdresult > ga.avgresult
	or mpsov3.avgresult - mpsov3.sdresult > tabu.avgresult
then '-' else '.' end) as mpsov3
from 
(
--wyniki mapso
SELECT  
	'sum' as name,
	sum([MAPSOMinResult]) as minresult,
	sum([MAPSOAvgResult]) - sum([MAPSOMinResult]) as sdresult,
    sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult] - [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
) as mapso
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
	sum([MAPSOAvgResult]) - sum([MAPSOMinResult]) as sdresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult] - [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
) as memso on mapso.name = memso.name
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
	sum([MAPSOAvgResult]) - sum([MAPSOMinResult]) as sdresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult] - [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
) as ga on mapso.name = ga.name
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
	sum([MAPSOAvgResult]) - sum([MAPSOMinResult]) as sdresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'Tabu'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult] - [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'Tabu'
) as tabu on mapso.name = tabu.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	STDEV(result) as sdresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv1x1+4 2OPT 8/040/100/050(0.02)' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as minresult, SUM(sd_result) as sdresult, SUM(avg_result) as avgresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	STDEV(result) as sd_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv1x1+4 2OPT 8/040/100/050(0.02)' and name not like 'okul%'
group by name) as tab
) as mpsov1 on mpsov1.name = mapso.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	STDEV(result) as sdresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x3+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as minresult, SUM(sd_result) as sdresult, SUM(avg_result) as avgresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	STDEV(result) as sd_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x3+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by name) as tab
) as mpsov3 on mpsov3.name = mapso.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	STDEV(result) as sdresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as minresult, SUM(sd_result) as sdresult, SUM(avg_result) as avgresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	STDEV(result) as sd_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by name) as tab
)as mpsov2 on mpsov2.name = mapso.name
order by CASE mapso.name
WHEN 'c50D.vrp' THEN 1
WHEN 'c75D.vrp' THEN 2
WHEN 'c100bD.vrp' THEN 4
WHEN 'c100D.vrp' THEN 3
WHEN 'c120D.vrp' THEN 5
WHEN 'c150D.vrp' THEN 6
WHEN 'c199D.vrp' THEN 7
WHEN 'f71D.vrp' THEN 8
WHEN 'f134D.vrp' THEN 9
WHEN 'tai75aD.vrp' THEN 10
WHEN 'tai75bD.vrp' THEN 11
WHEN 'tai75cD.vrp' THEN 12
WHEN 'tai75dD.vrp' THEN 13
WHEN 'tai100aD.vrp' THEN 14
WHEN 'tai100bD.vrp' THEN 15
WHEN 'tai100cD.vrp' THEN 16
WHEN 'tai100dD.vrp' THEN 17
WHEN 'tai150aD.vrp' THEN 18
WHEN 'tai150bD.vrp' THEN 19
WHEN 'tai150cD.vrp' THEN 20
WHEN 'tai150dD.vrp' THEN 21
ELSE 22 END
go


select
mapso.name,
(case when
	mpsov3.avgresult + mpsov3.sdresult < mapso.avgresult
then '+'
when mpsov3.avgresult - mpsov3.sdresult > mapso.avgresult
then '-' else '.' end) as mpsov3_vs_mapso,

(case when
	mpsov3.avgresult + mpsov3.sdresult < memso.avgresult
then '+'
when mpsov3.avgresult - mpsov3.sdresult > memso.avgresult
then '-' else '.' end) as mpsov3_vs_memso,

(case when
	mpsov3.avgresult + mpsov3.sdresult < ga.avgresult
then '+'
when mpsov3.avgresult - mpsov3.sdresult > ga.avgresult
then '-' else '.' end) as mpsov3_vs_ga,

(case when
	mpsov3.avgresult + mpsov3.sdresult < tabu.avgresult
then '+'
when mpsov3.avgresult - mpsov3.sdresult > tabu.avgresult
then '-' else '.' end) as mpsov3_vs_tabu
from 
(
--wyniki mapso
SELECT  
	'sum' as name,
	sum([MAPSOMinResult]) as minresult,
	sum([MAPSOAvgResult]) - sum([MAPSOMinResult]) as sdresult,
    sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult] - [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
) as mapso
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
	sum([MAPSOAvgResult]) - sum([MAPSOMinResult]) as sdresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult] - [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
) as memso on mapso.name = memso.name
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
	sum([MAPSOAvgResult]) - sum([MAPSOMinResult]) as sdresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult] - [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'GA'
) as ga on mapso.name = ga.name
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
	sum([MAPSOAvgResult]) - sum([MAPSOMinResult]) as sdresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'Tabu'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult] - [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'Tabu'
) as tabu on mapso.name = tabu.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	STDEV(result) as sdresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv1x1+4 2OPT 8/040/100/050(0.02)' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as minresult, SUM(sd_result) as sdresult, SUM(avg_result) as avgresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	STDEV(result) as sd_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv1x1+4 2OPT 8/040/100/050(0.02)' and name not like 'okul%'
group by name) as tab
) as mpsov1 on mpsov1.name = mapso.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	STDEV(result) as sdresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x3+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as minresult, SUM(sd_result) as sdresult, SUM(avg_result) as avgresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	STDEV(result) as sd_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x3+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by name) as tab
) as mpsov3 on mpsov3.name = mapso.name
join
(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name,
	MIN(result) as minresult,
	STDEV(result) as sdresult,
	AVG(result) as avgresult
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'sum' as name,SUM(min_result) as minresult, SUM(sd_result) as sdresult, SUM(avg_result) as avgresult
from
(select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	MIN(result) as min_result,
	STDEV(result) as sd_result,
	AVG(result) as avg_result
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type LIKE '2MPSOv2x1+4 2OPT 8/040/100/025(0.02)' and name not like 'okul%'
group by name) as tab
)as mpsov2 on mpsov2.name = mapso.name
order by CASE mapso.name
WHEN 'c50D.vrp' THEN 1
WHEN 'c75D.vrp' THEN 2
WHEN 'c100bD.vrp' THEN 4
WHEN 'c100D.vrp' THEN 3
WHEN 'c120D.vrp' THEN 5
WHEN 'c150D.vrp' THEN 6
WHEN 'c199D.vrp' THEN 7
WHEN 'f71D.vrp' THEN 8
WHEN 'f134D.vrp' THEN 9
WHEN 'tai75aD.vrp' THEN 10
WHEN 'tai75bD.vrp' THEN 11
WHEN 'tai75cD.vrp' THEN 12
WHEN 'tai75dD.vrp' THEN 13
WHEN 'tai100aD.vrp' THEN 14
WHEN 'tai100bD.vrp' THEN 15
WHEN 'tai100cD.vrp' THEN 16
WHEN 'tai100dD.vrp' THEN 17
WHEN 'tai150aD.vrp' THEN 18
WHEN 'tai150bD.vrp' THEN 19
WHEN 'tai150cD.vrp' THEN 20
WHEN 'tai150dD.vrp' THEN 21
ELSE 22 END
go
