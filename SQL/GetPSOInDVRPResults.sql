--^"([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2})"\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)$
--\t\1\t\2\t\3\t\4\t\5\t\6\t\7\t2MPSO 2OPT 8/100/200/400

--^([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2})\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)$
--.+\s+([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2})\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+.+
--\t\1\t\2\t\3\t\4\t\6\t\7\t\8\t2MPSOv2x1+50 2OPT 8/40/250/100

--^[^"].+$
--\n\r
--\t\r\n
/* important experiments
2MPSO 2OPT 8/40/250/800
2PSO 2OPT DAPSO 1/40/250/800 = 2PSOv2 2OPT 1/40/250/800
2MPSO 2OPT DAPSO 8/40/250/800  = 2MPSOv2 2OPT 8/40/250/800
2PSO 2OPT DAPSO 1/40/250x2/800 - nie wiem co z tym zrobic - eksperyment niepowtarzalny
2PSOv2x3 2OPT 1/40/500/1
2PSOv2x2 2OPT 1/40/500/1      
2MPSOv2x1 2OPT 8/40/250/800
2MPSOv2x2 2OPT 8/40/250/800
2MPSOv2x3 2OPT 8/40/250/800   
2PSOv2x3+4 2OPT 1/100/1000/500
Utrzymywanie wyniku = 2PSO 1/40/500/500
*/

use DataAnalyzer
go

/*
update PSO_DVRP_LIMITED_AREAS_PHASE
set [Type] = '2MPSOv2x1+50 2OPT 8/40/250/800'
where [Type] = '2MPSOv2x1x50 2OPT 8/40/250/800'
go
*/

update PSO_DVRP
set BestKnownResult = BestKnownResult * 10,
ResultToBest = ResultToBest / 10
where ResultToBest > 9.999
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = BestKnownResult * 10,
ResultToBest = ResultToBest / 10
where ResultToBest > 9.999
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 524.61,
ResultToBest = Result / 524.61
where Name = 'c50D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 835.26,
ResultToBest = Result / 835.26
where Name = 'c75D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 826.14,
ResultToBest = Result / 826.14
where Name = 'c100D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 819.56,
ResultToBest = Result / 819.56
where Name = 'c100bD.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1042.11,
ResultToBest = Result / 1042.11
where Name = 'c120D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1028.42,
ResultToBest = Result / 1028.42
where Name = 'c150D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1291.45,
ResultToBest = Result / 1291.45
where Name = 'c199D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1618.36,
ResultToBest = Result / 1618.36
where Name = 'tai75aD.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1344.64,
ResultToBest = Result / 1344.64
where Name = 'tai75bD.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1291.01,
ResultToBest = Result / 1291.01
where Name = 'tai75cD.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1365.42,
ResultToBest = Result / 1365.42
where Name = 'tai75dD.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2047.90,
ResultToBest = Result / 2047.90
where Name = 'tai100aD.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1940.61,
ResultToBest = Result / 1940.61
where Name = 'tai100bD.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1407.44,
ResultToBest = Result / 1407.44
where Name = 'tai100cD.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1581.25,
ResultToBest = Result / 1581.25
where Name = 'tai100dD.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 3055.23,
ResultToBest = Result / 3055.23
where Name = 'tai150aD.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2727.99,
ResultToBest = Result / 2727.99
where Name = 'tai150bD.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2362.79,
ResultToBest = Result / 2362.79
where Name = 'tai150cD.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2655.67,
ResultToBest = Result / 2655.67
where Name = 'tai150dD.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 11629.60,
ResultToBest = Result / 11629.60
where Name = 'f134D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 241.97,
ResultToBest = Result / 241.97
where Name = 'f71D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1581.41,
ResultToBest = Result / 1581.41
where Name = 'okul_100b_moreskewed_pseudoclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1656.00,
ResultToBest = Result / 1656.00
where Name = 'okul_100b_moreskewed_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2255.05,
ResultToBest = Result / 2255.05
where Name = 'okul_100b_moreskewed_structured_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2165.74,
ResultToBest = Result / 2165.74
where Name = 'okul_100b_moreskewed_uniform_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2087.73,
ResultToBest = Result / 2087.73
where Name = 'okul_100b_normal_pseudoclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1823.24,
ResultToBest = Result / 1823.24
where Name = 'okul_100b_normal_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2970.95,
ResultToBest = Result / 2970.95
where Name = 'okul_100b_normal_structured_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2983.82,
ResultToBest = Result / 2983.82
where Name = 'okul_100b_normal_uniform_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1878.11,
ResultToBest = Result / 1878.11
where Name = 'okul_100b_skewed_pseudoclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1801.15,
ResultToBest = Result / 1801.15
where Name = 'okul_100b_skewed_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2494.18,
ResultToBest = Result / 2494.18
where Name = 'okul_100b_skewed_structured_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2482.64,
ResultToBest = Result / 2482.64
where Name = 'okul_100b_skewed_uniform_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2290.67,
ResultToBest = Result / 2290.67
where Name = 'okul_100b_uniform_pseudoclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2207.55,
ResultToBest = Result / 2207.55
where Name = 'okul_100b_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2208.75,
ResultToBest = Result / 2208.75
where Name = 'okul_100c_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2407.61,
ResultToBest = Result / 2407.61
where Name = 'okul_100d_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2748.31,
ResultToBest = Result / 2748.31
where Name = 'okul_100e_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2299.06,
ResultToBest = Result / 2299.06
where Name = 'okul_100f_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1622.02,
ResultToBest = Result / 1622.02
where Name = 'okul_100g_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2343.225,
ResultToBest = Result / 2343.22
where Name = 'okul_100h_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2387.02,
ResultToBest = Result / 2387.02
where Name = 'okul_100i_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2451.31,
ResultToBest = Result / 2451.31
where Name = 'okul_100j_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2347.99,
ResultToBest = Result / 2347.99
where Name = 'okul_100k_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2168.20,
ResultToBest = Result / 2168.20
where Name = 'okul_100l_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2607.84,
ResultToBest = Result / 2607.84
where Name = 'okul_100m_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2013.21,
ResultToBest = Result / 2013.21
where Name = 'okul_100n_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2131.66,
ResultToBest = Result / 2131.66
where Name = 'okul_100o_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2156.44,
ResultToBest = Result / 2156.44
where Name = 'okul_100p_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2625.39,
ResultToBest = Result / 2625.39
where Name = 'okul_100q_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 2906.42,
ResultToBest = Result / 2906.42
where Name = 'okul_100b_uniform_structured_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 3017.86,
ResultToBest = Result / 3017.86
where Name = 'okul_100b_uniform_uniform_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1251.01,
ResultToBest = Result / 1251.01
where Name = 'okul_50b_moreskewed_pseudoclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1344.55,
ResultToBest = Result / 1344.55
where Name = 'okul_50b_moreskewed_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1740.47,
ResultToBest = Result / 1740.47
where Name = 'okul_50b_moreskewed_structured_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1658.73,
ResultToBest = Result / 1658.73
where Name = 'okul_50b_moreskewed_uniform_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1395.55,
ResultToBest = Result / 1395.55
where Name = 'okul_50b_normal_pseudoclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1276.26,
ResultToBest = Result / 1276.26
where Name = 'okul_50b_normal_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1816.36,
ResultToBest = Result / 1816.36
where Name = 'okul_50b_normal_structured_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1793.53,
ResultToBest = Result / 1793.53
where Name = 'okul_50b_normal_uniform_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1180.72,
ResultToBest = Result / 1180.72
where Name = 'okul_50b_skewed_pseudoclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1092.09,
ResultToBest = Result / 1092.09
where Name = 'okul_50b_skewed_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1350.22,
ResultToBest = Result / 1350.22
where Name = 'okul_50b_skewed_structured_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1420.65,
ResultToBest = Result / 1420.65
where Name = 'okul_50b_skewed_uniform_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1522.21,
ResultToBest = Result / 1522.21
where Name = 'okul_50b_uniform_pseudoclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1304.40,
ResultToBest = Result / 1304.40
where Name = 'okul_50b_uniform_reallyclustered_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1758.08,
ResultToBest = Result / 1758.08
where Name = 'okul_50b_uniform_structured_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE
set BestKnownResult = 1916.07,
ResultToBest = Result / 1916.07
where Name = 'okul_50b_uniform_uniform_D.vrp'
go

update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2208.40, ResultToBest = Result / 2208.40 where Name = 'okul_100c_moreskewed_structured_D.vrp'
update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2293.54, ResultToBest = Result / 2293.54 where Name = 'okul_100d_moreskewed_structured_D.vrp'
update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2037.06, ResultToBest = Result / 2037.06 where Name = 'okul_100e_moreskewed_structured_D.vrp'
update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2213.63, ResultToBest = Result / 2213.63 where Name = 'okul_100f_moreskewed_structured_D.vrp'
update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2067.37, ResultToBest = Result / 2067.37 where Name = 'okul_100g_moreskewed_structured_D.vrp'
update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2413.76, ResultToBest = Result / 2413.76 where Name = 'okul_100h_moreskewed_structured_D.vrp'
update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2304.19, ResultToBest = Result / 2304.19 where Name = 'okul_100i_moreskewed_structured_D.vrp'
update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2106.02, ResultToBest = Result / 2106.02 where Name = 'okul_100j_moreskewed_structured_D.vrp'
update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2272.55, ResultToBest = Result / 2272.55 where Name = 'okul_100k_moreskewed_structured_D.vrp'
update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2696.31, ResultToBest = Result / 2696.31 where Name = 'okul_100l_moreskewed_structured_D.vrp'
update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2453.74, ResultToBest = Result / 2453.74 where Name = 'okul_100m_moreskewed_structured_D.vrp'
update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2040.96, ResultToBest = Result / 2040.96 where Name = 'okul_100n_moreskewed_structured_D.vrp'
update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2099.25, ResultToBest = Result / 2099.25 where Name = 'okul_100o_moreskewed_structured_D.vrp'
update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2317.54, ResultToBest = Result / 2317.54 where Name = 'okul_100p_moreskewed_structured_D.vrp'
update PSO_DVRP_LIMITED_AREAS_PHASE  set BestKnownResult = 2203.66, ResultToBest = Result / 2203.66 where Name = 'okul_100q_moreskewed_structured_D.vrp'
go

use DataAnalyzer
go

--kto osi�gn�� minima?
SELECT res.Name, res.Result, PSO_DVRP_LIMITED_AREAS_PHASE.Type, PSO_DVRP_LIMITED_AREAS_PHASE.ResultToBest, ExternalResults.MAPSOMinResult / PSO_DVRP_LIMITED_AREAS_PHASE.BestKnownResult
, CASE WHEN PSO_DVRP_LIMITED_AREAS_PHASE.ResultToBest < ExternalResults.MAPSOMinResult / PSO_DVRP_LIMITED_AREAS_PHASE.BestKnownResult THEN '2PSO' ELSE 'MAPSO' END
FROM
(SELECT Name, MIN(Result) as Result from PSO_DVRP_LIMITED_AREAS_PHASE
where Type <> 'B��d' AND Type NOT LIKE 'Static Tree%'
 group by Name) as res
join PSO_DVRP_LIMITED_AREAS_PHASE
on res.Result = PSO_DVRP_LIMITED_AREAS_PHASE.Result
join ExternalResults
on res.Name = ExternalResults.Name
order by Name

-- 1.0870642857285714	1.1330141.16361109988

--wyniki mapso
SELECT  
	'agregate',
	sum([MAPSOMinResult]),
      sum([MAPSOAvgResult]),
      avg([MAPSORatioToBest]),
      avg([MAPSOAvgResult]/[MAPSOMinResult]*[MAPSORatioToBest])
  FROM [DataAnalyzer].[dbo].[ExternalResults]
  WHERE Algorithm = 'MEMSO(A)'
  union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult],
  [MAPSORatioToBest],
  [MAPSOAvgResult]/[MAPSOMinResult]*[MAPSORatioToBest]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
  WHERE Algorithm = 'MEMSO(A)'

-- wyniki podstawowe
select 
	PSO_DVRP.name,
	count(*) as expno,
	AVG(result) as avg_result,
	MIN(result) as min_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio
from PSO_DVRP
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from PSO_DVRP group by name) as namemin
on namemin.Name = PSO_DVRP.Name
group by PSO_DVRP.name
union
select 'avarage',SUM(expno),SUM(avg_result),SUM(min_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio)
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from PSO_DVRP
group by name) as tab
go

--c50 & 710.66 & 794.3 & 63.25 & 1.35 & 0.46 \\
use DataAnalyzer
select
	name + ' & ' +
	cast (cast (min_result as numeric (8,2)) AS varchar) + ' & ' +
	cast (cast (avg_result as numeric (8,2)) AS varchar) + ' & ' +
	cast (cast (stdev_result as numeric (8,2)) AS varchar) + ' & ' +
	cast (cast (bestratio as numeric (8,2)) AS varchar) + ' & ' +
	cast (cast (dod as numeric (8,2)) AS varchar) + ' \\'
from v_pso_dvrp_limited
go

--wyniki z blokad� zmiany obszar�w je�eli nie doszed� nowy punkt
select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	AVG(result) as avg_result,
	MIN(result) as min_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
where type = 'Utrzymywanie wyniku'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'Utrzymywanie wyniku','avarage',SUM(expno),SUM(avg_result),SUM(min_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio)
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type = 'Utrzymywanie wyniku'
group by name) as tab
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	AVG(result) as avg_result,
	MIN(result) as min_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
where type = 'Wa�ony klastering 0.1'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'Wa�ony klastering 0.1','avarage',SUM(expno),SUM(avg_result),SUM(min_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio)
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type = 'Wa�ony klastering 0.1'
group by name) as tab
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	AVG(result) as avg_result,
	MIN(result) as min_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'R�wnoleg�e instancje %'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'R�wnoleg�e','avarage',SUM(expno),SUM(avg_result),SUM(min_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE 'R�wnoleg�e instancje %'
group by name) as tab
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	MIN([ExternalResults].MAPSOMinResult) as minmapso,
	AVG([ExternalResults].MAPSOAvgResult) as avgmapso
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'R�wnoleg�e instancje %'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	AVG(result) as avg_result,
	MIN(result) as min_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'PSO 8/100%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'PSO 8','avarage',SUM(expno),SUM(avg_result),SUM(min_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE 'PSO 8/100%'
group by name) as tab
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	MIN([ExternalResults].MAPSOMinResult) as minmapso,
	AVG([ExternalResults].MAPSOAvgResult) as avgmapso
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'PSO 8/100%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	MIN([ExternalResults].MAPSOMinResult) as minmapso,
	AVG([ExternalResults].MAPSOAvgResult) as avgmapso
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'Static Tree MAPSO%'
group by type,[PSO_DVRP_LIMITED_AREAS_PHASE].name

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	MIN([ExternalResults].MAPSOMinResult) as minmapso,
	AVG([ExternalResults].MAPSOAvgResult) as avgmapso
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'Utrzymywanie wyniku'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
order by name
/*
--Analiza rozk�adu 1
select 
	min(typemin.Type),
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	AVG(result) as avg_result,
	MIN(result) as min_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join (select name, MIN(result) as min_result,AVG(result) as avg_result, type from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name,type) as typemin
on typemin.avg_result = namemin.avg_result
group by
[PSO_DVRP_LIMITED_AREAS_PHASE].name
--,[PSO_DVRP_LIMITED_AREAS_PHASE].type
order by name, avgratio;
*/

--Wypisujemy eksperymenty, gdzie by�o �rednio co najmniej 5 pr�b dla benchmarku
select * from (
select min(tab.type) as type,'avarage' as name,SUM(expno) as expno,SUM(avg_result) as avg_result,SUM(min_result) as min_result,SUM(stdev_result) as stdev_result,AVG(norm_stdev_result) as norm_stdev_result, 1 as helper, 1 as ratio,'' as cont1,AVG(bestratio) as best_ratio,sum(avg_result) / sum(min_result) * AVG(bestratio) as avg_ratio
from
(select type,name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where name NOT LIKE 'okul%' and ([Type] like '2MPSOv%x%+% 2OPT 8/%0/%' or [Type] like '2MPSOv%x%+% 2OPT 8/001/%' or [Type] like '2MPSOv%x%+% 2OPT 1/320/%')
group by name,type
) as tab
group by type
having (SUM(expno) >= 10)
) as tab2
order by avg_ratio
go

select 
    TYPE,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	AVG(result) as avg_result,
	MIN(result) as min_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) > 0.3 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
where type = '2MPSOv2x3+4 2OPT 8/040/250/100(0.01)' and namemin.name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select '2MPSOv2x3+4 2OPT 8/040/250/100(0.01)','avarage',SUM(expno),SUM(avg_result),SUM(min_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio)
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type = '2MPSOv2x3+4 2OPT 8/040/250/100(0.01)' and name not like 'okul%'
group by name) as tab
go

--Greedy DAPSO
select 
    TYPE,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	AVG(result) as avg_result,
	MIN(result) as min_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) > 0.3 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
where type = 'GDAPSO 1/40/250/2'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'GDAPSO 1/40/250/2','avarage',SUM(expno),SUM(avg_result),SUM(min_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio)
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type = 'GDAPSO 1/40/250/2'
group by name) as tab
go

--por�wnanie najlepszego MAPSO z pierwsz� wersj� 2PSO
use DataAnalyzer
go
select 
 v_pso_dvrp.name,
 v_pso_dvrp.avg_result,
 v_pso_dvrp.min_result,
 v_pso_dvrp.bestratio,
 ExternalResults.MAPSOAvgResult,
 ExternalResults.MAPSOMinResult,
 ExternalResults.MAPSORatioToBest,
 (CASE WHEN min_result < MAPSOMinResult
  THEN '2PSO' ELSE 'MAPSO' END) as algor,
 min_result / MAPSOMinResult as ratio2psomapso,
 (CASE WHEN avg_result < MAPSOAvgResult 
  THEN '2PSO' ELSE 'MAPSO' END) as algor_avg,
 avg_result / MAPSOAvgResult as ratio2psomapso_avg
from v_pso_dvrp
join ExternalResults
on ExternalResults.name = v_pso_dvrp.name
go


--por�wnanie najlepszego MAPSO z drug� wersj� 2PSO-Bounded
use DataAnalyzer
go
select 
 v_pso_dvrp_limited.name,
 v_pso_dvrp_limited.avg_result,
 v_pso_dvrp_limited.min_result,
 v_pso_dvrp_limited.bestratio,
 ExternalResults.MAPSOAvgResult,
 ExternalResults.MAPSOMinResult,
 ExternalResults.MAPSORatioToBest,
 (CASE WHEN min_result < MAPSOMinResult
  THEN '2PSO' ELSE 'MAPSO' END) as algor,
 min_result / MAPSOMinResult as ratio2psomapso,
 (CASE WHEN avg_result < MAPSOAvgResult 
  THEN '2PSO' ELSE 'MAPSO' END) as algor_avg,
 avg_result / MAPSOAvgResult as ratio2psomapso_avg
from v_pso_dvrp_limited
join ExternalResults
on ExternalResults.name = v_pso_dvrp_limited.name
go

--z 2OPTem
select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	MIN([ExternalResults].MAPSOMinResult) as minmapso,
	AVG([ExternalResults].MAPSOAvgResult) as avgmapso
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE '2MPSO%8%' OR type LIKE 'R�wnol%'
group by type,[PSO_DVRP_LIMITED_AREAS_PHASE].name
order by name, type

--najlepszy dotychczasowy algorytm
select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE '2MPSOv2x3+4 2OPT 8/040/250/100(0.01)' and namemin.name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select '2MPSOv2x3+4 2OPT 8/040/250/100(0.01)','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE '2MPSOv2x3+4 2OPT 8/040/250/100(0.01)' and name not like 'okul%'
group by name) as tab
go

--por�wnania fair dla MAPSO
select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE '2MPSOv2x3+4 2OPT 8/040/100/025(0.02)' and namemin.name not like 'okul%' and ExternalResults.Algorithm = 'MEMSO(A)'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select '2MPSOv2x3+4 2OPT 8/040/100/025(0.02)','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),AVG(minmapsoratio),AVG(avgmapsoratio)
from
(select
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE '2MPSOv2x3+4 2OPT 8/040/100/025(0.02)' and namemin.name not like 'okul%' and ExternalResults.Algorithm = 'MEMSO(A)'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name) as tab
go

--MonteCarlo w lesie rozpinaj�cym
select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'MC with Tree 2ndPrototype' and namemin.name not like 'okul%' and ExternalResults.Algorithm = 'MEMSO(A)'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'MC with Tree 2ndPrototype','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),AVG(minmapsoratio),AVG(avgmapsoratio)
from
(select
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'MC with Tree 2ndPrototype' and namemin.name not like 'okul%' and ExternalResults.Algorithm = 'MEMSO(A)'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name) as tab
go

--brak 2giej fazy
exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/040/100/000(0.02) No 2OPT'
go

--drzewo
exec spSelectSummary @Experiment = '2MPSOv2x1+4 2OPT 8/001/001/001(0.02) Tree2OPT'
go

--drzewo po naprawie naprawiania
exec spSelectSummary @Experiment = 'Tree2OPT 8/001/000/000'
go

--maxcluster po naprawie naprawiania
exec spSelectSummary @Experiment = 'MaxCluster2OPT 8/001/000/000'
go

--avgcluster po naprawie naprawiania
exec spSelectSummary @Experiment = 'AvgCluster2OPT 8/001/000/000'
go

--mincluster po naprawie naprawiania
exec spSelectSummary @Experiment = 'MinCluster2OPT 8/001/000/000'
go

--state of art po naprawie naprawiania
exec spSelectSummary @Experiment = 'SoTACluster2OPT 8/001/000/000'
go

--state of art po naprawie naprawiania i przywr�ceniu odcinania
exec spSelectSummary @Experiment = 'MinTree with CutOff x8'
go

--state of art po naprawie naprawiania i przywr�ceniu odcinania
exec spSelectSummary @Experiment = 'MinTree with CutOff Lambda x8'
go

--state of art po naprawie naprawiania i przywr�ceniu odcinania
exec spSelectSummary @Experiment = 'MC Lambda MinTreex8'
go

--state of art po naprawie naprawiania i przywr�ceniu odcinania
exec spSelectSummary @Experiment = 'MaxTree with CutOff x8'
go


--drzewo z MonteCarlo
exec spSelectSummary @Experiment = 'MCx8 with Tree 5thPrototype MaxCluster'
go

--drzewo z MonteCarlo
exec spSelectSummary @Experiment = 'MCx8 with 10 Trees 4rthPrototype'
go

--drzewo z MonteCarlo
exec spSelectSummary @Experiment = 'MCx8 with 25 Trees 4rthPrototype'
go

--drzewa z MonteCarlo i PSO po 0.5 dnia
exec spSelectSummary @Experiment = 'MC 20Trees+2MPSOv2x3+4 8/100/000/(0.02) Reloaded'
go

--drzewo z MonteCarlo i PSO po 0.5 dnia
exec spSelectSummary @Experiment = 'MC 10Trees+2MPSOv2x3+4 8/100/000/(0.02) Reloaded'
go

--por�wnania fair dla MAPSO
exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/040/100/025(0.02) Reloaded'
go

--por�wnania fair dla MAPSO
exec spSelectSummary @Experiment = '2MPSOv2x2+4 2OPT 8/040/100/025(0.02) Reloaded'
go

--por�wnania fair dla MAPSO
exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/040/100/025(0.02) Revolutions'
go

--por�wnania fair dla MAPSO
exec spSelectSummary @Experiment = '2MPSOv2x2+4 2OPT 8/040/100/025(0.02) Revolutions'
go

--drzewo z MonteCarlo i PSO po 0.5 dnia
exec spSelectSummary @Experiment = 'MC 25Trees+2MPSOv2x3+4 8/100/000/(0.02) Reloaded'
go

--drzewo z MonteCarlo i PSO po 0.5 dnia
exec spSelectSummary @Experiment = 'MCTree+2MPSOv2x3+4 8/100/000/(0.02) Reloaded'
go

--drzewo z MonteCarlo i PSO po 0.5 dnia
exec spSelectSummary @Experiment = 'MCTree+2MPSOv2x3+4 8/100/000/(0.02)'
go

--10 drzew do po�owy dnia a p�niej 2MPSO (w wersji ze star� implementacj� wielu klastr�w) i lambd�
exec spSelectSummary @Experiment = 'MC 10Trees+2MPSOv2x3+4 8/100/000/0.02Lambda'
go

--Po dodaniu UCB, z nowymi klastrami i mniejszym buforem zam�wie� w trakcie
--mo�liwe r�wnie� eksperymenty z ograniczaniem obszaru pojedynczego pojazdu
--lambda przesuni�ta do poprawy najlepszego
exec spSelectSummary @Experiment = '8x10TreesLambda 100 UCB 2MPSOv2x3 Rev 0.25 (0.02)'
go

--drzewo z MonteCarlo
exec spSelectSummary @Experiment = 'MCx8 with Tree 2ndPrototype'
go

exec spSelectSummary @Experiment = 'MC with Tree 2ndPrototype'
go

--por�wnania fair dla MAPSO
exec spSelectSummary @Experiment = '2MPSOv2x1+4 2OPT 8/040/100/025(0.02)'
go

exec spSelectSummary @Experiment = '2MPSOv2x1+4 2OPT 8/100/1000/100(0.02)'
go

exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/040/100/025(0.02)'
go

exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/040/150/000(0.02) No2ndPhase'
go

--super d�ugie obliczenia
exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/100/4000/1000(0.02)'
go

--d�ugie obliczenia
exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/100/400/100(0.02)'
go

--do prezentacji 24.03.2015

exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT L 8/040/100/025(0.02) Reloaded'
go

exec spSelectSummary @Experiment = 'MC Lambdax8 10 Trees PSO Lambda'
go

exec spSelectSummary @Experiment = 'MC Lambdax8 10 Trees Lambda'
go

exec spSelectSummary @Experiment = 'MCx8 Tree Lambda'
go

exec spSelectSummary @Experiment = 'MC Lambdax8 Tree Lambda'
go

exec spSelectSummary @Experiment = 'Tree Lambdax8'
go

exec spSelectSummary @Experiment = 'Treex8'
go

exec spSelectSummary @Experiment = 'Treex1'
go
---dot�d ze z�ym drzewem

-- lambda po po�owie dnia
exec spSelectSummary @Experiment = 'MCx8 TreeLambda2OPT'
go

--drzewo z MonteCarlo
exec spSelectSummary @Experiment = 'MCx8 with Tree 3rdPrototype'
go

--walka z Memetic
--memetic: 30 osobnik�w x 400 iteracji x 25 time slice'�w
exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 1/100/500/000(0.02) Reloaded'
go

exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 1/100/500/000(0.02) Revolutions'
go

--warto moze zrobi� jeszcze eksperyment std. z 0.9 (i old cluster)
--oraz new cluster, 0.9 i np. 50 x 8 x 25 x 100
exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/030/400/0(0.02) Revolutions0.9P'
go

--bazowy eksperyment lekko d�u�szy, �eby miec por�wnanie
exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/100/400/100(0.02)'
go

--eksperymenty (zarzucone) z buforem zam�wie�
exec spSelectSummary @Experiment = '2MPSOv2x3+0 2OPT 1/100/500/000(0.02) Reloaded 0.15'
go

exec spSelectSummary @Experiment = '2MPSOv2x3+0 2OPT 8/032/200/000(0.02) Reloaded 0.15'
go

--restart eksperyment�w pocz�tkowych z MC
exec spSelectSummary @Experiment = 'MCx10 2MPSOv2x3+4 2OPT 8/020/050/0(0.02)'
go

exec spSelectSummary @Experiment = 'MCx10+Pass 2MPSOv2x3+4 2OPT 8/020/050/0(0.02)'
go

exec spSelectSummary @Experiment = 'UCBx5+75 2MPSOv2x3+4 2OPT 8/020/050/0(0.02)'
go

exec spSelectSummary @Experiment = 'UCBx28+16 2MPSOv2x3+4 2OPT 1/160/050/0(0.02)'
go

exec spSelectSummary @Experiment = 'UCBx5+0 2MPSOv2x3+4 2OPT 8/010/024/0(0.02)'
go

exec spSelectSummary @Experiment = 'MCPSOx5+Debug 2MPSOv2x3+4 2OPT 8/020/050/0(0.02)'
go

exec spSelectSummary @Experiment = 'MCPSOx5 2MPSOv2x3+4 2OPT 8/020/050/0(0.02)'
go

exec spSelectSummary @Experiment = 'MCPSOx5(0.5) 2MPSOv2x3+4 2OPT 8/020/050/0(0.02)'
go

exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/020/050/0(0.02)'
go

exec spSelectSummary @Experiment = 'UCBx2+0 2MPSOv2x3+4 2OPT 8/005/006/0(0.02)'
go

exec spSelectSummary @Experiment = 'UCBx4+0 2MPSOv2x3+4 2OPT 8/010/012/0(0.02)'
go

exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/010/024/0(0.02)'
go

exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/010/012/0(0.02)'
go

select distinct([type]) from
[PSO_DVRP_LIMITED_AREAS_PHASE]


exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 1/100/500/00(0.02) OldGenerations'
go

exec spSelectSummary @Experiment = '2MPSOv2x2+0 2OPT 1/100/500/00(0.02) OldGener 0.25'
go

exec spSelectSummary @Experiment = '2MPSOv2x2+0 2OPT 1/100/500/00(0.02) NewGener 0.25'
go

exec spSelectSummary @Experiment = '2MPSOv2x3+0 2OPT 1/100/500/000(0.02) Reloaded 0.15'
go

exec spSelectSummary @Experiment = '2MPSOv2x3+0 2OPT 8/032/200/000(0.02) Reloaded 0.15'
go

--algorytm, kt�ry by� mo�e da rad� (wiele roj�w)
select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE '2MPSOv2x1+50 2OPT 8/%40/2%'
group by [Type],[PSO_DVRP_LIMITED_AREAS_PHASE].name
union
select '2MPSOv2x1+50 2OPT 8/%40/2%','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE '2MPSOv2x1+50 2OPT 8/%40/2%'
group by name) as tab
go

--wszystko

select
	name,
	type,
	min_result_to_best,
	result_to_best,
	norm_stdev_result
		,(CASE
		WHEN type LIKE '%v2%' THEN 2
		ELSE 1
	END) as ver
	,(CASE
		WHEN type LIKE '%x1+%' THEN 1
		WHEN type LIKE '%3+%' THEN 3
		ELSE 0
	END) as cluster		
	,(CASE
		WHEN type LIKE '%v1%' AND type LIKE '%x1+%' THEN '2MPSO_1'
		WHEN type LIKE '%v2%' AND type LIKE '%x1+%' THEN '2MPSO_2'
		WHEN type LIKE '%v2%' AND type LIKE '%x3+%' THEN '2MPSO_3'
		ELSE ''
	END) as m		
	,(CASE
		WHEN name LIKE '%_more%' THEN 'moreskewed'
		WHEN name LIKE '%_skewed%' THEN 'skewed'
		WHEN name LIKE '%_normal%' THEN 'normal'
		WHEN name LIKE '%_uniform%' THEN 'uniform'
		ELSE ''
	END) as requests		
	,(CASE
		WHEN name LIKE '%pseudo%' THEN 'pseudoclustered'
		WHEN name LIKE '%really%' THEN 'reallyclusterd'
		WHEN name LIKE '%uniform_D%' THEN 'uniform'
		WHEN name LIKE '%structured%' THEN 'structured'
		ELSE ''
	END) as spatial		

	from
	(
select 
	[PSO_DVRP_LIMITED_AREAS_PHASE].name as name
	,[Type] as type
	,count(*) as expno
	,MIN(result) as min_result
	,AVG(result) as avg_result
	,STDEV(result) as stdev_result
	,STDEV(result) / AVG(result) as norm_stdev_result
	,MIN(resulttobest) as min_result_to_best
	,AVG(resulttobest) as result_to_best
	,count(result) as experiments
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where [Name] LIKE 'okul_%0%b%' AND
	[Type] NOT LIKE '2MPSOv2x1+50 2OPT 8/040/250/100(0.01)' AND
	[Type] LIKE '%(0.01)'
group by
	[Name]
	,[Type]
	) as res1
order by
	name
	,result_to_best


select 
	'update PSO_DVRP_LIMITED_AREAS_PHASE ' +
	' set BestKnownResult = ' + CAST(MIN(result) AS varchar) + ', ' +
	'ResultToBest = Result / ' + CAST(MIN(result) AS varchar) + ' ' +
	'where Name = ''' +  [PSO_DVRP_LIMITED_AREAS_PHASE].name + ''' ' +
	'go'
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where [Name] LIKE 'okul_100%_moreskew%struct%_D.vrp'
group by
	[Name]
	
select
Type,
AVG(result_to_best),
AVG(norm_stdev_result)
from
(
select 
	[Type] as Type,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result) / AVG(result) as norm_stdev_result,
	MIN(resulttobest) as min_result_to_best,
	AVG(resulttobest) as result_to_best,
	count(result) as experiments
from [PSO_DVRP_LIMITED_AREAS_PHASE]
where ([Name] LIKE 'okul_50%'
OR [Name] LIKE 'okul_50%')
AND [Name] NOT LIKE 'okul_100b%'
AND TYPE LIKE '%(0.01)'
group by Name,[Type]
) as res
group by Type

select
Type,
AVG(result_to_best),
AVG(norm_stdev_result)
from
(
select 
CASE
	WHEN [Type] like '2MPSOv1x1%' then 'MPSOv1x1'
	WHEN [Type] like '2MPSOv2x1%' then 'MPSOv2x1'
	WHEN [Type] like '2MPSOv2x3%' then 'MPSOv2x3'
END as type,
	tab1.Name as name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result) / AVG(result) as norm_stdev_result,
	MIN(resulttobest) as min_result_to_best,
	AVG(resulttobest) as result_to_best,
	min(tab2.min_result) as total_min_result
from [PSO_DVRP_LIMITED_AREAS_PHASE] tab1
join
(
select Name as name,
	MIN(result) as min_result from
[PSO_DVRP_LIMITED_AREAS_PHASE]
group by name
) as tab2
on
tab1.name = tab2.name
where (tab1.[Name] LIKE 'okul_%_structured%'
OR tab1.[Name] LIKE 'okul_%_reallyclustered%'
OR tab1.[Name] LIKE 'okul_%_pseudoclustered%'
OR tab1.[Name] LIKE 'okul_%_uniform%')
AND TYPE LIKE '%(0.01)'
group by CASE
	WHEN [Type] like '2MPSOv1x1%' then 'MPSOv1x1'
	WHEN [Type] like '2MPSOv2x1%' then 'MPSOv2x1'
	WHEN [Type] like '2MPSOv2x3%' then 'MPSOv2x3'
END,tab1.Name
) as res
group by Type

-- very important exports (also with (0.02 at the end))
select 
CASE
	WHEN [Type] like '2MPSOv1x1%' then 'MPSOv1x1'
	WHEN [Type] like '2MPSOv2x1%' then 'MPSOv2x1'
	WHEN [Type] like '2MPSOv2x3%' then 'MPSOv2x3'
END as type,
--	count(distinct(type)) as types_no,
--	min(type) as some_type,
--	max(type) as other_type,
	tab1.Name as name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result) / AVG(result) as norm_stdev_result,
	MIN(resulttobest) as min_result_to_best,
	AVG(resulttobest) as result_to_best,
	min(tab2.min_result) as total_min_result
from [PSO_DVRP_LIMITED_AREAS_PHASE] tab1
join
(
select Name as name,
	MIN(result) as min_result from
[PSO_DVRP_LIMITED_AREAS_PHASE]
group by name
) as tab2
on
tab1.name = tab2.name
where
TYPE LIKE '2MPSOv%8/040/250/800' --'2MPSOv%8/100/1000/100' -- '2MPSOv%8/040/250/800' --'2MPSOv%8/040/100/%(0.02)'
AND
TYPE NOT LIKE '2MPSOv1x3+4 %'
AND
TYPE NOT LIKE '2MPSOv2x2%'
AND
TYPE NOT LIKE '2MPSOv2x3 %'
group by CASE
	WHEN [Type] like '2MPSOv1x1%' then 'MPSOv1x1'
	WHEN [Type] like '2MPSOv2x1%' then 'MPSOv2x1'
	WHEN [Type] like '2MPSOv2x3%' then 'MPSOv2x3'
END,tab1.Name

--modyfikacja powy�szego o redukcj� pojazd�w i rozszerzenie wieloskupiskowe
select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
left join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE '2MPSOv2x3+4 2OPT 8/40/250/800'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select '2MPSOv2x3+4 2OPT 8/40/250/800','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE '2MPSOv2x3+4 2OPT 8/40/250/800'
group by name) as tab
go

--modyfikacja powy�szego o redukcj� pojazd�w i rozszerzenie wieloskupiskowe
select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
left join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE '2MPSOv2x1+50 2OPT 8/100/1000/100'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select '2MPSOv2x1+50 2OPT 8/100/1000/100','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE '2MPSOv2x1+50 2OPT 8/100/1000/100'
group by name) as tab
go

--modyfikacja powy�szego o redukcj� pojazd�w i rozszerzenie wieloskupiskowe
select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
left join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE '2MPSOv2x3+4 2OPT 8/100/1000/100'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select '2MPSOv2x3+4 2OPT 8/100/1000/100','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE '2MPSOv2x3+4 2OPT 8/100/1000/100'
group by name) as tab
go

--algorytm, kt�ry by� mo�e da rad�
select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE '2MPSOv2x1+50 2OPT 8/40/250/100'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select '2MPSOv2x1+50 2OPT 8/40/250/100','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE '2MPSOv2x1+50 2OPT 8/40/250/100'
group by name) as tab
go

--algorytm, kt�ry by� mo�e da rad� - najpierw DAPSO cluster a potem DAPSO discrete
select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE '2PSO 2OPT DAPSO 1/40/250x2/800'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select '2PSO 2OPT DAPSO 1/40/250x2/800','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE '2PSO 2OPT DAPSO 1/40/250x2/800'
group by name) as tab
go


--SELECT max([Timestamp])
--  FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]

use DataAnalyzer
select
	tab_best.name + ' & ' +
	cast (cast (tab_best.min_result as numeric (8,2)) AS varchar) + ' & ' +
	cast (cast (tab_best.avg_result as numeric (8,2)) AS varchar) + ' & ' +
	cast (cast (tab_best.bestratio as numeric (8,2)) AS varchar) + ' & ' +
	cast (cast ((tab_best.bestratio * tab_best.avg_result / tab_best.min_result) as numeric (8,2)) AS varchar) + ' & ' +
--	cast (cast (tab_static.min_result as numeric (8,2)) AS varchar) + ' & ' +
--	cast (cast (tab_static.avg_result as numeric (8,2)) AS varchar) + ' & ' +
--	cast (cast (tab_static.bestratio as numeric (8,2)) AS varchar) + ' & ' +
--	cast (cast ((tab_static.bestratio * tab_static.avg_result / tab_static.min_result) as numeric (8,2)) AS varchar) + ' & ' +
	cast (cast (tab_partition.min_result as numeric (8,2)) AS varchar) + ' & ' +
	cast (cast (tab_partition.avg_result as numeric (8,2)) AS varchar) + ' & ' +
	cast (cast (tab_partition.bestratio as numeric (8,2)) AS varchar) + ' & ' +
	cast (cast ((tab_partition.bestratio * tab_partition.avg_result / tab_partition.min_result) as numeric (8,2)) AS varchar) +
	' \\ \hline'
from 
(select 
	PSO_DVRP_LIMITED_AREAS_PHASE.name as name,
	count(*) as expno,
	AVG(result) as avg_result,
	MIN(result) as min_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(MAX(result)-MIN(Result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(dod) as dod
from PSO_DVRP_LIMITED_AREAS_PHASE
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from PSO_DVRP group by name) as namemin
on namemin.Name = PSO_DVRP_LIMITED_AREAS_PHASE.Name
where type = 'Utrzymywanie wyniku'
group by PSO_DVRP_LIMITED_AREAS_PHASE.name
union
select 'avarage',SUM(expno),SUM(avg_result),SUM(min_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, avg(min_1_05),'',AVG(bestratio),AVG(dod)
from
(select PSO_DVRP_LIMITED_AREAS_PHASE.name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(MAX(result)-MIN(Result)) as norm_stdev_result, min(resulttobest) as bestratio, avg(dod) as dod,
SUM(case when result < tab1.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05 from PSO_DVRP_LIMITED_AREAS_PHASE
join (select min(result) as min_result, name from PSO_DVRP_LIMITED_AREAS_PHASE where type = 'Utrzymywanie wyniku' group by name) as tab1
on tab1.name = PSO_DVRP_LIMITED_AREAS_PHASE.name
where type = 'Utrzymywanie wyniku'
group by PSO_DVRP_LIMITED_AREAS_PHASE.name) as tab) as tab_best,
(select 
	PSO_DVRP_LIMITED_AREAS_PHASE.name as name,
	count(*) as expno,
	AVG(result) as avg_result,
	MIN(result) as min_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(MAX(result)-MIN(Result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(dod) as dod
from PSO_DVRP_LIMITED_AREAS_PHASE
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from PSO_DVRP group by name) as namemin
on namemin.Name = PSO_DVRP_LIMITED_AREAS_PHASE.Name
where type = '2MPSO Statyczny'
group by PSO_DVRP_LIMITED_AREAS_PHASE.name
union
select 'avarage',SUM(expno),SUM(avg_result),SUM(min_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, avg(min_1_05),'',AVG(bestratio),AVG(dod)
from
(select PSO_DVRP_LIMITED_AREAS_PHASE.name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(MAX(result)-MIN(Result)) as norm_stdev_result, min(resulttobest) as bestratio, avg(dod) as dod,
SUM(case when result < tab1.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05 from PSO_DVRP_LIMITED_AREAS_PHASE
join (select min(result) as min_result, name from PSO_DVRP_LIMITED_AREAS_PHASE where type = '2MPSO Statyczny' group by name) as tab1
on tab1.name = PSO_DVRP_LIMITED_AREAS_PHASE.name
where type = '2MPSO Statyczny'
group by PSO_DVRP_LIMITED_AREAS_PHASE.name) as tab) as tab_static,
(select 
	PSO_DVRP_LIMITED_AREAS_PHASE.name as name,
	count(*) as expno,
	AVG(result) as avg_result,
	MIN(result) as min_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(MAX(result)-MIN(Result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(dod) as dod
from PSO_DVRP_LIMITED_AREAS_PHASE
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from PSO_DVRP group by name) as namemin
on namemin.Name = PSO_DVRP_LIMITED_AREAS_PHASE.Name
where type = '2MPSO Ocena podzia�u'
group by PSO_DVRP_LIMITED_AREAS_PHASE.name
union
select 'avarage',SUM(expno),SUM(avg_result),SUM(min_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, avg(min_1_05),'',AVG(bestratio),AVG(dod)
from
(select PSO_DVRP_LIMITED_AREAS_PHASE.name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(MAX(result)-MIN(Result)) as norm_stdev_result, min(resulttobest) as bestratio, avg(dod) as dod,
SUM(case when result < tab1.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05 from PSO_DVRP_LIMITED_AREAS_PHASE
join (select min(result) as min_result, name from PSO_DVRP_LIMITED_AREAS_PHASE where type = '2MPSO Ocena podzia�u' group by name) as tab1
on tab1.name = PSO_DVRP_LIMITED_AREAS_PHASE.name
where type = '2MPSO Ocena podzia�u'
group by PSO_DVRP_LIMITED_AREAS_PHASE.name) as tab) as tab_partition
where tab_static.name = tab_best.name AND tab_static.name = tab_partition.name
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
left join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'MC 25 PSO 40/250'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'MC 25 PSO 40/250','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE 'MC 25 PSO 40/250'
group by name) as tab
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
left join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'UCB 10000 25 PSO 40/250'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'UCB 10000 25 PSO 40/250','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE 'UCB 10000 25 PSO 40/250'
group by name) as tab
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
left join [ExternalResults] on namemin.Name = ExternalResults.Name
where type  LIKE 'UCB RC3 % 5000%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'UCB 5000','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE 'UCB RC3 % 5000%'
group by name,type) as tab
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
left join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'OKUL PSO 40/250'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'OKUL PSO 40/250','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE 'OKUL PSO 40/250'
group by name) as tab
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
left join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'OKUL MC 100'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'OKUL MC 100','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE 'OKUL MC 100'
group by name) as tab
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
left join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'OKUL UCB 100'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'OKUL UCB 100','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE 'OKUL UCB 100'
group by name) as tab
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
left join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'OKUL MC%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'OKUL MC ALL','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE 'OKUL MC%'
group by name) as tab
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
left join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'OKUL UCB2 %'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'OKUL UCB2 Real ALL','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE 'OKUL UCB2 %'
group by name) as tab
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
left join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'OKUL UCB %'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'OKUL UCB ALL','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE 'OKUL UCB %'
group by name) as tab
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
left join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'UCB RC3%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'UCB RC3','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE 'UCB RC3%'
group by name) as tab
go

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
left join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'MC RC3%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'MC RC3','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE 'MC RC3%'
group by name) as tab
go

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [ID]
      ,[Timestamp]
      ,[Name]
      ,[Result]
      ,[Dod]
      ,[Iterations]
      ,[BestKnownResult]
      ,[ResultToBest]
      ,[Type]
  FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
  where id > 19927
  order by Timestamp,name,Result

SELECT
max((DATEPART(HOUR, tab2.timestamp - tab1.timestamp) * 3600) +
(DATEPART(MINUTE, tab2.timestamp - tab1.timestamp) * 60) +
(DATEPART(SECOND, tab2.timestamp - tab1.timestamp))), tab2.type, tab2.name
  FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE] as tab1
 JOIN  [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE] as tab2
 ON tab1.id = tab2.id - 1
 where
 tab1.type = tab2.type
 and
 tab2.type like '2MPSOv2x1+4 2OPT 8/040/100/025(0.02)%'
 and
 (DATEPART(HOUR, tab2.timestamp - tab1.timestamp) * 3600) +
(DATEPART(MINUTE, tab2.timestamp - tab1.timestamp) * 60) +
(DATEPART(SECOND, tab2.timestamp - tab1.timestamp)) > 0
and
(DATEPART(HOUR, tab2.timestamp - tab1.timestamp) * 3600) +
(DATEPART(MINUTE, tab2.timestamp - tab1.timestamp) * 60) +
(DATEPART(SECOND, tab2.timestamp - tab1.timestamp)) < 1800
and tab2.timestamp > '2014-06-20 10:12:07.000'
group by tab2.type, tab2.name
 
 SELECT distinct([Type])
  FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
  order by [Type]


SELECT
tab2.timestamp,
((DATEPART(HOUR, tab2.timestamp - tab1.timestamp) * 3600) +
(DATEPART(MINUTE, tab2.timestamp - tab1.timestamp) * 60) +
(DATEPART(SECOND, tab2.timestamp - tab1.timestamp))), tab2.type, tab2.name
  FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE] as tab1
 JOIN  [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE] as tab2
 ON tab1.id = tab2.id - 1
 where
 tab1.type = tab2.type
 and
 tab2.type like '2MPSOv2x1+4 2OPT 8/040/100/025(0.02)%'
 and
 (DATEPART(HOUR, tab2.timestamp - tab1.timestamp) * 3600) +
(DATEPART(MINUTE, tab2.timestamp - tab1.timestamp) * 60) +
(DATEPART(SECOND, tab2.timestamp - tab1.timestamp)) > 0
and
(DATEPART(HOUR, tab2.timestamp - tab1.timestamp) * 3600) +
(DATEPART(MINUTE, tab2.timestamp - tab1.timestamp) * 60) +
(DATEPART(SECOND, tab2.timestamp - tab1.timestamp)) < 1800

--UCB

select 
	type,
	[PSO_DVRP_LIMITED_AREAS_PHASE].name,
	count(*) as expno,
	MIN(result) as min_result,
	AVG(result) as avg_result,
	STDEV(result) as stdev_result,
	STDEV(result)/(AVG(result)) as norm_stdev_result,
	SUM(case when result BETWEEN namemin.avg_result - namemin.stdev_result AND namemin.avg_result + namemin.stdev_result then 1.0 else 0.0 end) / COUNT(*) as sigma,
	SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) as min_1_05,
	CASE WHEN SUM(case when result < namemin.min_result * 1.05 then 1.0 else 0.0 end) / COUNT(*) >= 0.25 THEN 'True' ELSE 'False' END AS stable,
	min(resulttobest) as bestratio,
	AVG(result) / MIN(result) * min(resulttobest) as avgratio,
	MIN(result) /MIN([ExternalResults].MAPSOMinResult) as minmapsoratio,
	AVG(result) / AVG([ExternalResults].MAPSOAvgResult) as avgmapsoratio
from [PSO_DVRP_LIMITED_AREAS_PHASE]
join (select name, MIN(result) as min_result,AVG(result) as avg_result, STDEV(result) as stdev_result from [PSO_DVRP_LIMITED_AREAS_PHASE] group by name) as namemin
on namemin.Name = [PSO_DVRP_LIMITED_AREAS_PHASE].Name
join [ExternalResults] on namemin.Name = ExternalResults.Name
where type LIKE 'UCB RC3 Forget 10000' and namemin.name not like 'okul%'
group by [PSO_DVRP_LIMITED_AREAS_PHASE].name,type
union
select 'UCB RC3 Forget 10000','avarage',SUM(expno),SUM(min_result),SUM(avg_result),SUM(stdev_result),AVG(norm_stdev_result), 1 as helper, 1 as ratio,'',AVG(bestratio),sum(avg_result) / sum(min_result) * AVG(bestratio),0,0
from
(select name,count(*) as expno, AVG(result) as avg_result, MIN(result) as min_result, STDEV(result) as stdev_result,STDEV(result)/(AVG(result)) as norm_stdev_result, min(resulttobest) as bestratio from [PSO_DVRP_LIMITED_AREAS_PHASE]
where type  LIKE 'UCB RC3 Forget 10000' and name not like 'okul%'
group by name) as tab
go