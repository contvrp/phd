select
(1-round(AVG(mpsov3.minresult / memso.minresult),4))*100,
(1-round(AVG(mpsov3.avgresult / memso.avgresult),4))*100,
(1-round(AVG(mpsov3large.minresult / memso.minresult),4))*100,
(1-round(AVG(mpsov3large.avgresult / memso.avgresult),4))*100,
(1-round(AVG(mpsov3.minresult / mapso.minresult),4))*100,
(1-round(AVG(mpsov3.avgresult / mapso.avgresult),4))*100
from 
(
--wyniki mapso
SELECT  
	'sum' as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MAPSO'
) as mapso
join (
SELECT  
	'sum'  as name,
	sum([MAPSOMinResult]) as minresult,
      sum([MAPSOAvgResult]) as avgresult
FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
union
  SELECT
  name,
  [MAPSOMinResult],
  [MAPSOAvgResult]
  FROM [DataAnalyzer].[dbo].[ExternalResults]
where Algorithm like 'MEMSO(A)'
) as memso on mapso.name = memso.name
join (
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].[MPSO3_8SWARM_50TS_2500FFE]
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].[MPSO3_8SWARM_50TS_2500FFE]) as tab
) as mpsov3 on mpsov3.name = replace(mapso.name,'D.vrp','')
join (
select 
	[Name] as name,
	min_result as minresult,
	avg_result as avgresult
from [dbo].MPSO3_8SWARM_50TS_25000FFE
union
select 'sum' as name,SUM(min_result) as avgresult,SUM(avg_result) as minresult
from
(select 
	[Name] as name,
	min_result,
	avg_result
from [dbo].MPSO3_8SWARM_50TS_25000FFE) as tab
) as mpsov3large on mpsov3large.name = replace(mapso.name,'D.vrp','')
where mapso.name <> 'sum'