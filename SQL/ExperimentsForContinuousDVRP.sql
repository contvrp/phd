SELECT [Type], count(*)
  FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
  where type like '%22%(0.025)'
  group by [Type]
  order by [Type]

SELECT distinct([Type])
  FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
  where Timestamp <= '2014-10-01'
  order by [Type]

SELECT *
  FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
  where Timestamp > '2016-01-02 15:00:00'

  order by Timestamp 

  --delete from PSO_DVRP_LIMITED_AREAS_PHASE
	--where id in (
	--111076,
	--111077,
	--111078,
	--111079,
	--111080,
	--111081,
	--111082
  --)

update [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
set [Type] = '2MPSO_RSv3x3+4 2OPT 8/22/140(0.025)'
where [Type] = '2MPSO_RS 20v3x3+4 8/022/140/0(0.025)'

exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/040/250/800'
go

exec spSelectSummary @Experiment = '2MPSOv2x3+4 2OPT 8/100/1000/100'
go

exec spSelectSummary @Experiment = '2MPSOv2x1+50 2OPT 8/040/250/800'
go

exec spSelectSummary @Experiment = '2MPSOv2x1+50 2OPT 8/100/1000/100'
go

exec spSelectSummary @Experiment = 'DE 260v3x3+4 8/010/250/0C0.90(0.02)'
go

exec spSelectSummary @Experiment = 'DE 260v3x3+4 8/010/125/0C0.90(0.01)'
go

exec spSelectSummary @Experiment = 'DE 260v3x3+4 8/014/178/0C0.90(0.02)'
go

exec spSelectSummary @Experiment = 'DE 260v3x3+4 8/010/031/0C0.90(0.025)'
go

exec spSelectSummary @Experiment = 'DE 260v3x3+4 8/016/195/0C0.90(0.025)'
go

exec spSelectTimeSummary @Experiment = 'DE 260v3x3+4 8/016/195/0C0.90(0.025)', @Machine = '%'
go

exec spSelectTimeSummary @Experiment = 'Tree 4 8(0.025)', @Machine = '%'
go

exec spSelectSummary @Experiment = 'DE 260v3x3+4 8/051/617/0C0.90(0.025)'
go

exec spSelectTimeSummary @Experiment = 'DE 260v3x3+4 8/051/617/0C0.90(0.025)', @Machine = '%'
go

exec spSelectSummary @Experiment = 'DE 260v3x3+4 8/018/173/0C0.90(0.025)'
go

exec spSelectTimeSummary @Experiment = 'DE 260v3x3+4 8/018/173/0C0.90(0.025)', @Machine = '%'
go

exec spSelectSummary @Experiment = 'DE 260v3x3+4 8/009/277/0C0.90(0.02)'
go

exec spSelectSummary @Experiment = '2MPSO 20v3x3+4 8/010/250/0(0.02)'
go

exec spSelectSummary @Experiment = '2MPSO_RSv3x3+4 2OPT 8/22/140(0.025)'
go

exec spSelectSummary @Experiment = 'PSO NoHistory 20v3x3+4 8/022/140/0(0.025)'
go

exec spSelectSummary @Experiment = 'PSO 16v3x3+4 8/022/140/0(0.025)'
go

exec spSelectSummary @Experiment = 'PSO 20v3x3+4 8/020/127/27(0.025)'
go

exec spSelectSummary @Experiment = 'PSO 20v3x3+4 8/020/127/0(0.025)'
go

exec spSelectTimeSummary @Experiment = '2MPSO 20v3x3+4 1/062/396/0(0.025)', @Machine = '%'
go

exec spSelectTimeSummary @Experiment = '2MPSO 20v3x3+4 1/176/140/0(0.025)', @Machine = '%'
go

exec spSelectTimeSummary @Experiment = '2MPSO 20v3x3+4 8/007/044/0(0.025)', @Machine = '%'
go

exec spSelectTimeSummary @Experiment = '2MPSO_RSv3x3+4 2OPT 8/22/140(0.025)', @Machine = '%'
go

exec spSelectTimeSummary @Experiment = 'PSO NoHistory 20v3x3+4 8/022/140/0(0.025)', @Machine = '%'
go

exec spSelectTimeSummary @Experiment = '2MPSO 20v3x3+4 8/070/443/0(0.025)', @Machine = '%'
go

exec spSelectTimeSummary @Experiment = 'PSO 20v3x3+4 8/220/1400/0(0.025)', @Machine = '%'
go

exec spSelectTimeSummary @Experiment = 'DE 260v3x3+4 8/160/1950/0C0.90(0.025)', @Machine = '%'
go

exec spSelectTimeSummary @Experiment = 'DE 260v3x3+4 8/009/277/0C0.90(0.02)', @Machine = '%'
go

--count 16 computer lab hours to finish computations
select type,sum(maxhours)/16 as estAvgTime,max(hours/CASE leftexperiments WHEN 0 THEN 1 ELSE leftexperiments END) as estSlowestTime from (
SELECT name, type, (avg(ComputationsTime) / 1000 / 3600 *
  CASE WHEN (30-count(*) > 0) THEN (30-count(*)) ELSE 0 END) as hours,
   (max(ComputationsTime) / 1000 / 3600 *
  CASE WHEN (30-count(*) > 0) THEN (30-count(*)) ELSE 0 END) as maxhours,
  CASE WHEN (30-count(*) > 0) THEN (30-count(*)) ELSE 0 END as leftexperiments
  FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
  where [Type] IN ('DE 260v3x3+4 8/160/1950/0C0.90(0.025)','PSO 20v3x3+4 8/220/1400/0(0.025)')
  group by name, type
  ) as tab
  group by type
  order by sum(hours)

SELECT Machine,count(*)
  FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
  where Timestamp > '2016-01-02 15:00:00'
  group by machine

SELECT Id,Timestamp,machine,minfirst30.[Name],allres.result FROM  [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE] as allres
  join
(SELECT name,min(result) as result from
(
SELECT Name,Type,result,machine,(RANK() OVER (PARTITION BY Type, Name ORDER BY ID)) as rnk 
  FROM [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE]
  where machine <> 'Unspecified' and Machine not like '%LOST'
  ) first30
  group by name
  ) minfirst30
  ON minfirst30.Name = allres.Name AND minfirst30.result >= allres.result
  where machine <> 'Unspecified' and Machine not like '%LOST'
  --and minfirst30.[Name] not in ('c100bD.vrp','c150D.vrp','f71D.vrp','f134D.vrp',
  --'tai100bD.vrp','c100D.vrp','c120D.vrp','c199D.vrp','tai100aD.vrp',
  --'tai100dD.vrp','tai150aD.vrp','tai150dD.vrp','tai75bD.vrp','tai100cD.vrp',
  --'tai150bD.vrp','tai150cD.vrp','tai75aD.vrp','tai75dD.vrp','c75D.vrp',
  --'c50D.vrp')
  order by timestamp, Name,allres.result
  

  --UPDATE [DataAnalyzer].[dbo].[PSO_DVRP_LIMITED_AREAS_PHASE] set Machine='P52601_LOST' where id = 106920

