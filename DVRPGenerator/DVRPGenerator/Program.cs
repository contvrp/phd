﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RDotNet;

namespace DVRPGenerator
{
    class Program
    {
        //N = 100 must remain first because of Random seed
        static int[] NS = { 100, 80, 50, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100 };
        static string[] seeds = { "1", "1", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18" };
        static int CAPACITY = 1000;
        static int UNLOAD_TIME = 10;
        static double[] BBOX = new double[] {-100, -100, 100, 100 };

        static RDotNet.NumericMatrix requests;

        private static string requestsString;
        private static string locationsString;
        private static string visitsString;
        private static string availability_timesString;
        private static string loading_timesString;

        private delegate void RequestGenerator();

        static void Main(string[] args)
        {
            CreateRandomCase();
        }

        private static void CreateRandomCase()
        {
            List<int> generatedNs = new List<int>();
            foreach (int N in NS)
            {
                RFacade.RFacade.Engine.Evaluate("set.seed(" + seeds[generatedNs.Count] + ")");
                //Time, Size, X, Y
                requests = new RDotNet.NumericMatrix(RFacade.RFacade.Engine, N, 4);

                RFacade.RFacade.Engine.SetSymbol("requests", requests);
                NumericVector n = new RDotNet.NumericVector(RFacade.RFacade.Engine,
                    new double[] { N });
                RFacade.RFacade.Engine.SetSymbol("n", n);

                NumericVector bbox = new RDotNet.NumericVector(RFacade.RFacade.Engine,
                    BBOX);
                RFacade.RFacade.Engine.SetSymbol("bbox", bbox);

                NumericVector maxTime = new RDotNet.NumericVector(RFacade.RFacade.Engine,
                    1);
                RFacade.RFacade.Engine.SetSymbol("max.time", maxTime);

                NumericVector capacity = new RDotNet.NumericVector(RFacade.RFacade.Engine,
                    new double[] { CAPACITY });
                RFacade.RFacade.Engine.SetSymbol("capacity", capacity);

                NumericVector unloadTime = new RDotNet.NumericVector(RFacade.RFacade.Engine,
                    new double[] { UNLOAD_TIME });
                RFacade.RFacade.Engine.SetSymbol("unload.time", unloadTime);

                string[] requestSizeDistributions = new string[] {
                "requests[,1] = round(-rbeta(n,1,10)*capacity)",
                "requests[,1] = round(-rbeta(n,10,10)*capacity/4.0)",
                "requests[,1] = round(-rbeta(n,1,1)*capacity/4.0)",
                "requests[,1] = round(-rbeta(n,0.4,10)*capacity*2.0)",
            };

                string[] requestSizeDistributionsNames = new string[] {
                "skewed",
                "normal",
                "uniform",
                "moreskewed",
            };

                RequestGenerator[] requestGenerators = new RequestGenerator[] {
                UniformRequests,
                StructuredRequests,
                PseudoclusteredRequests,
                ClusteredRequests
            };

                string[] requestGeneratorsNames = new string[] {
                "uniform",
                "structured",
                "pseudoclustered",
                "reallyclustered"
            };

                for (int j = 0; j < requestSizeDistributions.Length; ++j)
                {
                    RFacade.RFacade.Engine.Evaluate(requestSizeDistributions[j]);
                    RFacade.RFacade.Engine.Evaluate("requests[,1] = ifelse(requests[,1] > -1, -1, requests[,1])");
                    RFacade.RFacade.Engine.Evaluate("vehicles.lower.bound = ceiling(-sum(requests[,1])/capacity)");
                    RFacade.RFacade.Engine.Evaluate("max.time = round(vehicles.lower.bound*unload.time+3*sqrt((bbox[4]-bbox[2])^2+(bbox[3]-bbox[1])^2))");
                    RFacade.RFacade.Engine.Evaluate("requests[,2] = round(runif(n,0,1)*max.time)");
                    maxTime = RFacade.RFacade.Engine.Evaluate("max.time").AsNumeric();

                    for (int k = 0; k < requestGenerators.Length; ++k)
                    {

                        requestGenerators[k]();
                        requests = RFacade.RFacade.Engine.Evaluate("requests").AsNumericMatrix();

                        List<string> compactFormat = new List<string>();

                        requestsString = "";
                        locationsString = "";
                        visitsString = "";
                        availability_timesString = "";
                        loading_timesString = "";
                        for (int i = 0; i < N; i++)
                        {
                            requestsString += string.Format("  {0} {1}\n", i + 1, requests[i, 0]);
                            locationsString += string.Format("  {0} {1} {2}\n", i + 1, requests[i, 2], requests[i, 3]);
                            visitsString += string.Format("  {0} {1}\n", i + 1, i + 1);
                            availability_timesString += string.Format("  {0} {1}\n", i + 1, requests[i, 1]);
                            loading_timesString += string.Format("  {0} {1}\n", i + 1, UNLOAD_TIME);
                            compactFormat.Add(string.Format("{0} {1} {2} {3} {4} {5} {6}", i + 1, requests[i, 2], requests[i, 3], requests[i, 1] < maxTime[0] / 2 ? requests[i, 1] : 0, requests[i, 0], CAPACITY, maxTime[0]));
                        }

                        string name = string.Format("okul_{0}{3}_{1}_{2}_D.vrp", N, requestSizeDistributionsNames[j], requestGeneratorsNames[k], (char)('b' + generatedNs.Count(nn => nn == N)));
                        File.WriteAllText(name,
                            string.Format(Properties.Resources.FileTemplate,
                            int.MaxValue,
                            name,
                            N,
                            N + 1,
                            CAPACITY,
                            requestsString,
                            locationsString,
                            visitsString,
                            loading_timesString,
                            maxTime[0],
                            availability_timesString
                            )
                            );
                        File.WriteAllLines(
                            name + ".txt",
                            compactFormat.ToArray());
                    }
                }
            generatedNs.Add(N);
            }
        }

        private static void UniformRequests()
        {
            RFacade.RFacade.Engine.Evaluate("requests[,3] = round(runif(n,bbox[1],bbox[3]))");
            RFacade.RFacade.Engine.Evaluate("requests[,4] = round(runif(n,bbox[2],bbox[4]))");
        }

        private static void StructuredRequests()
        {
            RFacade.RFacade.Engine.Evaluate("xCenters = runif(vehicles.lower.bound, bbox[1], bbox[3])");
            RFacade.RFacade.Engine.Evaluate("yCenters = runif(vehicles.lower.bound, bbox[2], bbox[4])");
            RFacade.RFacade.Engine.Evaluate("requests[,3] = round(ifelse(runif(n,0,1) < 0.3, runif(n,bbox[1],bbox[3]), rnorm(n, xCenters, (bbox[3] - bbox[1])/vehicles.lower.bound^1.75)))");
            RFacade.RFacade.Engine.Evaluate("requests[,4] = round(ifelse(runif(n,0,1) < 0.3, runif(n,bbox[2],bbox[4]), rnorm(n, yCenters, (bbox[4] - bbox[2])/vehicles.lower.bound^1.75)))");
        }

        private static void PseudoclusteredRequests()
        {
            RFacade.RFacade.Engine.Evaluate("xCenters = runif(vehicles.lower.bound, bbox[1], bbox[3])");
            RFacade.RFacade.Engine.Evaluate("yCenters = runif(vehicles.lower.bound, bbox[2], bbox[4])");
            RFacade.RFacade.Engine.Evaluate("requests[,3] = round(rnorm(n, xCenters, (bbox[3] - bbox[1])/vehicles.lower.bound^1.75))");
            RFacade.RFacade.Engine.Evaluate("requests[,4] = round(rnorm(n, yCenters, (bbox[4] - bbox[2])/vehicles.lower.bound^1.75))");
            RFacade.RFacade.Engine.Evaluate("print(requests[,3])");
        }

        private static void ClusteredRequests()
        {
            RFacade.RFacade.Engine.Evaluate("currentSum = capacity");
            RFacade.RFacade.Engine.Evaluate("for (i in 1:nrow(requests)) {\n" +
                "if (currentSum - requests[i,1] > capacity) {\n" +
                    "xCenter = runif(1, bbox[1], bbox[3])\n" +
                    "yCenter = runif(1, bbox[2], bbox[4])\n" +
                    "currentSum = 0\n"+
                    "}\n" +
                "requests[i,3] = round(rnorm(1, xCenter, (bbox[3] - bbox[1])/vehicles.lower.bound^1.75))\n" +
                "requests[i,4] = round(rnorm(1, yCenter, (bbox[4] - bbox[2])/vehicles.lower.bound^1.75))\n" +
                "currentSum = currentSum - requests[i,1]\n" +
            "}");
            RFacade.RFacade.Engine.Evaluate("print(requests[,3])");

        }

    }
}
