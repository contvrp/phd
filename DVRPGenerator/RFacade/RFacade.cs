﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RDotNet;

namespace RFacade
{
    public class RFacade
    {
        static RFacade()
        {
            SetupREnvironmentPath();
            Engine = REngine.CreateInstance("RDotNet");
            Engine.Initialize(); // required since v1.5
        }

        private static void InstallAndLoadRPackage(string packageName)
        {
            Engine.Evaluate(string.Format("install.packages('{0}',repos = 'http://r.meteo.uni.wroc.pl/')", packageName));
            Engine.Evaluate(string.Format("require({0})", packageName));
        }

        public static REngine Engine
        {
            get;
            private set;
        }

        public static void SetupREnvironmentPath()
        {
            var oldPath = System.Environment.GetEnvironmentVariable("PATH");
            var rPath = System.Environment.Is64BitProcess ? @"C:\Program Files\R\R-3.0.2\bin\x64" : @"C:\Program Files\R\R-3.0.2\bin\i386";
            // Mac OS X
            //var rPath = "/Library/Frameworks/R.framework/Libraries";
            // Linux (in case of libR.so exists in the directory)
            //var rPath = "/usr/lib";
            if (Directory.Exists(rPath) == false)
                throw new DirectoryNotFoundException(string.Format("Could not found the specified path to the directory containing R.dll: {0}", rPath));
            var newPath = string.Format("{0}{1}{2}", rPath, System.IO.Path.PathSeparator, oldPath);
            System.Environment.SetEnvironmentVariable("PATH", newPath);
            // NOTE: you may need to set up R_HOME manually also on some machines
            string rHome = "";
            var platform = Environment.OSVersion.Platform;
            switch (platform)
            {
                case PlatformID.Win32NT:
                    break; // R on Windows seems to have a way to deduce its R_HOME if its R.dll is in the PATH
                case PlatformID.MacOSX:
                    rHome = "/Library/Frameworks/R.framework/Resources";
                    break;
                case PlatformID.Unix:
                    rHome = "/usr/lib/R";
                    break;
                default:
                    throw new NotSupportedException(platform.ToString());
            }
            if (!string.IsNullOrEmpty(rHome))
                Environment.SetEnvironmentVariable("R_HOME", rHome);
        }


    }
}
