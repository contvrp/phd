﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using DynamicVehicleRouting;
using System.Runtime.Serialization;

namespace BenchmarkProjectComputingService
{
    [ServiceBehavior(MaxItemsInObjectGraph = int.MaxValue)] 
    public class FunctionSolverService : BenchmarkProjectComputingService.IFunctionSolverService
    {
        public FunctionWithResult Solve(DVRPInstance function)
        {
            function.CacheDistances();
            double result = function.Solve();

            return new FunctionWithResult() { function = function, result = result };
        }
    }

    [DataContract]
    public class FunctionWithResult
    {
        [DataMember]
        public DVRPInstance function;
        [DataMember]
        public double result;
    }
}
