﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace BenchmarkProjectComputingService
{
    public class Program
    {
        static int port = 6540;

        static void Main(string[] args)
        {
            while (port < 6600)
            {
                ///Run as admin: netsh http add urlacl url=http://+:6666/DVRPSolver user=okulewicz (if HTTP is needed it must be binded)
                // Step 1 Create a URI to serve as the base address.
                //Uri baseAddress = new Uri("http://localhost:6666/DVRPSolver/");
                Uri baseAddress = new Uri(string.Format("net.tcp://localhost:{0}/", args[0]));
                //Uri baseAddress = new Uri(string.Format("net.pipe://localhost"));

                // Step 2 Create a ServiceHost instance
                ServiceHost selfHost = new ServiceHost(typeof(FunctionSolverService), baseAddress);
                try
                {
                    NetTcpBinding binding = new NetTcpBinding();
                    binding.CloseTimeout = new TimeSpan(1, 0, 0);
                    binding.OpenTimeout = new TimeSpan(1, 0, 0);
                    binding.ReceiveTimeout = new TimeSpan(1, 0, 0);
                    binding.SendTimeout = new TimeSpan(1, 0, 0);
                    binding.MaxReceivedMessageSize = int.MaxValue;
                    binding.MaxBufferSize = int.MaxValue;
                    binding.TransferMode = TransferMode.Buffered;

                    // Step 3 Add a service endpoint.
                    selfHost.AddServiceEndpoint(typeof(IFunctionSolverService), binding, "DVRPSolverService");


                    // Step 4 Enable metadata exchange.
                    ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                    //smb.HttpGetEnabled = true;
                    selfHost.Description.Behaviors.Add(smb);

                    ((ServiceDebugBehavior)selfHost.Description.Behaviors.First(bhv => bhv is ServiceDebugBehavior)).IncludeExceptionDetailInFaults = true;
                    // Step 5 Start the service.
                    selfHost.Open();
                    Console.WriteLine("The service is ready.");
                    Console.WriteLine("Press <ENTER> to terminate service.");
                    Console.WriteLine();
                    Console.ReadLine();

                    // Close the ServiceHostBase to shutdown the service.
                    selfHost.Close();
                    break;
                }
                catch (CommunicationException ce)
                {
                    Console.WriteLine("An exception occurred: {0}", ce.Message);
                    selfHost.Abort();
                    ++port;
                }
            }
        }
    }
}
