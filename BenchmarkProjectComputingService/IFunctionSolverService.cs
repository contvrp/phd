﻿using System;
using System.ServiceModel;
namespace BenchmarkProjectComputingService
{
    [ServiceContract(Namespace = "http://DVRP2MPSO/")]
    interface IFunctionSolverService
    {
        [OperationContract]
        FunctionWithResult Solve(DynamicVehicleRouting.DVRPInstance function);
    }
}
