\documentclass{llncs}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{url}

\begin{document}
\setlength{\floatsep}{4pt}
\setlength{\textfloatsep}{4pt}
\title{Application of Particle Swarm Optimization Algorithm to Dynamic Vehicle Routing Problem}
\titlerunning{Application of PSO Algorithm to DVRP}
\author{Micha{\l} Okulewicz \and Jacek Ma{\'n}dziuk}

\institute{Warsaw University of Technology,\\
Faculty of Mathematics and Information Science,\\
Koszykowa 75, 00-662 Warsaw, Poland\\
\{M.Okulewicz, J.Mandziuk\}@mini.pw.edu.pl\\}

\maketitle

\begin{abstract}
In this paper we apply Particle Swarm Optimization (PSO) algorithm
to Dynamic Vehicle Routing Problem (DVRP) for solving the client
assignment problem and optimizing the routes. Our approach to
solving the DVRP consists of two stages. First we apply an instance
of PSO to finding clients' requests that should be satisfied by the
same vehicle (thus solving the Bin Packing Problem (BPP) part of the
DVRP task) and then we apply several other instances of PSO, one for
each of the vehicles assigned to particular requests (thus solving
the Travelling Salesman Problem (TSP)). Proposed algorithm found
better solutions then other tested PSO approaches in 6 out of 21
benchmarks and better average solutions in~5~of~them.
\end{abstract}
\noindent\begin{keywords} Particle Swarm Optimization, Dynamic
Vehicle Routing Problem, Dynamic Optimization
\end{keywords}

\section{Introduction}
The goal of Vehicle Routing Problem (VRP) is to find the shortest
routes for $n$ homogeneous vehicles delivering cargo for $m$ clients.
Every client has its own demand of cargo. The capacity of each
vehicle is limited and the cargo could be loaded on the vehicle on
one of the $k$ depots.

VRP could be regarded as a composition of two NPC problems: the Bin
Packing Problem (optimizing the number of used vehicles by assigning
client's requests to the vehicles) and the Travelling Salesman
Problem (optimizing the route for each of the vehicles). In this
study we are focused on the dynamic version of the problem (DVRP)
where clients' requests (load demands and required destinations) are
not fully available beforehand and are partly defined during the
working day (the so-called Vehicle Routing Problem with Dynamic
Requests~\cite{DVRP:Study}). The goal of DVRP is to find the
shortest routes within the time bounds of the working day (i.e. the
opening hours of the depots).

In recent years, biologically inspired computational intelligence
algorithms have grown a lot of popularity. Examples of such
algorithms, based on the idea of swarm intelligence, are PSO and
bird flocking algorithm. Those algorithms have been used in the real
world applications in the area of computer graphics and animation
\cite{App:Graphics}, detection of outliers \cite{App:FlockOutliers}
or document clustering \cite{App:PSOCluster}.

PSO was proven to be a suitable algorithm for solving dynamic
problems \cite{PSO:Dynamic} and was applied with success to the
DVRP\cite{DVRP:DAPSO,DVRP:MAPSO} with the combination of 2-Opt
algorithm for solving the TSP part of the problem. In this work, we
employ a different approach, by using PSO algorithm for both phases
and furthermore by applying a different problem encoding in the
first phase.

One of our goals was to check the possibility of using a continuous
encoding of the DVRP problem, thus enabling the usage of the native
(continuous) version of the PSO algorithm. The other goal was to
check how good results might be achieved, without using the
approximation algorithm and relying only on the PSO in both parts of
the problem.

The remainder of the paper is organized as follows: First, in
section~ \ref{sec:pso} we briefly summarize the PSO algorithm. In
section~\ref{sec:dvrp} the DVRP is defined in a formal way.
Application of the PSO algorithm and the experimental setup and
results are presented in sections~\ref{sec:psoindvrp}
and~\ref{sec:tests}, respectively. The last section summarizes the
experimental findings and concludes the paper.

\section{Particle Swarm Optimization Algorithm}
\label{sec:pso}
\begin{figure}[t]
\scriptsize
\begin{verbatim}
PSO() {
  swarm.initializeRandomlyParticlesLocationAndVelocity();
  for i from 1 to maxIterations {
    for each particle in swarm {
      particle.updateVelocity();
      particle.updateLocation();
    }
  }
}

Particle {
  updateVelocity() {
    for (i from 1 to dimensions) {
      this.v[i] =
        random.uniform(0,g)*(this.neighbours.best[i] - this.x[i]) +
        random.uniform(0,l)*(this.best[i] - this.x[i]) +
        random.uniform(0,r)*(this.x[i] - this.neighbours.random().x[i]) +
        a * this.v[i] +
        y * random.normal(0,1);
    }
  }

  updateLocation() {
    for (i from 1 to dimensions) {
      this.x[i] = this.x[i] + this.v[i];
    }
    if (f(this.best) > f(this.x)) {
      this.best = this.x;
    }
  }
}
\end{verbatim}
\label{fig:pso}
\caption{Particle Swarm Optimization in pseudo-code}
\end{figure}

PSO algorithm is an iterative optimization method proposed in 1995
by Kennedy and Eberhart~\cite{PSO:Introduction} and
further studied and developed by many other researchers, e.g.,
\cite{PSO:Inertia}, \cite{PSO:Modified}, \cite{PSO:Params}. In
short, PSO implements the idea of swarm intelligence to solving hard
optimization tasks.

In the PSO algorithm, the optimization is performed by the set of
particles which are communicating with each other (see
Fig.~\ref{fig:pso}). Each particle has its location and velocity. In
every step $t$ a location of particle $i$, $x^i_t$ is updated based
on particle's velocity $v^i_t$:

\begin{equation}
    x^i_{t+1} = x^i_t + v^i_t.
\end{equation}

In our implementation of PSO (based on~\cite{Algorithm:PSO2011}
and~\cite{PSO:Inertia}) particle's $i$ velocity $v^i_{t}$ is updated
according to the following rule:
\begin{equation}
\begin{split}
    v^i_{t+1} =&
    u^{(1)}_{U[0;g]} (x^{neighbours_i}_{best} - x^i_t)  +
    u^{(2)}_{U[0;l]} (x^i_{best} - x^i_t) + \\
    & u^{(3)}_{U[0;r]} (x^i_t - x^{random_i}_t) +
    a v^i_t + f y_{N(0;1)},\label{eq:PSO_vi}
\end{split}
\end{equation}
where
\begin{itemize}
    \item $x^{neighbours_i}_{best}$ represents the best location in terms of optimization, found hitherto by the neighbourhood of the $i$th particle,
    \item $x^i_{best}$ represents the best location in terms of optimization, found hitherto by the particle
    $i$,
    \item $x^{random_i}_t$ is a location of a randomly chosen particle from the neighbours of the $i$th particle in
    iteration~$t$,
    \item $g$ is a neighbourhood attraction factor,
    \item $l$ is a local attraction factor,
    \item $r$ is a repulse factor,
    \item $a$ is an inertia coefficient,
    \item $a$ is a fluctuation coefficient,
    \item $u^{(1)}$, $u^{(2)}$, $u^{(3)}$ are random vectors with uniform
    distribution from the intervals $[0,g]$, $[0,l]$ and $[0,r]$,
    respectively,
    \item $y$ is a random vector with coordinates from standard normal distribution.
\end{itemize}

Such implementation allows to run algorithm as a classic PSO or as a
Repulsive PSO (RPSO). In the classic variant $r = 0$ and $g \neq 0$.
In RPSO $r \neq 0$ and $g = 0$ (the attraction of the
$x^{neighbours_i}_{best}$ point is changed into repulsion from randomly
chosen particle allowing for a greater disperse of a swarm).

\section{Problem Definition}
\label{sec:dvrp}

In the Dynamic Vehicle Routing Problem one considers a fleet $V$ of
$n$ vehicles and a series $C$ of $m$ clients (requests) to be served.

All vehicles $v_i, i=1,\ldots,n$ are identical and
have the same $capacity \in \mathbb{R}$ and the same $speed$\footnote{In all benchmarks used in this paper $speed$ is
defined as one distance unit per one time unit.} $\in
\mathbb{R}$. Each vehicle is
loaded in one of the $k$ depots\footnote{In all benchmarks used in
this paper $k = 1$.}.

Each depot $d_j, j=1,\ldots,k$ has a certain $location \in
\mathbb{R}^2$ and working hours $(start,
end)$, where $0 \leq start < end$.

Each client $c_l, l=1+k,\ldots, m+k$ has a given $location \in \mathbb{R}^2$,
$time \in \mathbb{R}$, which is a period of time when the request
becomes available ($\min\limits_{j \in 1,\ldots,k}d_j.start \leq time \leq \max\limits_{j \in 1,\ldots,k}d_jend$),
$unloadTime \in \mathbb{R}$, which is the time required to unload
the cargo, and the $requestSize \in \mathbb{R}$ - size of the
request ($requestSize < capacity$).

A travel distance $\rho(location_i,location_j)$ is an Euclidean
distance between $location_i$ and $location_j$ on the $\mathbb{R}^2$
plane, $i,j=1,\ldots, m+k$.

As previously stated, the goal is to serve the clients (requests),
according to their defined constraints, with minimal total cost
(travel distance).
Because of the assignment of the requests to the vehicles each vehicle will gain the following properties:
\begin{itemize}
    \item $PlannedClients_t \subset C$, a set of clients planned to be visited (at a given moment $t$),
    \item $AssignedClients_t \subset C$, an ordered set of clients (at a given moment $t$), which have already been visited or will be visited by the vehicle (the order of the set is defined by the order of visits),
    \item $distance_i \in \mathbb{R}$, a total travel distance of the vehicle,
    \item $\widehat{distance_i} \in \mathbb{R}$, an estimation of the total travel distance of the vehicle (based on the clients in the
$PlannedClients_t$ set but not on the route through them),
    \item $depot \in \lbrace 1,2,\ldots,k \rbrace$, the index of the depot to which the vehicle will return at the end of working day.
\end{itemize}
The sets of the $PlannedClients$ and $AssignedClients$ have the
following properties:
\begin{equation}
\label{eq:only_one_set} (\forall_{t \in \mathbb{R}}) (\forall_{v \in
V}) (\forall_{c \in C}) \ \ c \in v.PlannedClients_t \Leftrightarrow
c \notin v.AssignedClients_t
\end{equation}
\begin{equation}
\label{eq:only_one_vehicle}
\begin{split}
& (\forall_{t \in \mathbb{R}}) (\forall_{v, v\prime \in V})
(\forall_{c \in v.PlannedClients_t \cup v.AssignedClients_t})  \\
& c \in v\prime .PlannedClients_t \cup v\prime .AssignedClients_t \Rightarrow v = v\prime
\end{split}
\end{equation}
The formulae state that each client could be associated with only
one vehicle (eq.~\ref{eq:only_one_vehicle}) and only using one type
of association (eq.~\ref{eq:only_one_set}).

\section{A 2-Phase Particle Swarm Optimization in Dynamic Vehicle Routing Problem}\label{sec:psoindvrp}

In the rest of the paper we will use the following definitions:\\
\textbf{Available vehicles} - a set of vehicles to which the requests may still be assigned (added) without breaking the time bounds of the problem.\\
\textbf{Operating area} - an area enclosed by the polygon spanned by the locations of planned and assigned clients.\\
\textbf{Cut-off time} - a time of the day (within the working hours)
after which requests (defined in benchmark sets)
are postponed to the next working day (they are available at the next day's start time).

\subsection{The Algorithm}
The problem will be solved by dividing the working day into pairwise
equal time slices and solving partial problems with the use of the
best known solution previously found.

The number of time slices $n_{ts}$ equals $25$ and cut-off time
$T_{co}$ equals $0.5$ as proposed and tested by Montemanni et
al.\cite{DVRP:Ants}. We have stuck to the above-mentioned selection
in order to allow a direct comparison of our results with the ones
obtained previously for this
problem~\cite{DVRP:DAPSO,DVRP:MAPSO,DVRP:Ants,DVRP:GA:TS}. We also
measure the dynamism of the problem ($dod$) used by Khouadjia et
al.~\cite{DVRP:DAPSO}, defined as follows:

\begin{equation}
dod =\frac{\mathrm{Number\:of\:requests\:unknown\:at\:the
\:beginning\:of\:the\:working\:day}}{\mathrm{Number\:of\:all\:requests}}
\end{equation}

Because of the nature of the problem (which is essentially a
combination of two different NPC problems) the optimization
algorithm has been divided into two separate phases (stages) in each
time slice. In the first phase we assign clients to the vehicles and
in the second one optimize a route, separately for each of the
vehicles.

The optimization is performed for the client requests known at the
moment of the algorithm's start ($n_{ts}$ times a day), i.e. the
information about new requests is not updated during the algorithm's
run (in a given time step).

The general pseudocode of the algorithm is presented on Fig. \ref{fig:algorithm}.
\begin{figure}[t]
\label{fig:algorithm}
\begin{verbatim}
time = depot.start;
while (time < depot.end)
{
  foreach (v in Vehicles)
    v.Available = true;
  while (|UnassignedClients| > 0 and |AvailableVehicles| > 0)
  AssignClientsToAvailableVehicles();
  foreach (v in Vehicles)
  {
    v.OptimizeRoute();
    if (|v.LateClients| > 0)
      v.Available = false;
    v.RemoveLateClientsFromTheRoute(); //mark late clients as unassgined
    v.CommitVehicles();
  }
  time += (depot.end - depot.start) / n_ts;
}
\end{verbatim}
\caption{The pseudocode of the algorithm for applying PSO to the DVRP}
\end{figure}


\subsection{Client Assignment Encoding}
The arguments of the fitness function for the first stage are the
locations of the centres of the operating areas for the vehicles and
the result of the function is a sum of the estimated routes' lengths
and optionally an additional penalty for the estimated late return
to the depot (i.e. after its closing).

If $x = [x_1, x_2, \ldots, x_{2n}]$ is the argument of
the fitness function then the center of the
operation area of $vehicle_j$ is defined as follows: $center_j.x =
x_{2j - 1}$ and $center_j.y = x_{2j}$. The closest (in terms of
Euclidean distance between the client location and the vehicle
center of the operating area) clients are added to the list of the
\textit{planned clients} of the closest \textbf{available vehicle}.

The value of the vehicles' routes length estimation equals to the sum
of the estimated route length for each of the vehicles
$\sum\limits_{v \in V} v.\widehat{distance}$. The vehicle's route
length estimation is expressed as a sum of three factors:
$assignedRoute$ (length of route through the \textit{assigned
clients}), $\widehat{plannedRoute}$ (estimated length of the route
through the \textit{planned clients}) and $\widehat{clusterCost}$
(estimated distance from depot to the planned clients multiplied by
the doubled number of necessary returns to the depot for reloading the
vehicle).

\begin{align}
v.\widehat{distance} = & v.assignedRoute + v.\widehat{plannedRoute} + v.\widehat{clusterCost}
\\
\notag\\
v.assignedRoute =
&\sum\limits_{i = 2}^{|v.AssignedClients|}
\rho\left(v.AssignedClients_{i-1}.location, \right. \notag\\
& \left. v.AssignedClients_i.location \right)
\\
\notag\\
v.\widehat{plannedRoute} =
& \sum\limits_{i=1}^{|v.PlannedClients|}
\rho(v.PlannedClients_{i}.location, v.center)
\\
\notag\\
v.\widehat{clusterCost} =
& \left\lceil \frac{\sum\limits_{c \in v.AssignedClients \cup v.PlannedClients}
c.requestSize}{v.capacity} \right \rceil \ast \notag\\
& \ast 2 \rho(v.center,
v.depot.location)
\end{align}

The route through the \textit{planned clients} is estimated
by the sum of the distance from the vehicle's operating area
center to each of the \textit{planned clients}.
And the distance from the depot to the clients is estimated by the distance from  vehicle's operating area center to depot location.

\subsection{Managing the Vehicles}

\subsubsection*{Vehicle Route Encoding.} The argument of the fitness
function for the second stage defines an order of the
\textit{planned clients} and the result of the function is the
length of the route.

If $x = [x_1, x_2, \ldots, x_{2n}]$ is the argument of the function
then the rank of the vehicle's $j^{th}$ \textit{planned client} is
equal to the rank of the $x_j$ coordinate in the set $\lbrace x_1,
x_2,\ldots,x_{2n} \rbrace$ (if $x_k = x_l \wedge k \neq l$ then the
order of the $k^{th}$ and $l^{th}$ clients is defined by the order
of $l$ and $k$).

\subsubsection*{Committing the Vehicles.} After the second phase of
the optimization the algorithm returns proposed routes over the
\textit{planned clients} set for each of the vehicles.
The vehicles with small time reserve\footnote{Small time reserve was
experimentally set as arriving at the depot later then 3 time steps
before the depot closing time} are
committed to their subsequent \textit{planned clients}.

The set of the clients which are to be moved from the \textit{plannedClients}
to the \textit{assigned clients} of the vehicle $v$ is
defined as follows:
\begin{equation*}
\begin{split}
\lbrace & client: \,client \in v.PlannedClients
\,\wedge\, \\ &client.arrivalTime  \leq \min\limits_{c \in
v.PlannedClients} \left(c.arrivalTime > nextTimeStep \right)  \rbrace
\end{split}
\end{equation*}
where $arrvialTime$ is the time when the client will be visited by
the vehicle $v$ on the planned route.

\subsubsection*{Keeping Solution Within the Time Bounds.}

\begin{table}[t]
\caption{Presentation of the best and $\overline{x}$ (average)
results with the $S$ (standard deviation of the sample), ratio to the best known
VRP solution accomplished by the proposed method (denoted by 2-phase PSO).}
\begin{center}
\label{tab:results}
\begin{tabular*}{1.0\textwidth}{@{\extracolsep{\fill}}|c|lllll|}
\hline \multirow{2}{*}{Benchmark\cite{DVRP:Benchmark}} &  \multicolumn{5}{c|}{2-phase PSO} \\
 & Best & $\overline{x}$ & $S$ & $\frac{\mathrm{Best}}{\mathrm{VRPBest}}$\cite{DVRP:Best} & dod \\
\hline\hline
c50D.vrp & 582.88 & 675.14 & 40.62 & 1.11 & 0.46 \\
c75D.vrp & 912.23 & 1015.16 & 51.41 & 1.09 & 0.52 \\
c100D.vrp & 996.40 & 1149.48 & 79.78 & 1.21 & 0.59 \\
c100bD.vrp & 828.94 & 850.68 & 23.70 & 1.01 & 0.59 \\
c120D.vrp & 1087.04 & 1212.38 & 106.80 & 1.04 & 0.42 \\
c150D.vrp & 1173.94 & 1336.84 & 62.72 & 1.14 & 0.47 \\
c199D.vrp & 1446.93 & 1578.99 & 71.24 & 1.12 & 0.47 \\
\hline\hline
f71D.vrp & 315.00 & 356.75 & 23.70 & 1.30 & 0.59 \\
f134D.vrp & 12813.14 & 13491.60 & 319.41 & 1.10 & 0.57 \\
\hline\hline
tai75aD.vrp & 1871.06 & 2142.07 & 162.75 & 1.16 & 0.56 \\
tai75bD.vrp & 1460.95 & 1568.21 & 69.80 & 1.09 & 0.56 \\
tai75cD.vrp & 1500.23 & 1811.08 & 152.80 & 1.16 & 0.56 \\
tai75dD.vrp & 1462.82 & 1586.28 & 95.95 & 1.07 & 0.37 \\
tai100aD.vrp & 2317.76 & 2707.61 & 202.75 & 1.13 & 0.56 \\
tai100bD.vrp & 2187.86 & 2510.60 & 167.79 & 1.13 & 0.56 \\
tai100cD.vrp & 1564.25 & 1672.33 & 68.78 & 1.11 & 0.46 \\
tai100dD.vrp & 1859.70 & 2220.01 & 147.32 & 1.18 & 0.49 \\
tai150aD.vrp & 3638.75 & 4151.31 & 314.32 & 1.19 & 0.46 \\
tai150bD.vrp & 3107.95 & 3302.94 & 126.36 & 1.14 & 0.49 \\
tai150cD.vrp & 2781.02 & 2952.88 & 68.80 & 1.18 & 0.49 \\
tai150dD.vrp & 3048.24 & 3478.49 & 225.68 & 1.15 & 0.56 \\
\hline\hline
Sum/Avarage & 46957.09 & 51770.84 & 2582.48 & 1.13 & 0.51 \\
\hline
\end{tabular*}
\end{center}
\end{table}

\begin{table}[t]
\caption{Comparison of the best and average results accomplished by
various approaches and the best known solutions for the static
version of considered benchmark problems. The best results in each
case are bolded. Our method is presented as 2-phase PSO.}
\begin{center}
\label{tab:comparison}
\begin{tabular*}{1.0\textwidth}{@{\extracolsep{\fill}}|c|rrrrrr|r|}
\hline \multirow{3}{*}{Benchmark\cite{DVRP:Benchmark}} &  \multicolumn{6}{c|}{Algorithm} & \multirow{3}{*}{VRP\cite{DVRP:Best}} \\
 & \multicolumn{2}{c}{2-phase PSO} & \multicolumn{2}{c}{MAPSO\cite{DVRP:MAPSO}} & \multicolumn{2}{c|}{DAPSO\cite{DVRP:DAPSO}} & \\
 & Best & Avarage & Best & Avarage & Best & Avarage & \\
\hline\hline
 c50 & 582.88 & 675.14 & \textbf{571.34} & \textbf{610.67} & 575.89 & 632.38 & 524.61 \\
 c75 &  \textbf{912.23} & 1015.16 & 931.59 & \textbf{965.53} & 970.45 & 1031.76 & 835.26 \\
 c100 & 996.40 & 1149.48 & \textbf{953.79} & \textbf{973.01} & 988.27 & 1051.50 & 826.14 \\
 c100b & \textbf{828.94} & \textbf{850.68} & 866.42 & 882.39 & 924.32 & 964.47 & 819.56 \\
 c120 & \textbf{1087.04} & \textbf{1212.38} & 1223.49 & 1295.79 & 1276.88 & 1457.22 & 1042.11 \\
 c150 & \textbf{1173.94} & \textbf{1336.84} & 1300.43 & 1357.71 & 1371.08 & 1470.95 & 1028.42 \\
 c199 & \textbf{1446.93} & \textbf{1578.99} & 1595.97 & 1646.37 & 1640.40 & 1818.55 & 1291.45 \\
\hline\hline
 f71 & 315.00 & 356.75 & 287.51 & \textbf{296.76} & \textbf{279.52} & 312.35 & 241.97 \\
 f134 & \textbf{12813.14} & \textbf{13491.60} & 15150.50  & 16193.00 & 15875.00 & 16645.89 & 11629.60 \\
\hline\hline
 tai75a & 1871.06 & 2142.07 & \textbf{1794.38} & \textbf{1849.37} & 1816.07 & 1935.28 & 1618.36 \\
 tai75b & 1460.95 & 1568.21 & \textbf{1396.42} & \textbf{1426.67} & 1447.39 & 1484.73 & 1344.64 \\
 tai75c & 1500.23 & 1811.08 & 1483.10 & \textbf{1518.65} & \textbf{1481.35} & 1664.40 & 1291.01 \\
 tai75d & 1462.82 & 1586.28 & \textbf{1391.99} & \textbf{1413.83} & 1414.28 & 1493.47 & 1365.42 \\
 tai100a & 2317.76 & 2707.61 & \textbf{2178.86} & \textbf{2214.61} & 2249.84 & 2370.58 & 2047.90 \\
 tai100b & 2187.86 & 2510.60 &\textbf{ 2140.57} & \textbf{2218.58} & 2238.42 & 2385.54  & 1940.61 \\
 tai100c & 1564.25 & 1672.33 & \textbf{1490.40} & \textbf{1550.63} & 1532.56 & 1627.32 & 1407.44 \\
 tai100d & 1859.70 & 2220.01 & \textbf{1838.75} & \textbf{1928.69} & 1955.06 & 2123.90 & 1581.25 \\
 tai150a & 3638.75 & 4151.31 & \textbf{3273.24} & \textbf{3389.97} & 3400.33 & 3612.79 & 3055.23 \\
 tai150b & 3107.95 & 3302.94 & \textbf{2861.91} & \textbf{2956.84} & 3013.99 & 3232.11 & 2727.99 \\
 tai150c & 2781.02 & 2952.88 & \textbf{2512.01} & \textbf{2671.35} & 2714.34 & 2875.93 & 2362.79 \\
 tai150d & 3048.24 & 3478.49 & \textbf{2861.46} & \textbf{2989.24} & 3025.43 & 3347.60 & 2655.67 \\
\hline\hline
Sum & \textbf{46957.09} & 51770.84 & 48104.13 & \textbf{50349.66} & 50190.87 & 53538.72 & 41637.43 \\
\hline
\end{tabular*}
\end{center}
\end{table}

Keeping solution within the time bounds is achieved by adding a
simple penalty term $pf(vehicles)$ to the vehicles' routes length
estimation function. If despite the $pf$ function, the time bounds are exceeded,
the late client requests are assigned to the closest \textbf{available vehicle},
thus obtaining the feasible solution.

The penalty function is defined as follows:
\begin{equation}
pf(vehicles) = \sum\limits_{v \in vehicles}
    \begin{cases}
    (v.\widehat{time} -
v.depot.end)^2, & v.\widehat{time} > v.depot.end \\
    0, & \rm{otherwise}
    \end{cases}
\end{equation}

where $v.depot$ is the depot to which the vehicle
returns at the end of the working day
and $v.\widehat{time}$ is vehicle estimated travel time.
$v.\widehat{time}$ is equal to the value of the route length evaluation function
for the vehicle\footnote{The time can by unified with the distance
since the vehicle speed is defined as one distance unit per one time
unit.} plus time required to unload the cargo.

\subsubsection*{Passing Down Historic Knowledge.}

As the center of the search space is biased in the search for the global
optimum~\cite{Optimization:BIAS,Optimization:BIAS2}, the center of
the search space for the particular time slice is chosen based on
the best known solution from the previous time slice while missing
arguments (parameters defining the order of requests that were not
present before) are generated at random
with the underlying condition that
at least one particle's initial location is equal to
the best solution previously found.

\section{Tests and Results}\label{sec:tests}

\begin{figure}[t]
\begin{tabular}{lr}
\includegraphics[scale=0.3]{img/c75.ps} &
\includegraphics[scale=0.3]{img/c100b.ps} \\
\includegraphics[scale=0.3]{img/tai100d.ps} &
\includegraphics[scale=0.3]{img/f71.ps} \\
\end{tabular}
\caption{Benchmark problems examples with clients' requests having
spatially uniform location and symmetric volume distributions (c75),
spatially clustered location and skewed volume distributions
(c100b), spatially semiclustered location and largely skewed volume
distributions (tai100d), and spatially clustered location and
largely skewed volume distributions (f71) } \label{fig:distr}
\end{figure}

\subsubsection{Benchmark Sets.}
The algorithm was tested on the same benchmark sets as the ones used
in~\cite{DVRP:DAPSO,DVRP:MAPSO,DVRP:Ants,DVRP:GA:TS}. The best
achieved value, the mean value, the standard deviation, the relation
to the best known static solution and the value of $dod$ for each of
the tested problems are presented in Table~\ref{tab:results}.
Table~\ref{tab:comparison} shows the best achieved values of the
algorithm compared to other PSO approaches
\cite{DVRP:DAPSO,DVRP:MAPSO} for solving the DVRP.

\subsubsection{Tests Configuration.}
For all tests the system was run with the following parameters,
suggested in~\cite{PSO:Params,PSO:Inertia}: $g = 0.6$, $l = 2.2$, $r
= 0.0$, $a = 0.63$, $f = 0.0$ in eq. (\ref{eq:PSO_vi}). A random
star topology with probability $P(X$ is a neighbour of $Y) = 0.7$
was applied\footnote{Note, that the fact that $X$ is a neighbour of
$Y$ does not mean that $Y$ is a neighbour of $X$.}.

The algorithm was run 150 times for each of the benchmark problems.
In each case, the number of particles was equal to $40$, the number of iterations
for each time slice was restricted to $500$ (in each of the two phases of
the algorithm). Thus the total number of function evaluations for
the first phase was equal to $500\,000$ and the number
of function evaluations for the second phase was
about $4\,000\,000$ (depending on the number of vehicle routes).

\subsubsection{Choice of the Parameters.}
Parameter $P(X$ is a neighbour of $Y) = 0.7$ was empirically chosen
from the set of $\lbrace 0.5, 0.6, 0.7, 0.8, 0.9, 1.0 \rbrace$ based
on a small number of runs on benchmark problem's instances. The
other parameters were left at their suggested values (from the
literature).

\section{Discussion and Conclusions}
The DVRP discussed in this paper is an important problem not only
because of its real world applications but also a possibility to
study continuous optimization algorithms in solving dynamic
combinatorial tasks.

The proposed algorithm achieved better results than other PSO
approaches in 6 out of 21 test problems. On Christofides benchmark
set, 2-phase PSO best solution was averaged out at 0.96 times the
length of the MAPSO and DAPSO best results. On Fisher and Taillard
benchmark sets it was 0.99 and 1.06, respectively. The overall
result was on average 1.02 times longer in comparison with the best
results from 2-phase PSO and other approaches and 1.09 times longer
in terms of the average results.

Closer analysis of the clients' requests locations and volume
distributions reveals that the proposed algorithm performs better
for more spatially clustered benchmarks (e.g. c100b) and for more
symmetric volume distributions (e.g. c75).

The algorithm's worst performance on the f71 benchmark set, may be
related to the configuration of the largest and the second-largest
request in this instance of a DVRP. Those two requests are in a
close distance to each other and the joint volume of those requests
slightly exceeds the capacity of a vehicle, making the BPP part of
the task hard to solve efficiently. Moreover the largest request is
a strong outlier in terms of the request volume. It is more then
twice the size of the second-largest request in this particular set.
In all other test sets this ratio never exceeds 1.37. The examples
of the clients' requests location and volume distributions are
presented in Fig.~\ref{fig:distr}.

Overall, the results suggest that the DVRP may be efficiently solved
by the PSO even without its hybridization with 2-Opt algorithm.

Further research of the proposed algorithm will include the use of
alternative encodings of the clients' assignments, further analysis
of the impact of locations and requests distribution on the
algorithm's performance, and application of a~multi-swarm approach.

\subsubsection*{Acknowledgements.} Study was supported by research fellowship within "Information technologies: Research and their interdisciplinary applications" agreement number POKL.04.01.01-00-051/10-00.

The analysis of the requests distribution within the tests sets
has been done with the usage of R\cite{R}.

\bibliographystyle{splncs03}
\bibliography{PSO_in_DVRP}
\end{document}
