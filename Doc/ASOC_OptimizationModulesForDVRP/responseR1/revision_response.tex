\documentclass[10pt,a4paper]{letter}

\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage[table]{xcolor}

\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{url}
\usepackage[final]{changes}

\usepackage{graphicx}

\usepackage{letterbib}

\hyphenation{presence}

\newcommand{\initresponses}{\newcounter{pointcounter}}

\newenvironment{reviewer}{\setcounter{pointcounter}{1}}{}

\newcommand{\point}[1]{\medskip \noindent
               \textsl{{\fontseries{b}\selectfont Comment \#\thepointcounter}.
                 \stepcounter{pointcounter} #1}}
\newcommand{\reply}{\medskip \noindent \textbf{Reply:}\ }


\signature{\vspace{-4em}Michał Okulewicz\\
Jacek Mańdziuk}

\begin{document}
\begin{letter}{}
\opening{Dear Professors Bas van Vlijmen and Richard Allmendinger,}

We would like to thank the Reviewers and the Associate Editor for their kind remarks,
which have helped us to substantially improve the manuscript.

The major changes made to the manuscript are as follows:
\begin{itemize}
	\item results from the initial version of the 2MPSO algorithm
	presented at the IEEE SSCI / CIHLI 2014 conference, have been presented
	in Table 7, for the reference.
	\item 2MPSO modules description has been significantly shortened
	and moved to the introductory part of Section 4 (devoted to description of 2MPSO).
	\item Solution encodings for the PSO algorithm and a description of the solution transfer methods have been moved after the presentation of the overall optimization process activities.
\end{itemize}

In addition, the paper has been formatted without a double interline, so as to be able to present figures and tables in appropriate
places, i.e. in relation to the text.
Below are our detailed responses to the reviewers’ comments.

\initresponses
\begin{reviewer}
\textbf{\\ \Large Response to Reviewer \#1}

\point{The authors have answered my queries satisfactorily. Thus, I recommend the acceptance of the paper.}

\reply
Thank you.

\end{reviewer}

\begin{reviewer}
\textbf{\\ \Large Response to Reviewer \#2}

\point{The manuscript really improved after recent changes, however, it still requires several modifications to reach appropriate technical clarity. This regards Section 4 in the first place.
1. As Fig 2 seems crucial to understand the algorithm,  I would like to focus on it:
a) Syntax. Actually, I have never seen such use of guards within forks.  Typically guards folow a decision node and optional activities are modeled like in this drawing: \href{http://www.itmeyer.at/umlet/uml2/img/AC_dataflow.jpg}{http://www.itmeyer.at/umlet/uml2/img/AC\_dataflow.jpg} (upgrade Passenger)}

\reply
We do agree that such a guard condition on a fork is not syntactically correct in UML.
However, we have intentionally omitted the ``decision node - action node - merge node'' syntax
for the sake of the diagram readability.
If the upper-right part of the activity diagram would be 100\% syntactically correct it would look as follows:

\includegraphics[width=\textwidth,page=2]{../images/2MPSOOptimizationProcess}

Inserting that part in such a form would mean, either using a smaller font
in the whole diagram or making an even more disturbed top-down flow of the process.

In order to address the issue of using a simplified notation,
we have added a related explanation in the caption of Figure 2.

\point{b)  Are really these three activities parallel (Adapt previous assignments..., Compute centroids…)?}

\reply
Those three activities are independent.
Therefore, it is correct to model that fact with a ``fork node - action nodes - join node'' syntax.

\point{c) I feel somehow confused. The activity diagram shows a four-stage process, which progresses from the start to the end node without loops.  There are four activities related to optimization. As there is no loop within the activity diagram and no such activity as evaluation exists, I assume that each optimization activity employs a certain goal function and makes a number of iterations. However, I am not sure if the function given by formula (1) is always used. For example, it does not seem appropriate for "Optimize requests-to vehicles assignment by clustering requests with PSO"}

\reply
The activity diagram presents
a single optimization process during a single time step (i.e. for a ``frozen'' state of the problem).
The three following procedures ``Optimize route in each vehicle by ordering requests with 2-OPT'', ``Optimize requests-to-vehicles assignment by clustering requests with PSO'' and ``Optimize route in each vehicle by ordering requests with PSO'' use the route evaluating $COST(r_1,r_2,\ldots,r_n)$ (eq. 1) and $PENALTY(r_1,r_2,\ldots,r_n)$ (eq. 10) functions.

``Optimize requests-to-vehicles assignment...'', as stated in the note in the diagram, uses 2-OPT algorithm on each of the requests set assigned to a single vehicle (making the COST function possible to apply),
but the algorithm operates in a search space of cluster centers/Voronoi seeds (as described in subsection ``Requests-to-vehicles assignment encoding'')
In terms of a function evaluation, it could be presented as a composition
of the COST function and the 2-OPT algorithm $COST(2OPT(r'_1),2OPT(r'_2),\ldots, 2OPT(r'_m)))$
applied to a set of random $r'_i$ routes created from the clustered requests.

Additional explanation about using 2--OPT to obtain routes before evaluating
requests assignment is given in lines {\replaced{312}{313}, 397 and \replaced{446}{447}}.

\deleted{The whole optimization process, as described in introduction to Section 4.1, might be clarified with the figure previously proposed as the graphical abstract:}
%\includegraphics[width=0.75\textwidth]{../images/GraphicAbstract}

\point{Line 308 "The single optimization process of the 2MPSO algorithm consists of the following modules."
Basically, a module is rather a kind of software unit, while a process consists of activities. Modules were correctly marked on the activity diagrams as swimlanes, which correspond to entities that execute activities.}

\reply
\added{We start a description of the 2MPSO with defining its main components which we call ``modules'',
which are used as a background for presenting the activities. We agree that the word ``consists'' may be misleading in this context and therefore have replaced it with ``utilizes''.}
{
The sentence \added{(lines 304--305)} has been changed to ``[...] the 2MPSO algorithm utilizes the following modules.''
}

\point{Once again I would like to repeat the previous recommendation "Section 4 should provide the  algorithm  description guided by the Fig. 2, i.e. marked activities should be easily mapped on subsections."
a) Please observe, that authors and readers  usually differ  in text perception. I  understand that authors can be focused on  implemented modules, however, as a reader I am not interesetd in  them that much (until I would like to reuse the software). More intriguing are subsequent  algorithms steps, their goals, inputs, outputs, applied solutions, etc.  For example, instead of the description of the greedy module I would prefer separate descriptions of two activities that fall under greedy (placed in the correct order within the workflow).
b) As there are numerous activities, reader gets easily lost. I suggest assigning numbers to them (e.g. 1.1, 1.2, 2.1, 2.x, 3.x, 4.x) and map them somehow onto subsections, where each step is described.}

\reply
We have changed the order of the presentation retaining only
a half a page module description in the introduction,
in order to address the points raised in the above comments.
The description of 2MPSO has now the following structure:
\begin{itemize}
	\item 2MPSO development history and enhancements proposed in this paper.
	\item Short description of 2MPSO modules and execution branches.
	\item Activity diagram of a single optimization process.
	\item Activities description (a context in which a single optimization process is performed, followed by a discussion on actions in the initialization, 1st, 2nd and closing phases).
	\item Encodings utilized by the PSO for request-to-vehicles assignment and requests ordering.
	\item Solution transfer and adaptation methods.
\end{itemize}

\point{Algorithm 1:  please describe CreateSeparateTrees(V )?}

\reply
It creates the initial set of Trees containing single unassigned requests or routes of ultimately assigned requests.
An additional explanation has been given in a comment in the pseudo-code listing in Algorithm 2.

\point{Figure 3 The marked centers do not seem to be cluster centers? Was it drawn by hand or programmatically, as the underlying Voronoi tessallation also seems incorrect?}

\reply
The figure has been drawn by hand. These are the cluster centers in a sense of k-means
clustering before location update phase\deleted{ (maybe cluster seeds or Voronoi seeds would have been a more appropriate choice of a term)}.

\point{Algorithm 3: which goal function is actually used in Evaluate(swarm).}

\reply
As mentioned above in response to comment \#3, it is the COST function combined with 2--OPT algorithm $COST(2OPT(r_1),2OPT(r_2),\ldots, 2OPT(r_m)))$ applied to random routes.

\point{Section 4.3.2: it seems that two algorithm steps (Fig.2) are mixed?}

\reply
In the revised version of the manuscript those actions (initial ordering of requests by 2-OPT within evaluating requests' clusters and final ordering)
are presented in a separate subsections.
\added{The results of a previous step (requests-to-vehicles assignment with initial route ordering) are now mentioned only as an input
to the subsequent step (final routes ordering)}.


\point{Table 5. Column names are unclear. I understand that the dash is interpreted as switched off?
Hence, the first colum gives values for 1PSO+Dhist+Chist+Tree? Such presentation style is more common. Usually it is better to show a combination of elements (actually it was used in line 599).}

\reply
The dash was meant to be a minus sign, and its meaning was to be interpreted as switched off.
We have changed the names to the switched-on convention.
We retained denoting 1PSO+DHist+CHist+Tree as 2MPSO in subsequent tables, for comparison with the literature results.

\point{Table 6: What is the intended meaning of gray cell background?}

\reply
Statistically significant difference of average results between
the best average from the literature and our own approach.
Additional explanations have been given in the captions of Tables 6. and 7. 

\point{Line 620 (and subsequent lines): it seems that the correct term is bounded (as bounded set, bounded function)?
}

\reply
Thank you. We have made the suggested changes.

\end{reviewer}

\begin{reviewer}
\textbf{\Large\\ Response to Reviewer \#3}

\point{Within this paper, the authors present a PSO-based algorithm for tackling the Dynamic VRP. As this is the first revision, I mainly focus on my original points. It seems that all of them have been satisfactionally addressed in one or another way. One major weak point is still the graphic abstract. In my understanding this graphic abstract should be something like a stand-alone poster (without much text). I think, this needs to be overworked. I therefore, suggest to accept the paper with minor modifications.}

\reply
Graphical abstract has been redesigned in accordance with the above comments and the examples provided by the Elsevier on the following webpage:\\
\href{https://www.elsevier.com/authors/journal-authors/graphical-abstract#new_content_container_83816}{https://www.elsevier.com/authors/journal-authors/graphical-abstract}

\end{reviewer}

\begin{reviewer}
\textbf{\\ \Large Response to Reviewer \#4}

\point{This paper presents and analyzes a Two-Phase Multi-Swarm Particle Swarm Optimizer (2MPSO) solving the Dynamic Vehicle Routing Problem (DVRP). However, there are some related papers (e.g., Michał Okulewicz, Jacek Mańdziuk, Two-phase multi-swarm PSO and the dynamic vehicle routing problem, 2014 IEEE Symposium on Computational Intelligence for Human-like Intelligence (CIHLI), DOI: 10.1109/CIHLI.2014.7013391). Furthermore, the revised version lacks the citation of related papers (especially, the similar paper written by yourself) and comparisons to show your improvement.
In general, this revised version is still not novel and shows no contribution in the current form.}

\reply
Our article published in the CIHLI 2014 proceedings is (and has been in the previous version of the manuscript) referenced as [6] in the manuscript. Other published works of the authors, concerning the subject of the paper, are positioned as [14] and [22], while [39] points to the website of our research project.

Table 7. presenting the results of FFE bounded experiments has been
completed with the results from the above-mentioned conference proceedings' paper.
As stated in the conclusion of the current version of the manuscript:
\textit{The average results of the 2MPSO implementation presented in this paper outperform on its initial version (from the year 2014) [6] by \textbf{3.2\%}.}

We would also like to emphasize, that while the version of 2MPSO presented in this paper, due to upgrades described in the introduction to Section 4., performs better than initial 2MPSO version [6], the main contribution of this paper is the analysis of an impact of its particular components on the quality of achieved solutions (cf. Tables 4. and 5.).

\end{reviewer}

\closing{Yours sincerely,}
\end{letter}

\bibliographystyle{plain}
\bibliography{response}

\end{document}
