\documentclass[conference]{IEEEtran}
\usepackage{stmaryrd}
\usepackage{amsfonts}

\usepackage{graphicx,times,amsmath}
\usepackage[caption=false,font=footnotesize]{subfig}

\IEEEoverridecommandlockouts

\textwidth 178mm    % <------ These are the adjustments we made 10/18/2005
\textheight 239mm   % You may or may not need to adjust these numbers again
\oddsidemargin -7mm
\evensidemargin -7mm
\topmargin -6mm
\columnsep 5mm

\textfloatsep 0mm

\begin{document}

\title{\ \\ \LARGE\bf Monte Carlo method for Dynamic Vehicle Routing Problem\thanks{Micha{\l} Okulewicz and Jacek Ma{\'n}dziuk are with the Faculty of Mathematics
and Information Science of Warsaw University of Technology, Warsaw, Poland (email: \{{M.Okulewicz, J.Mandziuk}@mini.pw.edu.pl).}}

\author{Micha{\l} Okulewicz and Jacek Ma{\'n}dziuk}

\maketitle

\begin{abstract}
In this paper we propose a new approach for solving Dynamic Vehicle Routing Problem.
In this approach we generate unknown requests in order to create a safety
capacity and time buffer in the vehicles' routes.
\end{abstract}

\section{Introduction}
\PARstart{D}{}ynamic Vehicle Routing Problem (DVRP) is a hard
combinatorial optimization problem. It is a generalization of the
Traveling Salesman Problem (TSP) with additional travel time and
vehicle capacity constrains. In the problem instances considered in
this paper new clients' requests may arrive during the whole working
day.

The problem is demanding for both humans and machines. When solving
the DVRP people heavily rely on their life experience, imagination and
the ability to develop geometry-based graphical solutions. While
life experience and imagination are, to a large extent, beyond the
scope of current machines' capabilities, the ability to move in a
``geometrically-guided'' way in the search space in order to detect
the optimal cluster centers for individual vehicles' routes can be,
apparently quite effectively, accomplished by artificial agents. One
of such possibilities, based on the PSO meta-heuristic is proposed
in this paper.

In each point in time the DVRP may be looked at as a combination of
the two NP-Complete problems: the Bin Packing Problem (BPP) for
assigning the requests to vehicles and the TSP for finding an
optimal route for a given vehicle. Such a combination may be
effectively solved by approximate or metaheuristic algorithms,
e.g.~\cite{CARP:MA2},\cite{DVRP:GA:TS}, \cite{DVRP:Ants}. The
solution method chosen by the authors consists in applying Particle
Swarm Optimization (PSO) algorithm to solving both of these
sub-problems~\cite{DVRP:2PSO}. To the best of our knowledge it is the
first published attempt of applying a two-phase PSO approach to solving the DVRP\footnote{Division 
of a solution method into clustering and
routes' optimization phases was previously proposed in several
methods that were rooted in the Operational Research fields.}. 


\section{DVRP Definition}
\label{sec:dvrp}

In the class of Dynamic Vehicle Routing Problems discussed in this
article one considers a fleet $V$ of $n$ vehicles and a series $C$
of $m$ clients (requests) to be served (a cargo is to be delivered
to them).

The fleet of vehicles is homogeneous. Vehicles have identical $capacity
\in \mathbb{R}$ and the same $speed$~\footnote{In all benchmarks
used in this paper $speed$ is defined as one distance unit per one
time unit.} $\in \mathbb{R}$.

The cargo is taken from a dedicated depot $d$ which has a certain
$location_0 \in \mathbb{R}^2$ and working hours $(start, end)$,
where $0 \leq start < end$.

Each client $c_l, l=1,\ldots, m$ has a given $location_l \in
\mathbb{R}^2$, $time_l \in \mathbb{R}$, which is a point in time
when their request becomes available ($start \leq time_l \leq end$),
$unld_l \in \mathbb{R}$, which is the time required to unload the
cargo, and $size_l \in \mathbb{R}$ - size of the request ($size_l
\le capacity$).

A travel distance $\rho(i,j)$ is the Euclidean distance between
$location_i$ and $location_j$ on the $\mathbb{R}^2$ plane,
$i,j=0,\ldots, m$.

The $route_i$ of vehicle $v_i$ is a series of $p_i$ locations,
where $location_{i_1}$ and $location_{i_{p_i}}$ are locations of a depot
and a series of $p_i$ time points of arrivals at those locations
(denoted $arv_r$ for $location_r$).

As previously stated, the goal is to serve the clients (requests),
according to their defined constraints, with minimal total cost
(travel distance). Formally, the optimization goal and constraints can be written as:
\begin{equation}
\label{eq:goal}
\begin{split}
    \text{min} & \sum\limits_{i=1}^n \sum\limits_{r=1}^{p_i} \rho({i_{r-1}},{i_{r}}) \\
    \forall_{i \in [n]}\forall_{r \in [p_i]/\ \lbrace 1 \rbrace}            & arv_{i_r} \geq arv_{i_{r-1}} + \rho({i_{r-1}},{i_{r}}) + unld_{r_{i-1}} \\
    \forall_{i \in [n]} & arv_{i_1} \geq start_{i_1} \\
    \forall_{i \in [n]} & arv_{i_p} \leq end{i_p} \\
    \forall_{i \in [n]} & \sum\limits_{r=2}^{p_i-1} size_{r} \leq capacity \\
    \forall_{l \in \lbrace 1,2,\ldots m \rbrace} \exists!_{i \in [n]} & location_{l+k} \in route_i
\end{split}
\end{equation}

Please note that, according to equation (eq.~\ref{eq:goal}), each
client must be assigned to exactly one vehicle and all vehicles must
return to the depot before its closing.


\section{Solving the DVRP}
\label{sec:solving}

There are two general approaches to solving dynamic optimization
problems. In the first one the optimization algorithm is run every
time there is a change in the problem instance. In the second
approach time is divided into discrete slices and the algorithm is
run once for each time slice. Furthermore, the problem instance is
considered "frozen" during the whole time slice, i.e. any potential
changes introduced during the current time slot are handled in the
next algorithm's run (in the subsequent time slice period).

In our study we follow the second approach which, in the context of
the DVRP, was proposed by Kilby et al.~\cite{DVRP:Study}. In order to
assure a direct comparison of obtained results with our previous
work~\cite{DVRP:2PSO} and with other PSO-based
approaches~\cite{DVRP:DAPSO,DVRP:MAPSO}, the number of time slices
of the working day is equal to $25$.

Another critical DVRP parameter, which has a direct impact on ``the
degree of dynamism'' of a given problem instance, is the \emph{cut
off time} which defines the part of requests that is known at the
beginning of the working day. In real (practical) situations the
requests received after this time threshold are treated as received
at the beginning of the subsequent working day. In the
one-day-horizon simulations presented in this paper (as well as in
practically all other papers referring to Kilby et al.'s
benchmarks~\cite{DVRP:Benchmark}) the requests located after the cut
off time limit are simply treated as being known at the beginning of
the current day - they compose an initial instance of the DVRP being solved.

In the experiment presented in the paper, in order to allow a direct
comparison with previous works, the cut-off time is set in the
middle of a depot's working hours, i.e. lasts for half of a day.


\section{Problem Encoding}
\label{sec:encoding}

\section{Requests generation}
\label{sec:generation}
In order to approach 

\section{Solving algorithm}
\label{sec:algorithms}
As a solving algorithm we use a combination of two heuristic algorithms
in order to generate the assignment of the requests to the vehicles
we solve a capacitated clustering problem with the usage of the modified
Kruskal algorithm and optimize the routes with 2-OPT algorithm.

\section{Knowledge transfer}
\label{sec:transfer}

\subsection{Knowledge transfer between time slices}

In the MAPSO algorithms Khouadjia et al. proposed adding an adaptive
memory to each particle, in order to store its best known solution
(the vector of vehicles identifiers which are assigned to each of
the requests) from the previous time slice. When new requests
arrive, they are processed in a random order and assigned to
vehicles by a greedy algorithm, thus forming the initial swarm
locations for the PSO algorithm.

In the 2(M)PSO method a different approach is taken. Since the
solution of the first phase in the previous time slot consists of
locations of clusters centers, these coordinates are treated as
reliable estimations of the clusters centers after the arrival of
new requests (in the next time slice). Therefore initial swarm
location is defined around the center of the previous best known
solution within a given radius.


\section{Results}
\label{sec:results}


\section{Discussion and conclusions}
\label{sec:discussion}


\section*{Acknowledgment}
The research was financed by the National Science Centre
in Poland grant number DEC-2012/07/B/ST6/01527 and by the research 
fellowship within "Information
technologies: Research and their interdisciplinary applications"
agreement number POKL.04.01.01-00-051/10-00.
%MO - dodałem też IPI - skoro mają pokryć część kosztów konferencyjnych


\IEEEtriggeratref{10}
\bibliographystyle{IEEEtran}
\bibliography{IEEEabrv,MonteCarloForDVRP,MonteCarloForDVRPxref}

% JM1 - refs [1], [9], [12] i [13] sa niemkompletne
% JM1 - w ref [12] dodatkowo brakuje kreski nad n w moim nazwisku
% MO1 - poprawione

\end{document}
