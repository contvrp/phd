\documentclass{llncs}
\usepackage{amsfonts}

\usepackage{graphicx,times,amsmath}
\usepackage[caption=false,font=footnotesize]{subfig}
\usepackage{nameref}

\begin{document}
\setlength{\floatsep}{4pt}
\setlength{\textfloatsep}{4pt}

\title{Two-Phase Multi-Swarm PSO and the Dynamic Vehicle Routing Problem}
\titlerunning{Application of 2MPSO Algorithm to DVRP}
\author{Micha{\l} Okulewicz \and Jacek Ma{\'n}dziuk}

\institute{Warsaw University of Technology,\\
Faculty of Mathematics and Information Science,\\
Koszykowa 75, 00-662 Warsaw, Poland\\
\{M.Okulewicz, J.Mandziuk\}@mini.pw.edu.pl\\}

\maketitle

\begin{abstract}
In this paper we compare two types of Particle Swarm Optimization
approaches to solving Dynamic Vehicle Routing Problem. Several
evaluation functions and problem encodings are tested and a new
multi-swarm approach is proposed and compared with previously
introduced single-swarm method. For the cut-off time set at $0.5$
(i.e. in the middle of a day) our method found new best-literature
results for $15$ out of $21$ tested benchmark sets. On average, the
improvement of the mean routes' lengths equals~3\% for comparable
test conditions (the same numbers of evaluations of the fitness
function).
\end{abstract}

\section{Introduction}

Dynamic Vehicle Routing Problem (DVRP) is a~hard
dynamic combinatorial optimization problem. It is a~generalization
of the Traveling Salesman Problem (TSP) with additional travel time
and vehicle capacity constrains. In the problem instances considered
in this paper new clients' requests may arrive during the whole
working day.

In each point in time the DVRP may be looked upon as a~combination
of the two NP-Complete problems: the Bin Packing Problem (BPP) for
assigning the requests to vehicles and the TSP for finding an
optimal route for a~given vehicle. Such a~combination may be
effectively solved by approximate or metaheuristic algorithms,
e.g.~\cite{CARP:MA2},\cite{DVRP:GA:TS}, \cite{DVRP:Ants}. In this
paper, the latter approach is chosen and the Particle Swarm
Optimization (PSO) algorithm is used to solve both of these
sub-problems~\cite{DVRP:2PSO}. In effect, we significantly improved
our previous approach by introducing a~new multi-swarm algorithm,
which is tested for two different fitness functions. We compare our
results with the state-of-the-art solutions, and for the cut-off
time set in the middle of a~day we present new best results in the
case of $15$ out of $21$ benchmark problems used in our experiments.
%
%The rest of this paper is organized as follows. In
%section~\ref{sec:pso} the PSO algorithm and its parameters are
%briefly presented. In section~\ref{sec:dvrp} mathematical model for
%DVRP is given. In the subsequent sections (\ref{sec:solving},
%\ref{sec:encoding} and \ref{sec:transfer}) various problem encodings
%and fitness functions for the PSO method are proposed and discussed.
%Finally, sections~\ref{sec:results} and~\ref{sec:discussion} present
%the results and conclusions.

\section{Particle Swarm Optimization}
\label{sec:pso}
PSO is an iterative global optimization metaheuristic method
proposed in 1995 by Kennedy and Eberhart~\cite{PSO:Introduction} and
further studied and developed by many other researchers,
e.g.,~\cite{PSO:Inertia},
%\cite{PSO:Modified},
\cite{PSO:Params}. In short, PSO utilizes the idea of swarm
intelligence to solve hard optimization tasks. The basic idea of the
PSO algorithm consists in maintaining the swarm of particles moving
in the search space. Particles which communicate (to a~given
particle) their position and function value in that position are
called neighbors of that particle. Each particle maintains its
current position, velocity and a~set of neighbors, as well as
remembers its historically best visited location.
\subsubsection*{Update position} In
every step $t$, position of particle $i$, $x^i_t$ is updated based
on particle's velocity $v^i_t$:
%
\begin{equation}
    x^i_{t+1} = x^i_t + v^i_t.
\end{equation}

\subsubsection*{Update velocity} In our implementation of PSO (based on~\cite{Algorithm:PSO2011}
and~\cite{PSO:Inertia}) velocity $v^i_{t}$ of particle $i$ is
updated according to the following rule:
\begin{equation}
\begin{split}
    v^i_{t+1} = & u^{(1)}_{U[0;g]} (x^{neighbors_i}_{best} - x^i_t)  +
    u^{(2)}_{U[0;l]} (x^i_{best} - x^i_t) +
    a~v^i_t,\label{eq:PSO_vi}
\end{split}
\end{equation}
where $g$ is a~neighborhood attraction factor,
$x^{neighbors_i}_{best}$ represents the best position in terms of
optimization, found hitherto by the neighborhood of the $i$th
particle, $l$ is a~local attraction factor, $x^i_{best}$ represents
the best position in terms of optimization, found hitherto by
particle $i$, $a$ is an inertia coefficient, $u^{(1)}_{U[0;g]}$,
$u^{(2)}_{U[0;l]}$ are random vectors with uniform distribution from
the intervals $[0,g]$ and $[0,l]$, respectively.
Please note, that according to equation (\ref{eq:goal}), each
client must be assigned to exactly one vehicle.

In the experiments, parameters of the PSO were set in the following
way: $g = 0.60$, $l=2.20$, $a=0.63$, $P(X \text{ is a~neighbor of }
Y) = 0.50$, $\#$iterations $\in \left\lbrace 50,250,1000
\right\rbrace$, $\#$particles $\in \left\lbrace 20,40,100
\right\rbrace$, $\#$swarms $\in \left\lbrace 1,8 \right\rbrace$.

\section{DVRP Definition}
\label{sec:dvrp}

In the class of Dynamic Vehicle Routing Problems discussed in this
article one considers a~fleet $V$ of $n$ vehicles and a~series $C$
of $m$ clients (requests) to be served.
The fleet of vehicles is homogeneous, they have identical $capacity \in \mathbb{R}$ and the same $speed$\footnote{In all benchmarks used in this paper $speed$ is
defined as one distance unit per one time unit.} $\in
\mathbb{R}$.
The cargo is taken from one of the $k$ depots\footnote{In all benchmarks used in
this paper $k = 1$.}.
Each depot $d_j, j=1,\ldots,k$ has a~certain $location_j \in
\mathbb{R}^2$ and working hours $(start_j,
end_j)$, where $0 \leq start_j < end_j$.

Each client $c_l, l=k+1,\ldots, k+m$ has a~given $location_l \in
\mathbb{R}^2$, $time_l \in \mathbb{R}$, which is a~point of time
when their request becomes available ($\min\limits_{j \in
1,\ldots,k}start_j \leq time_l \leq \max\limits_{j \in
1,\ldots,k}end_j$), $unld_l \in \mathbb{R}$, which is the time
required to unload the cargo, and $size_l \in \mathbb{R}$ - size of
the request ($size_l < capacity$).

A travel distance $\rho(i,j)$ is an Euclidean
distance between $location_i$ and $location_j$ on the $\mathbb{R}^2$
plane, $i,j=1,\ldots, m+k$.

The $route_i$ of a~vehicle $v_i$ is a~series of locations and
associated time points which starts and ends in the depot location.
%locations are the where $location_{i_1}$ and
%$location_{i_p}$ are locations of a~depot and a~series of $p_i$ time
%points of arrivals at those locations (denoted $arv_r$) for
%$location_r$).

As previously stated, the goal is to serve the clients (requests),
according to their defined constraints, with minimal total cost
(travel distance).

Formally, the DVRP can be defined as follows:
\begin{equation}
\label{eq:goal}
\begin{split}
   & \text{min}  \sum\limits_{i=1}^n \sum\limits_{r=2}^{p_i}  \rho({i_{r-1}},{i_{r}}) \\
   & \forall_{i \in [n]}\forall_{r \in [p_i]/\ \lbrace 1 \rbrace}             arv_{i_r}  \leq arv_{i_{r-1}} + \rho({i_{r-1}},{i_{r}}) + unld_{r_{i-1}} \\
   & \forall_{i \in [n]}  arv_{i_1} \geq start_{i_1} \text{,}\,\,\,
    \forall_{i \in [n]}  arv_{i_p} \leq end{i_p} \\
   & \forall_{i \in [n]}  \sum\limits_{r=2}^{p_i-1} size_{r} \leq capacity
   \text{,}\,\,\,
        \forall_{l \in \lbrace 1,2,\ldots m \rbrace} \exists!_{i \in [n]}  location_{l+k} \in route_i
\end{split}
\end{equation}

\section{Solving the DVRP}
\label{sec:solving}

There are two general approaches to solving dynamic optimization
problems. In the first one the optimization algorithm is run each
time when there is a~change in the problem instance. In the second
approach, time is divided into discrete slices and the algorithm is
run once for each time slice. Furthermore, the problem instance is
considered "frozen" for the whole time slice, i.e. any potential
changes introduced during the current time slot are handled in the
next algorithm's run (in the subsequent time slice period).

In our study we follow the latter approach which, in the context of
DVRP, was proposed by Kilby et al.~\cite{DVRP:Study}. In order to
assure a~direct comparison of obtained results with our previous
work~\cite{DVRP:2PSO} and with other PSO-based
approaches~\cite{DVRP:DAPSO,DVRP:MAPSO}, the number of time slices
of the working day is equal to $25$.

In order for the problem instances from the benchmark
sets~\cite{DVRP:Benchmark} to be solvable under the time
constraints, a~cut-off time parameter that decides which part of the
requests is known at the beginning of the working day, needs to be
set. The requests received after this time threshold are treated as
received at the beginning of the subsequent working day. In this
paper the cut-off time is set at the half of the day.

\section{Problem Encoding}
\label{sec:encoding} Due to natural graph-based DVRP representation,
various problem encodings have been tested in the literature.
Khouadjia et al. proposed Dynamic Adapted PSO (DAPSO) (and its
multi-swarm equivalent Multiswarm Adaptive Memory PSO
(MAPSO))~\cite{DVRP:DAPSO,DVRP:MAPSO} which uses a~discrete
% JM - discrete zamiast integer
version of the PSO to solve the DVRP. In our approach, denoted by
2-Phase Particle Swarm Optimization (2PSO)~\cite{DVRP:2PSO}, we
propose splitting the process of solving DVRP into two phases: a
clustering phase, in which requests are assigned to particular
vehicles, and an ordering phase, in which the tour for each vehicle is found with the use of (a separate
instance of) the standard PSO algorithm. In this paper we present
two versions of this method differing by the fitness
functions used in the first phase.

Additionally, we introduce a~2-Phase Multiswarm PSO (2MPSO)
algorithm, for both of the proposed versions of the 2PSO algorithm.
The main difference between a~single- and multi- swarm versions is a~need for
synchronization of problems between swarms in the latter case,
discussed in section~\ref{sec:transfer}.

\subsubsection{MAPSO/DAPSO Algorithms}
The main features of Khouadjia et al.'s
approach~\cite{DVRP:DAPSO,DVRP:MAPSO} are as follows:
\begin{itemize}
\item Each particle represents an integer vector containing
identifiers of the vehicles assigned to each of the served
requests (see section \nameref{sec:enc.examp}).
\item The route for each of the vehicles is constructed by a~greedy algorithm and improved by the 2-OPT
method~\cite{TSP:2OPT}.
\item The fitness function value is obtained as a~sum of all vehicles routes' lengths (see Fig.~\ref{fig:RoutesFunctions}).
\end{itemize}

\subsubsection{2(M)PSO}
In the first version of our 2PSO approach~\cite{DVRP:2PSO} (see
Fig.~\ref{fig:activ}) the following problem representation is used:
\begin{itemize}
    \item In the first phase, each particle represents a~real numbers vector of centers of clusters of requests assigned to vehicles (see section \nameref{sec:enc.examp}).
    \item The fitness function value in the first phase is calculated as a~sum of distances from
    the inter-cluster requests to the clusters' centers (a measure of quality of a~clustering) and the twice the distances from the clusters' centers to the depot location (a measure of a~cost of creating a
    cluster), see Fig.~\ref{fig:RoutesFunctions}.
    \item In the second phase each particle represents an ordering of requests assigned to a~given
    vehicle (please recall that each cluster/vehicle is solved by a~separate PSO
    instance).
    \item The fitness value in the second phase (in each of the PSO instances) is equal to the length of a~route (for a~given vehicle) defined by the proposed
    ordering.
    \item The final value is the sum of fitness functions' values
    of the best solutions found by each of the PSO instances.
\end{itemize}

\subsubsection{2(M)PSOv2 (version 2)}
The second version of the 2PSO algorithm differs from the
above-described basic variant, by the fitness function used in the
first phase. In spite of the fact, that the routes have not yet been
optimized by the PSO algorithm, the same function as in DAPSO and
MAPSO algorithms, i.e. the total length of all vehicles' routes from
the proposed clusters, formed with the use of 2-OPT, is used here.

If we denote by $m$ the number of requests ($50 \le m \le 199$ in
the benchmarks used), by $n$ the number of available vehicles ($n =
50$ in the tested benchmarks), then the theoretical and the experimental dimension
sizes of the search spaces are as follows:
\begin{center}
\begin{tabular}{l|l|c|c}
Algorithm & Search space type & Theoretical size & Experimental size \\ \hline
MAPSO/DAPSO & Discrete & $m$ & $[50,199]$ \\
2(M)PSO & Continuous & $2n$ & $100$ \\
2(M)PSO ver. 2 & Continuous & $2n$ & $100$ \\
\end{tabular}
\end{center}
%
%
\subsubsection{Examples of Encodings and Fitness Function Calculations}
\label{sec:enc.examp}
\begin{figure}[t]
\centering
\vspace{-1.5em}
\resizebox{\textwidth}{!}{
\begin{tabular}{lcr}
\subfloat[\label{fig:RoutesFunctions} Example of 2PSO and 2PSOv2
encodings and the graphs used for calculation of the fitness
functions.]{
    \includegraphics[width=0.45\textwidth]{images/RoutesFunctions.ps}}
    &
    \hspace{1em}
    &
    \subfloat[\label{fig:RoutesUnrepresantable} Example of non-representable in 2PSO assignment of clients' requests.]{
    \includegraphics[width=0.45\textwidth]{images/RoutesUnrepresantable.ps}}
%JM - dlaczego przypadek b jest niemozliwy do reprezentacji?
    \end{tabular}
    }
    \caption{Example of a~DVRP problem with 2 vehicles and 5 clients' requests.
    Solid lines represent possible routes,
    whose lengths are used as an evaluation function by DAPSO and 2PSOv2.
    Dashed lines represent estimated cluster cost (quality),
    which is used as an evaluation function by 2PSO.
    Dotted line separates the two operating areas assigned to vehicles.}
\label{fig:vrp.examples}
\end{figure}
%
In DAPSO algorithm the encoding of assignment is an integer vector
with vehicle identifiers assigned to each of the requests. In the
2PSO family of algorithms the encoding of assignment is a~real
numbers vector with coordinates of centers of operating areas of
each of the vehicles. The area were clients' requests appear is
divided among vehicles on the basis of Euclidean distance from
operating area center (i.e. the request is assigned to the vehicle
with the nearest operating area center).

For better explanation of differences between DAPSO, 2PSO and 2PSOv2
clients' requests assignment encodings, Fig.~\ref{fig:vrp.examples}
presents an example of DVRP with with 2 vehicles and 5 requests.
For Figures~\ref{fig:RoutesFunctions} and \ref{fig:RoutesUnrepresantable} the encodings of DAPSO and 2PSO
are as follows:
\begin{center}
\begin{tabular}{cc}
Example from Fig.~\ref{fig:RoutesFunctions}
&
Example from Fig.~\ref{fig:RoutesUnrepresantable}
\\
\begin{tabular}{l|l}
    & \\[-0.5em]
        DAPSO
    &\hspace{0.5em}
        \begin{tabular}{|c|c|c|c|c|}\hline
            1 & 2 & 2 & 2 & 1 \\\hline
        \end{tabular}
    \\[0.5em]\hline
    & \\[-0.5em]
        2PSO(v2)
    &\hspace{0.5em}
        \begin{tabular}{|c|c||c|c|}\hline
            $\text{Veh1}_x$ & $\text{Veh1}_y$ & $\text{Veh2}_x$ & $\text{Veh2}_y$ \\\hline
        \end{tabular}\\[0.5em]
\end{tabular}
 &
\begin{tabular}{l|l}
    & \\[-0.5em]
        DAPSO
    &\hspace{0.5em}
        \begin{tabular}{|c|c|c|c|c|}\hline
            1 & 2 & 2 & 1 & 2 \\\hline
        \end{tabular}
    \\[0.5em]\hline
    & \\[-0.5em]
        2PSO(v2)
    &\hspace{0.5em}
        Encoding does not exist\\[0.5em]
\end{tabular}
\end{tabular}
\end{center}

In addition Figure~\ref{fig:RoutesFunctions} also presents the way the fitness
function is calculated in each algorithm. In DAPSO/MAPSO and 2(M)PSOv2 algorithms it is
the sum of solid-line edges while in the 2PSO algorithm the sum of
dashed-line edges.
%
\section{Knowledge Transfer}
\begin{figure}[t]
\centering
    \includegraphics[width=0.95\textwidth]{images/Activity.eps}
    \caption{Activity diagram of the multi-swarm 2PSO (2MPSO) algorithm}
    \label{fig:activ}
\end{figure}
\label{sec:transfer}
%
In dynamic problems, one of the crucial tasks is efficient transfer
of knowledge from partial (incomplete) problems to the final
solution. Generally, it is assumed that solutions obtained for the
two problem instances which are close in time should not differ much
and therefore knowledge transfer may, in principle, be very
advantageous. Another issue is the problem of efficient usage of parallel or
distributed architecture and knowledge transfer between partial
problems within the same problem instance (time slot).
Both methods (i.e. DAPSO/MAPSO algorithms and the 2(M)PSO family)
deal with these two issues differently.

\begin{table}[ht]
\centering
\caption{Baseline algorithms comparison}
\vspace{-2em}
\label{tab:state.of.art}
\begin{center}
\resizebox{0.9\textwidth}{!}{
\begin{tabular}{|r||r|r|r|r|r|r|r|r|r|r|r|r|}
\hline
 &
    \multicolumn{2}{|c|}{2PSO ($(10^4)$)} &
    \multicolumn{2}{|c|}{2MPSO ($(10^4)$)} &
    \multicolumn{2}{|c|}{2MPSOv2 ($(10^4)$)} &
    \multicolumn{2}{|c|}{MAPSO ($(10^4)$)} \\\hline
  & Min & Avg
  & Min & Avg
  & Min & Avg
  & Min & Avg
\\\hline\hline
 c50
    & 582.88 & 675.14
    & 589.24 & 626.57
    & 578.61 & \textbf{608.71}
    & \textbf{571.34} & \textit{610.67}
 \\\hline
 c75
    & 912.23 & 1015.16
    & 937.41 & 988.58
    & \textbf{892.04} & \textbf{944.18}
    & 931.59 & 965.53
 \\\hline
 c100
    & 996.4 & 1149.48
    & 957.54 & 1041.18
    & \textbf{941.3} & \textbf{969.16}
    & 953.79 & \textit{973.01}
 \\\hline
 c100b
    & \textbf{828.94} & \textit{850.68}
     & \textbf{828.94} & \textbf{849.69}
     & 833.25 & 875.92
     & 866.42 & 882.39
 \\\hline
 c120
    & 1087.04 & 1212.38
    & 1079.14 & \textbf{1162.65}
    & \textbf{1061.01} & \textit{1178.43}
    & 1223.49 & 1295.79
 \\\hline
 c150
    & 1173.94 & 1336.84
    & 1173.25 & 1240.9
    & \textbf{1142.57} & \textbf{1196.88}
    & 1300.43 & 1357.71
 \\\hline
 c199
    & 1446.93 & 1578.99
    & 1408.74 & \textit{1458.24}
    & \textbf{1394.61} & \textbf{1470.16}
    & 1595.97 & 1646.37
 \\\hline
 \hline
 f71
    & 315 & 356.75
    & 293.1 & 317.15
    & 291.2 & 313.92
    & \textbf{287.51} & \textbf{296.76}
 \\\hline
 f134
    & 12813.14 & 13491.6
    & 12304.03 & 12587.49
    & \textbf{12011.71} & \textbf{12509.83}
    & 15150.5 & 16193
 \\\hline
 \hline
 tai75a
    & 1871.06 & 2142.07
    & 1814.95 & 1958.95
    & \textbf{1742.31} & \textit{1869.48}
    & 1794.38 & \textbf{1849.37}
 \\\hline
 tai75b
    & 1460.95 & 1568.21
    & 1435.76 & 1486.39
    & 1401.22 & 1470.92
    & \textbf{1396.42} & \textbf{1426.67}
 \\\hline
 tai75c
    & 1500.23 & 1811.08
    & 1497.64 & 1660.36
    & \textbf{1461.74} & 1547.57
    & 1483.1 & \textbf{1518.65}
 \\\hline
 tai75d
    & 1462.82 & 1586.28
    & 1459.68 & 1496.22
    & 1421.48 & 1463.29
    & \textbf{1391.99} & \textbf{1413.83}
 \\\hline
 tai100a
    & 2317.76 & 2707.61
    & 2198.02 & 2381.24
    & 2197.94 & 2278.07
    & \textbf{2178.86} & \textbf{2214.61}
 \\\hline
 tai100b
    & 2187.86 & 2510.6
    & 2134.31 & 2267.1
    & \textbf{2045.47} & \textbf{2156.24}
    & 2140.57 & 2218.58
 \\\hline
 tai100c
    & 1564.25 & 1672.33
    & 1555.73 & 1611.17
    & \textbf{1480.89} & \textbf{1541.56}
    & 1490.4 & 1550.63 \\\hline
 tai100d
    & 1859.7 & 2220.01
    & 1819.56 & 1939.34
    & \textbf{1739.25} & \textbf{1789.74}
    & 1838.75 & 1928.69
 \\\hline
 tai150a
    & 3638.75 & 4151.31
    & 3480.84 & 3667.11
    & 3350.14 & 3527.45
    & \textbf{3273.24} & \textbf{3389.97}
 \\\hline
 tai150b
    & 3107.95 & 3302.94
    & 3004.98 & 3118
    & 2918.39 & 3032.84
    & \textbf{2861.91} & \textbf{2956.84}
 \\\hline
 tai150c
    & 2781.02 & 2952.88
    & 2714.25 & 2821.53
    & \textbf{2497.55} & \textbf{2603.02}
    & 2512.01 & 2671.35
 \\\hline
 tai150d
    & 3048.24 & 3478.49
    & 3029.75 & 3174.42
    & 2915.98 & 3000.5
    & \textbf{2861.46} & \textbf{2989.24}
 \\\hline
 \hline
 \textbf{sum}
    & 46957.09 & 51770.83
    & 45716.86 & 47854.28
    & \textbf{44318.66} & \textbf{46347.87}
    & 48104.13 & 50349.66
 \\\hline
     \end{tabular}
} \end{center}
\vspace{-0.5em}
\end{table}
%
\begin{table}[ht]
\centering
\caption{Comparison of multi-swarm algorithms with initial routes
length fitness function and various numbers of fitness evaluations
per time slice}
\vspace{-0.5em}
\label{tab:multi.v2}
\begin{center}
\resizebox{0.9\textwidth}{!}{
\begin{tabular}{|r||r|r|r|r|r|r|r|r|r|r|r|r|}
\hline
 &
 \multicolumn{2}{|c|}{2MPSOv2 ($10^3$)} &
 \multicolumn{2}{|c|}{2MPSOv2 ($10^4$)} &
 \multicolumn{2}{|c|}{2MPSOv2 ($10^5$)} &
 \multicolumn{2}{|c|}{MAPSO ($(10^4)$)} \\\hline
  & Min & Avg
  & Min & Avg
  & Min & Avg
  & Min & Avg
\\\hline\hline
 c50 & \textbf{568.82} & 629.08 & 578.61 & \textbf{608.71} & 571.53 & \textit{614.61} & 571.34 & \textit{610.67} \\\hline
 c75 & 908.6 & 972.58 & \textbf{892.04} & \textit{944.18} & 896.33 & \textbf{930.94} & 931.59 & 965.53 \\\hline
 c100 & 982 & 1033.03 & 941.3 & \textit{969.16} & \textbf{920.11} & \textbf{957.49} & 953.79 & 973.01 \\\hline
 c100b & \textbf{829.54} & 898.19 & 833.25 & \textbf{875.92} & 848.5 & \textit{881.7} & 866.42 & \textit{882.39} \\\hline
 c120 & 1060.39 & \textbf{1158.68} & 1061.01 & \textit{1178.43} & \textbf{1057.94} & \textit{1174.65} & 1223.49 & 1295.79 \\\hline
 c150 & 1138.03 & 1269.34 & 1142.57 & 1196.88 & \textbf{1121.5} & \textbf{1168.59} & 1300.43 & 1357.71 \\\hline
 c199 & 1447.55 & 1581.75 & \textbf{1394.61} & \textit{1470.16} & 1404.46 & \textbf{1461.58} & 1595.97 & 1646.37 \\\hline
 \hline
 f71 & 300.46 & 333.37 & 291.2 & 313.92 & 302.5 & 317.66 & \textbf{287.51} & \textbf{296.76} \\\hline
 f134 & 12079.07 & 12529.43 & 12011.71 & 12509.83 & \textbf{11988.76} & \textbf{12324.98} & 15150.5 & 16193 \\\hline
 \hline
 tai75a & 1798.99 & 1993.76 & 1742.31 & 1869.48 & \textbf{1727.89} & \textbf{1812.55} & 1794.38 & 1849.37 \\\hline
 tai75b & 1441.3 & 1530.25 & 1401.22 & 1470.92 & 1400.33 & 1438.5 & \textbf{1396.42} & \textbf{1426.67} \\\hline
 tai75c & 1511.04 & 1616.08 & 1461.74 & 1547.57 & \textbf{1440.2} & \textbf{1491.64} & 1483.1 & \textit{1518.65} \\\hline
 tai75d & 1432.06 & 1502.44 & 1421.48 & 1463.29 & 1439.27 & 1470.93 & \textbf{1391.99} & \textbf{1413.83} \\\hline
 tai100a & 2244.3 & 2413.5 & 2197.94 & 2278.07 & \textbf{2146.53} & 2260.21 & 2178.86 & \textbf{2214.61} \\\hline
 tai100b & 2073.27 & 2247.82 & 2045.47 & \textit{2156.24} & \textbf{2045.24} & \textbf{2119.36} & 2140.57 & 2218.58 \\\hline
 tai100c & 1527.25 & 1598.37 & 1480.89 & 1541.56 & \textbf{1469.12} & \textbf{1516.97} & 1490.4 & 1550.63 \\\hline
 tai100d & 1762.17 & 1845.61 & 1739.25 & \textit{1789.74} & \textbf{1685.53} & \textbf{1775.09} & 1838.75 & 1928.69 \\\hline
 tai150a & 3566.83 & 3898.32 & 3350.14 & 3527.45 & 3345.88 & 3402.23 & \textbf{3273.24} & \textbf{3389.97} \\\hline
 tai150b & 3005.9 & 3215.82 & 2918.39 & 3032.84 & 2885.21 & \textbf{2942.49} & \textbf{2861.91} & \textit{2956.84} \\\hline
 tai150c & 2593.7 & 2727.12 & 2497.55 & 2603.02 & \textbf{2472.7} & \textbf{2543.47} & 2512.01 & 2671.35 \\\hline
 tai150d & 3042.33 & 3198.31 & 2915.98 & 3000.5 & \textbf{2844.7} & \textbf{2949.2} & 2861.46 & 2989.24 \\\hline
 \hline
 \textbf{sum} & 45313.6 & 48192.85 & 44318.66 & 46347.87 & \textbf{44014.23} & \textbf{45554.84} & 48104.13 & 50349.66 \\\hline
     \end{tabular}
}
\end{center}
\vspace{-0.5em}
\end{table}

\subsubsection{Knowledge Transfer Between Time Slices}
In the DAPSO/MAPSO algorithms Khouadjia et al. proposed adding a
memory to each particle, in order to store its best known
solution (the vector of identifiers of vehicles assigned to
each of the requests) from the previous time slice. When new
requests arrive, they are processed in a~random order and assigned
to vehicles by a~greedy algorithm, thus forming the initial swarm
locations for the PSO method.

In the 2PSO algorithms a~different approach is
taken. Since the solution of the first phase in the previous time
slot consists of locations of clusters centers, these coordinates
are treated as reliable estimations of the clusters centers after
the arrival of new requests (in the next time slice). Therefore
initial swarm location is defined around the center of the previous
best known solution within a~given radius.
%
\subsubsection{Knowledge Transfer Between Swarms}
In the MAPSO algorithm knowledge is transferred between swarms by
migrating particles. In every iteration for each particle there is a
small probability that a~particle will migrate to a~different swarm.
As MAPSO allows for distributed way of solving the problem there is,
in general, no guarantee that in a~given moment all swarms are
solving the problem for the same time slice. Therefore a~particle
after migration may need to wait to be incorporated into a~new swarm
or must be re-initialized with newly received requests.

The 2MPSO algorithm assumes that the problem is solved in a~parallel
way on a~single multithreading computer. Therefore, we take an easy
approach were, within a~given time slice, each thread works in
isolation and at the end of allotted time (slice time span) all
threads are synchronized and the best solution found is spread again
among the threads (see Fig.~\ref{fig:activ}). Such approach is
motivated by the assumption that at the end of a~time slice some
vehicles are committed to serve given sets of requests and it might
be meaningless to solve problem instances not synchronized with the
current state of the problem instance.

\section{Results}
\label{sec:results}

In order to evaluate the performance of the algorithm we used
dynamic versions of Chritofides', Fisher's and Taillard's benchmark
sets~\cite{DVRP:Benchmark}. We compare our algorithm with the MAPSO
approach which gave best average literature results and most of the
best literature solutions. The basic comparison was made for the
same number of swarms and the same number of fitness function
evaluations per time slice. Additionally we present the results for
different numbers of function evaluations per time slice to check
whether using more function evaluations will further improve the
2(M)PSO results.
%, regardless of existence of solutions non-representable
%with  (see Fig.~\ref{fig:RoutesUnrepresantable}).

Figure~\ref{fig:cft.state} presents the aggregated minimum and
average values together with standard error bars obtained for the
basic, single-swarm (2PSO) and two multi-swarm (2MPSO and 2MPSOv2),
versions of this algorithm. Figure~\ref{fig:cft.multi.v2}
 presents the same type of data, for the second version of the 2MPSO algorithm with various
numbers of fitness function evaluations. In each plot a~solid
horizontal line marks the average performance of the MAPSO algorithm
for the problem instances in a~given benchmark set, while dashed
horizontal line depicts the average of the best results of MAPSO for
a given benchmark set.

A detailed comparison of numerical results for the same experiments
is presented in tables~\ref{tab:state.of.art} and
~\ref{tab:multi.v2}, respectively with the best results marked in
bold and statistically insignificantly worse average results marked
in italics. The significance of the differences in results of
2(M)PSO method was calculated with the use of the
\textit{Mann-Whitney U test}~\cite{Test:M-W} and the significance of
the difference in results between 2(M)PSO and MAPSO was tested using
the \textit{Wilcoxon signed-rank test}~\cite{Test:Wilcox} with a
null hypothesis saying that a~distribution of the 2(M)PSO
algorithm's results was symmetric around the average performance of
MAPSO. The best overall obtained results for each of the benchmark
problems and the algorithm versions that found them are listed in
table~\ref{tab:best}.
%
\section{Discussion and Conclusions}
\label{sec:discussion}

\begin{table*}[t]
\centering \caption{Best literature results for the tested
benchmarks. TS$_{2-Opt}$ is a Tabu Search method~\cite{DVRP:GA:TS}}
\label{tab:best} \vspace{-1.5em} \resizebox{\textwidth}{!}{
\begin{tabular}{ccc}
    \subfloat{
    \begin{tabular}{|c|r|r|}
\hline \textbf{Name} & \textbf{Best result} & \textbf{Algorithm}
\\\hline
 c50 & 568.82 & 2MPSOv2 $10^3$ \\\hline
 c75 & 892.04 & 2MPSOv2 $10^4$ \\\hline
 c100 & 920.11 &  2MPSOv2 $10^5$ \\\hline
 c100b & 828.94 & 2MPSO $10^4$\\\hline
 c120 & 1057.94 & 2MPSOv2 $10^5$ \\\hline
 c150 & 1121.50 & 2MPSOv2 $10^5$ \\\hline
 c199 & 1394.61 & 2MPSOv2 $10^4$  \\\hline
%
\end{tabular}
%
    }
    &
    \subfloat{
    \begin{tabular}{|c|r|r|}
\hline \textbf{Name} & \textbf{Best result} & \textbf{Algorithm}
\\\hline
 f71 & 280.23 & TS$_{2-Opt}$\cite{DVRP:GA:TS} \\\hline
 f134 &  11988.76 & 2MPSOv2 $10^5$ \\\hline
 tai75a & 1727.89 &  2MPSOv2 $10^5$ \\\hline
 tai75b & 1396.42 & MAPSO\cite{DVRP:MAPSO} \\\hline
 tai75c & 1406.27 & TS$_{2-Opt}$\cite{DVRP:GA:TS} \\\hline
 tai75d & 1391.99 & MAPSO\cite{DVRP:MAPSO} \\\hline
 tai100a & 2146.53 & 2MPSOv2 $10^5$ \\\hline
%
\end{tabular}
    }
    &
    \subfloat{
    \begin{tabular}{|c|r|r|}
\hline \textbf{Name} & \textbf{Best result} & \textbf{Algorithm}
\\\hline
 tai100b & 2045.24 &  2MPSOv2 $10^5$  \\\hline
 tai100c & 1469.12 & 2MPSOv2 $10^5$  \\\hline
 tai100d & 1685.53 & 2MPSOv2 $10^5$ \\\hline
 tai150a & 3273.24 & MAPSO\cite{DVRP:MAPSO} \\\hline
 tai150b & 2861.91 & MAPSO\cite{DVRP:MAPSO} \\\hline
 tai150c & 2472.70 & 2MPSOv2 $10^5$ \\\hline
 tai150d & 2844.70 & 2MPSOv2 $10^5$ \\\hline
%
\end{tabular}
    }
    \end{tabular}
    }
\end{table*}

The most significant performance improvement of the 2PSO algorithm
was accomplished using a~multi-swarm version of this method. Even a
simple collection of isolated swarms synchronized only once per time
slice (at the end of it) allowed for visible gain in the solutions'
quality (around $8\%$, on average).

\begin{figure*}[b]
\centering
\vspace{-1em}
\resizebox{\textwidth}{!}{
\begin{tabular}{cc}
\subfloat[\label{fig:cft.state} Results for the baseline
    versions of the 2(M)PSO algorithms.]{
    \includegraphics[width=0.45\textwidth]{images/all.state.of.art.eps}}
    &
    \subfloat[\label{fig:cft.multi.v2} Results for the 2MPSOv2 with various numbers of function
    evaluations.]{
    \includegraphics[width=0.45\textwidth]{images/all.multi.v2.eps}}
%
    \end{tabular}
    }
    \vspace{-1em}
    \caption{Performance comparison of various versions of 2(M)PSO algorithm.
     Solid and dashed lines represent, respectively, the means of average and best performance of MAPSO algorithm
    and gray triangle represents the mean best performance for all benchmark instances.}
\label{fig:cft1}
\end{figure*}

Moreover, in majority of the test cases, changing the fitness
function from a~total sum of clusters' weights to the estimation of
a total sum of routes' lengths proved to be beneficial. 2PSO
algorithms based on the modified, routes lengths-based fitness
function used for optimizing the assignment, perform significantly
better than the first version of the algorithm except for the cases
of highly spatial clusters composed of uniformly distributed request
sizes (e.g. instances \textit{c100b}, \textit{c120}, \textit{c199}).

For the easiest (Christofides) benchmark sets the results get
slightly deteriorated when the number of the fitness function
evaluations was raised to $10^5$ times per time slice (see
Fig.~\ref{tab:multi.v2}). This phenomenon may possibly stem from the
fact, that the algorithm is stuck in the close to optimal solution
in the initial time slices, which is not the part of the final
optimal solution and, as a~result, vehicles are too early committed
to some of the requests. Detection of such "over-fitting" may
potentially be used as a~stopping criterion for the method.

During the experiments new best solutions for the cut-off time set
to $0.5$ (the same cut-off time was used in the referenced
DAPSO/MAPSO papers) were found for $15$ out of $21$ benchmark sets.
All tested multi-swarm approaches outperformed MAPSO in terms of the
average value of the best results and nearly all of them were more
effective when the average values of (all) the results were
considered (only 2MPSOv2 with $10^3$ fitness evaluations per time
slice found the routes which were, on average, $1\%$ longer than
those of MAPSO). The best average results were achieved by 2MPSOv2
with $10^5$ fitness evaluations per time slice (they were $3\%$
better than MAPSO in terms of the average of the minima and nearly
$5\%$ better in terms of the average lengths).

The results suggest that problem instances could be differentiated
based on the spatial distribution of request' locations, as well as
on the requests' sizes distribution (c.f. the results for
\textit{c100b} as an illustrative example). The task of autonomous
selection of problem encoding and fitness function to be used for a
particular problem instance is one of our future research goals. We
also plan to perform additional tests in order to further validate
the 2(M)PSO ability to effectively solving the DVRP.

\section*{Acknowledgment}
The research was partially financed by the National Science Centre
in Poland, based on the decision DEC-2012/07/B/ST6/01527 and
partially supported by the research fellowship within "Information
technologies: Research and their interdisciplinary applications"
agreement number POKL.04.01.01-00-051/10-00.

\bibliographystyle{splncs03}
\bibliography{2MPSOv2,2MPSOv2xref}
\end{document}
