if (Sys.info()[["nodename"]] == "T540P-KOMPUTER")
  dvrp.dir = '~/DVRP/AdaptiveMetaheuristicsForCVRPwDR'
if (Sys.info()[["nodename"]] == "P52601L")
  dvrp.dir = '~/DVRP/GlobalOptimization'
if (Sys.info()[["nodename"]] == "P52601")
  dvrp.dir= '~/DVRP'

setwd('~/IPI/Phd/Rscripts/')
# trzeba poszuka� najbli�szych sobie par centr�w klastr�w
# z drugiej strony - jak si� sprawy maj� z przerzucaniem klient�w?

# for (file in list.files(path = '.',pattern = '*solutions.series')) {
#   solutionseries = readLines(file)
#   lengths = c()
#   distances = c()
#   for (i in 1:length(solutionseries)) {
#     solution = as.numeric(unlist(strsplit(gsub(',','.',solutionseries[i],";"),split=';')))
#     lengths = c(lengths,(length(solution))/4)
#     if (i > 1 && length(unlist(strsplit(solutionseries[i-1],split=";"))) == length(unlist(strsplit(solutionseries[i],split=";")))) {
#       a = as.numeric(unlist(strsplit(gsub(',','.',solutionseries[i-1],";"),split=';')))
#       b = as.numeric(unlist(strsplit(gsub(',','.',solutionseries[i],";"),split=';')))
#       distances = c(distances,mean(sqrt(((a - b)^2))))
#     } else {
#       #distances = c(distances,0)
#     }
#   }
#   par(mfrow=c(2,1))
#   plot(lengths,type="l",lwd=2,main=file)
#   plot(distances,type="l",lwd=2,main=file)
#   print(mean(distances))
# }

namedtypes = list(
                  # `Hanshar` =
                  # c(
                  #   `Hanshar GA 10^6` = 'DR5 Log4 8192'
                  #   ,`Hanshar GA 10^6 0.7` = 'DR9x1.25[+]1 0.350S 0.7 8192 16v3OrgnlDSharePx2'
                  # ),
                  `TCO05` =
                    c(`Hanshar's GA` = 'DR5 Log4 8192'
                      ,`ContDVRP+0.0P 0.04` ='DR5 Log4 20'
                      ,`ContDVRP+1.0P 0.2` ='DR7x1[+]1[(]0[.]2S[)] 20 16v3OrgnlDSharePx2[+]4 8-022-140'
                      #,`ContDVRP+1.1P 0.2` ='DR9x1.1[+]1 0.200S 0.5 20 16v3OrgnlDSharePx2'
                      
                      #,`ContDVRP 10^5` ='DR5 20 16v3OrgnlDx2[+]4 8-007-044-0'
                      )
                  # ,`TCO 0.6` =
                  #   c(`ContDVRP+Penalty 10^6 0.6` ='DR9x1[+]1 0.250S 0.6 20 16v3OrgnlDSharePx2'
                  #     ,`ContDVRP 0.6` = 'DR9x1[+]1 0.250S 0.6 20 16v3OrgnlDSharex2')
                  # ,`TCO 0.7` = 
                  #   c(`ContDVRP+1.25Penalty 10^6 0.7` ='DR9x1.25[+]1 0.350S 0.7 20 16v3OrgnlDSharePx2'
                  #     ,`ContDVRP+Penalty 10^6 0.7` = 'DR8x1[+]1[(]0.35S[)][(]0.7[)] 20 16v3OrgnlDSharePx2'
                  #     ,`ContDVRP 0.7` ='DR8x1[+]1[(]0.35S[)][(]0.7[)] 20 16v3OrgnlDSharex2'
                  #     ,`Hanshar GA 10^6 0.7` = 'DR9x1.25[+]1 0.350S 0.7 8192 16v3OrgnlDSharePx2'
                  #   )
                  ,`ContDVRP` = c(
                    `ContDVRP 0.7` = 'DR8x1[+]1[(]0.35S[)][(]0.7[)] 20 16v3OrgnlDSharex2'
                    ,`ContDVRP 0.6` ='DR9x1[+]1 0.250S 0.6 20 16v3OrgnlDSharex2'
                    ,`ContDVRP 0.5` ='DR5 Log4 20'
                  )
                    ,`ContDVRPP` = c(
                    `ContDVRP+P 0.7` = 'DR8x1[+]1[(]0.35S[)][(]0.7[)] 20 16v3OrgnlDSharePx2'
                    ,`ContDVRP+P 0.6` ='DR9x1[+]1 0.250S 0.6 20 16v3OrgnlDSharePx2'
                    ,`ContDVRP+P 0.5` ='DR7x1[+]1[(]0[.]2S[)] 20 16v3OrgnlDSharePx2[+]4 8-022-140'
                  )
)
for (namedtype in names(namedtypes)) {
  for (plotData in c('distances','current.vehicles','size','whole.normalized.length.distances')) {
    pdf(paste0(dvrp.dir,'/Doc/EvCo/figures/',namedtype,plotData,'.pdf'),width=7,height=7)
    plot(NULL,xlim=c(0,41),ylim=c(0,1)
         ,ylab = 'Relative distance'
         ,xlab = 'Time step'
         ,main = 'Dynamics of problem changes'
         #,sub = plotData
    )
    types = namedtypes[[namedtype]]
    labels = names(namedtypes[[namedtype]])
    
    colors = gray((1:length(types)-1)/length(types)) # rainbow(length(types),v=0.75)
    for (type in types) {
      simple.final.distances.matrix = c()
      final.distances.matrix = c()
      for (file in list.files(path = '.',pattern = paste0(type,'.*assignments.*.series$'))) {
        solutionseries = readLines(file)
        if (length(solutionseries) != 123) next
        final.distances = c()
        final.vehicles = c()
        current.vehicles = c()
        whole.length.distances = c()
        whole.normalized.length.distances = c()
        simple.final.distances = c()
        naive.final.distances = c()
        distances = c()
        commited = c()
        size = c()
        relative.vehicles = c()
        solutionseries = lapply(strsplit(solutionseries,split=','), as.numeric)
        last.solution = solutionseries[[length(solutionseries)-2]]
        for (i in 1:(length(solutionseries)/3)) {
          solution = solutionseries[[3*i-2]]
          en.route.vehicles = unique(solutionseries[[3*i-2]][solutionseries[[3*i-1]] & !solutionseries[[3*i]]])
          considered.vehicles = unique(solutionseries[[3*i-2]][solutionseries[[3*i-1]] & T])
          client.in.en.route.vehicles = solutionseries[[3*i-2]] %in% en.route.vehicles
          relative.vehicles = c(relative.vehicles, length(en.route.vehicles) / length(unique(solutionseries[[3*i-2]][solutionseries[[3*i-1]] & T])))
          final.vehicles = c(final.vehicles, length(en.route.vehicles) / length(unique(last.solution)))
          current.vehicles = c(current.vehicles, length(considered.vehicles) / length(unique(last.solution)))
          simple.final.distances = c(simple.final.distances,mean(solution!=last.solution & solutionseries[[3*i-1]])/mean(solutionseries[[3*i-1]]))
          naive.final.distances = c(naive.final.distances,mean(solution!=last.solution))
          whole.normalized.length.distances = c(whole.normalized.length.distances,mean(solution!=last.solution & client.in.en.route.vehicles & solutionseries[[3*i-1]])/mean(client.in.en.route.vehicles & solutionseries[[3*i-1]]))
          whole.length.distances = c(whole.length.distances,mean(solution!=last.solution & client.in.en.route.vehicles))
          final.distances = c(final.distances,mean(solution!=last.solution & client.in.en.route.vehicles & solutionseries[[3*i]])/mean(client.in.en.route.vehicles & solutionseries[[3*i]]))
          commited = c(commited,mean(!solutionseries[[3*i]]&solutionseries[[3*i-1]]))
          size = c(size,mean(solutionseries[[3*i]]))
          if (i > 3) {
            a = solutionseries[[3*i-5]]
            flips = mean(a != solution & solutionseries[[3*i-4]] & solutionseries[[3*i-3]])/mean(solutionseries[[3*i-4]] & solutionseries[[3*i-3]])
            distances = c(distances,flips)
          } else {
            distances = c(distances,NA)
          }
        }
        if (length(final.distances.matrix) > 0 && ncol(final.distances.matrix) != length(eval(parse(text=plotData)))) {
          #print(file)
        } else {
          final.distances.matrix = eval(parse(text=paste0('rbind(final.distances.matrix,',plotData,')')))
        }
        # plot(distances,type="l",lwd=2,main=file,ylim=c(0,1))
        # lines(final.distances,col='red',lwd=2)
        # lines(simple.final.distances,col='red')
        # lines(relative.vehicles,col='blue',lwd=2)
        # lines(commited,col='blue')
        # lines(size,col='green')
        # print(paste(file,mean(final.distances,na.rm=T)))
        # legend("topleft",legend=c('Rel.dist','Fin.dist','Simple fin.dist','Commited','Assigned','Problem size'),
        #        lwd=c(2,2,1,2,1,1),col=c('black','red','red','blue','blue','green'))
      }
      
      lines(1:ncol(final.distances.matrix)-1,colMeans(final.distances.matrix, na.rm=T),lwd=2,col=colors[type==types])
      #text(x=16,y=colMeans(final.distances.matrix, na.rm=T)[16],labels=type, pos=4)
      print(paste(type,nrow(final.distances.matrix),mean(colMeans(final.distances.matrix, na.rm=T),na.rm = T)))
    }
    legend("topright",legend=labels,lwd=2,col=colors,inset=c(0,0.1))
    dev.off()
  }
}