\documentclass[10pt,a4paper]{letter}

\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage[table]{xcolor}

\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{url}
\usepackage[final]{changes}
\usepackage{cite}

\usepackage{graphicx}

\usepackage{letterbib}
\usepackage{xr}
\externaldocument[article-]{../SWEVO_ContinuousDVRP}

\hyphenation{presence}

\newcommand{\initresponses}{\newcounter{pointcounter}}

\newenvironment{reviewer}{\setcounter{pointcounter}{1}}{}

\newcommand{\point}[1]{\medskip \noindent
               \textsl{{\fontseries{b}\selectfont Comment \#\thepointcounter}.
                 \stepcounter{pointcounter} #1}}
\newcommand{\reply}{\medskip \noindent \textbf{Reply:}\ }


\signature{\vspace{-4em}Michał Okulewicz\\
Jacek Mańdziuk}

\begin{document}
\begin{letter}{}
\opening{Dear Dr Swagatam Das,}

We would like to thank the Reviewers for their kind remarks,
which have helped us to substantially improve the manuscript.

Before giving a detailed response to each of the Reviewers' comments
we would like to stress a few points we have failed to deliver
in a clear way within the previous version of the manuscript:
\begin{itemize}
	\item Our algorithm does not combine GA, PSO or DE. Each of these metaheuristics is treated in the paper as a manually selected hyper-parameter within a broad system,
	named Parallel Services, {proposed in the paper}. This system is responsible
	for organizing the computations, synchronization between independent
	computations services, providing new data to them etc.
	\item Our approach is mostly experimental with the average benchmark result
	seen as the most important quality measure.
	\item This paper functions as a continuation to \cite{Okulewicz2017},
	where a detailed comparison of the numerical results and analysis of
	the algorithm itself is performed. In this paper we focus on:
	\begin{itemize}
	\item[(a)] observing the impact of switching from PSO to DE as the main
	optimization engine,
	\item[(b)] observing the impact of including
	encoding of requests order within the solution vector
	together with requests cluster centers,
	\item[(c)] observing the impact of adding a penalty term to the solution cost function,
	performing as a predictor for a final total volume of requests,
	\item[(d)] analyzing the level of dynamism of the benchmarks
	and stability of subsequent (incomplete) solutions, as the working
	day proceeds in distinctive algorithm and quality function settings.
	
	\end{itemize}
\end{itemize}

In accordance to reviewers comments, the following major changes were made to the manuscript:
\begin{itemize}
	\item abstract has been rewritten with emphasis on the research contributions,
	\item literature review has been enhanced with 3 additional survey articles (cf. section \ref{article-sec:related-work}) and 8 additional
	computational experiments articles (cf. section \ref{article-sec:recent-dvrp}),
	\item algorithm description in section \ref{article-sec:generalization} has been rewritten, with additional activity diagrams presenting the high-level process of the
	optimization services (Figure~\ref{article-fig:algorithms-ps}) and a brief depiction of a single step of optimization process (Figure~\ref{article-fig:algorithms-hierarchy}),
	\item introduction has been enhanced with the research motivation,
	\item encodings description and manuscript conclusions have been enhanced with discussion of the method complexity (cf. section \ref{article-sec:complexity}) and efficiency (cf. section \ref{article-sec:conclusions}).
\end{itemize}

Below are our detailed responses to the reviewers’ comments.

\initresponses
\begin{reviewer}
\textbf{\\ \Large Response to Reviewer \#1}

\point{This paper presents a metaheuristic approach to solve dynamic vehicle routing problem. The problem addressed in the paper is interesting. Though there is no innovative technique here except a combination of GA, PSO and DE, the results are good. There are some comments for authors to improve this paper.}

\reply We would like to underline that the paper \textbf{compares},
and \textbf{not combines} GA, PSO and DE.
In order to clarify this point, the following statement
has been added to the manuscript's abstract:

\textbf{\textit{The paper compares Particle Swarm Optimization (PSO) and Differential
Evolution (DE) operating on two continuous search spaces (giving in total four
distinctive approaches) and a state-of-the art discrete encoding utilizing Genetic Algorithm (GA)}}

\point{(1) The manuscript is not well written, it does not make a proper use of the English language. There are many spelling and grammatical errors through the whole document, such as approach to solving $\rightarrow$ approach to solve, dynamic DVRP $\rightarrow$ DVRP, Furthermore, Solving $\rightarrow$ Furthermore, solving, and etc.}

\reply
The language of the article has been reviewed.

\point{(2) The literature review regarding the dynamic vehicle routing problem is poor. There have been many surveys of DVRP. The author should discuss and describe some of the most recent papers about DVRP and compare them with their work.}

\reply After carefully reviewing recent literature on DVRP, the authors selected three relevant surveys
done by Psaraftis et al. \cite{Psaraftis2016}, Mavrovouniotis et al. \cite{Mavrovouniotis2017}
and Marinakis et al. \cite{Marinakis2017}. Review of the surveys opens section \ref{article-sec:related-work} on related work of the manuscript.
Additionally we included a review of 8 recent DVRP approaches (\cite{AbdAllah2017,Benaini2018,Chen2017,DVRP:ACOLNS,Sarasola2016,Xu2018,Yi2017,Zhou2017}) tested on Kilby's instances. The review has been included in section \ref{article-sec:recent-dvrp}.

\point{(3) The author should describe the proposed algorithm much more clearly. The structure of this paper is not so good. It is hard to figure out the proposed algorithm. Maybe it is better to present more figures of operators, flow charts and pseudo codes.}

\reply
The algorithm's description in section \ref{article-sec:generalization} has been rewritten,
with additional activity diagrams presented in Figures~{\ref{article-fig:algorithms-ps}} and~\ref{article-fig:algorithms-hierarchy}.
%It is also important to note, that a more detailed description can be found in the authors' previous works on DVRP~\cite{DVRP:2MPSO,Okulewicz2017}.
\replaced{The}{this} main focus of this article is analysis of the impact of \replaced{selecting}{selection of} a particular search space, \deleted{{\color{brown}problem encoding,}} {metaheuristic}
and penalty term on the quality of results, while treating the algorithm itself as a sort of given, with its efficiency for solving DVRP already established in the previously published paper~\cite{Okulewicz2017}.

\point{(4) The continuous encoding obviously enlarges the search space and the discrete encoding is much more popular in the recent paper regarding to the VRP. The author should add experiments to compare this two encoding methods.}

\reply We have compared PSO and DE utilizing a continuous search space with our implementation of Hanshar's GA \cite{DVRP:GA:TS:2007} utilizing a discrete search space
of giant tours and with the literature results of Khouadjia's discrete PSO \cite{DVRP:MEMSO} utilizing a discrete search space of requests assignments (cf. Tables~\ref{article-tab:encodings-metaheuristics} and~\ref{article-tab:comparable}).
A comparison of various continuous encodings and their discrete counterparts is presented in Figure~\ref{article-fig:vrp-przyklad}.

Furthermore, a short discussion about recent approaches to DVRP has been added in section~\ref{article-sec:recent-dvrp}.

\point{(5) How to decide the parameters of the proposed algorithm? The author should do parameter testing first.}

\reply The parameters for the algorithm are listed in Tables \ref{article-tab:parameters}, \ref{article-tab:settings.de.vs.pso}, \ref{article-tab:settings.penalty} and \ref{article-tab:settings.tco}).

In our previous paper~\cite{Okulewicz2017} we have conducted quite an extensive parameter study regarding the values of advanced commitment time $T_{AC}$, number of time slices $N_{TS}$, number of requests clusters per vehicle $k$
and population size to iterations ratio.
A report from that tuning has been presented in Appendix A of \cite{Okulewicz2017}.

{The controlling parameters of PSO and DE were tuned during an initial phase of our experiments and have not been changed since then. Parameters of GA were taken from
the relevant literature \cite{DVRP:GA:TS:2007} and have not undergone additional tuning.}

\end{reviewer}

\begin{reviewer}
\textbf{\\ \Large Response to Reviewer \#2}

\point{
The paper presents a metaheuristic approach to solving Dynamic Vehicle Routing Problem using two population based algorithms: Particle Swarm Optimization (PSO) and Differential Evolution (DE). There are lots of issues should be solved.\\
1. The Abstract is not clear, where the highlights of this study were not obviously presented.}

\reply The abstract has been rewritten.
The highlights have also been listed in section \ref{article-sec:main-contribution} presenting the main contributions of this research.

\point{2. In this study, the authors used lot of pages to describe the PSO, DE, and the proposed algorithm. However, what is the theoretical contribution of this study? For example, why the hybrid of PSO and DE can perform better? How to verify it through theoretical way?}

\reply We would like to underline that the paper \textbf{compares},
and \textbf{not combines} GA, PSO and DE.
In order to clarify this point, the following statement
has been added to the manuscript's abstract:

\textbf{\textit{The paper compares Particle Swarm Optimization (PSO) and Differential
Evolution (DE) operating on two continuous search spaces (giving in total four
distinctive approaches) and a state-of-the art discrete encoding utilizing Genetic Algorithm (GA).}}

On a general note, the paper presents an experimental approach. Theoretical analysis is planned to be a subject of our future research and has not been considered in the presented study.

Referring to the reasons of why the selected search space results in higher quality solutions compared to other approaches, we have the following intuitive explanation:
\begin{itemize}
	\item Each solution in the proposed search space relies on clustering the requests,
	and only particularly ill-conditioned benchmarks would not be tractable by
	such an approach.
	\item Therefore, low quality solutions are encountered relatively rarely in this search space.
	The same cannot be said about the search spaces where random assignment
	of requests is possible.
	\item The proposed search space is also more robust in terms of accommodating the incoming requests.
	If new request can be accepted to one of the already existing clusters
	the rest of the solution remains unchanged. In practice, this property allows spending more computational time on searching for the solution.
\end{itemize}

The above reasoning has been included in the conclusions of the manuscript (section \ref{article-sec:conclusions}).

Despite a generally practical focus of this paper, one of its analytical contributions is the proposed measure of \textit{empirical degree of dynamism}.
We believe that the \textit{empirical degree of dynamism} better captures the ``true'' level of dynamism than the {commonly used} \textit{degree of dynamism} measure, which does not take into account the time moment when decision has to be made. Consequently, the DVRP problems {with} \textit{cut-off time} = 0.5 seem to be more dynamic, then they really are.

\point{3. In the Experimental section, the authors should make detailed comparisons with more efficient algorithms.}

\reply To the best of our knowledge, within the computation budget of around
$10^6$ fitness function evaluations and average computational time around 100 seconds
on Intel Core i7 processor, our 2MPSO \cite{Okulewicz2017}, Khouadjia's MEMSO \cite{DVRP:MEMSO} and Hanshar's GA \cite{DVRP:GA:TS:2007} are still the most efficient algorithms for solving DVRP.
In the case of higher computational budget only Mańdziuk and Żychowski's Memetic Algorithm \cite{mandziuk2016} was able to outperform 2MPSO in terms of best results and yielded comparable to 2MPSO average results.

The literature review presented in the manuscript has been enhanced by adding a discussion on 8 recent approaches
tested on Kilby's benchmarks (\cite{AbdAllah2017,Benaini2018,Chen2017,DVRP:ACOLNS,Sarasola2016,Xu2018,Yi2017,Zhou2017}). None of these methods was able to outperform the above-mentioned algorithms in terms of the average results.

\point{4. In the parameter setting section, why not use the DOE method?}

\reply
An extensive study of selection of the following parameters: advanced commitment time $T_{AC}$, number of time slices $N_{TS}$, number of requests clusters per vehicle $k$
and population size to iterations ratio, was perform\added{ed} in our previous paper~\cite{Okulewicz2017} and discussed in detail in Appendix A.

{The controlling parameters of PSO and DE were tuned during an initial phase of the series of experiments and have not been changed since then. Parameters of GA were taken from the relevant literature \cite{DVRP:GA:TS:2007} and have not been subject of additional tuning. In order to keep the subsequent results comparable, a similar scenario was repeated in all experiments and, consequently, the main steering parameters were not altered or specifically tuned.}

\point{5. What’s the main difficulty or issue considering the dynamic VRP?}

\reply There are two main features that make DVRP an interesting problem:
\begin{itemize}
	\item already its static version is an NP-hard problem,
	\item a dynamic nature of the problem imposes {a large amount of uncertainty and requires} seeking the solution that would be optimal for the final
	set of requests {(which is only gradually revealed to the solving system).}
\end{itemize}

An introduction section of the manuscript has been updated to include the above-mentioned reasons.

\point{6. What’s the computational complexity of the proposed algorithm?}

\reply We presume that the question is about the complexity of computing the cost function.
This complexity can be estimated as $O(k\hat{n}m)$, where $k$ is the number of possible requests clusters
per vehicle, $\hat{n}$ is the number of necessary vehicles estimated by a heuristic algorithm,
and $m$ is the number of client's requests.

{Because the complexity of the function evaluation is larger than a typical level of $O(m)$ presented by other algorithms, in our previous paper we had additionally \replaced{tested}{presented test results of} our method in the computation time limits (set to 75 seconds on Intel Core i7 machine in order to be comparable with the results available in literature for 750 seconds on Pentium IV).
Detailed outcomes of this experiment are presented in Table 6. of \cite{Okulewicz2017}.}

A short discussion of that issue has been added in section \ref{article-sec:complexity}.

\end{reviewer}

\begin{reviewer}
\textbf{\\ \Large Response to Reviewer \#3}

\point{This paper proposed a metaheuristic approach to solve DVRP, and has sufficient experiments to demonstrate the performance of the proposed algorithm, which is significant.
However, this paper has the following issues to be improved before being accepted for publication.\\
1. You should reorganize your paper. For the details of DE and PSO, the reader can refer to [40] and [41], and not repetitively enumerate in Section 2. Thus, Section 2 should be deleted.}

\reply The reason for keeping Section 2. is that the research community does not seem  to agree as to which version of the PSO is considered to be the standard one. Therefore, for the sake of reproducibility of results a short description of PSO is provided in the paper. For the section cohesion, DE is also described in a comparable amount of space,
although it should be noted that DE naming convention is much more clear than that of various PSO versions.

\point{2. The authors have compared with some algorithms, such as GA, PSO, and MEMSO that are very old, which makes your experiments unconvincing. Why not compare the proposed algorithm with some ones that is published in 2017 or 2018? In addition, you should add some compared algorithms to verify the performance of the proposed algorithm.}

\reply To the best of our knowledge, within the computation budget of around
$10^6$ fitness function evaluations and average computational time around 100 seconds
on Intel Core i7 processor our 2MPSO \cite{Okulewicz2017}, Khouadjia's MEMSO \cite{DVRP:MEMSO} and Hanshar's GA \cite{DVRP:GA:TS:2007} are still the most efficient algorithms for solving DVRP.
In the case of higher computational budget only Mańdziuk and Żychowski's Memetic Algorithm \cite{mandziuk2016} was able to outperform 2MPSO in terms of best results and yielded comparable to 2MPSO average results.

The literature review presented in the manuscript has been enhanced by adding a discussion on 8 recent approaches
tested on Kilby's benchmarks (\cite{AbdAllah2017,Benaini2018,Chen2017,DVRP:ACOLNS,Sarasola2016,Xu2018,Yi2017,Zhou2017}). None of these methods was able to outperform the above-mentioned algorithms in terms of the average results.

\point{\\3. To highlight your contributions, please explain what your motivations are?\\%
4. Why the problem considered in your paper is studied or what are the differences between the existing DVRP and the one considered in your paper, and why you employed the proposed algorithm to solve the above problem?}

\reply The main contributions of the paper are highlighted in Section \textit{1.2.}. {Furthermore, the abstract has been rewritten and enhanced by adding an explicitly stated motivation of the research presented in the paper.}

In short, there are two main features that make DVRP an interesting problem:
\begin{itemize}
	\item {already} its static version is an NP-hard problem,
	\item a dynamic nature of the problem imposes {a large amount of uncertainty and requires} seeking the solution that would be optimal for the final
	set of requests {(which is only gradually revealed to the solving system).}
\end{itemize}

There are no differences between our DVRP version and the {main} version studied in the literature, apart from an additionally tested variant with higher-than-typical values of the \emph{cut-off time} in widely-used benchmark problems.

The proposed algorithm has been designed in response to MEMSO \cite{DVRP:MEMSO}, which utilizes a dedicated discretized version of PSO, with very promising results.
Our interest was in designing an approach that does not modify the PSO, but uses its generic continuous version. \deleted{can achieve similar or better results.}
This assumption led us to designing the clustering approach \cite{DVRP:2PSO}, which we have studied and improved in subsequent papers~\cite{DVRP:2MPSO,DVRP:MC,DVRP:Hyper,Okulewicz2017}, and finally
combined and then compared, in the current paper, with a similar approach of Ai and Kachitvichyanukul~\cite{Ai2009a} applied to static VRPs (presented in section 4.1.1) .
\end{reviewer}

\begin{reviewer}
\textbf{\\ \Large Response to Reviewer \#4}

\point{This is a good work about DVRP. The idea is nice. The design of experiments is comprehensive and solid, confirming the effectiveness of the proposed idea, especially about transforming DVRP into a continuous optimization problem by continuous encoding. Besides, the writing quality is fine.\\
Regarding the encoding, I expect the authors to make an analysis about whether the real optimal solution will be strictly covered by the continuous encoding or not.}

\reply In order to fully reply to this comment we need to make a small introduction:
\begin{enumerate}
\item First of all, to the best of our knowledge, optimal solutions for the Kilby's benchmark set \cite{DVRP:Study} are not known.
At the time of performing the experiments our algorithm was able to obtain best known results for 19 (out of 21) benchmark instances, as reported in \cite{DVRP:2MPSO} and~\cite{Okulewicz2017}. Therefore, at least for the distribution of requests within this benchmark set our approach allowed to achieve very high quality results.
These results are also available at our project website: \url{http://www.mini.pw.edu.pl/~mandziuk/dynamic/?page_id=67}

To the best of our knowledge, the currently best known results for the Kilby's benchmark set~\cite{DVRP:Study} were obtained by the memetic approach~\cite{mandziuk2016} (10 results), our ContVRP algorithm~\cite{Okulewicz2017} (6 results), GPU approach~\cite{Benaini2018} (4 results) and MEMSO~\cite{DVRP:MEMSO} (1 result).
\item In the proposed problem encoding, the optimal solution of a dynamic problem does not need to be covered/available through the entire optimization process.
The critical factor is whether at the time of making the ultimate decision about the final request-to-vehicle assignment, an optimal assignment can (in principle) be made.
We believe that this property could be the reason why our approach performed better with $40$ time slices instead of $25$ of them, typically used in the literature. {Due to this difference, final decisions regarding request-to-vehicle assignment, were made in each time step with respect to smaller (in average) groups of requests.}
\item Finally, if we consider a static problem or a frozen state of the system it seems that it is possible to construct a problem instance which, for any
given number of requests clusters per vehicle $k$, would make obtaining the optimal solution with a proposed continuous encoding impossible
.

\begin{center}
\fbox{\includegraphics[scale=1.3]{Figures/AntiProof}}\\
{A sketch of a construction supporting inability to achieve the optimal solution with the proposed encoding.
\added{The numbers below and above the locations denote the respective request size.}}
\end{center}

In the above figure it is easy to observe that an optimal solution could be achieved with just two vehicle's routes (assuming a vehicles capacity to be equal to $100$).
\added{With the first route consisting of the light grey locations, and the second route of the dark grey ones.}
\replaced{As all those locations are positioned on the same line}{Meanwhile}, no distribution of cluster centers with $k$ clusters per vehicle that could create such an assignment exists, what leads to the necessity of utilizing the third vehicle.

\end{enumerate}

\point{Similar to the random key encoding, the continuous encodings about priorities or cluster centres imply much redundancy, that is, the same solution may have numerous different encodings. I wonder why the redundancy brings in much benefits, instead of harm, to the search for optimal or near-optimal solutions. The authors should at least make some analysis on this.}

{While this issue was not directly investigated in the paper, we believe that the following intuitive explanation is valid:
\begin{itemize}
	\item Each solution in the proposed search space relies on clustering the requests, and only particularly ill-conditioned benchmarks would not be tractable by
	such an approach.
	\item Therefore, low quality solutions are encountered relatively rarely in this search space. The same cannot be said about the search spaces where random assignment
	of requests is possible.
	\item The proposed search space is also more robust in terms of accommodating the incoming requests. If new request can be accepted to one of the already existing clusters
	the rest of the solution remains unchanged. In practice, this property allows spending more computational time on searching for the solution.
\end{itemize}

The above reasoning has been included in the conclusions of the manuscript (section \ref{article-sec:conclusions}).
}

\added{Additionally, for the dynamic problem the utilized encoding is in fact not as redundant
as it would be for a static instance of a problem.
Please observe, that when a particular vehicle has at least one ultimately assigned request
it immediately becomes unique, so its cluster centers cannot be switched with other vehicles,
as this would lead to a different VRP solution.}

\added{The only redundancy that remains through the whole optimization process is possibility of switching
the cluster centers belonging to the same vehicle, as it would result in achieving identical solutions.}

\point{Some minor points:
Page 14, Line 402: consist in $\rightarrow$ consists in }

\reply The language of the article has been reviewed.

\end{reviewer}

\closing{Yours sincerely,}
\end{letter}

\bibliographystyle{plain}
\bibliography{SWEVO,SWEVO2,EvCoContinuousDVRP,R2Surveys,R2Kilbys}

\end{document}
