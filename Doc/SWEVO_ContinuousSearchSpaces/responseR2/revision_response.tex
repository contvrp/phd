\documentclass[10pt,a4paper]{letter}

\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage[table]{xcolor}

\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{url}
\usepackage[final]{changes}
\usepackage{cite}

\usepackage{graphicx}

\usepackage{letterbib}
\usepackage{xr}
\externaldocument[article-]{../SWEVO_ContinuousDVRP}

\hyphenation{presence}

\newcommand{\initresponses}{\newcounter{pointcounter}}

\newenvironment{reviewer}{\setcounter{pointcounter}{1}}{}

\newcommand{\point}[1]{\medskip \noindent
               \textsl{{\fontseries{b}\selectfont Comment \#\thepointcounter}.
                 \stepcounter{pointcounter} #1}}
\newcommand{\reply}{\medskip \noindent \textbf{Reply:}\ }


\signature{\vspace{-4em}Michał Okulewicz\\
Jacek Mańdziuk}

\begin{document}
\begin{letter}{}
\opening{Dear Dr Swagatam Das,}

We would like to thank Associate Editor and the Reviewers for their helpful comments and for an opportunity to improve the manuscript.

The most relevant change compared to the previous version of the manuscript is addition of Table \ref{article-tab:relevant}, along with an associated description presented in section \ref{article-sec:penalty-impact}. The table presents a requested comparison of our method with selected recent approaches on Kilby et al.'s benchmark set, together with statistical significance tests.

Below please find a detailed response to the Reviewers’ comments.

\initresponses
\begin{reviewer}
\textbf{\\ \Large Response to Reviewer \#1}

\point{There are still some typos and grammatical errors throughout the manuscript. Please proofread.}

\reply The language has been thoroughly revised. We hope that the current version of the paper meets the language quality expectations.

\point{The components of the proposed algorithm are from the literature including the encoding, the clustering method, the parallel computing framework, the algorithms (PSO, DE, GA) and etc. The design of proposed algorithm is lack of innovation and theoretical explanation.}

\reply
The contribution of the paper is presented in~section~\ref{article-sec:main-contribution}. The main focus of proposed research is on understanding a relation between a DVRP encoding and the efficacy of corresponding results. In this context, to the best of our knowledge, the following aspects of DVRP have not yet been studied in the literature (at least not to the extent proposed in this paper):
\begin{itemize}
	\item addressing directly the fact that the final set of requests is larger than the initial set by means of a penalty term added to the quality function,
	\item considering the search space which contains both priorities and multi-clusters,
	\item application of DE to the DVRP,
	\item analysis of solution stability during the optimization process,
	\item hardware independent comparison between discrete and continuous encodings with respect to the number of fitness function evaluations.
\end{itemize}

In effect, {\bf the paper proposes a new methodology of approaching discrete optimization problem with continuous optimization algorithm.}
Generally speaking, the proposed approach can be summarized in the two following steps:
%
\begin{itemize}
	\item Identification of the key features of the problem, based on which a complete valid solutions can be obtained with a simple heuristic.
	\item Designing a continuous search space based on these features such that small movements in this space result in adequately small changes in the problem solution.
\end{itemize}

%
%{\color{red}
%Apart from the VRP, we have successfully applied this methodology to a variant of packing and stock cutting problem in: \textit{Okulewicz M., 2-Dimensional Rectangles-in-Circles %Packing and Stock Cutting with Particle Swarm Optimization, 2017 IEEE Symposium Series on Computational Intelligence (SSCI) Proceedings (2017) pp. 3125-3129}
%}
%
\point{The comparison algorithms are too old, the author should compare more up-to-date algorithms. Moreover, the statistical tests are missing.}

\reply A new table (Table~\ref{article-tab:relevant}) presenting comparison of our results with relevant new approaches has been added. A related discussion has been placed at the end of section~\ref{article-sec:penalty-impact} and has extended a brief summary of experimental comparison previously included in section \ref{article-sec:recent-dvrp}.

Statistical relevance of numerical results has been tested in the paper based on the Student $t$-test for differences in mean results, including the newly added Table~\ref{article-tab:relevant}. Application of $t$-test was justified by the fact that our sample sizes are large enough (30 experiments for each results). At the same time the Wilcoxon–Mann–Whitney test could not be applied due to the lack of required median values for the literature algorithms proposed by other authors.

\end{reviewer}

\begin{reviewer}
\textbf{\\ \Large Response to Reviewer \#2}

\point{The authors have answered all of the comments.}

\reply Thank you for your time.

\end{reviewer}

\begin{reviewer}
\textbf{\\ \Large Response to Reviewer \#3}

\point{The authors have not compared the proposed algorithm with some ones that is published in 2017  or 2018, which makes your experiments unconvincing. In fact, there are some papers or algorithms for dynamic vehicle routing problem, such as,
\begin{itemize}
\item Okulewicz M, Mańdziuk J. The impact of particular components of the PSO-based algorithm solving the Dynamic Vehicle Routing Problem. Applied Soft Computing, 2017.
\item Chen S, Rong C, Wang G G, et al. An adaptive large neighborhood search heuristic for dynamic vehicle routing problems. Computers \& Electrical Engineering, 2018.
\item Yi R, Luo W, Bu C, et al. A hybrid genetic algorithm for vehicle routing problems with dynamic requests. Computational Intelligence. 2018.
\end{itemize}
and so on.}

\reply A new table (Table~\ref{article-tab:relevant}) presenting comparison of our results with relevant new approaches has been added. A related discussion has been placed at the end of section~\ref{article-sec:penalty-impact} and has extended a brief summary of experimental comparison previously included in section \ref{article-sec:recent-dvrp}.

Table~\ref{article-tab:relevant} includes, among others, results of the algorithms presented in the two above-suggested papers:
%
\begin{itemize}
\item [{[11]}] Okulewicz M, Mańdziuk J. The impact of particular components of the PSO-based algorithm solving the Dynamic Vehicle Routing Problem. Applied Soft Computing, 58, (2017), 586--604.
\item [{[46]}] Yi R, Luo W, Bu C, et al. A hybrid genetic algorithm for vehicle routing problems with dynamic requests. 2017 IEEE Symposium Series on Computational Intelligence (SSCI), IEEE, (2017) 1--8.
\end{itemize}

The third paper proposed by the Reviewer ``An adaptive large neighborhood search heuristic for dynamic vehicle routing problems'' was not included in the experiments and the bibliography list since it solves the Dynamic Vehicle Routing Problem with Time Windows, and therefore is not well suited for Kilby et al.'s set of benchmark instances. However, two other relevant algorithms utilizing various forms of neighborhood search are compared and cited in the manuscript:
%
\begin{itemize}
\item [{[5]}] M. J. Elhassania, B. Jaouad, E. A. Ahmed, A new hybrid algorithm to solve the vehicle routing problem in the dynamic environment, International Journal of Soft Computing 8(5), (2013), 327--334.
\item [{[43]}] B. Sarasola, K. F. Doerner, V. Schmid, E. Alba, Variable neighborhood search for the stochastic and dynamic vehicle routing problem, Annals of Operations Research 236(2), (2016), 42--461. doi:10.1007/s10479-015-1949-7.
%\item [57] M. R. Khouadjia, B. Sarasola, E. Alba, L. Jourdan, E.-G. Talbi, A comparative study between dynamic adapted PSO and VNS for the vehicle routing problem with dynamic %requests, Applied Soft Computing 12 (4) (2012) 1426 1439. doi:10.1016/j.asoc.2011.10.023. {\color{red}(describing the same type of VNS as in [43])}.
\end{itemize}

%{\color{red}It has also been stated more clearly, that the basic configuration of the PSO--based approach is the same as the one presented in the first paper listed by the Reviewer %-- our previous work published in the Applied Soft Computing journal.}
%
\end{reviewer}

\begin{reviewer}
\textbf{\\ \Large Response to Reviewer \#4}

\point{No more comments}

\reply Thank you for your time.

\end{reviewer}

\closing{Yours sincerely,}
\end{letter}

%\bibliographystyle{plain}
%\bibliography{SWEVO,SWEVO2,EvCoContinuousDVRP,R2Surveys,R2Kilbys}

\end{document}
