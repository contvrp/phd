\documentclass{scrartcl}

\usepackage[utf8]{inputenc} % kodowanie
\usepackage[OT4]{fontenc} % nowe czcionki dla jęz. europejskich

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{tabularx}
\usepackage{float}
\usepackage{url}
 
\usepackage[polish]{babel} % po polsku

\begin{document}
\setlength{\floatsep}{4pt}
\setlength{\textfloatsep}{4pt}
\title{Zastosowanie algorytmów inspirowanych naturą i zachowaniami społecznymi w~rozwiązywaniu dynamicznych problemów optymalizacyjnych}
\subtitle{Sprawozdanie z pracy naukowej za semestr zimowy 2012/13}

\author{Micha{\l} Okulewicz}

\maketitle

\begin{abstract}
W pierwszej części dokumentu przypomniane są podstawowe
zagadnienia dotyczące tematyki badań, przedstawione w planie badawczym.
W drugiej części zaprezentowane zostały wyniki badań nad zastosowaniem
dwufazowego algorytmu optymalizacji rojowej (\textit{Particle Swarm Optimization}) w problemie dynamicznej marszrutyzacji (\textit{Dynamic Vehicle Routing Problem}) przeprowadzonych
w pierwszym semestrze roku akademickiego 2012/13 oraz wskazane dalsze
obszary prac na drugi semestr b.r.
\end{abstract}

\section{Podstawowe zagadnienia}
\subsection{Dynamiczny problem optymalizacyjny}
Problem optymalizacyjny to zadanie do zaplanowania lub wykonania, które można zaprezentować w postaci funkcji:
\begin{center}
$f:\mathbb{R}^n\rightarrow\mathbb{R}$
\end{center}
dla której poszukujemy $x^{*} \in \mathbb{R}^n$ takiego, że: 
\begin{center}
$\forall_{x \in \mathbb{R}^n} f(x) \geq f(x^{*})$
\end{center}

Wśród problemów optymalizacyjnych możemy wyróżnić problemy dynamiczne,
tzn. takie w których optymalizowany problem podlega zmianom w czasie prowadzenia optymalizacji.
Optymalizowana funkcja przybiera wtedy postać:
\begin{center}
$f:\mathbb{R}^n\times T \rightarrow\mathbb{R}$
\end{center}
gdzie $T$ jest zbiorem momentów czasu $t$, w których chcemy znaleźć $x^{*}(t) \in \mathbb{R}^n$ taki, że:
\begin{center}
$\forall_{x \in \mathbb{R}^n} f(x,t) \geq f(x^{*}(t),t) \wedge t \in T$
\end{center}

Najprostszym podejściem w rozwiązywaniu problemów dynamicznych może być zastosowanie dla każdej chwili $t \in T$ znanego algorytmu do rozwiązywania odpowiedniego problemu statycznego, ale takie postępowanie może być mało wydajne.
Jeżeli zmiany zachodzące w zadaniu wraz z upływem czasu są niewielkie albo mają wręcz ciągły charakter, lepszym podejściem wydaje się zastosowanie wiedzy o rozwiązaniu problemu w poprzedniej chwili czasowej i dostosowanie go do bieżącej sytuacji.
Przykładem problemu dynamicznego, którym obecnie się zajmuję jest problem dynamicznej marszrutyzacji (\textit{Dynamic Vehicle Routing Problem}), który jest bliżej zaprezentowany w sekcji \ref{sec:DVRP}.

Wśród algorytmów optymalizacyjnych szczególnie interesujące są algorytmy bazujące na idei Inteligencji Rojowej (\textit{Swarm Intelligence}). Postulatem algorytmów Inteligencji Rojowej jest osiąganie skomplikowanych celów przez wiele niezależnych prostych bytów (kierujących się prostymi i jednakowymi zasadami) dzięki ich wzajemnej komunikacji. Jednym z przykładów takiego algorytmu jest Optymalizacja Rojowa (\textit{Particle Swarm Optimization}), która bliżej zaprezentowana jest w sekcji \ref{sec:PSO}.
Obecnie istnieją również algorytmy odchodzące od postulatu prostoty czy jednakowości bytów, mające na celu modelować bardziej skomplikowane zachowania społeczne. Przykładami takich algorytmów jest Algorytm Cywilizacyjny \cite{SI:CSO}, czy Sztucznego Ula (\cite{SI:ABC}).

Algorytmy Inteligencji Rojowej oraz algorytmy społeczne wydają się być szczególnie dobrym wyborem do rozwiązywania problemów dynamicznych, gdyż ze swego założenia mają modelować zachowanie żywych organizmów, a te działają w dynamicznym środowisku.
\subsection{Optymalizacja Rojowa (PSO)}
\label{sec:PSO}
Algorytm Optymalizacji Rojowej (\textit{Particle Swarm Optimization}) został po raz pierwszy zaprezentowany w 1995 roku przez Kennedy'ego i Eberharta \cite{PSO:Introduction} i jest wciąż analizowany i aktualizowany \cite{PSO:Modified,PSO:Inertia,PSO:Params,PSO:Dynamic,Optimization:BIAS,Optimization:BIAS2}.
Algorytm PSO jest algorytmem iteracyjnym, w którym optymalizacja jest wykonywana przy użyciu roju cząstek posiadających położenie $x$ i prędkość $v$ oraz komunikujących się ze swoimi sąsiadami. Położenie cząsteczki $p$ jest argumentem optymalizowanej funkcji $f$. Prędkość $v$ cząsteczki w $i+1$ iteracji $p$ jest aktualizowana w każdym kroku algorytmu w następujący sposób:
\begin{center}
	$p.v_{i+1} = u_1 * g * (p.neighbours.best.x - p.x_i) + u_2 * l * (p.history.best.x - p.x_i) + u_3 * i * p.v_i$
\end{center}
gdzie
\begin{itemize}
\item $neighbours.best.x$ to najlepsze (niekoniecznie aktualne) z położeń (w sensie optymalizowanej funkcji) sąsiadów cząsteczki,
\item $history.best.x$ to najlepsze położenie cząsteczki znane do danej iteracji,
\item $u_1$, $u_2$, $u_3 \sim U(0,1)$ są zmiennymi z rozkładu jednostajnego na przedziale $[0,1]$
\item $g$ (współczynnik przyciągania globalnego), $l$ (współczynnik przyciągania lokalnego), $i$ (współczynnik bezwładności) oraz sposób wyboru sąsiedztwa są parametrami algorytmu.
\end{itemize}

Aktualną implementację algorytmu, listę prac oraz otwartych problemów można znaleźć na \textit{Particle Swarm Central} \cite{Algorithm:PSO2011}.

\subsection{Problem dynamicznej marszrutyzacji (DVRP)}
\label{sec:DVRP}
Problem marszrutyzacji (\textit{Vehicle Routing Problem})\cite{DVRP:Study,DVRP:Benchmark,DVRP:Best} jest modyfikacją problemu komiwojażera. Polega na znalezieniu najkrótszej łącznej trasy dla floty pojazdów dostawczych z dodatkowym ograniczeniem na pojemność każdego z pojazdów oraz informacją o wielkości zamówień. W dynamicznej wersji, którą się obecnie zajmuję, zamówienia pojawiają się w ciągu dnia roboczego, ale nie znikają ani nie podlegają zmianom. Jest to tzw. wariant \textit{Vehicle Routing Problem with Dynamic Requests}.

\subsection{Aktualny stan badań}
Algorytm PSO został z powodzeniem zastosowany do rozwiązywania problemu DVRP \cite{DVRP:DAPSO,DVRP:MAPSO}, uzyskując lepsze wyniki na znanych przypadkach testowych \cite{DVRP:Benchmark} niż algorytm genetyczny \cite{DVRP:GA:TS}, przeszukiwanie z tabu \cite{DVRP:GA:TS} oraz algorytm mrówkowy\cite{DVRP:Ants} co motywuje do prowadzenia dalszych badań w tym kierunku. Należy też podkreślić, że zarówno algorytmy oparte na PSO oraz algorytm mrówkowy oprócz metaheurystyki do przydzielania zamówień wykorzystywały algorytm 2-opt \cite{TSP:2OPT} do rozwiązywania problemu komiwojażera dla poszczególnych pojazdów.

\subsection{Zastosowane podejście do rozwiązania problemu DVRP}
W zastosowanym podejściu wykorzystuję wyłącznie algorytm PSO, zarówno do dokonania przydziału zamówień do pojazdów, jak również do rozwiązania problemu komiwojażera dla każdego z pojazdów.
Ponadto, w przeciwieństwie do przytoczonych wcześniej podejść z wykorzystaniem PSO, utrzymuję problem zakodowany w sposób ciągły, co umożliwia bezpośrednie stosowanie tego algorytmu (jak również potencjalnie innych algorytmów optymalizujących funkcje rzeczywiste) bez dodatkowych modyfikacji ich działania.

Problem DVRP rozwiązuję dwuetapowo niezależnymi instancjami algorytmu: najpierw optymalizując przydział zamówień do pojazdów a następnie optymalizując trasę dla każdego z pojazdów. W pierwszym etapie argumentem są położenia centrów obszarów operacyjnych dla pojazdów. Zamówienia są przypisywane do dostępnego pojazdu z najbliżej położonym centrum obszaru operacyjnego.
W drugim etapie algorytmu optymalizuję trasę każdego z pojazdów rozwiązując niezależne problemy komiwojażera. Argumentem w drugiej fazie jest kolejność obsługi zamówień na trasie pojazdu, zakodowana jako ranga przypisany każdemu z zamówień. Posortowanie zamówień po randze decyduje o kolejności ich obsłużenia.
\newpage
\section{Przeprowadzone eksperymenty}
W drugiej części prezentuję wyniki przeprowadzonych eksperymentów.

\subsection*{Zbadanie wpływu rozkładu położenia zamówień w przestrzeni oraz rozkładu wielkości zamówień na działanie algorytmu}

\begin{figure}[h]
\caption{Zależność uzyskanych wyników od współczynników instancji problemu}
\label{rys:miary}
\includegraphics[scale=0.55,natwidth=1024,natheight=600]{rys/2MPSO.png}
\end{figure}

Wstępne obserwacje sugerowały, że proponowany algorytm
radzi sobie lepiej dla instancji testowych problemu,
w których rozkład zamówień jest symetryczny i zbliżony
do jednostajnego a zadania mają wyraźne skupiska w przestrzeni.

Na potrzeby analizy dla każdej z instancji problemu zostały obliczone m.in.:
\begin{itemize}
\item $skewness$ -  \textbf{współczynnik skośności rozkładu wielkości} zadań ,
\item $clusterization$ - \textbf{współczynnik skupienia przestrzennego} zadań,
\item $min.vehicles$ - \textbf{ograniczenie dolne liczby tras/pojazdów} w jakich
można instancję problemu obsłużyć.
\end{itemize}
Jako $skewness$ został przyjęty trzeci
zestudentyzowany moment centralny.
Jako $clusterization$ zostało przyjęte maksimum z 30 eksperymentów
wyszukiwania klas za pomocą sieci Kohonena z wartości wyrażenia $ \frac{n}{\overline{error} \cdot \#class}$, gdzie $n$ jest liczbą zamówień,
$\overline{error}$ średnim błędem (odległością zamówienia od środka klasy) a
$\#class$ liczbą klas.
Natomiast $min.vehicles$ to sumaryczna objętość zamówień podzielona przez ładowność pojazdu.

Rysunek \ref{rys:miary} prezentuje zależność najlepszego wyniku (liczonego jako stosunek
długości najkrótszej trasy w stosunku do
najkrótszej znanej trasy w problemie statycznym) od poszczególnych współczynników
dla danej instancji problemu.

\begin{figure}[h]
\begin{center}
\caption{Zależność uzyskanych wyników od względnego współczynnika trudności instancji}
\label{rys:trudn}
\includegraphics[scale=0.55,natwidth=280,natheight=300]{rys/2MPSO_wsp.png}
\end{center}
\end{figure}

Dla trzech współczynników wymienionych na początku dało się zauważyć
zależność wartości uzyskiwanego rozwiązania od nich. W związku z czym
został zaproponowany względny współczynnik trudności zadania:

\begin{equation}
\scriptsize
\label{rown:trudn}
hardness = 
max\left(
-\frac{clusterization - \overline{clusterization}}{S_{clusterization}},
\frac{skewness - \overline{skewness}}{S_{skewness}},
\frac{min.vehicles - \overline{min.vehicles}}{S_{min.vehicles}}
\right)
\end{equation}

Rysunek \ref{rys:trudn} prezentuje zależność wyników od współczynnika trudności zaproponowanego w wyrażeniu (\ref{rown:trudn}).

\subsection*{Zastosowanie algorytmu do działania w trybie rzeczywistym} 
\begin{table}[h!]
\caption{Porównanie wyników zastosowanego podejścia wielorojowego (M2PSO), zastosowanego podejścia jednorojowego (2PSO) oraz podejścia
wielorojowego z literatury (MAPSO). Najlepsze wyniki zostały wytłuszczone.}
\begin{center}
\label{tab:comparison}
\begin{tabular*}{1.0\textwidth}{@{\extracolsep{\fill}}|c|rrrrrr|}
\hline \multirow{3}{*}{Instancja\cite{DVRP:Benchmark}} &  \multicolumn{6}{c|}{Algorytm} \\
& \multicolumn{2}{c}{2MPSO}
 & \multicolumn{2}{c}{2PSO} & \multicolumn{2}{c}
 {MAPSO\cite{DVRP:MAPSO}} \\
 & Min. & Śr. & Min. & Śr. & Min. & Śr.\\
\hline\hline
 c50 & 597.18 & 633.44 & 582.88 & 675.14 & \textbf{571.34} & \textbf{610.67} \\
 c75 & 929.35 & 984.77 &  \textbf{912.23} & 1015.16 & 931.59 & \textbf{965.53} \\
 c100 & 976.73 & 1056.45 & 996.40 & 1149.48 & \textbf{953.79} & \textbf{973.01} \\
 c100b & \textbf{828.94} & 860.93 & \textbf{828.94} & \textbf{850.68} & 866.42 & 882.39 \\
 c120 & \textbf{1084.92} & \textbf{1138.39} & 1087.04 & 1212.38 & 1223.49 & 1295.79 \\
 c150 & \textbf{1164.83} & \textbf{1257.20} & 1173.94 & 1336.84 & 1300.43 & 1357.71 \\
 c199 & \textbf{1444.01} & \textbf{1506.74} & 1446.93 & 1578.99 & 1595.97 & 1646.37 \\
\hline\hline
 f71 & 305.54 & 341.97 & 315.00 & 356.75 & \textbf{287.51} & \textbf{296.76} \\
 f134 & 12813.53 & \textbf{13179.18} & \textbf{12813.14} & 13491.60 & 15150.50  & 16193.00 \\
\hline\hline
 tai75a & \textbf{1791.95} & 1951.23 & 1871.06 & 2142.07 & 1794.38 & \textbf{1849.37}  \\
 tai75b & 1442.22 & 1518.25 & 1460.95 & 1568.21 & \textbf{1396.42} & \textbf{1426.67} \\
 tai75c & 1567.33 & 1676.99 & 1500.23 & 1811.08 & \textbf{1483.10} & \textbf{1518.65}  \\
 tai75d & 1436.99 & 1483.24 & 1462.82 & 1586.28 & \textbf{1391.99} & \textbf{1413.83}  \\
 tai100a & 2297.99 & 2442.46 & 2317.76 & 2707.61 & \textbf{2178.86} & \textbf{2214.61}  \\
 tai100b & 2166.98 & 2291.85 & 2187.86 & 2510.60 &\textbf{ 2140.57} & \textbf{2218.58}   \\
 tai100c & 1504.08 & 1599.69 & 1564.25 & 1672.33 & \textbf{1490.40} & \textbf{1550.63} \\
 tai100d & \textbf{1820.39} & 1958.91 & 1859.70 & 2220.01 & 1838.75 & \textbf{1928.69} \\
 tai150a & 3614.99 & 3824.43 & 3638.75 & 4151.31 & \textbf{3273.24} & \textbf{3389.97} \\
 tai150b & 3043.12 & 3209.66 & 3107.95 & 3302.94 & \textbf{2861.91} & \textbf{2956.84} \\
 tai150c & 2758.66 & 2874.09 & 2781.02 & 2952.88 & \textbf{2512.01} & \textbf{2671.35} \\
 tai150d & 3064.58 & 3226.06 & 3048.24 & 3478.49 & \textbf{2861.46} & \textbf{2989.24} \\
\hline\hline
Suma & \textbf{46654.31} & \textbf{49015.94} & 46957.09 & 51770.84 & 48104.13 & 50349.66  \\
\hline
\end{tabular*}
\end{center}
\end{table}

Ze względu na to, że średnia długość drogi uzyskiwana przez zastosowane podejście była dosyć duża (zarówno w stosunku do najlepszych wyników
znanych z literatury jak i najlepszych wyników uzyskanych przez
zastosowane podejście), został przeprowadzony eksperyment polegający
na zastosowaniu wielu (w prezentowanym przykładzie 8) równoległych rojów, wymieniającymi między sobą najlepsze rozwiązanie pod koniec każdego kroku czasowego.
To podejście pozwoliło na znaczne poprawienie średnich wyników
z nieznaczną tylko stratą wyników najlepszych w niektórych przypadkach w porównaniu z podejściem jednorojowym oraz pozwoliła uzyskać lepsze minimalne wyniki w dwóch dodatkowych przypadkach
w stosunku do podejścia z literatury.

Tabela \ref{tab:comparison} prezentuje porównanie minimalnych i średnich wyników działania algorytmów wielorojowego (2MPSO), jednorojowego (2PSO) i wielorojowego zaprezentowanego w \cite{DVRP:MAPSO} (MAPSO).

\subsection*{Weryfikacja przekazywania wiedzy historycznej}

\begin{table}[h!]
\caption{Porównanie wyników zastosowanego podejścia jednorojowego z przekazywaniem wiedzy historycznej (2PSO+hist) i bez niego (2PSO-hist)}
\begin{center}
\label{tab:hist}
\begin{tabular*}{1.0\textwidth}{@{\extracolsep{\fill}}|c|rrrr|}
\hline \multirow{3}{*}{Instancja\cite{DVRP:Benchmark}} &  \multicolumn{4}{c|}{Algorytm} \\

 & \multicolumn{2}{c}{2PSO+hist} & \multicolumn{2}{c|}
 {2PSO-hist} \\
 & Min. & Śr. & Min. & Śr.\\
\hline\hline
 c50 & 582.88 & 675.14 & 664.81 & 740.84 \\
 c75 &  912.23 & 1015.16 & 1048.40 & 1127.68 \\
 c100 & 996.40 & 1149.48 & 1073.32 & 1245.17 \\
 c100b & 828.94 & 850.68 & 830.42 & 843.66 \\
 c120 & 1087.04 & 1212.38 & 1132.72 & 1225.50 \\
 c150 & 1173.94 & 1336.84 & 1440.77 & 1617.51 \\
 c199 & 1446.93 & 1578.99 & 2057.46 & 2264.11 \\
\hline\hline
 f71 & 315.00 & 356.75 & 361.89 & 430.82 \\
 f134 & 12813.14 & 13491.60 & 14199.40 & 16014.09 \\
\hline\hline
 tai75a & 1871.06 & 2142.07 & 2293.31 & 2662.20  \\
 tai75b & 1460.95 & 1568.21 & 1606.57 & 1853.05 \\
 tai75c & 1500.23 & 1811.08 & 1759.97 & 2135.28   \\
 tai75d & 1462.82 & 1586.28 & 1505.83 &  1801.09 \\
 tai100a & 2317.76 & 2707.61 & 3034.34 & 3447.18  \\
 tai100b & 2187.86 & 2510.60 & 2412.56 & 3136.52   \\
 tai100c & 1564.25 & 1672.33 & 1680.62 & 2028.9  \\
 tai100d & 1859.70 & 2220.01 & 2060.26 & 2382.48 \\
 tai150a & 3638.75 & 4151.31 & 4456.29 & 5016.12 \\
 tai150b & 3107.95 & 3302.94 & 3751.56 & 4966.28 \\
 tai150c & 2781.02 & 2952.88 & 2985.95 & 3204.07 \\
 tai150d & 3048.24 & 3478.49 & 3722.83 & 4554.70 \\
\hline
\end{tabular*}
\end{center}
\end{table}

W celu zweryfikowania poprawności podejścia polegającego
na umieszczaniu w kolejnych krokach czasowych cząstek z roju
znajdujących się w najlepszym znanym rozwiązaniu z poprzedniego
kroku czasowego został przeprowadzony eksperyment polegający
na obliczaniu w każdym kroku instancji problemu od początku.
Wyniki uzyskiwane przez algorytm, w którym była przekazywana
wiedza o najlepszym rozwiązaniu uzyskał znacznie lepsze wyniki
w porównaniu z algorytmem, w którym nie było przekazywania tej wiedzy.
Tabela \ref{tab:hist} prezentuje porównanie wyników
wariantów z przekazywaniem wiedzy historycznej i bez niego dla jednorojowego 2PSO.

\section{Dalsze planowane prace}

Dalsze planowane prace dotyczą:
\begin{itemize}
\item zmiany sposobu kodowania przydziału zamówień na umożliwiający
przydzielenie dowolnego zamówienia do dowolnego pojazdu,
\item zbadania wpływu algorytmu 2-opt dla problemu komiwojażera na ostateczny wynik działania algorytmu,
\item dalszej analizy zależności uzyskiwanych wyników od rodzaju
instancji problemu.
\end{itemize}

\section{Lista prezentacji i publikacji}

Wyniki badań zostały zaprezentowane na następujących wystąpieniach
i publikacjach:
\begin{enumerate}
\item M.Okulewicz, Zastosowanie metody PSO
w Dynamic Vehicle Routing Problem, Seminarium Intelgencji Obliczeniowej na Wydziale MiNI Politechniki Warszawskiej, 26.10.2012, \url{http://www.mini.pw.edu.pl/~mandziuk/2012-10-26.pdf}
\item M.Okulewicz, Zastosowanie wielorojowego PSO w Dynamic Vehicle Routing Problem, Seminarium Intelgencji Obliczeniowej na Wydziale MiNI Politechniki Warszawskiej, 26.02.2013, \url{http://www.mini.pw.edu.pl/~mandziuk/2013-02-26.pdf}
\item M.Okulewicz, J.Mańdziuk, Application of Particle Swarm Optimization Algorithm to Dynamic Vehicle Routing Problem, LNAI, przyjęta na konferencję ICAISC 2013
\end{enumerate}

\bibliographystyle{splncs03}
\bibliography{PSO_in_DVRP}

~\\
~\\
~\\
\begin{center}
\begin{tabularx}{1.0\textwidth}{c X c}
opiekun naukowy & & doktorant \\ 
 &  &   \\ 
 &  &   \\ 
......................................... &  &  ......................................... \\ 
prof. dr hab. Jacek Mańdziuk & & mgr inż. Michał Okulewicz \\ 
 
\end{tabularx} 
\end{center}
\end{document}
