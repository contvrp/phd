\documentclass{scrartcl}

\usepackage[utf8]{inputenc} % kodowanie
\usepackage[OT4]{fontenc} % nowe czcionki dla jęz. europejskich

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{float}
\usepackage{url}
 
\usepackage[polish]{babel} % po polsku

\begin{document}
\setlength{\floatsep}{4pt}
\setlength{\textfloatsep}{4pt}
\title{Plan badawczy}
\subtitle{Zastosowanie algorytmów inspirowanych naturą i zachowaniami społecznymi w rozwiązywaniu dynamicznych problemów optymalizacyjnych}

\author{Micha{\l} Okulewicz}

\maketitle

\begin{abstract}
W dokumencie zaprezentowany został opis klasy problemów oraz metod ich rozwiązywania, którymi chciałbym się zająć w ramach projektu badawczego. Omówiony został również przykład takiego problemu (\textit{Dynamic Vehicle Routing Problem}) oraz metoda (\textit{Particle Swarm Optimization}) wraz z aktualnymi wynikami. Na końcu zaprezentowane zostały planowane prace na rok akademicki 2012/13.
\end{abstract}

\section{Wstęp}

Problem optymalizacyjny to zadanie do zaplanowania lub wykonania, które można zaprezentować w postaci funkcji:
\begin{center}
$f:\mathbb{R}^n\rightarrow\mathbb{R}$
\end{center}
dla której poszukujemy $x^{*} \in \mathbb{R}^n$ takiego, że: 
\begin{center}
$\forall_{x \in \mathbb{R}^n} f(x) \geq f(x^{*})$
\end{center}

Wśród problemów optymalizacyjnych możemy wyróżnić problemy dynamiczne,
tzn. takie w których optymalizowany problem podlega zmianom w czasie prowadzenia optymalizacji.
Optymalizowana funkcja przybiera wtedy postać:
\begin{center}
$f:\mathbb{R}^n\times T \rightarrow\mathbb{R}$
\end{center}
gdzie $T$ jest zbiorem momentów czasu $t$, w których chcemy znaleźć $x^{*}(t) \in \mathbb{R}^n$ taki, że:
\begin{center}
$\forall_{x \in \mathbb{R}^n} f(x,t) \geq f(x^{*}(t),t) \wedge t \in T$
\end{center}

Najprostszym podejściem w rozwiązywaniu problemów dynamicznych może być zastosowanie dla każdej chwili $t \in T$ znanego algorytmu do rozwiązywania odpowiedniego problemu statycznego, ale takie postępowanie może być mało wydajne.
Jeżeli zmiany zachodzące w zadaniu wraz z upływem czasu są niewielkie albo mają wręcz ciągły charakter, lepszym podejściem wydaje się zastosowanie wiedzy o rozwiązaniu problemu w poprzedniej chwili czasowej i dostosowanie go do bieżącej sytuacji.
Przykładem problemu dynamicznego, którym obecnie się zajmuję jest problem dynamicznej marszrutyzacji (\textit{Dynamic Vehicle Routing Problem}), który jest bliżej zaprezentowany w sekcji \ref{sec:DVRP}.

Wśród algorytmów optymalizacyjnych szczególnie interesujące są algorytmy bazujące na idei Inteligencji Rojowej (\textit{Swarm Intelligence}). Postulatem algorytmów Inteligencji Rojowej jest osiąganie skomplikowanych celów przez wiele niezależnych prostych bytów (kierujących się prostymi i jednakowymi zasadami) dzięki ich wzajemnej komunikacji. Jednym z przykładów takiego algorytmu jest Optymalizacja Rojowa (\textit{Particle Swarm Optimization}), która bliżej zaprezentowana jest w sekcji \ref{sec:PSO}.
Obecnie istnieją również algorytmy odchodzące od postulatu prostoty czy jednakowości bytów, mające na celu modelować bardziej skomplikowane zachowania społeczne. Przykładami takich algorytmów jest Algorytm Cywilizacyjny \cite{SI:CSO}, czy Sztucznego Ula (\cite{SI:ABC}).

Algorytmy Inteligencji Rojowej oraz algorytmy społeczne wydają się być szczególnie dobrym wyborem do rozwiązywania problemów dynamicznych, gdyż ze swego założenia mają modelować zachowanie żywych organizmów, a te działają w dynamicznym środowisku.
\section{Optymalizacja Rojowa (PSO)}
\label{sec:PSO}
Algorytm Optymalizacji Rojowej (\textit{Particle Swarm Optimization}) został po raz pierwszy zaprezentowany w 1995 roku przez Kennedy'ego i Eberharta \cite{PSO:Introduction} i jest wciąż analizowany i aktualizowany \cite{PSO:Modified,PSO:Inertia,PSO:Params,PSO:Dynamic,Optimization:BIAS,Optimization:BIAS2}.
Algorytm PSO jest algorytmem iteracyjnym, w którym optymalizacja jest wykonywana przy użyciu roju cząstek posiadających położenie $x$ i prędkość $v$ oraz komunikujących się ze swoimi sąsiadami. Położenie cząsteczki $p$ jest argumentem optymalizowanej funkcji $f$. Prędkość $v$ cząsteczki w $i+1$ iteracji $p$ jest aktualizowana w każdym kroku algorytmu w następujący sposób:
\begin{center}
	$p.v_{i+1} = u_1 * g * (p.neighbours.best.x - p.x_i) + u_2 * l * (p.history.best.x - p.x_i) + u_3 * i * p.v_i$
\end{center}
gdzie
\begin{itemize}
\item $neighbours.best.x$ to najlepsze (niekoniecznie aktualne) z położeń (w sensie optymalizowanej funkcji) sąsiadów cząsteczki,
\item $history.best.x$ to najlepsze położenie cząsteczki znane do danej iteracji,
\item $u_1$, $u_2$, $u_3 \sim U(0,1)$ są zmiennymi z rozkładu jednostajnego na przedziale $[0,1]$
\item $g$ (współczynnik przyciągania globalnego), $l$ (współczynnik przyciągania lokalnego), $i$ (współczynnik bezwładności) oraz sposób wyboru sąsiedztwa są parametrami algorytmu.
\end{itemize}

Aktualną implementację algorytmu, listę prac oraz otwartych problemów można znaleźć na \textit{Particle Swarm Central} \cite{Algorithm:PSO2011}.

\section{Problem dynamicznej marszrutyzacji (DVRP)}
\label{sec:DVRP}
Problem marszrutyzacji (\textit{Vehicle Routing Problem})\cite{DVRP:Study,DVRP:Benchmark,DVRP:Best} jest modyfikacją problemu komiwojażera. Polega na znalezieniu najkrótszej łącznej trasy dla floty pojazdów dostawczych z dodatkowym ograniczeniem na pojemność każdego z pojazdów oraz informacją o wielkości zamówień. W dynamicznej wersji, którą się obecnie zajmuję, zamówienia pojawiają się w ciągu dnia roboczego, ale nie znikają ani nie podlegają zmianom. Jest to tzw. wariant \textit{Vehicle Routing Problem with Dynamic Requests}.

\section{Aktualny stan badań}
Algorytm PSO został z powodzeniem zastosowany do rozwiązywania problemu DVRP \cite{DVRP:DAPSO,DVRP:MAPSO}, uzyskując lepsze wyniki na znanych przypadkach testowych \cite{DVRP:Benchmark} niż algorytm genetyczny \cite{DVRP:GA:TS}, przeszukiwanie z tabu \cite{DVRP:GA:TS} oraz algorytm mrówkowy\cite{DVRP:Ants} co motywuje do prowadzenia dalszych badań w tym kierunku. Należy też podkreślić, że zarówno algorytmy oparte na PSO oraz algorytm mrówkowy oprócz metaheurystyki do przydzielania zamówień wykorzystywały algorytm 2-opt \cite{TSP:2OPT} do rozwiązywania problemu komiwojażera dla poszczególnych pojazdów.

W proponowanym podejściu wykorzystuję wyłącznie algorytm PSO, zarówno do dokonania przydziału zamówień do pojazdów, jak również do rozwiązania problemu komiwojażera dla każdego z pojazdów.
Ponadto, w przeciwieństwie do przytoczonych wcześniej podejść z wykorzystaniem PSO, utrzymuję problem zakodowany w sposób ciągły, co umożliwia bezpośrednie stosowanie tego algorytmu (jak również potencjalnie innych algorytmów optymalizujących funkcje rzeczywiste) bez dodatkowych modyfikacji ich działania.

Problem DVRP rozwiązuję dwuetapowo niezależnymi instancjami algorytmu: najpierw optymalizując przydział zamówień do pojazdów a następnie optymalizując trasę dla każdego z pojazdów. W pierwszym etapie argumentem są położenia centrów obszarów operacyjnych dla pojazdów. Zamówienia są przypisywane do dostępnego pojazdu z najbliżej położonym centrum obszaru operacyjnego.
W drugim etapie algorytmu optymalizuję trasę każdego z pojazdów rozwiązując niezależne problemy komiwojażera. Argumentem w drugiej fazie jest kolejność obsługi zamówień na trasie pojazdu, zakodowana jako ranga przypisany każdemu z zamówień. Posortowanie zamówień po randze decyduje o kolejności ich obsłużenia.
\newpage
\section{Planowane eksperymenty}
W ostatniej sekcji prezentuję planowane do przeprowadzenia w tym roku akademickim eksperymenty i ich motywację.
\subsection*{Zbadanie wpływu rozkładu położenia zamówień w przestrzeni na działanie algorytmu}
Stosowany przeze mnie sposób przydziału zamówień do pojazdów oraz uzyskiwane wyniki wydają się świadczyć o tym, że algorytm radzi sobie lepiej, kiedy zamówienia posiadają wyraźne skupienia. W celu potwierdzenia albo obalenia tej teorii planuję sprawdzić wyniki uzyskiwane przez algorytm dla identycznych co rozmiaru i zasięgu przestrzennego zamówień (a zatem wymagających użycia takiej samej albo zbliżonej liczby pojazdów), ale o zmiennym rozkładzie w przestrzeni.
\subsection*{Zbadanie wpływu rozkładu rozmiarów zamówień na działanie algorytmu}
Uzyskiwane wyniki wydają się świadczyć o tym, że algorytm radzi sobie lepiej z danymi, w których rozkład rozmiarów zamówień jest symetryczny
oraz nie posiada \textit{outlayerów}. Analogicznie do poprzedniego punktu planuję sprawdzić wyniki uzyskiwane przez algorytm dla danych o identycznym rozkładzie przestrzennym, ale różnych rodzajach rozkładów wielkości zamówień.
\subsection*{Zastosowanie algorytmu do działania w trybie rzeczywistym} 
W celu sprawdzenia możliwości rzeczywistego wykorzystania zaproponowanego algorytmu chcę sprawdzić wyniki uzyskiwane przez jednoczesne uruchomienie niezależnych instancji algorytmu. Instancje te synchronizowałyby między sobą wyłącznie informacje o pojazdach wysłanych w trasę z najlepszego znanego w danej chwili rozwiązania ze wszystkich instancji.
\subsection*{Weryfikacja przekazywania wiedzy historycznej}
Celem eksperymentu jest sprawdzenie wyników uzyskiwanych przez algorytm, jeżeli zrezygnowałbym z przekazywania wiedzy o najlepszym rozwiązaniu z poprzednich kroków czasowych.
\subsection*{Próba znalezienia bardziej elastycznego sposobu przydziału zamówień do pojazdów}
W obecnej wersji algorytmu sposób przydziału zamówień do pojazdu powoduje, że istnieją zbiory zamówień, dla których algorytm nie ma możliwości dokonania optymalnego przydziału zamówień. Celem badań będzie znalezienie kodowania problemu, które umożliwi zarówno ciągłą reprezentację jak i osiąganie dowolnych przydziałów przy jak najmniejszym wzroście wymiarowości problemu.
\newpage
\bibliographystyle{splncs03}
\bibliography{PSO_in_DVRP}
\end{document}
