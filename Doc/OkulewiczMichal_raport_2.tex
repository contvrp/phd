\documentclass{scrartcl}

\usepackage[utf8]{inputenc} % kodowanie
\usepackage[OT4]{fontenc} % nowe czcionki dla jęz. europejskich

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{tabularx}
\usepackage{float}
\usepackage{url}
 
\usepackage[polish]{babel} % po polsku

\begin{document}
\setlength{\floatsep}{4pt}
\setlength{\textfloatsep}{4pt}
\title{Zastosowanie algorytmów inspirowanych naturą i zachowaniami społecznymi w~rozwiązywaniu dynamicznych problemów optymalizacyjnych}
\subtitle{Sprawozdanie z pracy naukowej za semestr letni 2012/13}

\author{Micha{\l} Okulewicz}

\maketitle

\begin{abstract}
W pierwszej części dokumentu przypomniane są podstawowe
zagadnienia dotyczące tematyki badań, przedstawione w planie badawczym.
W drugiej części zaprezentowane zostały wyniki badań nad zastosowaniem
dwufazowego algorytmu optymalizacji rojowej (\textit{Particle Swarm Optimization}) ze zmienioną funkcją oceniająca w problemie dynamicznej marszrutyzacji (\textit{Dynamic Vehicle Routing Problem}) przeprowadzonych
w drugim semestrze roku akademickiego 2012/13. Ponadto omówione są wyniki badań nad zastosowaniem PSO w procesie
nauki sieci neuronowej typu Multi Layer Perceptron.
Na końcu znajduje się proponowany obszar badań na najbliższy rok akademicki.
\end{abstract}

\section{Podstawowe zagadnienia}
\subsection{Dynamiczny problem optymalizacyjny}
Problem optymalizacyjny to zadanie do zaplanowania lub wykonania, które można zaprezentować w postaci funkcji:
\begin{center}
$f:\mathbb{R}^n\rightarrow\mathbb{R}$
\end{center}
dla której poszukujemy $x^{*} \in \mathbb{R}^n$ takiego, że: 
\begin{center}
$\forall_{x \in \mathbb{R}^n} f(x) \geq f(x^{*})$
\end{center}

Wśród problemów optymalizacyjnych możemy wyróżnić problemy dynamiczne,
tzn. takie w których optymalizowany problem podlega zmianom w czasie prowadzenia optymalizacji.
Optymalizowana funkcja przybiera wtedy postać:
\begin{center}
$f:\mathbb{R}^n\times T \rightarrow\mathbb{R}$
\end{center}
gdzie $T$ jest zbiorem momentów czasu $t$, w których chcemy znaleźć $x^{*}(t) \in \mathbb{R}^n$ taki, że:
\begin{center}
$\forall_{x \in \mathbb{R}^n} f(x,t) \geq f(x^{*}(t),t) \wedge t \in T$
\end{center}

Najprostszym podejściem w rozwiązywaniu problemów dynamicznych może być zastosowanie dla każdej chwili $t \in T$ znanego algorytmu do rozwiązywania odpowiedniego problemu statycznego, ale takie postępowanie może być mało wydajne.
Jeżeli zmiany zachodzące w zadaniu wraz z upływem czasu są niewielkie albo mają wręcz ciągły charakter, lepszym podejściem wydaje się zastosowanie wiedzy o rozwiązaniu problemu w poprzedniej chwili czasowej i dostosowanie go do bieżącej sytuacji.
Przykładem problemu dynamicznego, którym obecnie się zajmuję jest problem dynamicznej marszrutyzacji (\textit{Dynamic Vehicle Routing Problem}), który jest bliżej zaprezentowany w sekcji \ref{sec:DVRP}.

Wśród algorytmów optymalizacyjnych szczególnie interesujące są algorytmy bazujące na idei Inteligencji Rojowej (\textit{Swarm Intelligence}). Postulatem algorytmów Inteligencji Rojowej jest osiąganie skomplikowanych celów przez wiele niezależnych prostych bytów (kierujących się prostymi i jednakowymi zasadami) dzięki ich wzajemnej komunikacji. Jednym z przykładów takiego algorytmu jest Optymalizacja Rojowa (\textit{Particle Swarm Optimization}), która bliżej zaprezentowana jest w sekcji \ref{sec:PSO}.
Obecnie istnieją również algorytmy odchodzące od postulatu prostoty czy jednakowości bytów, mające na celu modelować bardziej skomplikowane zachowania społeczne. Przykładami takich algorytmów jest Algorytm Cywilizacyjny \cite{SI:CSO}, czy Sztucznego Ula (\cite{SI:ABC}).

Algorytmy Inteligencji Rojowej oraz algorytmy społeczne wydają się być szczególnie dobrym wyborem do rozwiązywania problemów dynamicznych, gdyż ze swego założenia mają modelować zachowanie żywych organizmów, a te działają w dynamicznym środowisku.
\subsection{Optymalizacja Rojowa (PSO)}
\label{sec:PSO}
Algorytm Optymalizacji Rojowej (\textit{Particle Swarm Optimization}) został po raz pierwszy zaprezentowany w 1995 roku przez Kennedy'ego i Eberharta \cite{PSO:Introduction} i jest wciąż analizowany i aktualizowany \cite{PSO:Modified,PSO:Inertia,PSO:Params,PSO:Dynamic,Optimization:BIAS,Optimization:BIAS2}.
Algorytm PSO jest algorytmem iteracyjnym, w którym optymalizacja jest wykonywana przy użyciu roju cząstek posiadających położenie $x$ i prędkość $v$ oraz komunikujących się ze swoimi sąsiadami. Położenie cząsteczki $p$ jest argumentem optymalizowanej funkcji $f$. Prędkość $v$ cząsteczki w $i+1$ iteracji $p$ jest aktualizowana w każdym kroku algorytmu w następujący sposób:
\begin{center}
	$p.v_{i+1} = u_1 * g * (p.neighbours.best.x - p.x_i) + u_2 * l * (p.history.best.x - p.x_i) + u_3 * i * p.v_i$
\end{center}
gdzie
\begin{itemize}
\item $neighbours.best.x$ to najlepsze (niekoniecznie aktualne) z położeń (w sensie optymalizowanej funkcji) sąsiadów cząsteczki,
\item $history.best.x$ to najlepsze położenie cząsteczki znane do danej iteracji,
\item $u_1$, $u_2$, $u_3 \sim U(0,1)$ są zmiennymi z rozkładu jednostajnego na przedziale $[0,1]$
\item $g$ (współczynnik przyciągania globalnego), $l$ (współczynnik przyciągania lokalnego), $i$ (współczynnik bezwładności) oraz sposób wyboru sąsiedztwa są parametrami algorytmu.
\end{itemize}

Aktualną implementację algorytmu, listę prac oraz otwartych problemów można znaleźć na \textit{Particle Swarm Central} \cite{Algorithm:PSO2011}.

\subsection{Problem dynamicznej marszrutyzacji (DVRP)}
\label{sec:DVRP}
Problem marszrutyzacji (\textit{Vehicle Routing Problem})\cite{DVRP:Study,DVRP:Benchmark,DVRP:Best} jest modyfikacją problemu komiwojażera. Polega na znalezieniu najkrótszej łącznej trasy dla floty pojazdów dostawczych z dodatkowym ograniczeniem na pojemność każdego z pojazdów oraz informacją o wielkości zamówień. W dynamicznej wersji, którą się obecnie zajmuję, zamówienia pojawiają się w ciągu dnia roboczego, ale nie znikają ani nie podlegają zmianom. Jest to tzw. wariant \textit{Vehicle Routing Problem with Dynamic Requests}.

\subsection{Aktualny stan badań}
Algorytm PSO został z powodzeniem zastosowany do rozwiązywania problemu DVRP \cite{DVRP:DAPSO,DVRP:MAPSO}, uzyskując lepsze wyniki na znanych przypadkach testowych \cite{DVRP:Benchmark} niż algorytm genetyczny \cite{DVRP:GA:TS}, przeszukiwanie z tabu \cite{DVRP:GA:TS} oraz algorytm mrówkowy\cite{DVRP:Ants} co motywuje do prowadzenia dalszych badań w tym kierunku. Należy też podkreślić, że zarówno algorytmy oparte na PSO oraz algorytm mrówkowy oprócz metaheurystyki do przydzielania zamówień wykorzystywały algorytm 2-OPT \cite{TSP:2OPT} do rozwiązywania problemu komiwojażera dla poszczególnych pojazdów.

\subsection{Zastosowane podejście do rozwiązania problemu DVRP}

W badanym w tym semestrze podejściu wykorzystuję algorytm PSO, a ostateczne trasy dla każdego z pojazdów są dodatkowo poprawiane
algorytmem aproksymacyjnym 2-OPT \cite{2OPT}.
Ponadto pozostała zmieniona funkcja oceniająca w pierwszej fazie na funkcję sumującą aktualne długości tras (przed ich poprawieniem algorytmem PSO).
Problem w dalszym ciągu jest reprezentowany w sposób ciągły, co umożliwia bezpośrednie stosowanie algorytmu PSO (jak również potencjalnie innych algorytmów optymalizujących funkcje rzeczywiste) bez dodatkowych modyfikacji ich działania.

Problem DVRP rozwiązuję dwuetapowo niezależnymi instancjami algorytmu: najpierw optymalizując przydział zamówień do pojazdów a następnie optymalizując trasę dla każdego z pojazdów. W pierwszym etapie argumentem są położenia centrów obszarów operacyjnych dla pojazdów. Zamówienia są przypisywane do dostępnego pojazdu z najbliżej położonym centrum obszaru operacyjnego.
W drugim etapie algorytmu optymalizuję trasę każdego z pojazdów rozwiązując niezależne problemy komiwojażera. Argumentem w drugiej fazie jest kolejność obsługi zamówień na trasie pojazdu, zakodowana jako ranga przypisany każdemu z zamówień. Posortowanie zamówień po randze decyduje o kolejności ich obsłużenia.

Testowane było również podejście, w którym problem reprezentowany jest w sposób dyskretny, ale nie udało
powtórzyć się wyników z prac prezentujących ten algorytm
\cite{DVRP:DAPSO,DVRP:MAPSO}. W tym podejściu (DAPSO/MAPSO) algorytm
PSO optymalizuje problem poprzez zmienianie przypisania zamówień do pojazdów (natomiast w obrębie pojazdu zamówienia są wstawiane w sposób zachłanny, a następnie trasa jest poprawiana algorytmem 2-OPT).
Takie kodowanie problemu pozwala na tworzenie dowolnych zbiorów tras.
W praktyce algorytm PSO nie był w stanie opuszczać minimów lokalnych
zbyt często trafiając w rozwiązania niedopuszczalne.

\newpage
\section{Przeprowadzone eksperymenty}
W drugiej części prezentuję wyniki przeprowadzonych eksperymentów.

\begin{table}[h!]
\caption{Porównanie wyników zastosowanego podejścia wielorojowego z poprawianiem 2-OPTem (M2PSO+2OPT), zastosowanego podejścia wielorojewgo ze zmienioną funkcją oceny (2MPSO+DAPSO) oraz podejścia
wielorojowego z literatury (MAPSO). Najlepsze wyniki zostały wytłuszczone.}
\begin{center}
\label{tab:comparison}
\begin{tabular*}{1.0\textwidth}{@{\extracolsep{\fill}}|c|rrrrrr|}
\hline \multirow{3}{*}{Instancja\cite{DVRP:Benchmark}} &  \multicolumn{6}{c|}{Algorytm} \\
& \multicolumn{2}{c}{2MPSO+2OPT}
 & \multicolumn{2}{c}{2MPSO+DAPSO} & \multicolumn{2}{c}
 {MAPSO\cite{DVRP:MAPSO}} \\
 & Min. & Śr. & Min. & Śr. & Min. & Śr.\\
\hline\hline
 c50 & 589.24 & 626.57 & 597.54 & \textbf{608.50} & \textbf{571.34} & {610.67} \\
 c75 & 937.41 & 988.58 &  \textbf{909.43} & \textbf{941.87} & 931.59 & {965.53} \\
 c100 & 957.54 & 1041.18 & \textbf{941.30} & \textbf{967.61} & 953.79 & 973.01 \\
 c100b & \textbf{828.94} & \textbf{849.69} & 837.23 & 869.06 & 866.42 & 882.39 \\
 c120 & 1079.14 & 1162.65 & \textbf{1061.01} & \textbf{1159.05} & 1223.49 & 1295.79 \\
 c150 & {1173.25} & {1240.90} & \textbf{1142.57} & \textbf{1183.15} & 1300.43 & 1357.71 \\
 c199 & {1408.74} & {1458.24} & \textbf{1396.99} & \textbf{1449.17} & 1595.97 & 1646.37 \\
\hline\hline
 f71 & 293.10 & 317.15 & 291.20 & 305.35 & \textbf{287.51} & \textbf{296.76} \\
 f134 & 12304.03 & {12587.49} & \textbf{12011.71} & \textbf{12447.48} & 15150.50  & 16193.00 \\
\hline\hline
 tai75a & {1814.95} & 1958.95 & \textbf{1742.31} & 1873.63 & 1794.38 & \textbf{1849.37}  \\
 tai75b & 1435.76 & 1486.39 & 1401.22 & 1468.30 & \textbf{1396.42} & \textbf{1426.67} \\
 tai75c & 1497.64 & 1660.36 & \textbf{1467.20} & 1531.97 & {1483.10} & \textbf{1518.65}  \\
 tai75d & 1459.68 & 1496.22 & 1421.48 & 1451.27 & \textbf{1391.99} & \textbf{1413.83}  \\
 tai100a & 2198.02 & 2381.24 & 2216.75 & 2262.74 & \textbf{2178.86} & \textbf{2214.61}  \\
 tai100b & 2134.31 & 2267.10 & \textbf{2045.47} & \textbf{2142.17} &{ 2140.57} & {2218.58}   \\
 tai100c & 1555.73 & 1611.17 & 1496.63 & \textbf{1546.26} & \textbf{1490.40} & {1550.63} \\
 tai100d & {1819.56} & 1939.34 & \textbf{1770.46} & \textbf{1814.01} & 1838.75 & {1928.69} \\
 tai150a & 3480.84 & 3667.11 & 3362.84 & 3446.21 & \textbf{3273.24} & \textbf{3389.97} \\
 tai150b & 3004.98 & 3118.00 & 2940.38 & 3026.59 & \textbf{2861.91} & \textbf{2956.84} \\
 tai150c & 2714.25 & 2821.53 & 2556.86 & \textbf{2634.54} & \textbf{2512.01} & {2671.35} \\
 tai150d & 3029.75 & 3174.42 & 2936.18 & 2989.27 & \textbf{2861.46} & \textbf{2989.24} \\
\hline\hline
Suma & 45716.86 & 47854.30 & \textbf{44546.76} &	\textbf{46118.18} & 48104.13 & 50349.66  \\
\hline
\end{tabular*}
\end{center}
\end{table}

Wprowadzenie algorytmu 2-OPT pozwoliło poprawić średni najlepszy wynik do 1.11 w stosunku do najlepszego rowiązania problemu statycznego oraz średni wynik do 1.16 (bez poprawiania wyników algorytmem 2-OPT, te wartości to odpowiednio 1.13 i 1.18).
Dzięki zmianie funkcji oceniającej udało się osiągnąć dalszą poprawę tych wartości do 1.09 i 1.15, co oznacza średnie wyniki lepsze niż algorytm MAPSO z wartościami 1.12 i 1.16.
\section{Badania dodatkowe}
Ponadto w zeszłym semestrze prowadziłem (wraz z J.Karwowskim, studentem wydziału MiNI PW oraz J.Legierskim, pracownikiem Orange Labs Poland) badania nad zastosowaniem algorytmu PSO w procesie nauki sieci neuronowej.

Algorytm PSO został z powodzeniem zastosowany jako algorytm inicjalizujący wagi perceptronu wielowarstwowego przed procesem
nauki przy użyciu algorytmu propagacji wstecznej w trybie on-line.

Algorytm sprawdził się w tym charakterze zarówno dla problemu
klasyfikacyjnego jak i dla zadania regresji dla sieci od 2 do 5 warstw ukrytych. Sam algorytm propagacji wstecznej nie był
w stanie osiągnąć tak dobrych wyników. Ze względu na specyfikę
danych przy większych sieciach zatrzymywał się w klasie większościowej w problemie klasyfikacji oraz w wartości średniej
w zadaniu regresji.

\section{Dalsze planowane prace}

Dalsze planowane prace dotyczą:
\begin{itemize}
\item dalszych prób powtórzenia wyników algorytmu DAPSO
\item modyfikacji algorytmu PSO w zakresie zachowań poszczególnych cząsteczek
\item dodanie do algorytmu wiedzy o problemie (znajomość rozkładu zamówień, przenoszenie wiedzy pomiędzy zadaniami)
\end{itemize}

\section{Lista prezentacji i publikacji}

Wyniki badań zostały zaprezentowane na następujących wystąpieniach
i publikacjach:
\begin{enumerate}
\item M.Okulewicz, Zastosowanie algorytmu Optymalizacji Rojowej w problemie dynamicznej marszrutyzacji, Seminarium z Mistrzem, Wydział EiTI Politechniki Warszawskiej, 5.06.2013, \url{http://www.ii.pw.edu.pl/ii_pol/content/download/2096/16233/version/1/file/Application_of_PSO_in_DVRP-okulewicz.pdf}
\item M.Okulewicz, J.Mańdziuk, (2013), Application of Particle Swarm Optimization Algorithm to Dynamic Vehicle Routing Problem, Lecture Notes in Computer Science, vol. 7895, 547-558, Springer-Verlag

\item J.Karwowski, M.Okulewicz, J.Legierski, (2013), Application of Particle Swarm Optimization Algorithm to Neural Network Training Process in the Localization of the Mobile Terminal, Communications in Computer and Information Science, vol. 383, 122–131, Springer-Verlag
\end{enumerate}

\bibliographystyle{splncs03}
\bibliography{PSO_in_DVRP}

~\\
~\\
~\\
\begin{center}
\begin{tabularx}{1.0\textwidth}{c X c}
opiekun naukowy & & doktorant \\ 
 &  &   \\ 
 &  &   \\ 
......................................... &  &  ......................................... \\ 
prof. dr hab. Jacek Mańdziuk & & mgr inż. Michał Okulewicz \\ 
 
\end{tabularx} 
\end{center}
\end{document}
