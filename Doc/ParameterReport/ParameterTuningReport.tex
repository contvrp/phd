\documentclass[a4paper]{article}
\usepackage[top=4em,bottom=5em,left=5em,right=5em]{geometry}
\usepackage[cmex10]{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage[OT1]{fontenc}

\usepackage{graphicx}

\usepackage{algorithmic}
\usepackage[table]{xcolor}
\usepackage{color}
\definecolor{darkgreen}{RGB}{0,150,0}
\usepackage{url}

\usepackage{array}

\usepackage{multirow}
\usepackage[OT4]{fontenc}

\hyphenation{}


\begin{document}


\title{Parameter tuning for continuous DVRP}


\author{Micha{\l} Okulewicz,~Jacek Ma{\'n}dziuk}


\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Function cross sections}
The plots present the first phase optimization (the clustering) function.
Figure~\ref{fig:0.1} presents how the function changes over time as new
requests arrive. Figure~\ref{fig:best.heuristic} compares the cross section
of the function initialized by the previous best solution and heuristic clustering.
Finally, figure~\ref{fig:0.2.0.3} presents the cross section for the dimensions
belonging to two different clusters.
\begin{figure}[ht]
	\includegraphics[height=0.18\textheight]{plots/cross_section0_04values_c100D_0_1} 
	\includegraphics[height=0.18\textheight]{plots/cross_section0_06values_c100D_0_1} 
	\includegraphics[height=0.18\textheight]{plots/cross_section0_08values_c100D_0_1}\\ 
	\includegraphics[height=0.18\textheight]{plots/cross_section0_10values_c100D_0_1} 
	\includegraphics[height=0.18\textheight]{plots/cross_section0_12values_c100D_0_1} 
	\includegraphics[height=0.18\textheight]{plots/cross_section0_14values_c100D_0_1}\\
	\includegraphics[height=0.18\textheight]{plots/cross_section0_16values_c100D_0_1}
	\includegraphics[height=0.18\textheight]{plots/cross_section0_18values_c100D_0_1}
	\includegraphics[height=0.18\textheight]{plots/cross_section0_20values_c100D_0_1}
	\caption{Cross section of the first two arguments of the first phase objective function for time from $0.04$ to $0.20$.}
	\label{fig:0.1}
\end{figure}

\begin{figure}[ht]
	\includegraphics[height=0.27\textheight]{plots/cross_section0_12values_c100D_0_1} 
	\includegraphics[height=0.27\textheight]{plots/cross_section0_12values_c100D_0_1_from_heuristic}
	\caption{Cross section of the first two arguments of the first phase objective function
	for function initialized by the previous best (on the left) and clustering heuristic (on the right).}
	\label{fig:best.heuristic}
\end{figure}
\begin{figure}[ht]
	\includegraphics[height=0.27\textheight]{plots/cross_section0_12values_c100D_0_2}
	\includegraphics[height=0.27\textheight]{plots/cross_section0_12values_c100D_0_3}
	\caption{Cross section of the first and third argument (on the left)
	of the first phase objective function and first and fourth argument (on the right).}
	\label{fig:0.2.0.3}
\end{figure}

\section{Advance commitment time and number of time slices}
In order to choose the proper number of time slices and the size
of the time buffer the tests were run for all the benchmark instances.
Figure~\ref{fig:TS} presents the results for $\left\lbrace 10, 20, \ldots, 100 \right\rbrace$
time slices for the 8-swarm MPSO algorithm with $1:6.25$ population size to iterations ratio and total
function evaluation budget equal to $1.25\times10^5$ per swarm and advanced commitment time equal to $0.04$.
Figure~\ref{fig:TAC}  presents the results for the advanced commitment time buffer set to  $\left\lbrace 0, 0.02, \ldots, 0.16 \right\rbrace$
for the 8-swarm MPSO algorithm with $1:6.25$ population size to iterations ratio and total
function evaluation budget equal to $1.25\times10^5$ per swarm and $40$ time slices.

As there seems to be no significant difference between the experiments with $40$ and more time slices
and between experiments with relative commitment time between the $0.02$ and $0.14$ I have chosen
$40$ time slices and $0.04$ time buffer which has the lowest 3rd quartile among the results.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{plots/2MPSO_DifferentTSsAll}
	\caption{Relative performance of the MPSO algorithm on all of the benchmark files
	for different number of time slices (with the same total evaluations budget
	and population size to iterations ratio).}
	\label{fig:TS}
\end{figure}

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{plots/2MPSO_DifferentTACsAll}
	\caption{Relative performance of the MPSO algorithm on all of the benchmark files
	for different value of the advanced commitment time.}
	\label{fig:TAC}
\end{figure}

\section{Population size to iterations ratio}
Figures~\ref{fig:MPSO} and~\ref{fig:DE} present the relative results for different population
size to iterations ratio for constant budget of $1.25\times10^5$ evaluation per population/swarm.
For the PSO algorithm there seems to be no significant differences within the tested range of swarm
size ($\lbrace 11, 20, 30, 40 \rbrace$), while for the DE the smaller population size ($\lbrace 11, 20 \rbrace$)
seems to work better than very small ($8$) and larger ($\lbrace 30, 40 \rbrace$).
Also it should be noted that for DE the $8$ and one of the $11$ population size experiments were performed
for the $40$ time slices while the rest for $50$ time slices.
Using the same method (lowest 3rd quartile among the best insignificantly different results),
I choose the $1:20.6$ population size to iterations ratio for the DE algorithm and $1 : 6.25$ for the MPSO algorithm.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{plots/2MPSO_DifferentSwarmSizeAll}
	\caption{Relative performance of the MPSO algorithm on all of the benchmark files
	for different population size to iterations ratio within the constant evaluations
	budget.}
	\label{fig:MPSO}
\end{figure}

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{plots/2DE_DifferentSwarmSizeAll}
	\caption{Relative performance of the DE algorithm on all of the benchmark files
	for different population size to iterations ratio within the constant evaluations
	budget.}
	\label{fig:DE}
\end{figure}

\end{document}
