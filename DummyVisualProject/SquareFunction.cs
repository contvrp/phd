﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParticleSwarmOptimization;

namespace DummyVisualProject
{
    public class SquareFunction: IFunction
    {
        private int dimension;
        private int valueCount = 0;

        public SquareFunction(int dim)
        {
            dimension = dim;
        }

        public double Value(double[] x)
        {
            ++valueCount;
            double returnValue = 0.0;
            for (int i = 0; i < dimension && i < x.Length; i++)
            {
                returnValue += x[i] * x[i];
            }
            return returnValue;
        }

        public int ValueCount
        {
            get { return valueCount; }
        }

        public int Dimension
        {
            get { return dimension; }
        }
    }
}
