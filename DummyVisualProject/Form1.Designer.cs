﻿namespace DummyVisualProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripMinValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripAvarageSpeed = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripDiameter = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripIterations = new System.Windows.Forms.ToolStripStatusLabel();
            this.openFileDialogTestFile = new System.Windows.Forms.OpenFileDialog();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonStartStop = new System.Windows.Forms.Button();
            this.numericUpDownIter2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownTime = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownIter1 = new System.Windows.Forms.NumericUpDown();
            this.vehicleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.getIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FinishTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countClientsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countCommitedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.needDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientsRouteTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDowStep = new System.Windows.Forms.NumericUpDown();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIter2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDowStep)).BeginInit();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMinValue,
            this.toolStripAvarageSpeed,
            this.toolStripDiameter,
            this.toolStripIterations});
            this.statusStrip1.Location = new System.Drawing.Point(0, 453);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1051, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripMinValue
            // 
            this.toolStripMinValue.Name = "toolStripMinValue";
            this.toolStripMinValue.Size = new System.Drawing.Size(118, 17);
            this.toolStripMinValue.Text = "toolStripStatusLabel1";
            // 
            // toolStripAvarageSpeed
            // 
            this.toolStripAvarageSpeed.Name = "toolStripAvarageSpeed";
            this.toolStripAvarageSpeed.Size = new System.Drawing.Size(118, 17);
            this.toolStripAvarageSpeed.Text = "toolStripStatusLabel1";
            // 
            // toolStripDiameter
            // 
            this.toolStripDiameter.Name = "toolStripDiameter";
            this.toolStripDiameter.Size = new System.Drawing.Size(118, 17);
            this.toolStripDiameter.Text = "toolStripStatusLabel1";
            // 
            // toolStripIterations
            // 
            this.toolStripIterations.Name = "toolStripIterations";
            this.toolStripIterations.Size = new System.Drawing.Size(118, 17);
            this.toolStripIterations.Text = "toolStripStatusLabel1";
            // 
            // openFileDialogTestFile
            // 
            this.openFileDialogTestFile.Filter = "VRP|*.vrp|All files|*.*";
            this.openFileDialogTestFile.InitialDirectory = "..\\..\\..\\TestFiles\\christofides\\";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.getIdDataGridViewTextBoxColumn,
            this.FinishTime,
            this.countClientsDataGridViewTextBoxColumn,
            this.countCommitedDataGridViewTextBoxColumn,
            this.needDataGridViewTextBoxColumn,
            this.clientsRouteTimeDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.vehicleBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Right;
            this.dataGridView1.Location = new System.Drawing.Point(709, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(342, 453);
            this.dataGridView1.TabIndex = 1;
            // 
            // buttonStartStop
            // 
            this.buttonStartStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonStartStop.Location = new System.Drawing.Point(667, 453);
            this.buttonStartStop.Name = "buttonStartStop";
            this.buttonStartStop.Size = new System.Drawing.Size(84, 20);
            this.buttonStartStop.TabIndex = 2;
            this.buttonStartStop.Text = "Start";
            this.buttonStartStop.UseVisualStyleBackColor = true;
            this.buttonStartStop.Click += new System.EventHandler(this.buttonStartStop_Click);
            // 
            // numericUpDownIter2
            // 
            this.numericUpDownIter2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownIter2.Location = new System.Drawing.Point(859, 454);
            this.numericUpDownIter2.Name = "numericUpDownIter2";
            this.numericUpDownIter2.Size = new System.Drawing.Size(34, 20);
            this.numericUpDownIter2.TabIndex = 3;
            this.numericUpDownIter2.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // numericUpDownTime
            // 
            this.numericUpDownTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownTime.DecimalPlaces = 2;
            this.numericUpDownTime.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownTime.Location = new System.Drawing.Point(926, 455);
            this.numericUpDownTime.Name = "numericUpDownTime";
            this.numericUpDownTime.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownTime.TabIndex = 4;
            this.numericUpDownTime.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            // 
            // numericUpDownIter1
            // 
            this.numericUpDownIter1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownIter1.Location = new System.Drawing.Point(791, 454);
            this.numericUpDownIter1.Name = "numericUpDownIter1";
            this.numericUpDownIter1.Size = new System.Drawing.Size(33, 20);
            this.numericUpDownIter1.TabIndex = 5;
            this.numericUpDownIter1.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // vehicleBindingSource
            // 
            this.vehicleBindingSource.DataSource = typeof(DynamicVehicleRoutingProblem.Vehicle);
            // 
            // getIdDataGridViewTextBoxColumn
            // 
            this.getIdDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.getIdDataGridViewTextBoxColumn.DataPropertyName = "GetId";
            this.getIdDataGridViewTextBoxColumn.HeaderText = "Id";
            this.getIdDataGridViewTextBoxColumn.Name = "getIdDataGridViewTextBoxColumn";
            this.getIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // FinishTime
            // 
            this.FinishTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FinishTime.DataPropertyName = "FinishTime";
            this.FinishTime.HeaderText = "FinishTime";
            this.FinishTime.Name = "FinishTime";
            this.FinishTime.ReadOnly = true;
            // 
            // countClientsDataGridViewTextBoxColumn
            // 
            this.countClientsDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.countClientsDataGridViewTextBoxColumn.DataPropertyName = "CountClients";
            this.countClientsDataGridViewTextBoxColumn.HeaderText = "CountClients";
            this.countClientsDataGridViewTextBoxColumn.Name = "countClientsDataGridViewTextBoxColumn";
            this.countClientsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // countCommitedDataGridViewTextBoxColumn
            // 
            this.countCommitedDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.countCommitedDataGridViewTextBoxColumn.DataPropertyName = "CountCommited";
            this.countCommitedDataGridViewTextBoxColumn.HeaderText = "CountCommited";
            this.countCommitedDataGridViewTextBoxColumn.Name = "countCommitedDataGridViewTextBoxColumn";
            this.countCommitedDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // needDataGridViewTextBoxColumn
            // 
            this.needDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.needDataGridViewTextBoxColumn.DataPropertyName = "Need";
            this.needDataGridViewTextBoxColumn.HeaderText = "Need";
            this.needDataGridViewTextBoxColumn.Name = "needDataGridViewTextBoxColumn";
            this.needDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // clientsRouteTimeDataGridViewTextBoxColumn
            // 
            this.clientsRouteTimeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clientsRouteTimeDataGridViewTextBoxColumn.DataPropertyName = "ClientsRouteTime";
            this.clientsRouteTimeDataGridViewTextBoxColumn.HeaderText = "ClientsRouteTime";
            this.clientsRouteTimeDataGridViewTextBoxColumn.Name = "clientsRouteTimeDataGridViewTextBoxColumn";
            this.clientsRouteTimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(757, 457);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Iter1";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(829, 457);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Iter2";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(899, 457);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Min.";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(976, 456);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Step";
            // 
            // numericUpDowStep
            // 
            this.numericUpDowStep.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDowStep.DecimalPlaces = 2;
            this.numericUpDowStep.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDowStep.Location = new System.Drawing.Point(1007, 454);
            this.numericUpDowStep.Name = "numericUpDowStep";
            this.numericUpDowStep.Size = new System.Drawing.Size(44, 20);
            this.numericUpDowStep.TabIndex = 9;
            this.numericUpDowStep.Value = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1051, 475);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numericUpDowStep);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDownIter1);
            this.Controls.Add(this.numericUpDownTime);
            this.Controls.Add(this.numericUpDownIter2);
            this.Controls.Add(this.buttonStartStop);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIter2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDowStep)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripMinValue;
        private System.Windows.Forms.ToolStripStatusLabel toolStripAvarageSpeed;
        private System.Windows.Forms.ToolStripStatusLabel toolStripDiameter;
        private System.Windows.Forms.ToolStripStatusLabel toolStripIterations;
        private System.Windows.Forms.OpenFileDialog openFileDialogTestFile;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource vehicleBindingSource;
        private System.Windows.Forms.Button buttonStartStop;
        private System.Windows.Forms.NumericUpDown numericUpDownIter2;
        private System.Windows.Forms.NumericUpDown numericUpDownTime;
        private System.Windows.Forms.NumericUpDown numericUpDownIter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn getIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn FinishTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn countClientsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn countCommitedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn needDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clientsRouteTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDowStep;
    }
}

