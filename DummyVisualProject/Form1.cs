﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ParticleSwarmOptimization;
using DynamicVehicleRoutingProblem;
using System.IO;

namespace DummyVisualProject
{
    public partial class Form1 : Form
    {
        ParticleSwarmOptimization.PSOAlgorithm pso;
        double[] center;
        double radius;
        int dimension;
        int swarmSize;
        private DVRPInstance f;
        private double best;

        //TODO: nałożyć ograniczenie czasowe polegające na przeniesieniu niemieszczących się
        //TODO: uwzględnić zmieniający się czas i przenoszenie danych
        //TODO: zrealizować k-means z realnym k

        public Form1()
        {
            InitializeComponent();
            DoubleBuffered = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        void f_UpdatedBest()
        {
            this.Invalidate();
            backgroundWorker1.ReportProgress((int)(f.Time * 100.0M));
        }

        private void SetPSO()
        {
            if (f.BestVehicles.ContainsKey(f.Time - (f.NextTime - f.Time)))
            {
                decimal x = f.Time - (f.NextTime - f.Time);
                for (int i = 0; i < f.BestVehicles[x].Where(vhcl => vhcl.clientsToServe.Count == 0).ToArray().Length; i++)
                {
                    center[2 * i] = f.BestVehicles[x].Where(vhcl => vhcl.clientsToServe.Count == 0).ToArray()[i].X;
                    center[2 * i + 1] = f.BestVehicles[x].Where(vhcl => vhcl.clientsToServe.Count == 0).ToArray()[i].Y;
                }
                    
            }
            pso = new ParticleSwarmOptimization.PSOAlgorithm(f, swarmSize, center, radius, 0.5);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime start = DateTime.Now;
            while (true)
            try
            {
                pso.Step();
                backgroundWorker1.ReportProgress((int)(f.Time * 100.0M));
                best = pso.BestValue;
                if (pso.AvarageSpeedValue < 1e-10)
                {
                    center = (double[])pso.Best.Clone();
                    SetPSO();
                    pso.Step();
                    best = pso.BestValue;
                }
                if ((pso.Iterations > this.numericUpDownIter1.Value && (DateTime.Now - start).TotalMinutes > (double)this.numericUpDownTime.Value) || f is DVRPSpanningForestInstance)
                {
                    f.UpdateTime();
                    SetPSO();
                    start = DateTime.Now;
                }
                if (backgroundWorker1.CancellationPending || f.Time >= 1.0M)
                {
                    e.Cancel = backgroundWorker1.CancellationPending;
                    break;
                }
            }
            catch (Exception ex)
            {
                Utils.LogInfo("log.txt", "{0}", ex.Message);
                Utils.LogInfo("exceptions.txt", ex.StackTrace);
            }

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.Invalidate();
            this.Refresh();
            if (pso != null)
            {
                toolStripAvarageSpeed.Text = string.Format("Średnia prędkość {0:0.00E+0}", pso.AvarageSpeedValue);
                toolStripDiameter.Text = string.Format("Średnica roju {0:0.00E+0}", pso.SwarmDiameter);
                toolStripMinValue.Text = string.Format("Najlepsza wartość {0:0.00E+0}", best);
                toolStripIterations.Text = string.Format("Iteracja {0:#}, Obliczeń funkcji {1:#}, Pamięć: {2}MB", pso.Iterations, pso.ValueCount,
                    System.Diagnostics.Process.GetCurrentProcess().WorkingSet64 / 1024 / 1024);
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                this.Text = f.Time.ToString() + " " + this.backgroundWorker1.IsBusy;
                vehicleBindingSource.DataSource = f.BestVehicles[f.Time].Where(vhcl => vhcl.clients.Count + vhcl.clientsToServe.Count > 0).ToList();
            }
            catch
            {
            }
            DynamicVehicleRoutingProblem.Visualize.Visualizer.DrawVehicleAssignment(this.f, e.Graphics);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            buttonStartStop.Text = "Start";
            buttonStartStop.Enabled = true;
            this.Text = "Skończyłem";
            this.Invalidate();
            this.Refresh();
            if (!e.Cancelled)
            {
                Utils.LogInfo("wyniki.txt", "{0}: {1}: {2}", DateTime.Now.ToString(), f.ToString(), best);
                f.RestartTime();
                SetPSO();
                buttonStartStop.PerformClick();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                backgroundWorker1.CancelAsync();
                buttonStartStop.Text = "Oczekiwanie";
                buttonStartStop.Enabled = false;
                MessageBox.Show("Zaczekaj na zakończenie fazy obliczeń");
                e.Cancel = true;
            }
        }

        private void buttonStartStop_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker1.IsBusy)
            {
                if (f == null)
                {
                    if (openFileDialogTestFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        if (MessageBox.Show("Czy chcesz optymalizować pojazdy?", "Jaka funkcja?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            swarmSize = 1;
                            f = new DVRPInstance(openFileDialogTestFile.FileName, numericUpDowStep.Value, 40, (int)this.numericUpDownIter1.Value, 0.04M);
                        }
                        else
                        {
                            swarmSize = 40;
                            f = new DVRPClientsAssignmentInstance(openFileDialogTestFile.FileName, 0.1M, 40, (int)this.numericUpDownIter1.Value, 0.1M);
                        }
                        f.UpdatedBest += new DVRPInstance.UpdateBestEvent(f_UpdatedBest);
                    }
                    else
                        return;
                }
                else
                {
                    f.RestartTime();
                }
                dimension = f.Dimension;
                radius = 100;
                center = new double[dimension];
                for (int i = 0; i < dimension; ++i)
                    center[i] = (Utils.random.NextDouble() - 0.5) * 0.25 * radius;
                SetPSO();
                this.Text = f.Time.ToString();
                buttonStartStop.Text = "Stop";
                buttonStartStop.Enabled = true;
                backgroundWorker1.RunWorkerAsync();
            }
            else
            {
                buttonStartStop.Text = "Oczekiwanie";
                buttonStartStop.Enabled = false;
                backgroundWorker1.CancelAsync();
            }
        }
    }
}
