﻿#define DEPOT_RULE //the rule blocking joining distant (with certain relation to depot) clusters
//#undef DEPOT_RULE

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParticleSwarmOptimization;

namespace DynamicVehicleRouting
{
    public class TreeClusterFunction<ClientType> : ClusterFunction where ClientType : Client, new()
    {
        private int maxVehicleId;
        private int minVehicleId;
        private IEnumerable<Vehicle> iterateVehicles;
        private int reservoirVehiclesNumber;
        public Client[] setClients = null;

        class ClientClientEdge : IComparable<ClientClientEdge>, IEquatable<ClientClientEdge>
        {
            public Client client1;
            public Client client2;
            private double distance;

            public double Distance
            {
                get { return distance; }
            }

            public ClientClientEdge(Client client1, Client client2)
            {
                this.client1 = client1;
                this.client2 = client2;
                distance = client1.DistanceTo(client2);
                /*
                distance = Utils.Instance.EuclideanDistance(
                    new double[] { client1.X, client1.Y },
                    new double[] { client2.X, client2.Y },
                    //client1.Id,
                    //client2.Id
                    );
                */
            }

            public int CompareTo(ClientClientEdge other)
            {
                if (this.Equals(other))
                    return 0;
                int comp = this.distance.CompareTo(other.distance);
                if (comp == 0)
                    comp = this.client1.Id.CompareTo(other.client1.Id);
                if (comp == 0)
                    comp = this.client2.Id.CompareTo(other.client2.Id);
                return comp;
            }

            public override bool Equals(object obj)
            {
                if (obj is ClientClientEdge)
                {
                    return this.Equals(obj as ClientClientEdge);
                }
                return false;
            }

            public override int GetHashCode()
            {
                return (int)distance;
            }


            public bool Equals(ClientClientEdge other)
            {
                return ((this.client1.Id == other.client1.Id) && (this.client2.Id == other.client2.Id)) || ((this.client2.Id == other.client1.Id) && (this.client1.Id == other.client2.Id));
            }
        }

        public TreeClusterFunction(Vehicle[] vehicles, Client[] clients, Depot[] depots, decimal currentProblemTime, double[,] distances, double weightPowerParameter, int classesForVehicle, int reservoirVehiclesCount)
            : base(vehicles, clients, depots, currentProblemTime, distances, weightPowerParameter, classesForVehicle)
        {
            maxVehicleId = Vehicles.Max(vhcl => vhcl.Id);
            minVehicleId = Vehicles.Min(vhcl => vhcl.Id);
            iterateVehicles = Vehicles.Where(vhcl => vhcl.Available || Vehicles.Count(vhcl2 => vhcl2.Available) == 0);
            this.reservoirVehiclesNumber = reservoirVehiclesCount;
        }

        protected override void AssignClientsToVehicles(double[] x)
        {
            SortedSet<ClientClientEdge> edges = CreateConnectionGraph();
            if (setClients != null)
            {
                for (int i = 0; i < setClients.Length; ++i)
                {
                    this.ClientsToAssign.First(clnt => clnt.Id == setClients[i].Id).FakeVehicleId = setClients[i].FakeVehicleId;
                }
            }
            CreateCapacitedMinimumSpanningTree(edges);
            //foreach (Client client in ClientsToAssign)
            //    Console.WriteLine("{0} {1} {2}", client.FakeVehicleId, client.X, client.Y);
            SetClientsVehicleAssignment();
            SetVehiclesCentersOfOperatingAreas();
        }

        private static double MaxAngleDistance(double firstMin, double firstMax, double secondMin, double secondMax)
        {
            return (new double[] { firstMin, firstMax })
                .Max(first => new double[] {secondMin, secondMax}
                    .Max(second => new double[] { Math.Abs(first - second), 2 * Math.PI - Math.Abs(first - second)}
                        .Min()));
        }

        private void CreateCapacitedMinimumSpanningTree(SortedSet<ClientClientEdge> edges)
        {
            Dictionary<int, int> vehicleLoadDictionary = new Dictionary<int, int>();
            Dictionary<int, double> vehicleMinAnglesDictionary = new Dictionary<int, double>();
            Dictionary<int, double> vehicleMaxAnglesDictionary = new Dictionary<int, double>();
            var allClients = iterateVehicles.SelectMany(vhcl => vhcl.assignedClients.Where(clnt => !(clnt is Depot))).Concat(ClientsToAssign).ToList();
            foreach (Client client in allClients)
            {
                if (!vehicleLoadDictionary.ContainsKey(client.FakeVehicleId))
                {
                    vehicleLoadDictionary.Add(client.FakeVehicleId,
                         allClients.Where(clnt => clnt.FakeVehicleId == client.FakeVehicleId).Sum(clnt => clnt.Need));
                    IOrderedEnumerable<double> angles = allClients.Where(clnt => clnt.FakeVehicleId == client.FakeVehicleId).Select(clnt =>
                        ((clnt.Y - Depots[0].Y) < 0 ? 2 * Math.PI : 0) + ((clnt.Y - Depots[0].Y) < 0 ? -1 : 1) *
                             Math.Acos((clnt.X - Depots[0].X)  /
                             Math.Sqrt((clnt.X - Depots[0].X) * (clnt.X - Depots[0].X) + (clnt.Y - Depots[0].Y) * (clnt.Y - Depots[0].Y))))
                             .OrderBy(val => val);
                    if (angles.Any())
                    {
                        double first = angles.First();
                        double last = angles.Last();
                        vehicleMinAnglesDictionary.Add(client.FakeVehicleId, Math.Abs(last - first) >= Math.PI ? last : first);
                        vehicleMaxAnglesDictionary.Add(client.FakeVehicleId, Math.Abs(last - first) >= Math.PI ? first : last);
                    }
                }
            }
            int maxClientId = allClients.Max(clnt => clnt.Id);
            ClientClientEdge[] edgesSet = edges.ToArray();
            int capacity = Vehicles.Min(vhcl => vhcl.Capacity);
            for (int i = 0; i < edgesSet.Length; ++i )
            {
                if (edgesSet[i].client1.FakeVehicleId != edgesSet[i].client2.FakeVehicleId
                    && (edgesSet[i].client1.FakeVehicleId > maxVehicleId || edgesSet[i].client2.FakeVehicleId > maxVehicleId))
                {
                    //var firstCluster = ClientsToAssign.Where(clnt => clnt.FakeVehicleId == edgesSet[i].client1.FakeVehicleId);
                    //var secondCluster = ClientsToAssign.Where(clnt => clnt.FakeVehicleId == edgesSet[i].client2.FakeVehicleId);
                    if (-vehicleLoadDictionary[edgesSet[i].client1.FakeVehicleId]
                        - vehicleLoadDictionary[edgesSet[i].client2.FakeVehicleId] <= capacity)
                    {
                        //Console.WriteLine("{0} {1}", edgesSet[i].client1.Id, edgesSet[i].client2.Id);
                        if (
#if DEPOT_RULE
edgesSet[i].Distance <
                            Depots.Min(dpt => edgesSet[i].client1.DistanceTo(dpt)) &&
                            edgesSet[i].Distance <
                            Depots.Min(dpt => edgesSet[i].client2.DistanceTo(dpt))
                            //MaxAngleDistance(
                            //    vehicleMinAnglesDictionary[edgesSet[i].client1.FakeVehicleId],
                            //    vehicleMaxAnglesDictionary[edgesSet[i].client1.FakeVehicleId],
                            //    vehicleMinAnglesDictionary[edgesSet[i].client2.FakeVehicleId],
                            //    vehicleMaxAnglesDictionary[edgesSet[i].client2.FakeVehicleId]) <= Math.PI / 3
#else
                            true
#endif
                            )
                        {
                            int winningId = Math.Min(edgesSet[i].client1.FakeVehicleId, edgesSet[i].client2.FakeVehicleId);
                            int losingId = Math.Max(edgesSet[i].client1.FakeVehicleId, edgesSet[i].client2.FakeVehicleId);
                            if (!typeof(ClientType).Equals(typeof(Client)))
                            {
                                allClients.Remove(edgesSet[i].client1);
                                allClients.Remove(edgesSet[i].client2);
                                edges.RemoveWhere(edg => edg.client1.Id == edgesSet[i].client1.Id || edg.client2.Id == edgesSet[i].client2.Id ||
                                    edg.client2.Id == edgesSet[i].client1.Id || edg.client1.Id == edgesSet[i].client2.Id);
                                ClientType clusterClient = new ClientType()
                                    {
                                        FakeVehicleId = winningId,
                                        Id = ++maxClientId,
                                        Need = edgesSet[i].client1.Need + edgesSet[i].client2.Need
                                    };
                                (clusterClient as ClusterClient).points = new List<SpatialPoint>()
                                {
                                    edgesSet[i].client1,
                                    edgesSet[i].client2
                                };
                                foreach (Client client in allClients)
                                {
                                    edges.Add(new ClientClientEdge(clusterClient, client));
                                }
                                allClients.Add(clusterClient);
                            }
                            if (vehicleMinAnglesDictionary[winningId] - vehicleMinAnglesDictionary[losingId] < -Math.PI || vehicleMinAnglesDictionary[winningId] - vehicleMinAnglesDictionary[losingId] >= 0)
                                vehicleMinAnglesDictionary[winningId] = vehicleMinAnglesDictionary[losingId];
                            if (-vehicleMaxAnglesDictionary[winningId] + vehicleMaxAnglesDictionary[losingId] < Math.PI && -vehicleMaxAnglesDictionary[winningId] + vehicleMaxAnglesDictionary[losingId] > 0)
                                vehicleMaxAnglesDictionary[winningId] = vehicleMaxAnglesDictionary[losingId];
                            vehicleLoadDictionary[winningId] = vehicleLoadDictionary[winningId] + vehicleLoadDictionary[losingId];
                            foreach (Client client in ClientsToAssign.Where(clnt => clnt.FakeVehicleId == losingId))
                            {
                                client.FakeVehicleId = winningId;
                            }
                        }
                    }
                }
                if (!typeof(ClientType).Equals(typeof(Client)))
                {
                    edges.Remove(edgesSet[i]);
                    i = -1;
                    edgesSet = edges.ToArray();
                }
            }
        }

        private SortedSet<ClientClientEdge> CreateConnectionGraph()
        {
            SortedSet<ClientClientEdge> edges = new SortedSet<ClientClientEdge>();
            int fakeVehicleId = maxVehicleId + 1;
            for (int i = 0; i < ClientsToAssign.Length; ++i)
            {
                Client client1 = ClientsToAssign[i];
                client1.FakeVehicleId = fakeVehicleId++;
                for (int j = i + 1; j < ClientsToAssign.Length; ++j )
                {
                    Client client2 = ClientsToAssign[j];
                    if (client1.Id != client2.Id)
                        edges.Add(new ClientClientEdge(client1, client2));
                }
                //TODO: Nie jestem pewien czy powinienem iterowac po klientach niezatwierdzonych
                foreach (Vehicle vehicle in iterateVehicles.Where(vhcl => vhcl.clientsToAssign.Count > 0 || vhcl.assignedClients.Count > 0))
                {
                    foreach (Client client2 in vehicle.clientsToAssign)
                    {
                        client2.FakeVehicleId = vehicle.Id;
                        edges.Add(new ClientClientEdge(client1, client2));
                    }
                    foreach (Client client2 in vehicle.assignedClients)
                    {
                        client2.FakeVehicleId = vehicle.Id;
                        edges.Add(new ClientClientEdge(client1, client2));
                    }
                }
            }
            return edges;
        }

        private void SetClientsVehicleAssignment()
        {
            foreach (Client client in ClientsToAssign)
            {
                if (client.FakeVehicleId > maxVehicleId)
                {
                    int idToChange = client.FakeVehicleId;
                    int newId = iterateVehicles.First(vhcl => vhcl.Need == iterateVehicles.Max(vhcl2 => vhcl2.Need)).Id;
                    foreach (Client clientToChange in ClientsToAssign.Where(clnt => clnt.FakeVehicleId == idToChange))
                        clientToChange.FakeVehicleId = newId;
                }
                if (Vehicles.First(vhcl => vhcl.Id == client.FakeVehicleId).clientsToAssign.Count(clnt => clnt.Id == client.Id) > 0)
                    throw new Exception(string.Format("Klient o id {0} już istnieje", client.Id));
                iterateVehicles.First(vhcl => vhcl.Id == client.FakeVehicleId).clientsToAssign.Add(client);

                client.positionForClientAssignment = new double[ClientAssignmentFunction.vehicleCoding[client.FakeVehicleId - minVehicleId].Length - 1];
                double[] vehicleCoding = new double[ClientAssignmentFunction.vehicleCoding[client.FakeVehicleId - minVehicleId].Length];
                for (int i = 0; i < vehicleCoding.Length; ++i)
                {
                    vehicleCoding[i] = ClientAssignmentFunction.vehicleCoding[client.FakeVehicleId - minVehicleId][i];
                }
                for (int i = 0; i < client.positionForClientAssignment.Length; ++i)
                {
                    client.positionForClientAssignment[i] = Math.Asin(vehicleCoding[i]);
                    for (int j = i + 1; j < vehicleCoding.Length; ++j)
                    {
                        vehicleCoding[j] /= Math.Cos(Math.Asin(vehicleCoding[i]));
                    }
                }
            }
        }

        private void SetVehiclesCentersOfOperatingAreas()
        {
            int counter = 0;
            foreach (Vehicle vehicle in Vehicles)
            {
                if (vehicle.clientsToAssign.Count > 0)
                {
                    for (int i = 0; i < vehicle.X.Length; ++i)
                    {
                        vehicle.X[i] = vehicle.clientsToAssign.Average(clnt => clnt.X) + Utils.Instance.random.NextDouble() / 10 - 0.05;
                        vehicle.Y[i] = vehicle.clientsToAssign.Average(clnt => clnt.Y) + Utils.Instance.random.NextDouble() / 10 - 0.05;
                    }
                }
                else if (ClientsToAssign.Length > 0)
                {
                    if (counter < reservoirVehiclesNumber)
                        vehicle.clientsToAssign.Add(Depots[0]);
                    //Rozstawianie nieprzypisanych po rogach sensownej przestrzeni
                    for (int i = 0; i < vehicle.X.Length; ++i)
                    {
                        vehicle.X[i] = (((counter % 4) % 2 == 0) ? -1 : 1) * 2 * ClientsToAssign.Max(clnt => Math.Abs(clnt.X)) + ClientsToAssign.Average(clnt => clnt.X) + Utils.Instance.random.NextDouble() / 10 - 0.05;
                        vehicle.Y[i] = (((counter % 4) / 2 == 0) ? -1 : 1) * 2 * ClientsToAssign.Max(clnt => Math.Abs(clnt.Y)) + ClientsToAssign.Average(clnt => clnt.Y) + Utils.Instance.random.NextDouble() / 10 - 0.05;
                    }
                    ++counter;
                }
            }
        }

        public override int Dimension
        {
            get
            {
                return 0;
            }
        }
    }

}
