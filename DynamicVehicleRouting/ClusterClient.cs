﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRouting
{
    public abstract class ClusterClient : Client
    {
        public List<SpatialPoint> points = new List<SpatialPoint>();
    }

    public class MaxClusterClient : ClusterClient
    {
        public override double DistanceTo(SpatialPoint point)
        {
            if (point is MaxClusterClient)
                return points.Max(pt => (point as MaxClusterClient).points.Max(pt2 => pt.DistanceTo(pt2)));
            return points.Max(pt => pt.DistanceTo(point));
        }
    }

    public class AvgClusterClient : ClusterClient
    {
        public override double DistanceTo(SpatialPoint point)
        {
            if (point is AvgClusterClient)
                return points.Average(pt => (point as AvgClusterClient).points.Average(pt2 => pt.DistanceTo(pt2)));
            return points.Average(pt => pt.DistanceTo(point));
        }
    }

    public class MinClusterClient : ClusterClient
    {
        public override double DistanceTo(SpatialPoint point)
        {
            if (point is MinClusterClient)
                return points.Min(pt => (point as MinClusterClient).points.Min(pt2 => pt.DistanceTo(pt2)));
            return points.Min(pt => pt.DistanceTo(point));
        }
    }

}
