﻿using ParticleSwarmOptimization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRouting
{
    class MonteCarloWithSolutionProposals<ClientType> where ClientType : Client, new()
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance">The instance to be solved</param>
        /// <param name="basicModelsCount">The number of propositions</param>
        /// <param name="generateDataFromModel">Generate from oracle?</param>
        public MonteCarloWithSolutionProposals(DVRPInstance instance, int basicModelsCount, int additionalModelsCount,
            double C, decimal fakeBufferSize,
            bool useLambdaInterchange, bool generateDataFromModel = true)
        {
            this.instance = instance;
            this.generateDataFromModel = generateDataFromModel;
            this.basicModelsCount = basicModelsCount;
            this.additionalModelsCount = additionalModelsCount;
            this.C = C;
            this.fakeBufferSize = fakeBufferSize;
            this.useLambdaInterchange = useLambdaInterchange;
        }

        private DVRPInstance instance;
        private bool generateDataFromModel;
        private int basicModelsCount;
        private int additionalModelsCount;
        private double C;
        private decimal fakeBufferSize;
        private bool useLambdaInterchange;

        internal static DVRPInstance GenerateFakeRequests(DVRPInstance instance, decimal bufferSize, bool generateDataFromModel)
        {
            int countNew = 0;
            int known = instance.Clients.Count(clnt => clnt.StartAvailable <= instance.CurrentTime * (decimal)instance.MaxTime);
            if (generateDataFromModel)
                countNew = (int)Math.Round(Math.Max((0.5M - instance.CurrentTime), 0) * 2 * bufferSize * known);
            else
                countNew = instance.Clients.Length - known;
            double maxX = instance.Clients.Max(clnt => clnt.X);
            double maxY = instance.Clients.Max(clnt => clnt.Y);
            double minX = instance.Clients.Min(clnt => clnt.X);
            double minY = instance.Clients.Min(clnt => clnt.Y);
            DVRPInstance simulationInstance = instance.Clone();
            int originalLength = simulationInstance.Clients.Length;
                simulationInstance.Clients = simulationInstance.Clients.Concat(new Client[countNew]).ToArray();
            
                //TODO: how to generate to preserve IDs for caching and reassignment and references for assignedClients?
                //Answer: add fake at the end and ignore the rest

                for (int i = originalLength; i < originalLength + countNew; ++i)
                {
                    Client client = simulationInstance.Clients[i] = new Client();
                    client.Id = i+1;
                    if (generateDataFromModel)
                    {
                        double x = 0.0;
                        double y = 0.0;
                        do
                        {
                            bool cont = false;
                            x = Utils.Instance.random.NextDouble() * (maxX - minX) + minX;
                            y = Utils.Instance.random.NextDouble() * (maxY - minY) + minY;
                            //foreach (Client setClient in simulationInstance.Clients.Where(clnt => clnt != null && clnt.Id <= originalLength && (double)clnt.StartAvailable / simulationInstance.MaxTime <= (double)simulationInstance.CurrentTime))
                            //{
                            //    double dist = 1 - Math.Exp(-0.5 * ((x - setClient.X) * (x - setClient.X) + (y - setClient.Y) * (y - setClient.Y)) / ((maxX - minX) * (maxY - minY) / countNew));
                            //    if (Utils.random.NextDouble() > dist)
                            //    {
                            //        cont = true;
                            //        break;
                            //    }
                            //}
                            if (!cont)
                                break;
                        } while (true);
                        client.X = x;
                        client.Y = y;
                        client.Rank = (decimal)Utils.Instance.random.NextDouble();
                        client.Need = simulationInstance.ClientsToAssign[Utils.Instance.random.Next(simulationInstance.ClientsToAssign.Count)].Need;
                        client.FakeVehicleId = 1000;
                        client.TimeToUnload = simulationInstance.Clients[0].TimeToUnload;
                    }
                    else
                    {
                        client.Rank = simulationInstance.Clients[i - originalLength].Rank;
                        client.X = simulationInstance.Clients[i - originalLength].X;
                        client.Y = simulationInstance.Clients[i - originalLength].Y;
                        client.Need = simulationInstance.Clients[i - originalLength].Need;
                        client.FakeVehicleId = 1000;
                    }
                    client.StartAvailable = (int)Math.Floor(simulationInstance.CurrentTime * (decimal)simulationInstance.MaxTime);
                    client.VisitTime = (int)Math.Floor(simulationInstance.CurrentTime * (decimal)simulationInstance.MaxTime);
                simulationInstance.ClientsToAssign.Add(client);
                }
            simulationInstance.CacheDistances();
            return simulationInstance;

        }

        public class ValuedClusterings
        {
            public List<double> values;
            public TreeClusterFunction<ClientType> cluster;
            public Client[] clients;
            public DVRPInstance instance;
        }

        public ClusterFunction Solve()
        {
            //TODO: Rezultat powinien być najlepszą średnią a nie najlepszym wynikiem
            //(z drugiej strony powstaje pytanie o scalanie)
            List<ValuedClusterings> results = new List<ValuedClusterings>();
            //if (instance.CurrentTime > 0)
            //{
            //    DVRPInstance fakeInstance = GenerateFakeRequests();
            //    TreeClusterFunction<ClientType> clusterFunction;
            //    double sum;
            //    ComputeWithTree2Opt(fakeInstance, out clusterFunction, out sum,
            //        fakeInstance.ClientsToAssign.Where(clnt => clnt.StartAvailable < (int)Math.Floor(instance.CurrentTime * (decimal)instance.MaxTime)).ToArray());
            //    //Console.WriteLine(sum);
            //    results.Add(new ValuedClusterings()
            //    {
            //        values = new List<double>() { sum },
            //        cluster = clusterFunction,
            //        clients = clusterFunction.ClientsToAssign.Where(clnt => instance.ClientsToAssign.Any(clnt2 => clnt.Id == clnt2.Id)).ToArray(),
            //        instance = fakeInstance
            //    });
            //}
            while (results.Count < basicModelsCount)
            {
                DVRPInstance fakeInstance = GenerateFakeRequests(instance, fakeBufferSize, generateDataFromModel);
                TreeClusterFunction<ClientType> clusterFunction;
                double sum;
                ComputeWithTree2Opt(fakeInstance, out clusterFunction, out sum);
                //Console.WriteLine(sum);
                results.Add(new ValuedClusterings()
                {
                    values = new List<double>() { sum},
                    cluster = clusterFunction,
                    clients = clusterFunction.ClientsToAssign.Where(clnt => instance.ClientsToAssign.Any(clnt2 => clnt.Id == clnt2.Id)).ToArray(),
                    instance = fakeInstance
                });
            }
            double maxSolutionValue = 3 * results.Max(rst => rst.values[0]);
            for (int i = 0; i < results.Count; ++i)
            {
                for (int j = 0; j < results.Count; ++j )
                {
                    if (i != j)
                    {
                        TreeClusterFunction<ClientType> clusterFunction;
                        double sum;
                        //Console.WriteLine("{0} {1}",i,j);
                        //Console.WriteLine(string.Join(",", results[j].clients.Select(clnt => clnt.FakeVehicleId).ToArray()));
                        ComputeWithTree2Opt(results[i].instance.Clone(), out clusterFunction, out sum, results[j].clients);
                        if (double.IsNaN(sum))
                            sum = maxSolutionValue;
                        results[i].values.Add(sum);
                        //Console.WriteLine(sum);
                    }
                }
            }
            for (int i = 0; i < additionalModelsCount; i++)
            {
                DVRPInstance fakeInstance = GenerateFakeRequests(instance, fakeBufferSize, generateDataFromModel);
                ValuedClusterings choice = results.OrderBy(rs => rs.values.Average()
                    + C * maxSolutionValue * Math.Sqrt((double)rs.values.Count / Math.Log(results.Sum(rs2 => rs2.values.Count)))).First();
                //Console.WriteLine(string.Join(",", results.Select(rs => (int)(rs.values.Average()
                //    + maxSolutionValue / 3.0 * Math.Sqrt((double)rs.values.Count / Math.Log(results.Sum(rs2 => rs2.values.Count)))))
                //    ));
                TreeClusterFunction<ClientType> clusterFunction;
                double sum;
                //Console.WriteLine("{0} {1}",i,j);
                //Console.WriteLine(string.Join(",", results[j].clients.Select(clnt => clnt.FakeVehicleId).ToArray()));
                ComputeWithTree2Opt(fakeInstance, out clusterFunction, out sum, choice.clients);
                if (double.IsNaN(sum))
                    sum = maxSolutionValue;
                choice.values.Add(sum);
                //Console.WriteLine(sum);
            }
            ClusterFunction resultClusterFunction = results.OrderBy(rs => rs.values.Average()).ElementAt(0).cluster;
            if (useLambdaInterchange)
            {
                DVRPInstance.OptimizeWithLambdaInterchange(
                    resultClusterFunction.Vehicles,
                    1,
                    1,
                    results.OrderBy(rs => rs.values.Average()).ElementAt(0).instance.Depots,
                    results.OrderBy(rs => rs.values.Average()).ElementAt(0).instance.CurrentTime,
                    results.OrderBy(rs => rs.values.Average()).ElementAt(0).instance.distances);
            }
            return resultClusterFunction;
        }

        private void ComputeWithTree2Opt(DVRPInstance fakeInstance, out TreeClusterFunction<ClientType> clusterFunction, out double sum, Client[] clients = null)
        {
            clusterFunction = new TreeClusterFunction<ClientType>(fakeInstance.Vehicles, fakeInstance.Clients, fakeInstance.Depots, fakeInstance.CurrentTime, fakeInstance.distances, 0.0, 1, 4);
            clusterFunction.setClients = clients;
            clusterFunction.Value(new double[0]);
            var vehiclesToOptimize = clusterFunction.Vehicles.Where(vhcl => vhcl.assignedClients.Count + vhcl.clientsToAssign.Count > 0);
            foreach (Vehicle vehicle in vehiclesToOptimize)
            {
                DVRPInstance.UpdateTotalRouteDistanceWith2OptedRoutes(fakeInstance.Depots, fakeInstance.distances, fakeInstance.CurrentTime, vehicle);
            }
            //if (useLambdaInterchange)
            //{
            //    DVRPInstance.OptimizeWithLambdaInterchange(vehiclesToOptimize.ToArray(), 1, 1, fakeInstance.Depots, fakeInstance.CurrentTime, fakeInstance.distances);
            //}
            sum = 0.0;
            foreach (Vehicle vehicle in vehiclesToOptimize)
            {
                sum += DVRPInstance.GetRouteLength(vehicle, fakeInstance.Depots, fakeInstance.CurrentTime, fakeInstance.distances);
            }
            foreach (Vehicle vehicle in vehiclesToOptimize)
            {
                vehicle.clientsToAssign.RemoveAll(clnt => !(clnt is Depot) && !instance.ClientsToAssign.Any(clnt2 => clnt2.Id == clnt.Id));
            }
        }


    }
}
