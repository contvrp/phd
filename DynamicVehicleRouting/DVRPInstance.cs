﻿#define DEBUG_MODE
#undef DEBUG_MODE

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using ParticleSwarmOptimization;
using RDotNet;

namespace DynamicVehicleRouting
{
    [DataContract]
    [KnownType(typeof(double[]))]
    public class DVRPInstance
    {
        public static TimeSpan timeout = new TimeSpan(2, 0, 0);

        [DataMember]
        public double bestKnown;

        [DataMember]
        public int capacity;

        [DataMember]
        public Client[] Clients;

        [DataMember]
        public Depot[] Depots;

        [DataMember]
        public int EvaluationCount = 0;

        [DataMember]
        public Vehicle[] Vehicles;

        private static readonly object loggerLocker = new object();

        [DataMember]
        private decimal capacitySlope;

        [DataMember]
        private int previousStepClientsCount = 0;

        [DataMember]
        private List<Client> clientsToAssign;

        [DataMember]
        private decimal currentTime;

        [DataMember]
        private bool discrete = true;

        internal double[,] distances;

        [DataMember]
        private DegreeOfDynamicity dod;

        [DataMember]
        internal string filename;

        [DataMember]
        private decimal initialCapacityPart;

        [DataMember]
        private int internalSwarmIterations;

        [DataMember]
        private int internalSwarmSize;

        [DataMember]
        private double inventorsRatio1;

        [DataMember]
        private double inventorsRatio2;

        [DataMember]
        private double neighbourProbability1;

        [DataMember]
        private double neighbourProbability2;

        [DataMember]
        private int optimizer;

        [DataMember]
        private double previousBestValue = double.MaxValue;

        [DataMember]
        private int routeSwarmIterations;

        [DataMember]
        private Vehicle[] StartVehicles;

        [DataMember]
        private decimal timeStep;

        [DataMember]
        private bool traceInfo;

        [DataMember]
        private bool use2Opt;

        [DataMember]
        private bool usePreviousSolutions;

        [DataMember]
        private double weightPowerParameter;

        [DataMember]
        private int classesForVehicle;
        [DataMember]
        private int reservoirVehiclesCount;
        [DataMember]
        private decimal initialPartOfTheDay;
        [DataMember]
        private int initialMonteCarloProposals;
        [DataMember]
        private int additionalMonteCarloProposals;
        [DataMember]
        private bool generateDataFromModel;
        [DataMember]
        public object bandits;
        [DataMember]
        public List<object> listOfBandits = new List<object>();
        [DataMember]
        private bool useUCB;
        [DataMember]
        private bool rememberBandits;
        [DataMember]
        private bool useGreedyInMC;
        [DataMember]
        private Clustering clustering;
        [DataMember]
        private bool useLambdaInterchange;
        [DataMember]
        private bool useLambdaInterchangeInMC;
        [DataMember]
        private int advanceCommitTimeSlices;
        [DataMember]
        private double C;
        [DataMember]
        private decimal fakeBufferSize;

        public DVRPInstance(
            string filename,
            decimal timeStep,
            int internalSwarmSize,
            int internalSwarmIterations,
            int routeSwarmIterations,
            double neighbourProbability1,
            double inventorsRatio1,
            double neighbourProbability2,
            double inventorsRatio2,
            decimal currentTime,
            DegreeOfDynamicity dod,
            int optimizer,
            decimal initialCapacityPart,
            decimal capacitySlope,
            bool traceInfo,
            double weightPowerParameter,
            bool usePreviousSolutions,
            bool use2Opt,
            int classesForVehicle,
            int reservoirVehiclesCount,
            decimal initialPartOfTheDay,
            int initialMonteCarloProposals,
            int additionalMonteCarloProposals,
            bool generateDataFromModel,
            bool useUCB,
            bool remeberBandits,
            bool useLambdaInMC,
            bool useGreedyInMC,
            Clustering clustering,
            bool useLambdaInterchange,
            int advanceCommitTimeSlices,
            double UCBCFactor,
            decimal fakeBufferSize
            )
        {
            this.filename = filename;
            this.timeStep = timeStep;
            this.internalSwarmSize = internalSwarmSize;
            this.internalSwarmIterations = internalSwarmIterations;
            this.routeSwarmIterations = routeSwarmIterations;
            this.optimizer = optimizer;
            this.dod = dod;
            this.neighbourProbability1 = neighbourProbability1;
            this.inventorsRatio1 = inventorsRatio1;
            this.neighbourProbability2 = neighbourProbability2;
            this.inventorsRatio2 = inventorsRatio2;
            this.traceInfo = traceInfo;
            this.initialCapacityPart = initialCapacityPart;
            this.capacitySlope = capacitySlope;
            this.weightPowerParameter = weightPowerParameter;
            this.usePreviousSolutions = usePreviousSolutions;
            this.use2Opt = use2Opt;
            this.classesForVehicle = classesForVehicle;
            this.reservoirVehiclesCount = reservoirVehiclesCount;
            this.initialMonteCarloProposals = initialMonteCarloProposals;
            this.initialPartOfTheDay = initialPartOfTheDay;
            this.additionalMonteCarloProposals = additionalMonteCarloProposals;
            this.useLambdaInterchangeInMC = useLambdaInMC;
            this.generateDataFromModel = generateDataFromModel;
            this.useUCB = useUCB;
            this.rememberBandits = remeberBandits;
            this.useGreedyInMC = useGreedyInMC;
            this.clustering = clustering;
            this.useLambdaInterchange = useLambdaInterchange;
            this.advanceCommitTimeSlices = advanceCommitTimeSlices;
            this.C = UCBCFactor;
            this.fakeBufferSize = fakeBufferSize;
            StreamReader reader = new StreamReader(filename);
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (line.StartsWith("COMMENT: Best known objective:"))
                {
                    line = line.Replace("COMMENT: ", "");
                    this.bestKnown = GetDoubleNumberFromLine(line);
                }
                else if (line.StartsWith("NUM_DEPOTS"))
                {
                    Depots = new Depot[GetNumberFromLine(line)];
                }
                else if (line.StartsWith("NUM_CAPACITIES"))
                {
                    if (GetNumberFromLine(line) != 1)
                        throw new Exception("Nie obsługuję różnych typów towarów");
                }
                else if (line.StartsWith("NUM_VISITS"))
                {
                    Clients = new Client[GetNumberFromLine(line)];
                }
                else if (line.StartsWith("NUM_LOCATIONS"))
                {
                    Depots = new Depot[GetNumberFromLine(line) - Clients.Length];
                }
                else if (line.StartsWith("NUM_VEHICLES"))
                {
                    Vehicles = new Vehicle[GetNumberFromLine(line)];
                }
                else if (line.StartsWith("CAPACITIES"))
                {
                    int capacity = GetNumberFromLine(line);
                    this.capacity = capacity;
                    for (int i = 0; i < Vehicles.Length; ++i)
                    {
                        Vehicles[i] = new Vehicle();
                        Vehicles[i].Id = i + Clients.Length;
                        Vehicles[i].clientsToAssign = new List<Client>();
                        Vehicles[i].assignedClients = new List<Client>();
                        Vehicles[i].Available = true;
                        Vehicles[i].Capacity = capacity;
                    }
                }
                else if (line.StartsWith("DEPOTS"))
                {
                    for (int i = 0; i < Depots.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Depots[i] = new Depot();
                        Depots[i].Id = numbers[0];
                    }
                }
                else if (line.StartsWith("DEMAND_SECTION"))
                {
                    for (int i = 0; i < Clients.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Clients[i] = new Client();
                        Clients[i].Id = numbers[0];
                        Clients[i].Need = numbers[1];
                        Clients[i].Rank = (decimal)(2.0 * (Utils.Instance.random.NextDouble() - 0.5));
                        Clients[i].FakeVehicleId = Utils.Instance.random.Next(Vehicles.Length) + Vehicles.Min(vhcl => vhcl.Id);
                    }
                }
                else if (line.StartsWith("LOCATION_COORD_SECTION"))
                {
                    for (int i = 0; i < Clients.Length + Depots.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Client client = null;
                        if (Clients.Count(clnt => clnt.Id == numbers[0]) > 0)
                        {
                            client = Clients.First(clnt => clnt.Id == numbers[0]);
                        }
                        else
                        {
                            client = Depots.First(dpt => dpt.Id == numbers[0]);
                            for (int j = 0; j < Vehicles.Length; j++)
                            {
                                Vehicles[j].X = new double[classesForVehicle];
                                Vehicles[j].Y = new double[classesForVehicle];
                                for (int m = 0; m < classesForVehicle; ++m)
                                {
                                    Vehicles[j].X[m] = numbers[1];
                                    Vehicles[j].Y[m] = numbers[2];
                                }
                            }
                        }
                        client.X = numbers[1];
                        client.Y = numbers[2];
                    }
                }
                else if (line.StartsWith("DURATION_SECTION"))
                {
                    for (int i = 0; i < Clients.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Client client = Clients.First(clnt => clnt.Id == numbers[0]);
                        if (client != null)
                            client.TimeToUnload = numbers[1];
                    }
                }
                else if (line.StartsWith("DEPOT_TIME_WINDOW_SECTION"))
                {
                    for (int i = 0; i < Depots.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Depot depot = Depots.First(dpt => dpt.Id == numbers[0]);
                        if (depot != null)
                        {
                            depot.StartAvailable = numbers[1];
                            depot.EndAvailable = numbers[2] * (this.dod == DegreeOfDynamicity.None ? 100 : 1);
                        }
                    }
                }
                else if (line.StartsWith("TIME_AVAIL_SECTION"))
                {
                    for (int i = 0; i < Clients.Length; i++)
                    {
                        line = reader.ReadLine();
                        int[] numbers = GetNumbersFromLine(line);
                        Client client = Clients.First(clnt => clnt.Id == numbers[0]);
                        if (client != null)
                        {
                            client.StartAvailable = numbers[1];
                            if (dod == DegreeOfDynamicity.Automatic && client.StartAvailable + client.TimeToUnload +
                                (decimal)(2 * distances[client.Id, Depots.First().Id]) > (decimal)MaxTime)
                            {
                                client.StartAvailable = 0;
                            }
                            else if (dod == DegreeOfDynamicity.Half && client.StartAvailable > (decimal)MaxTime / 2)
                            {
                                client.StartAvailable = 0;
                            }
                        }
                    }
                }
            }
            reader.Close();
            reader.Dispose();
            CacheDistances();
            /*
            NumericVector needs = engine.CreateNumericVector(Clients.Where(clnt => clnt.StartAvailable == 0).Select(clnt => (double)-clnt.Need).ToArray());
            engine.SetSymbol("needs", needs);
            double moment = engine.Evaluate("moment((needs-mean(needs))/sd(needs), order=3) ").AsNumeric()[0];

            NumericVector x = engine.CreateNumericVector(Clients.Where(clnt => clnt.StartAvailable == 0).Select(clnt => (double)clnt.X).ToArray());
            NumericVector y = engine.CreateNumericVector(Clients.Where(clnt => clnt.StartAvailable == 0).Select(clnt => (double)clnt.Y).ToArray());
            engine.SetSymbol("x", x);
            engine.SetSymbol("y", y);
            engine.Evaluate("xy = cbind(x,y)");
            engine.Evaluate("gap = clusGap(xy, FUN = kmeans, nstart = 20, K.max = nrow(xy)/2, B = 50)");
            engine.Evaluate("idx = which.max(ifelse(gap$Tab[,3]==Inf,0,gap$Tab[,3]))");
            double gap = engine.Evaluate("nrow(xy)/(min(which(gap$Tab[idx,3]-gap$Tab[idx,4] < gap$Tab[,3])))").AsNumeric()[0];

            Console.WriteLine(moment);
            Console.WriteLine(gap);
            */
            this.CurrentTime = currentTime;
        }

        static DVRPInstance()
        {
            /*
            try
            {
                SetupREnvironmentPath();
                engine = REngine.CreateInstance("RDotNet");
                engine.Initialize(); // required since v1.5
                InstallAndLoadRPackage("moments");
                InstallAndLoadRPackage("cluster");
            }
            catch
            {
                Console.Error.WriteLine("R environment could not have been initialized!");
            }
            */
        }

        private static void InstallAndLoadRPackage(string packageName)
        {
            engine.Evaluate(string.Format("install.packages('{0}',repos = 'http://r.meteo.uni.wroc.pl/')", packageName));
            engine.Evaluate(string.Format("require({0})", packageName));
        }

        static REngine engine;
        private const double THRESHOLD = 0.01;
        public static void SetupREnvironmentPath()
        {
            var oldPath = System.Environment.GetEnvironmentVariable("PATH");
            var rPath = System.Environment.Is64BitProcess ? @"C:\Program Files\R\R-3.0.2\bin\x64" : @"C:\Program Files\R\R-3.0.2\bin\i386";
            // Mac OS X
            //var rPath = "/Library/Frameworks/R.framework/Libraries";
            // Linux (in case of libR.so exists in the directory)
            //var rPath = "/usr/lib";
            if (Directory.Exists(rPath) == false)
                rPath = System.Environment.Is64BitProcess ? @"C:\Users\" + Environment.UserName + @"\R-3.1.0\bin\x64" : @"C:\Users\" + Environment.UserName + @"\R-3.1.0\bin\i386";
            if (Directory.Exists(rPath) == false)
                throw new DirectoryNotFoundException(string.Format("Could not found the specified path to the directory containing R.dll: {0}", rPath));
            var newPath = string.Format("{0}{1}{2}", rPath, System.IO.Path.PathSeparator, oldPath);
            System.Environment.SetEnvironmentVariable("PATH", newPath);
            // NOTE: you may need to set up R_HOME manually also on some machines
            string rHome = rPath;
            var platform = Environment.OSVersion.Platform;
            switch (platform)
            {
                case PlatformID.Win32NT:
                    break; // R on Windows seems to have a way to deduce its R_HOME if its R.dll is in the PATH
                case PlatformID.MacOSX:
                    rHome = "/Library/Frameworks/R.framework/Resources";
                    break;
                case PlatformID.Unix:
                    rHome = "/usr/lib/R";
                    break;
                default:
                    throw new NotSupportedException(platform.ToString());
            }
            if (!string.IsNullOrEmpty(rHome))
                Environment.SetEnvironmentVariable("R_HOME", rHome);
        }

        public void CacheDistances()
        {
            if (distances == null)
                distances = new double[Clients.Length + Depots.Length, Clients.Length + Depots.Length];
            else if (distances.GetLength(0) < Clients.Length + Depots.Length || distances.GetLength(1) < Clients.Length + Depots.Length)
                distances = new double[Clients.Length + Depots.Length, Clients.Length + Depots.Length];
            foreach (Depot d1 in Depots)
            {
                foreach (Depot d2 in Depots)
                {
                    distances[d1.Id, d2.Id] = Utils.Instance.EuclideanDistance(new double[] { d1.X, d1.Y }, new double[] { d2.X, d2.Y });
                }
                foreach (Client c in Clients)
                {
                    distances[d1.Id, c.Id] = Utils.Instance.EuclideanDistance(new double[] { d1.X, d1.Y }, new double[] { c.X, c.Y });
                }
            }
            foreach (Client c1 in Clients)
            {
                foreach (Depot d in Depots)
                {
                    distances[c1.Id, d.Id] = Utils.Instance.EuclideanDistance(new double[] { c1.X, c1.Y }, new double[] { d.X, d.Y });
                }
                foreach (Client c2 in Clients)
                {
                    distances[c1.Id, c2.Id] = Utils.Instance.EuclideanDistance(new double[] { c1.X, c1.Y }, new double[] { c2.X, c2.Y });
                }
            }
        }

        private DVRPInstance()
        {
            listOfBandits = new List<object>();
        }

        public delegate void UpdateBestEvent(DVRPInstance function);

        [DataContract]
        public enum DegreeOfDynamicity
        {
            [EnumMember(Value="1")]
            Full = 1,
            [EnumMember(Value = "2")]
            Automatic = 2,
            [EnumMember(Value = "3")]
            Half = 3,
            [EnumMember(Value = "4")]
            None = 4
        }

        [DataContract]
        public enum Clustering
        {
            [EnumMember(Value = "1")]
            Tree = 1,
            [EnumMember(Value = "2")]
            Min = 2,
            [EnumMember(Value = "3")]
            Avg = 3,
            [EnumMember(Value = "4")]
            Max = 4
        }

        [DataContract]
        public enum FirstPhaseOptimizer
        {
            [EnumMember(Value="1")]
            VehicleClustering = 1,
            [EnumMember(Value = "2")]
            ClientAssignment = 2,
            [EnumMember(Value = "4")]
            TreeClustering = 4,
            [EnumMember(Value = "8")]
            Partitioning = 8,
            [EnumMember(Value = "16")]
            Dapso = 16,
            [EnumMember(Value = "32")]
            MonteCarlo = 32,
            [EnumMember(Value = "64")]
            TreesMonteCarlo = 64,
        }
        public List<Vehicle> AvailableVehicles
        {
            get
            {
                return Vehicles.Where(vhcl => vhcl.Available).ToList();
            }
        }

        public int ClientRequestsCount
        {
            get
            {
                return Clients.Where(clnt => clnt.StartAvailable <= currentTime * Depots.Max(dpt => dpt.EndAvailable)).Count();
            }
        }

        public int ClientsInVehiclesCount
        {
            get
            {
                return Vehicles.Sum(vhcl => vhcl.clientsToAssign.Where(clnt => !(clnt is Depot)).Count() + vhcl.assignedClients.Where(clnt => !(clnt is Depot)).Count());
            }
        }

        public int AssignedClientsInVehiclesCount
        {
            get
            {
                return Vehicles.Sum(vhcl => vhcl.assignedClients.Where(clnt => !(clnt is Depot)).Count());
            }
        }

        public List<Client> ClientsToAssign
        {
            get
            {
                return clientsToAssign;
            }
        }

        public decimal Completness
        {
            get
            {
                return (decimal)Clients.Count(clnt => clnt.VisitTime + clnt.TimeToUnload +
                    (decimal)Depots.Min(dpt => distances[
                        dpt.Id,
                        clnt.Id
                        ]) <= (decimal)MaxTime
                    ) / Clients.Length;
            }
        }

        public decimal CurrentTime
        {
            get
            {
                return currentTime;
            }
            set
            {
                currentTime = value;
                SetClientsToAssign();
                ResetVehicles();
            }
        }

        public decimal DOD
        {
            get
            {
                return (decimal)Clients.Count(clnt => clnt.StartAvailable > 0) / Clients.Length;
            }
        }
        /// <summary>
        /// Czas zamknięcia zajezdni w jednostkach problemu
        /// </summary>
        public double MaxTime
        {
            get { return Depots.Max(depot => depot.EndAvailable); }
        }

        public decimal NextTime
        {
            get
            {
                return currentTime + 3 * timeStep;
            }
        }

        /// <summary>
        /// Poprzedni czas (w częściach całości)
        /// </summary>
        public decimal PreviousTime
        {
            get
            {
                return currentTime - timeStep;
            }
        }

        /// <summary>
        /// Kopiuje instancję problemu - trzeba pamiętać o aktualizacji tej funkcji po istotniejszych zmianach
        /// </summary>
        /// <returns></returns>
        public DVRPInstance Clone()
        {
            DVRPInstance cloneFunction = new DVRPInstance();
            cloneFunction.bestKnown = this.bestKnown;
            cloneFunction.capacity = this.capacity;
            cloneFunction.capacitySlope = this.capacitySlope;
            cloneFunction.Clients = new Client[this.Clients.Length];
            for (int i = 0; i < Clients.Length; i++)
            {
                cloneFunction.Clients[i] = (Client)this.Clients[i].Clone();
            }
            cloneFunction.previousStepClientsCount = this.previousStepClientsCount;
            cloneFunction.Depots = new Depot[this.Depots.Length];
            for (int i = 0; i < Depots.Length; i++)
            {
                cloneFunction.Depots[i] = (Depot)this.Depots[i].Clone();
            }
            cloneFunction.Vehicles = new Vehicle[this.Vehicles.Length];
            for (int i = 0; i < Vehicles.Length; i++)
            {
                cloneFunction.Vehicles[i] = (Vehicle)this.Vehicles[i].Clone();
            }
            if (StartVehicles != null)
            {
                cloneFunction.StartVehicles = new Vehicle[this.StartVehicles.Length];
                for (int i = 0; i < StartVehicles.Length; i++)
                {
                    cloneFunction.StartVehicles[i] = (Vehicle)this.StartVehicles[i].Clone();
                }
            }
            //TODO: bandits should be merged
            cloneFunction.bandits = this.bandits;
            cloneFunction.dod = this.dod;
            cloneFunction.EvaluationCount = this.EvaluationCount;
            cloneFunction.filename = this.filename;
            cloneFunction.initialCapacityPart = this.initialCapacityPart;
            cloneFunction.internalSwarmIterations = this.internalSwarmIterations;
            cloneFunction.internalSwarmSize = this.internalSwarmSize;
            cloneFunction.inventorsRatio1 = this.inventorsRatio1;
            cloneFunction.inventorsRatio2 = this.inventorsRatio2;
            cloneFunction.neighbourProbability1 = this.neighbourProbability1;
            cloneFunction.neighbourProbability2 = this.neighbourProbability2;
            cloneFunction.optimizer = this.optimizer;
            cloneFunction.routeSwarmIterations = this.routeSwarmIterations;
            cloneFunction.timeStep = this.timeStep;
            cloneFunction.traceInfo = this.traceInfo;
            cloneFunction.CurrentTime = this.currentTime;
            cloneFunction.weightPowerParameter = this.weightPowerParameter;
            cloneFunction.usePreviousSolutions = this.usePreviousSolutions;
            cloneFunction.use2Opt = this.use2Opt;
            cloneFunction.discrete = this.discrete;
            cloneFunction.classesForVehicle = this.classesForVehicle;
            cloneFunction.reservoirVehiclesCount = this.reservoirVehiclesCount;
            cloneFunction.initialMonteCarloProposals = this.initialMonteCarloProposals;
            cloneFunction.initialPartOfTheDay = this.initialPartOfTheDay;
            cloneFunction.additionalMonteCarloProposals = this.additionalMonteCarloProposals;
            cloneFunction.generateDataFromModel = this.generateDataFromModel;
            cloneFunction.useUCB = this.useUCB;
            cloneFunction.rememberBandits = this.rememberBandits;
            cloneFunction.useGreedyInMC = this.useGreedyInMC;
            cloneFunction.useLambdaInterchangeInMC = this.useLambdaInterchangeInMC;
            cloneFunction.distances = this.distances;
            cloneFunction.previousBestValue = this.previousBestValue;
            cloneFunction.clustering = this.clustering;
            cloneFunction.advanceCommitTimeSlices = this.advanceCommitTimeSlices;
            cloneFunction.C = this.C;
            cloneFunction.fakeBufferSize = this.fakeBufferSize;

            foreach (Vehicle vehicle in cloneFunction.Vehicles.Where(vhcl => vhcl.clientsToAssign.Count + vhcl.assignedClients.Count > 0))
            {
                VehicleRouteFunction vehicleRouteFunction = new VehicleRouteFunction(vehicle, cloneFunction.Depots, CurrentTime * (decimal)MaxTime, AvailableVehicles.Count == 0, distances);
                double[] center = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Select(clnt => decimal.ToDouble(clnt.Rank)).ToArray();
                CommitVehicles(vehicle, vehicleRouteFunction, center);
            }
            cloneFunction.CurrentTime = this.currentTime;
            return cloneFunction;
        }

        public void SetSwarms(int size, int routeIterations, int clusterIterations)
        {
            this.internalSwarmIterations = clusterIterations;
            this.routeSwarmIterations = routeIterations;
            this.internalSwarmSize = size;
        }

        /// <summary>
        /// Zerowanie licznika czasu
        /// </summary>
        public void RestartTime()
        {
            currentTime = 0;
            foreach (Vehicle vehicle in Vehicles)
            {
                vehicle.assignedClients.Clear();
            }
        }

        public double Solve()
        {
            StartVehicles = new Vehicle[this.Vehicles.Length];
            for (int i = 0; i < this.Vehicles.Length; i++)
                StartVehicles[i] = (Vehicle)Vehicles[i].Clone();
            double value = 0.0;
            bool firstRun = true;
            PrepareVehicles();
            DateTime start = DateTime.Now;

            while ((firstRun || (ClientRequestsCount != ClientsInVehiclesCount && AvailableVehicles.Count > 0)) && (DateTime.Now - start) < timeout)
            {
                if (!firstRun)
                    Console.WriteLine("Another attempt. Function: {3}, Time: {0}, Clients: {1}, Vehicles: {2}", currentTime, ClientsToAssign.Count, AvailableVehicles.Count, filename);
                OptimizeOperatingAreas();
                firstRun = false;
                value = OptimizeRoutes();
                clientsToAssign = Clients.Where(
                    clnt => clnt.StartAvailable <= CurrentTime * Depots.Max(dpt => dpt.EndAvailable) && Vehicles.Sum(vhcl => vhcl.assignedClients.Count(clnt2 => clnt2.Id == clnt.Id) + vhcl.clientsToAssign.Count(clnt2 => clnt2.Id == clnt.Id)) == 0
                    ).ToList();
                if (ClientsInVehiclesCount + clientsToAssign.Count != ClientRequestsCount)
                    throw new Exception("There is a leak in clients requests");
                if (clientsToAssign.Count > 0)
                {
                    foreach (Client newClient in clientsToAssign)
                    {
                        List<Vehicle> consideredVehicles = AvailableVehicles.Where(
                            vhcl => vhcl.CountAssignedClients + vhcl.CountClientsToAssign > 0 && -vhcl.Need - newClient.Need <= vhcl.Capacity).ToList();
                        consideredVehicles.AddRange(AvailableVehicles.Where(
                            vhcl => vhcl.CountAssignedClients + vhcl.CountClientsToAssign == 0).Take(1));
                        int bestVehicleId = -1;
                        decimal bestRank = 0;
                        int bestVehicleNeed = int.MinValue;
                        double smallestIncrease = double.PositiveInfinity;
                        foreach (Vehicle vehicle in consideredVehicles)
                        {
                            int localBestVehicleId = -1;
                            double localSmallestIncrease = double.PositiveInfinity;
                            decimal localBestRank = 0.0M;
                            CheckPossibilityAndCostOfAssigningClientToVehicle(
                                newClient,
                                ref localBestVehicleId,
                                ref localBestRank,
                                ref localSmallestIncrease,
                                (Vehicle)vehicle.Clone(),
                                Depots,
                                CurrentTime * (decimal)MaxTime,
                                distances,
                                ref bestVehicleNeed);
                            if (localSmallestIncrease < smallestIncrease)
                            {
                                smallestIncrease = localSmallestIncrease;
                                bestVehicleId = localBestVehicleId;
                                bestRank = localBestRank;
                            }
                        }
                        if (bestVehicleId != -1)
                        {
                            Vehicle chosenVehicle = Vehicles.First(vhcl => vhcl.Id == bestVehicleId);
                            chosenVehicle.clientsToAssign.Add(newClient);
                            newClient.Rank = bestRank;
                            newClient.FakeVehicleId = bestVehicleId;
                            value += smallestIncrease;
                            //TODO: should I commit here?
                        }
                        else
                        {
                            value = double.MaxValue;
                            break;
                        }
                    }
                    foreach (Vehicle vehicle in Vehicles)
                    {
                        vehicle.clientsToAssign.RemoveAll(clnt => clnt is Depot);
                    }
                    value = OptimizeRoutes(true);
                    clientsToAssign = Clients.Where(
                        clnt => clnt.StartAvailable <= CurrentTime * Depots.Max(dpt => dpt.EndAvailable) && Vehicles.Sum(vhcl => vhcl.assignedClients.Count(clnt2 => clnt2.Id == clnt.Id) + vhcl.clientsToAssign.Count(clnt2 => clnt2.Id == clnt.Id)) == 0
                    ).ToList();

                }
                if (this.Clients.Count(clnt => (double)clnt.StartAvailable / MaxTime <= (double)currentTime) == previousStepClientsCount)
                {
                    //jezeli mamy tyle samo klientow co wczesniej to koniec obliczen
                    break;
                }
                break;
            }

            if (this.Clients.Count(clnt => (double)clnt.StartAvailable / MaxTime <= (double)currentTime) == previousStepClientsCount &&
                (value > previousBestValue || ClientsToAssign.Count > 0))
            {
                /* TODO: Tutaj konieczne sprawdzenie czemu nie działa i powoduje wyrzucenie klienta z powrotem */
                value = RestoreBetterSolution(StartVehicles);
            }
            else
            {
                previousBestValue = value;
            }
            if (ClientsToAssign.Count > 0)
            {
                foreach (Client client in Vehicles.First(vhcl => vhcl.Id == ClientsToAssign[0].FakeVehicleId).assignedClients)
                {
                    Console.Out.WriteLine(client.Id);
                }
                foreach (Client client in Vehicles.First(vhcl => vhcl.Id == ClientsToAssign[0].FakeVehicleId).clientsToAssign)
                {
                    Console.Out.WriteLine(client.Id);
                }
                throw new Exception("Not all the clients have been assigned");
            }
            previousStepClientsCount = this.Clients.Count(clnt => (double)clnt.StartAvailable / MaxTime <= (double)currentTime);
            return value;
        }

        private double RestoreBetterSolution(Vehicle[] StartVehicles)
        {
            double value = 0.0;
            for (int i = 0; i < Vehicles.Length; i++)
                this.Vehicles[i] = (Vehicle)StartVehicles[i].Clone();
            foreach (Vehicle vehicle in this.Vehicles.Where(vhcl => vhcl.clientsToAssign.Count + vhcl.assignedClients.Count > 0))
            {
                VehicleRouteFunction vehicleRouteFunction = new VehicleRouteFunction(vehicle, Depots, CurrentTime * (decimal)MaxTime, AvailableVehicles.Count == 0, distances);
                double[] center = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Select(clnt => decimal.ToDouble(clnt.Rank)).ToArray();
                if (use2Opt)
                    center = EnhanceWith2Opt(vehicle, vehicleRouteFunction, center, Depots, distances);
                value += CommitVehicles(vehicle, vehicleRouteFunction, center);
            }
            clientsToAssign = Clients.Where(
    clnt => clnt.StartAvailable <= CurrentTime * Depots.Max(dpt => dpt.EndAvailable) && Vehicles.Sum(vhcl => vhcl.assignedClients.Count(clnt2 => clnt2.Id == clnt.Id) + vhcl.clientsToAssign.Count(clnt2 => clnt2.Id == clnt.Id)) == 0
    ).ToList();
            return value;
        }

        public override string ToString()
        {
            return base.ToString() + new FileInfo(filename).Name;
        }

        /// <summary>
        /// Aktualizuje czas
        /// </summary>
        public void UpdateTime()
        {
            CurrentTime += timeStep;
        }
        /// <summary>
        /// Poprawia znaleziony wynik przy użyciu 2Opta
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="vehicleRouteFunction"></param>
        /// <param name="center"></param>
        /// <returns></returns>
        internal static double[] EnhanceWith2Opt(Vehicle vehicle, VehicleRouteFunction vehicleRouteFunction, double[] center, Depot[] Depots, double[,] distances)
        {
            //TODO: znaleźć przyczynę zapętlenia
            int counter = 0;
            bool change = true;
            int counterUpperBound = int.MaxValue>>17;
            while (change && counter++ < counterUpperBound)
            {
                double length = vehicleRouteFunction.ValueWithout2OPT(center);
                counterUpperBound = vehicleRouteFunction.Route.Count * vehicleRouteFunction.Route.Count * vehicleRouteFunction.Route.Count * vehicleRouteFunction.Route.Count;
                change = false;
                double gain = 0.0;
                int to1BestId = 0;
                int from2BestId = 0;
                for (int i = 0; i < vehicleRouteFunction.Route.Count - 1; i++)
                {
                    if (vehicleRouteFunction.Route[i] is Depot)
                        continue;
                    for (int j = i + 2; j < vehicleRouteFunction.Route.Count + 1; j++)
                    {
                        Client from1 = null;
                        if (i == 0)
                        {
                            from1 = vehicle.assignedClients.LastOrDefault();
                            if (from1 == null)
                            {
                                from1 = Depots.FirstOrDefault();
                            }
                        }
                        else
                        {
                            from1 = vehicleRouteFunction.Route[i - 1];
                        }
                        Client from2 = vehicleRouteFunction.Route[j - 1];
                        Client to1 = vehicleRouteFunction.Route[i];
                        Client to2 = null;
                        if (j == vehicleRouteFunction.Route.Count)
                        {
                            to2 = Depots.First();
                        }
                        else
                        {
                            if (vehicleRouteFunction.Route[j - 1] is Depot)
                                break;
                            to2 = vehicleRouteFunction.Route[j];
                        }
                        double tempGain =
                            distances[from1.Id, to1.Id] +
                            distances[from2.Id, to2.Id] -
                            distances[from1.Id, from2.Id] -
                            distances[to2.Id, to1.Id];
                        if (tempGain - gain > THRESHOLD)
                        {
                            gain = tempGain;
                            change = true;
                            to1BestId = i;
                            from2BestId = j - 1;
                        }
                    }
                }
                if (change)
                {
                    //Console.WriteLine(length);
                    //for (int i = 0; i < vehicleRouteFunction.Route.Count - 1; ++i )
                    //{
                    //    Console.Write("{0:0.00};", distances[vehicleRouteFunction.Route[i].Id,
                    //        vehicleRouteFunction.Route[i + 1].Id]);
                    //}
                    //Console.WriteLine();
                    //Console.WriteLine(string.Join(",", vehicleRouteFunction.Route.Select(clnt => clnt.Id).ToArray()));
                    for (int i = to1BestId, j = from2BestId; i < j; ++i, --j)
                    {
                        while (vehicleRouteFunction.Route[i].Rank == vehicleRouteFunction.Route[j].Rank)
                        {
                            decimal minDist = vehicleRouteFunction.Route.Min(clnt => (vehicleRouteFunction.Route.Min(clnt2 => (clnt.Rank - clnt2.Rank == 0) ? decimal.MaxValue : Math.Abs(clnt.Rank - clnt2.Rank))));
                            vehicleRouteFunction.Route[i].Rank -= minDist / 2;
                        }
                        decimal temp = vehicleRouteFunction.Route[i].Rank;
                        vehicleRouteFunction.Route[i].Rank = vehicleRouteFunction.Route[j].Rank;
                        vehicleRouteFunction.Route[j].Rank = temp;
                    }
                    center = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Select(clnt => decimal.ToDouble(clnt.Rank)).ToArray();
                }
            }
            return center;
        }

        private static double GetDoubleNumberFromLine(string line)
        {
            try
            {
                return double.Parse(line.Trim().Split(':')[1].Trim().Replace(".", ","));
            }
            catch
            {
                return double.Parse(line.Trim().Split(':')[1].Trim());
            }
        }

        private static int GetNumberFromLine(string line)
        {
            return int.Parse(line.Trim().Split(':')[1].Trim());
        }

        private static int[] GetNumbersFromLine(string line)
        {
            string[] data = line.Trim().Split(' ');
            int[] numbers = new int[data.Length];
            for (int i = 0; i < data.Length; ++i)
                numbers[i] = int.Parse(data[i].Trim());
            return numbers;
        }

        /// <summary>
        /// Wysyłanie pojazdów do klientów (ustalanie trasy)
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="vehicleRouteFunction"></param>
        /// <param name="best"></param>
        /// <returns></returns>
        private double CommitVehicles(Vehicle vehicle, VehicleRouteFunction vehicleRouteFunction, double[] best)
        {
            double bestValue = vehicleRouteFunction.Value(best);
            vehicle.clientsToAssign = vehicleRouteFunction.Route;
            if (vehicleRouteFunction.Unassigned.Count > 0)
                vehicle.Available = false;
            if (vehicle.FinishTime + (advanceCommitTimeSlices + 1) * timeStep * (decimal)MaxTime > (decimal)MaxTime)
            {
                while (vehicle.clientsToAssign.Count > 0
                    && (vehicle.assignedClients.Count == 0 || vehicle.assignedClients.Last().VisitTime < NextTime * (decimal)MaxTime))
                {
                    vehicle.assignedClients.Add(vehicle.clientsToAssign[0]);
                    vehicle.clientsToAssign.RemoveAt(0);
                }
                if (vehicle.assignedClients.Count == 1)
                {
                    vehicle.clientsToAssign.Insert(0, vehicle.assignedClients[0]);
                    vehicle.assignedClients.RemoveAt(0);
                }
            }
            //Console.WriteLine("{0:0000} {1:0000} {2:00} {3:00}",vehicle.Id, vehicle.FinishTime, vehicle.CountAssignedClients, vehicle.CountClientsToAssign);
            return bestValue;
        }

        /// <summary>
        /// Poszukiwanie trasy dla pojedynczego pojazdu
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="quick">If set to true only 2-OPT performed</param>
        /// <returns></returns>
        private double[] GetOptimumRouteForVehicle(Vehicle vehicle, bool quick = false)
        {
            VehicleRouteFunction vehicleRouteFunction = new VehicleRouteFunction(vehicle, Depots, CurrentTime * (decimal)MaxTime, AvailableVehicles.Count == 0, distances);
            double[] center = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Select(clnt => decimal.ToDouble(clnt.Rank)).ToArray();
            //without historic knowledge start from a random permutation
            if (!usePreviousSolutions)
            {
                center = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Select(clnt => Utils.Instance.random.NextDouble()).ToArray();
            }
            if (!quick)
            {
                if (vehicle.Available || AvailableVehicles.Count == 0)
                {
                    double radius = center.Length > 0 ? ((vehicle.clientsToAssign.Max(clnt => decimal.ToDouble(clnt.Rank)) - vehicle.clientsToAssign.Min(clnt => decimal.ToDouble(clnt.Rank)) + 0.5) * Math.Sqrt(center.Length)) : 1;
                    if (!usePreviousSolutions)
                        radius = 2 * (center.Length > 0 ? Math.Sqrt(center.Length) : 1);
                    ParticleSwarmOptimization.PSOAlgorithm pso = new PSOAlgorithm(vehicleRouteFunction, (int)(2.5 * internalSwarmSize), center, radius, neighbourProbability2, inventorsRatio2, false);
                    for (int i = 0; i < routeSwarmIterations && center.Length > 0; i++)
                    {
                        pso.Step();
                        EvaluationCount += internalSwarmSize;
                        if (traceInfo)
                        {
                            Utils.Instance.LogInfo("wydajnosc.csv", "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}", "vehicle" + vehicle.Id, currentTime, i, pso.BestValue, EvaluationCount, AvailableVehicles.Count, ClientsToAssign.Count);
                        }
                    }
                    center = pso.Best;
                }
            }
            if (use2Opt)
                center = EnhanceWith2Opt(vehicle, vehicleRouteFunction, center, Depots, distances);
            return center;
        }

        private void OptimizeOperatingAreas()
        {
            IFunction clusterFunction = null;
            double[] center = null;
            int[] discreteCenter = null;
            double radius = 1;
            int discreteRadius = AvailableVehicles.Count / 2 + 1;
            PSOAlgorithm other = null;
            if (((optimizer & (int)FirstPhaseOptimizer.TreesMonteCarlo) > 0 && currentTime >= 0.5M) || (optimizer & (int)FirstPhaseOptimizer.TreeClustering) > 0 || CurrentTime == 0M)
            {
                InitializeBySpanningTree(ref clusterFunction, ref center, ref other);
            }
            else
            {
                InitializeByInsertionChain(out discreteCenter);
            }

            if ((optimizer & (int)FirstPhaseOptimizer.ClientAssignment) > 0)
            {
                InitializeForClientAssignment(out clusterFunction, out center, out discreteCenter);
                if (discrete)
                {
                    PerformDiscreteClientAssignmentOptimization(clusterFunction, discreteCenter, discreteRadius);
                }
                else
                {
                    PerformContinuousClientAssignmentOptimization(clusterFunction, center, radius);
                }
            }
            else if ((optimizer & (int)FirstPhaseOptimizer.VehicleClustering) > 0)
            {
                InitializeForVehicleClustering(out clusterFunction, out center, out radius, clusterFunction);
                PerformContinousClusteringOptimization(clusterFunction, center, radius, other);
            }
            else if ((optimizer & (int)FirstPhaseOptimizer.Partitioning) > 0)
            {
                InitializeForPartitioning(out clusterFunction, out center, out radius);
                PerformContinousClusteringOptimization(clusterFunction, center, radius, other);
            }
            else if (((optimizer & (int)FirstPhaseOptimizer.Dapso) > 0) && ((optimizer & (int)FirstPhaseOptimizer.TreesMonteCarlo) == 0 || CurrentTime >= 0.5M))
            {
                InitializeForDAPSOClustering(out clusterFunction, out center, out radius, clusterFunction);
                PerformContinousClusteringOptimization(clusterFunction, center, radius, other);
            }
            else if ((optimizer & (int)FirstPhaseOptimizer.MonteCarlo) > 0)
            {
                MonteCarloSearcher monteCarloSearcher = new MonteCarloSearcher(this, out clusterFunction, initialPartOfTheDay, initialMonteCarloProposals, additionalMonteCarloProposals, generateDataFromModel, useUCB, useGreedyInMC, (listOfBandits == null || listOfBandits.Count == 0) ? null : listOfBandits);
                bandits = (rememberBandits/* || CurrentTime > 0.5*/) ? monteCarloSearcher.States : null;
            }
            else if ((optimizer & (int)FirstPhaseOptimizer.TreesMonteCarlo) > 0 && CurrentTime < 0.5M)
            {
                if (clustering == Clustering.Max)
                {
                    MonteCarloWithSolutionProposals<MaxClusterClient> monteCarlo = new MonteCarloWithSolutionProposals<MaxClusterClient>(
                        this, initialMonteCarloProposals, additionalMonteCarloProposals,
                        C, fakeBufferSize,
                        useLambdaInterchangeInMC, generateDataFromModel);
                    clusterFunction = monteCarlo.Solve();
                }
                else if (clustering == Clustering.Avg)
                {
                    MonteCarloWithSolutionProposals<AvgClusterClient> monteCarlo = new MonteCarloWithSolutionProposals<AvgClusterClient>(
                        this, initialMonteCarloProposals, additionalMonteCarloProposals,
                        C, fakeBufferSize,
                        useLambdaInterchangeInMC, generateDataFromModel);
                    clusterFunction = monteCarlo.Solve();
                }
                else if (clustering == Clustering.Min)
                {
                    MonteCarloWithSolutionProposals<MinClusterClient> monteCarlo = new MonteCarloWithSolutionProposals<MinClusterClient>(
                        this, initialMonteCarloProposals, additionalMonteCarloProposals,
                        C, fakeBufferSize,
                        useLambdaInterchangeInMC, generateDataFromModel);
                    clusterFunction = monteCarlo.Solve();
                }
                else
                {
                    MonteCarloWithSolutionProposals<Client> monteCarlo = new MonteCarloWithSolutionProposals<Client>(
                        this, initialMonteCarloProposals, additionalMonteCarloProposals,
                        C, fakeBufferSize,
                        useLambdaInterchangeInMC, generateDataFromModel);
                    clusterFunction = monteCarlo.Solve();
                }
            }

            //Utils.LogInfo("log.txt", "[{2:yyyy-MM-dd HH:mm:ss}];{3};{0:0.00};{1:0.00};estimation", currentTime, pso.BestValue, DateTime.Now, new FileInfo(filename).Name);
            for (int i = 0; i < Vehicles.Length; i++) {
                Vehicle vehicle = (clusterFunction as ClusterFunction).Vehicles.FirstOrDefault(vhcl => vhcl.Id == this.Vehicles[i].Id);
                if (vehicle != null)
                    this.Vehicles[i] = (Vehicle)vehicle.Clone();
            }
            for (int i = 0; i < clientsToAssign.Count; i++)
                this.clientsToAssign[i] = (Client)(clusterFunction as ClusterFunction).ClientsToAssign[i].Clone();
        }

        /// <summary>
        /// Finds greedy optimum of inserting client into Vehicle, sets -1 as bestVehicleId if
        /// assignment is impossible.
        /// Initially
        /// bestVehicleNeed should be set to -Capacity - 1
        /// smallestIncrease should be set to double.PositiveInfinity
        /// bestVehicleId should be set to double.PositiveInfinity
        /// </summary>
        /// <param name="newClient"></param>
        /// <param name="bestVehicleId"></param>
        /// <param name="bestRank"></param>
        /// <param name="smallestIncrease"></param>
        /// <param name="vehicle"></param>
        public static void CheckPossibilityAndCostOfAssigningClientToVehicle(Client newClient, ref int bestVehicleId, ref decimal bestRank, ref double smallestIncrease, Vehicle vehicle,
            Depot[] depots, decimal currentProblemTime, double[,] distances, ref int bestVehicleNeed)
        {
            if (-vehicle.Need - newClient.Need > vehicle.Capacity)
            {
                if (vehicle.clientsToAssign.Count > 0)
                    bestRank = vehicle.clientsToAssign.Max(clnt => clnt.Rank) + 1;
                else
                    bestRank = 1;
                return;
            }
            //Compute initial route length (before adding client)
            //This might be neccessary, but if not (and the length of te route is known)
            //we will only need to compute the increases
            double originalValue = GetRouteLength(vehicle, depots, currentProblemTime, distances);
            decimal[] ranks = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Select(clnt => clnt.Rank).OrderBy(rnk => rnk).ToArray();
            vehicle.clientsToAssign.Add(newClient);
            newClient.FakeVehicleId = vehicle.Id;
            for (int i = 0; i < ranks.Length + 1; ++i)
            {
                if (ranks.Length == 0)
                {
                    newClient.Rank = 0.5M;
                }
                else if (i == 0)
                {
                    newClient.Rank = (decimal)ranks[0] - 1M;
                }
                else if (i == ranks.Length)
                {
                    newClient.Rank = ranks[ranks.Length - 1] + 1M;
                }
                else
                {
                    newClient.Rank = (ranks[i - 1] + ranks[i]) / 2M;
                }
                //TODO: This is very non-optimal
                //Instead of setting ranks for client and recalculating route
                //I should compute just the increase of insertion (with taking into account
                //possibility of validating time constrains: needs testing
                double newValue = GetRouteLength(vehicle, depots, currentProblemTime, distances);
                if (double.IsNaN(newValue))
                    continue;
                double localbestVehicleId = bestVehicleId;
                if (newValue - originalValue < smallestIncrease || ((newValue - originalValue <= smallestIncrease) && -vehicle.Need < -bestVehicleNeed))
                {
                    smallestIncrease = newValue - originalValue;
                    bestRank = newClient.Rank;
                    bestVehicleId = newClient.FakeVehicleId;
                    bestVehicleNeed = vehicle.Need;
                }
            }
            vehicle.clientsToAssign.Remove(newClient);
        }

        internal static double GetRouteLength(Vehicle vehicle, Depot[] depots, decimal currentProblemTime, double[,] distances)
        {
            VehicleRouteFunction vehicleRouteFunction = new VehicleRouteFunction(vehicle, depots, currentProblemTime, false, distances);
            decimal[] xRank = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Select(clnt => clnt.Rank).ToArray();
            double newValue = vehicleRouteFunction.Value(xRank.Select(xx => decimal.ToDouble(xx)).ToArray());
            if (vehicleRouteFunction.Unassigned.Count > 0)
                return double.NaN;
            //For old MonteCarlo removal of the Depots from route is neccesary
            vehicle.clientsToAssign = vehicleRouteFunction.Route.ToList();
            return newValue;
        }

        /// <summary>
        /// Optimizes the routes by interchanging up to maxL1 and maxL2
        /// client between all pairs of routes
        /// </summary>
        /// <param name="vehicles">Vehicles with assigned clients and clients to assign</param>
        /// <param name="maxL1">Maximum number of clients that might be taken from first vehicle</param>
        /// <param name="maxL2">Maximum number of clients that might be taken from the second vehicle</param>
        public static void OptimizeWithLambdaInterchange(Vehicle[] vehicles, int maxL1, int maxL2, Depot[] depots, decimal currentProblemTime, double[,] distances)
        {
            /*
             * Dla każdych dwóch pojazdów (Id1 < Id2), dla każdych wartości
             * od 1 do maxL1 i od 0 do maxL2 umieszczaj (po kolei)
             * zamówienia w drugim pojeździe
             * 
             */
            int v1 = 0;
            int v2 = 0;
        FIRST_VEHICLE:
            for (; v1 < vehicles.Length; ++v1)
            {
                GetRouteLength(vehicles[v1], depots, currentProblemTime, distances);
                for (; v2 < vehicles.Length; ++v2)
                {
                    if (v1 == v2)
                        continue;
                    GetRouteLength(vehicles[v2], depots, currentProblemTime, distances);
                    for (int c11 = 0; maxL1 > 0 && c11 < vehicles[v1].clientsToAssign.Count; ++c11)
                    {
                        if (vehicles[v1].clientsToAssign[c11] is Depot)
                            continue;
                        Client client11 = (Client)vehicles[v1].clientsToAssign[c11];
                        decimal client11Rank = client11.Rank;
                        double gain = RemoveAtAndComputeRouteGain(vehicles[v1], c11);
                        //TODO: Resolve problem of checking both before and after removing client
                        decimal c11BestRank = 0.0M;
                        double increase = GetRankOfTheBestAssignment(vehicles[v2], depots, currentProblemTime, distances, client11, ref c11BestRank);
                        if (gain - increase > THRESHOLD)
                        {
                            AssignClientToVehicleBasedOnRank(vehicles[v2], client11, c11BestRank);
                            goto FIRST_VEHICLE;
                        }
                        for (int c21 = 0; maxL2 > 0 && c21 < vehicles[v2].clientsToAssign.Count && v1 < v2; c21++)
                        {
                            if (vehicles[v2].clientsToAssign[c21] is Depot || vehicles[v2].clientsToAssign[c21].Id == client11.Id)
                                continue;
                            //TODO: problem occuring -> additional depot is present when there are assignedClients
                            //for Tree (4. optimizer) and c100bD.vrp and one parallel instance
                            //BREAKPOINT: c21 == 10 && c11 == 3 && v1 == 0 && v2 == 4 && currentProblemTime == 0.3M
                            //For random seed set to 100
                            //The cause of the problem is that at the entry point there are more depots and later
                            //one is removed and the assignment is no longer accurate
                            //What I do not see is how it is even possible in tree clusterer to have
                            //overloaded vehicle?
                            //But in general such situation might occur - therefore I need to make the code ready
                            Client client21 = (Client)vehicles[v2].clientsToAssign[c21];
                            decimal client21Rank = client21.Rank;
                            double gain2 = RemoveAtAndComputeRouteGain(vehicles[v2], c21);
                            //TODO: Resolve problem of checking both before and after removing client
                            decimal c21BestRank = 0.0M;
                            increase = GetRankOfTheBestAssignment(vehicles[v2], depots, currentProblemTime, distances, client11, ref c11BestRank);
                            increase += GetRankOfTheBestAssignment(vehicles[v1], depots, currentProblemTime, distances, client21, ref c21BestRank);
                            if (gain + gain2 - increase > THRESHOLD)
                            {
                                AssignClientToVehicleBasedOnRank(vehicles[v2], client11, c11BestRank);
                                AssignClientToVehicleBasedOnRank(vehicles[v1], client21, c21BestRank);
                                goto FIRST_VEHICLE;
                            }
                            //for (int c12 = c11 + 1; maxL1 > 1 && c12 < vehicles[v1].clientsToAssign.Count; ++c12)
                            //{
                            //    if (vehicles[v1].clientsToAssign[c12] is Depot || vehicles[v1].clientsToAssign[c12].Id == client21.Id)
                            //        continue;
                            //    Client client12 = (Client)vehicles[v1].clientsToAssign[c12];
                            //    vehicles[v1].clientsToAssign.RemoveAt(c12);
                            //    for (int c22 = c21 + 1; maxL2 > 1 && c22 < vehicles[v2].clientsToAssign.Count; ++c22)
                            //    {
                            //        if (vehicles[v2].clientsToAssign[c22] is Depot || vehicles[v2].clientsToAssign[c22].Id == client11.Id || vehicles[v2].clientsToAssign[c22].Id == client12.Id)
                            //            continue;

                            //        Client client22 = (Client)vehicles[v2].clientsToAssign[c22];
                            //        vehicles[v2].clientsToAssign.RemoveAt(c22);

                            //        vehicles[v2].clientsToAssign.Insert(c22, client22);
                            //    }
                            //    vehicles[v1].clientsToAssign.Insert(c12, client12);
                            //}
                            AssignClientToVehicleBasedOnRank(vehicles[v2], client21, client21Rank);
                            GetRouteLength(vehicles[v2], depots, currentProblemTime, distances);

                        }
                        AssignClientToVehicleBasedOnRank(vehicles[v1], client11, client11Rank);
                        GetRouteLength(vehicles[v1], depots, currentProblemTime, distances);
                    }
                }
                v2 = 0;
            }

        }

        /// <summary>
        /// Linear search and insertion in proper place in the sorted route
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="client"></param>
        /// <param name="bestRank"></param>
        private static void AssignClientToVehicleBasedOnRank(Vehicle vehicle, Client client, decimal bestRank)
        {
            client.FakeVehicleId = vehicle.Id;
            client.Rank = bestRank;
            int i = 0;
            for (i = 0; i < vehicle.clientsToAssign.Count; ++i)
            {
                if (!(vehicle.clientsToAssign[i] is Depot) && bestRank > vehicle.clientsToAssign[i].Rank)
                    break;
            }
            vehicle.clientsToAssign.Insert(i, client);
        }

        private static double GetRankOfTheBestAssignment(Vehicle vehicle, Depot[] depots, decimal currentProblemTime, double[,] distances, Client client, ref decimal bestRank)
        {
            int assignmentPossibility = -1;
            double routeLengthIncrease = double.PositiveInfinity;
            int vehicleNeed = -vehicle.Capacity -1;
            CheckPossibilityAndCostOfAssigningClientToVehicle(client, ref assignmentPossibility, ref bestRank,
                ref routeLengthIncrease, vehicle, depots,
                currentProblemTime, distances, ref vehicleNeed);
            if (assignmentPossibility < 0)
                routeLengthIncrease = double.PositiveInfinity;
            return routeLengthIncrease;
        }

        /// <summary>
        /// Computes route length and 2OPTs it
        /// </summary>
        /// <param name="fakeInstance"></param>
        /// <param name="sum"></param>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        internal static double UpdateTotalRouteDistanceWith2OptedRoutes(Depot[] depots, double[,] distances, decimal currentTime, Vehicle vehicle)
        {
            VehicleRouteFunction vehicleRouteFunction = new VehicleRouteFunction(vehicle, depots, currentTime, true, distances);
            double[] center = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Select(clnt => decimal.ToDouble(clnt.Rank)).ToArray();
            center = DVRPInstance.EnhanceWith2Opt(vehicle, vehicleRouteFunction, center, depots, distances);
            return vehicleRouteFunction.Value(center);
        }


        /// <summary>
        /// This method expects that the assignedClients list is sorted
        /// and computes the gain from removing a client from the route
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="clientIndex"></param>
        /// <returns></returns>
        private static double RemoveAtAndComputeRouteGain(Vehicle vehicle, int clientIndex)
        {
            double gain = 0.0;
            if (clientIndex > 0 && clientIndex < vehicle.clientsToAssign.Count - 1)
                gain = vehicle.clientsToAssign[clientIndex - 1].DistanceTo(vehicle.clientsToAssign[clientIndex])
                    + vehicle.clientsToAssign[clientIndex + 1].DistanceTo(vehicle.clientsToAssign[clientIndex])
                    - vehicle.clientsToAssign[clientIndex - 1].DistanceTo(vehicle.clientsToAssign[clientIndex + 1]);
            else if (clientIndex == vehicle.clientsToAssign.Count - 1 && vehicle.assignedClients.Count == 0)
                gain = vehicle.clientsToAssign[clientIndex - 1].DistanceTo(vehicle.clientsToAssign[clientIndex])
                    + vehicle.clientsToAssign[0].DistanceTo(vehicle.clientsToAssign[clientIndex])
                    - vehicle.clientsToAssign[clientIndex - 1].DistanceTo(vehicle.clientsToAssign[0]);
            else if (clientIndex == vehicle.clientsToAssign.Count - 1 && clientIndex == 0 && vehicle.assignedClients.Count > 0)
                gain = vehicle.assignedClients.Last().DistanceTo(vehicle.clientsToAssign[clientIndex])
                    + vehicle.assignedClients[0].DistanceTo(vehicle.clientsToAssign[clientIndex])
                    - vehicle.assignedClients.Last().DistanceTo(vehicle.assignedClients[0]);
            else if (clientIndex == vehicle.clientsToAssign.Count - 1 && vehicle.assignedClients.Count > 0)
                gain = vehicle.clientsToAssign[clientIndex - 1].DistanceTo(vehicle.clientsToAssign[clientIndex])
                    + vehicle.assignedClients[0].DistanceTo(vehicle.clientsToAssign[clientIndex])
                    - vehicle.clientsToAssign[clientIndex - 1].DistanceTo(vehicle.assignedClients[0]);
            else if (clientIndex == 0 && vehicle.assignedClients.Count > 0)
                gain = vehicle.assignedClients.Last().DistanceTo(vehicle.clientsToAssign[clientIndex])
                    + vehicle.clientsToAssign[clientIndex + 1].DistanceTo(vehicle.clientsToAssign[clientIndex])
                    - vehicle.assignedClients.Last().DistanceTo(vehicle.clientsToAssign[clientIndex + 1]);
            vehicle.clientsToAssign.RemoveAt(clientIndex);
            return gain;
        }


        internal double PerformContinousClusteringOptimization(IFunction clusterFunction, double[] center, double radius, PSOAlgorithm other)
        {
            //use center of the search space (clients positions) instead of previous solution
            //works only for tree and vehicle functions
            ParticleSwarmOptimization.PSOAlgorithm pso = new PSOAlgorithm(clusterFunction, optimizer != (int)FirstPhaseOptimizer.TreeClustering ? internalSwarmSize : 1, center, radius, neighbourProbability1, inventorsRatio1, true);
            if (other != null && !(clusterFunction is ClientAssignmentFunction))
            {
                pso.Particles[pso.Particles.Length - 1].x = other.Particles[0].x;
                pso.Particles[pso.Particles.Length - 1].v = other.Particles[0].v;
                pso.Particles[pso.Particles.Length - 1].best = other.Particles[0].best;
                pso.Particles[pso.Particles.Length - 1].bestValue = other.Particles[0].bestValue;
            }
            for (int i = 0; clusterFunction.Dimension > 0 && i < internalSwarmIterations; i++)
            {
                pso.Step();
                EvaluationCount += internalSwarmSize;
                if (traceInfo)
                {
                    Utils.Instance.LogInfo("wydajnosc.csv", "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}", "estimation", currentTime, i, pso.BestValue, EvaluationCount, AvailableVehicles.Count, ClientsToAssign.Count);
                }
            }
            return clusterFunction.Value(pso.Best);
        }

        internal double PerformContinuousClientAssignmentOptimization(IFunction clusterFunction, double[] center, double radius)
        {
            ParticleSwarmOptimization.PSOAlgorithm pso = new ParticleSwarmOptimization.PSOAlgorithm(clusterFunction as ParticleSwarmOptimization.IFunction, optimizer != (int)FirstPhaseOptimizer.TreeClustering ? internalSwarmSize : 1, center, radius, neighbourProbability1, inventorsRatio1, true);
            for (int i = 0; clusterFunction.Dimension > 0 && i < internalSwarmIterations; i++)
            {
                pso.Step();
                EvaluationCount += internalSwarmSize;
            }
            (clusterFunction as ParticleSwarmOptimization.IFunction).Value(center);
            return (clusterFunction as ParticleSwarmOptimization.IFunction).Value(pso.Best);
            //Console.WriteLine(pso.BestValue);
        }

        internal double PerformDiscreteClientAssignmentOptimization(IFunction clusterFunction, int[] discreteCenter, int discreteRadius)
        {
#if (DEBUG_MODE)
                Console.Title = "0";
#endif
            DiscreteParticleSwarmOptimization.PSOAlgorithm pso = new DiscreteParticleSwarmOptimization.PSOAlgorithm(clusterFunction as DiscreteParticleSwarmOptimization.IFunction, optimizer != (int)FirstPhaseOptimizer.TreeClustering ? internalSwarmSize : 1, discreteCenter, discreteRadius, neighbourProbability1, inventorsRatio1);
            for (int i = 0; (clusterFunction as DiscreteParticleSwarmOptimization.IFunction).DiscreteDimension > 0 && i < internalSwarmIterations; i++)
            {
                pso.Step();
                EvaluationCount += internalSwarmSize;
#if (DEBUG_MODE)
                    lock (loggerLocker)
                    {
                        Console.Title = Math.Max(i, int.Parse(Console.Title)).ToString();
                    }
#endif
            }
            (clusterFunction as DiscreteParticleSwarmOptimization.IFunction).Value(discreteCenter);
            return (clusterFunction as DiscreteParticleSwarmOptimization.IFunction).Value(pso.Best);
            //Console.WriteLine(pso.BestValue);
        }

        internal void InitializeForDAPSOClustering(out IFunction clusterFunction, out double[] center, out double radius, IFunction initialSolution)
        {
            IEnumerable<int> ids = Vehicles.Select(vhcl => vhcl.Id);
            if (initialSolution != null)
                ids = (initialSolution as ClusterFunction).Vehicles.Where(vhcl => vhcl.assignedClients.Count + vhcl.clientsToAssign.Count > 0).Select(vhcl => vhcl.Id);
            clusterFunction = new DapsoClusterFunction(Vehicles.Where(vhcl => ids.Contains(vhcl.Id)).ToArray(), clientsToAssign.ToArray(), Depots, CurrentTime * (decimal)MaxTime, distances, this.weightPowerParameter, this.classesForVehicle);
            center = new double[0];
            for (int i = 0; i < classesForVehicle; ++i)
            {
                center = center.Concat((clusterFunction as ClusterFunction).Vehicles.OrderBy(vhcl => vhcl.Id).Where(vhcl => (vhcl.Available || AvailableVehicles.Count == 0) && ids.Contains(vhcl.Id)).SelectMany(vhcl => new double[] { vhcl.X[i], vhcl.Y[i] })).ToArray();
            }
            if (ClientsToAssign.Count > 1)
                radius = (Math.Max(clientsToAssign.Max(clnt => clnt.X) - ClientsToAssign.Min(clnt => clnt.X),
            clientsToAssign.Max(clnt => clnt.Y) - ClientsToAssign.Min(clnt => clnt.Y))) * Math.Sqrt(AvailableVehicles.Count);
            else
                radius = 1.0;
        }

        internal void InitializeForPartitioning(out IFunction clusterFunction, out double[] center, out double radius)
        {
            clusterFunction = new PartitionFunction(Vehicles, clientsToAssign.ToArray(), Depots, CurrentTime * (decimal)MaxTime, distances, this.weightPowerParameter, this.classesForVehicle);
            center = new double[0];
            for (int i = 0; i < classesForVehicle; ++i)
            {
                center = center.Concat((clusterFunction as ClusterFunction).Vehicles.OrderBy(vhcl => vhcl.Id).Where(vhcl => vhcl.Available || AvailableVehicles.Count == 0).SelectMany(vhcl => new double[] { vhcl.X[i], vhcl.Y[i] })).ToArray();
            }
            if (ClientsToAssign.Count > 1)
                radius = (Math.Max(clientsToAssign.Max(clnt => clnt.X) - ClientsToAssign.Min(clnt => clnt.X),
            clientsToAssign.Max(clnt => clnt.Y) - ClientsToAssign.Min(clnt => clnt.Y))) * Math.Sqrt(AvailableVehicles.Count);
            else
                radius = 1.0;
        }

        internal void InitializeForVehicleClustering(out IFunction clusterFunction, out double[] center, out double radius, IFunction initialSolution)
        {
            IEnumerable<int> ids = Vehicles.Select(vhcl => vhcl.Id);
            if (initialSolution != null)
                ids = (initialSolution as ClusterFunction).Vehicles.Where(vhcl => vhcl.assignedClients.Count + vhcl.clientsToAssign.Count > 0).Select(vhcl => vhcl.Id);
            clusterFunction = new ClusterFunction(Vehicles.Where(vhcl => ids.Contains(vhcl.Id)).ToArray(), clientsToAssign.ToArray(), Depots, CurrentTime * (decimal)MaxTime, distances, this.weightPowerParameter, this.classesForVehicle);
            center = new double[0];
            for (int i = 0; i < classesForVehicle; ++i)
            {
                center = center.Concat((clusterFunction as ClusterFunction).Vehicles.OrderBy(vhcl => vhcl.Id).Where(vhcl => (vhcl.Available || AvailableVehicles.Count == 0) && ids.Contains(vhcl.Id)).SelectMany(vhcl => new double[] { vhcl.X[i], vhcl.Y[i] })).ToArray();
            }
            if (ClientsToAssign.Count > 1)
                radius = (Math.Max(clientsToAssign.Max(clnt => clnt.X) - ClientsToAssign.Min(clnt => clnt.X),
            clientsToAssign.Max(clnt => clnt.Y) - ClientsToAssign.Min(clnt => clnt.Y))) * Math.Sqrt(AvailableVehicles.Count);
            else
                radius = 1.0;
        }

        internal void InitializeForClientAssignment(out IFunction clusterFunction, out double[] center, out int[] discreteCenter)
        {
            clusterFunction = new ClientAssignmentFunction(Vehicles, clientsToAssign.ToArray(), Depots, CurrentTime * (decimal)MaxTime, distances, this.weightPowerParameter, classesForVehicle);
            center = ClientsToAssign.OrderBy(clnt => clnt.Id).SelectMany(clnt => clnt.positionForClientAssignment).ToArray();
            discreteCenter = ClientsToAssign.OrderBy(clnt => clnt.Id).Select(clnt => clnt.FakeVehicleId - Vehicles.Min(vhcl => vhcl.Id)).ToArray();
            //Console.WriteLine((clusterFunction as DiscreteParticleSwarmOptimization.IFunction).Value(discreteCenter));
            //                Console.WriteLine(string.Join(",", Clients.Select(clnt => string.Format("{0:000}:{1:00}",clnt.Id,clnt.FakeVehicleId-Vehicles.Length))));
        }

        internal void InitializeByInsertionChain(out int[] discreteCenter)
        {
#if DEBUG_MODE
            int counter = 1;
#endif
            if (AvailableVehicles.Count == Vehicles.Length)
            {
                for (int i = 0; i < Vehicles.Length; ++i )
                {
                    Vehicles[i] = (Vehicle)StartVehicles[i].Clone();
                }
            }
            foreach (Client newClient in clientsToAssign.Where(clnt => clnt.StartAvailable > PreviousTime * (decimal)MaxTime).OrderBy(a => Utils.Instance.random.NextDouble()))
            {
                int bestVehicleId = -1;
                decimal bestRank = 0;
                double smallestIncrease = double.PositiveInfinity;
                foreach (Vehicle vehicle in AvailableVehicles)
                {
                    if (-vehicle.Need - newClient.Need > vehicle.Capacity)
                        continue;
                    //Compute initial route length (before adding client)
                    double originalValue = GetRouteLength(vehicle);
                    decimal[] ranks = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Select(clnt => clnt.Rank).OrderBy(rnk => rnk).ToArray();
                    vehicle.clientsToAssign.Add(newClient);
                    newClient.FakeVehicleId = vehicle.Id;
                    for (int i = 0; i < ranks.Length + 1; ++i)
                    {
                        if (ranks.Length == 0)
                        {
                            newClient.Rank = 0.5M;
                        }
                        else if (i == 0)
                        {
                            newClient.Rank = (decimal)ranks[0] - 1M;
                        }
                        else if (i == ranks.Length)
                        {
                            newClient.Rank = ranks[ranks.Length - 1] + 1M;
                        }
                        else
                        {
                            newClient.Rank = (ranks[i - 1] + ranks[i]) / 2M;
                        }
                        //TODO: This is very non-optimal
                        double newValue = GetRouteLength(vehicle);
                        if (newValue - originalValue < smallestIncrease || ((newValue - originalValue <= smallestIncrease) && -vehicle.Need < -Vehicles.First(vhcl => vhcl.Id == bestVehicleId).Need))
                        {
                            smallestIncrease = newValue - originalValue;
                            bestRank = newClient.Rank;
                            bestVehicleId = newClient.FakeVehicleId;
                        }
                    }
                    vehicle.clientsToAssign.Remove(newClient);
                }
                Vehicle chosenVehicle = Vehicles.First(vhcl => vhcl.Id == bestVehicleId);
                chosenVehicle.clientsToAssign.Add(newClient);
                newClient.Rank = bestRank;
                newClient.FakeVehicleId = bestVehicleId;
                GetRouteLength(chosenVehicle);
#if DEBUG_MODE
                    lock (loggerLocker)
                    {
                        System.Drawing.Image img = new System.Drawing.Bitmap(1024, 768);
                        DynamicVehicleRouting.Visualize.Visualizer.DrawVehicleAssignment(this, System.Drawing.Graphics.FromImage(img));
                        img.Save(string.Format("{0}.{1}-{3:000}.{2}", new FileInfo(filename).Name, this.CurrentTime, "png", counter++), System.Drawing.Imaging.ImageFormat.Png);
                    }
#endif
            }
            if (AvailableVehicles.Count == Vehicles.Length)
            {
                PrepareVehicles();
            }
            else
            {
                foreach (Vehicle vehicle in AvailableVehicles)
                {
                    foreach (Client client in ClientsToAssign)
                    {
                        vehicle.clientsToAssign.Remove(client);
                    }
                }
            }
            int minVehicleId = Vehicles.Min(vhcl => vhcl.Id);
            discreteCenter = clientsToAssign.Select(clnt => clnt.FakeVehicleId - minVehicleId).ToArray();
        }

        internal double GetRouteLength(Vehicle vehicle)
        {
            VehicleRouteFunction vehicleRouteFunction = new VehicleRouteFunction(vehicle, Depots, CurrentTime * (decimal)MaxTime, false, distances);
            decimal[] xRank = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Select(clnt => clnt.Rank).ToArray();
            double newValue = vehicleRouteFunction.Value(xRank.Select(xx => decimal.ToDouble(xx)).ToArray());
            if (vehicleRouteFunction.Unassigned.Count > 0)
                return double.MaxValue;
            vehicle.clientsToAssign = vehicleRouteFunction.Route;
            return newValue;
        }

        private void InitializeBySpanningTree(ref IFunction clusterFunction, ref double[] center, ref PSOAlgorithm other)
        {
            if (clustering == Clustering.Max)
            {
                clusterFunction = new TreeClusterFunction<MaxClusterClient>(Vehicles.ToArray(), clientsToAssign.ToArray(), Depots, CurrentTime * (decimal)MaxTime, distances, this.weightPowerParameter, this.classesForVehicle, reservoirVehiclesCount);
            }
            else if (clustering == Clustering.Avg)
            {
            clusterFunction = new TreeClusterFunction<AvgClusterClient>(Vehicles.ToArray(), clientsToAssign.ToArray(), Depots, CurrentTime * (decimal)MaxTime, distances, this.weightPowerParameter, this.classesForVehicle, reservoirVehiclesCount);
            }
            else if (clustering == Clustering.Min)
            {
                clusterFunction = new TreeClusterFunction<MinClusterClient>(Vehicles.ToArray(), clientsToAssign.ToArray(), Depots, CurrentTime * (decimal)MaxTime, distances, this.weightPowerParameter, this.classesForVehicle, reservoirVehiclesCount);
            }
            else
            {
                clusterFunction = new TreeClusterFunction<Client>(Vehicles.ToArray(), clientsToAssign.ToArray(), Depots, CurrentTime * (decimal)MaxTime, distances, this.weightPowerParameter, this.classesForVehicle, reservoirVehiclesCount);
            }
            clusterFunction.Value(new double[0]);
            center = new double[0];
            for (int i = 0; i < classesForVehicle; ++i)
            {
                center = center.Concat((clusterFunction as ClusterFunction).Vehicles.OrderBy(vhcl => vhcl.Id).Where(vhcl => (vhcl.Available || AvailableVehicles.Count == 0) && vhcl.clientsToAssign.Count + vhcl.assignedClients.Count > 0).SelectMany(vhcl => new double[] { vhcl.X[i], vhcl.Y[i] })).ToArray();
            }
            other = new PSOAlgorithm(clusterFunction, 1, center, 1e-300, neighbourProbability1, inventorsRatio1, false);
        }

        /// <summary>
        /// Optimizes the routes for all vehicles in instance
        /// </summary>
        /// <param name="quick">If set to true only 2-OPT used</param>
        /// <returns></returns>
        private double OptimizeRoutes(bool quick = false)
        {
            var vehiclesToOptimize = Vehicles.Where(vhcl => vhcl.clientsToAssign.Count + vhcl.assignedClients.Count > 0);
            double val = 0.0;
            foreach (Vehicle vehicle in vehiclesToOptimize)
            {
                GetOptimumRouteForVehicle(vehicle, quick);
            }
            //Console.WriteLine(vehiclesToOptimize.Sum(vhcl => GetRouteLength(vhcl, Depots, this.currentTime, distances)));
            if (useLambdaInterchange && currentTime >= 0.5M)
            {
                //TODO: Explain why it is working uite efficient here and very slow inside MC?
                OptimizeWithLambdaInterchange(vehiclesToOptimize.ToArray(), 1, 1, Depots, this.currentTime, distances);
            }
            //Console.WriteLine(vehiclesToOptimize.Sum(vhcl => GetRouteLength(vhcl, Depots, this.currentTime, distances)));
            foreach (Vehicle vehicle in vehiclesToOptimize)
            {
                VehicleRouteFunction vehicleRouteFunction = new VehicleRouteFunction(vehicle, Depots, CurrentTime * (decimal)MaxTime, AvailableVehicles.Count == 0, distances);
                double vehicleRoute = CommitVehicles(vehicle, vehicleRouteFunction, vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Select(clnt => (double)clnt.Rank).ToArray());
                val += vehicleRoute;
            }
            //Console.WriteLine(vehiclesToOptimize.Sum(vhcl => GetRouteLength(vhcl, Depots, this.currentTime, distances)));
            //Utils.LogInfo("log.txt","[{2:yyyy-MM-dd HH:mm:ss}];{3};{0:0.00};{1:0.00};precise", currentTime, val, DateTime.Now, new FileInfo(filename).Name);
            return val;
        }

        internal void ConditionallyCopyFromStartVehicles()
        {
            if (AvailableVehicles.Count != Vehicles.Length)
            {
                foreach (Vehicle vehicle in Vehicles)
                {
                    vehicle.Available = true;
                }
            }
                for (int i = 0; i < Vehicles.Length; ++i)
                {
                    Vehicles[i] = (Vehicle)StartVehicles[i].Clone();
                }
        }


        internal void PrepareVehicles()
        {
            if (AvailableVehicles.Count == Vehicles.Length)
            {
                for (int i = 0; i < Vehicles.Length; i++)
                {
                    if (Vehicles[i].clientsToAssign.Any(clnt => !(clnt is Depot)))
                    {
                        for (int j = 0; j < Vehicles[i].X.Length; ++j)
                        {
                            Vehicles[i].X[j] = Vehicles[i].clientsToAssign.Where(clnt => !(clnt is Depot)).Average(clnt => clnt.X);
                            Vehicles[i].Y[j] = Vehicles[i].clientsToAssign.Where(clnt => !(clnt is Depot)).Average(clnt => clnt.Y);
                        }
                    }
                    if (Vehicles[i].Available)
                        Vehicles[i].clientsToAssign.Clear();
                }
            }
            else
            {
                foreach (Vehicle vehicle in AvailableVehicles)
                {
                    if (vehicle.clientsToAssign.Any(clnt => !(clnt is Depot)))
                    {
                        for (int j = 0; j < vehicle.X.Length; ++j)
                        {
                            vehicle.X[j] = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Average(clnt => clnt.X);
                            vehicle.Y[j] = vehicle.clientsToAssign.Where(clnt => !(clnt is Depot)).Average(clnt => clnt.Y);
                        }
                    }
                    vehicle.clientsToAssign.Clear();
                }
                Vehicle[] unavailableVehicles = Vehicles.Where(vhcl => !vhcl.Available).ToArray();
                for (int i = 0; i < unavailableVehicles.Length; ++i) {
                    unavailableVehicles[i] = (Vehicle)StartVehicles.First(vhcl => vhcl.Id == unavailableVehicles[i].Id).Clone();
                }
            }
        }

        private void ResetVehicles()
        {
            foreach (Vehicle vehicle in Vehicles)
            {
                vehicle.Available = true;

                vehicle.Capacity =
                                    (int)Math.Min(
            Math.Max(ClientsToAssign.Count > 0 ? ClientsToAssign.Max(clnt => -clnt.Need) : 0,
            capacity * initialCapacityPart + Math.Ceiling((capacitySlope * capacity * currentTime))),
            capacity);
            }
        }

        private void SetClientsToAssign()
        {
            if (Clients.Length > 0 && Vehicles.Length > 0 && Depots.Length > 0)
            {
                clientsToAssign = Clients.Where(
                    clnt => clnt.StartAvailable <= (decimal)this.MaxTime * this.CurrentTime &&
                        !Vehicles.Any(vhcl => vhcl.assignedClients.Any(clnt2 => clnt2.Id == clnt.Id))
                    ).ToList();
                /*
                                    availableCapacity =
                                        //        (decimal)Vehicles.Max(vhcl => vhcl.Capacity);

                                Math.Min(
                                Math.Max(ClientsToAssign.Count > 0 ? ClientsToAssign.Max(clnt => -clnt.Need) : 0.0M,
                                (decimal)Vehicles.Max(vhcl => vhcl.Capacity) * currentTime * 1.2M),
                                (decimal)Vehicles.Max(vhcl => vhcl.Capacity));
                 */
            }
            else
            {
                clientsToAssign = new List<Client>();
            }
        }

    }
}