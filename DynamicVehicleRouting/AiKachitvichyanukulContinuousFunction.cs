﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRouting
{
    internal class AiKachitvichyanukulContinuousFunction: DapsoClusterFunction
    {
        public AiKachitvichyanukulContinuousFunction(Vehicle[] vehicles, Client[] clients, Depot[] depots, decimal currentProblemTime, double[,] distances, double weightPowerParameter,
            int classesForVehicle, bool fullMulticlustering, bool penalizeInsufficentVehicles, decimal cutoffTime, decimal bufferSize)
            : base(vehicles, clients, depots, currentProblemTime, distances, weightPowerParameter, classesForVehicle, fullMulticlustering, penalizeInsufficentVehicles, cutoffTime, bufferSize)
        {
        }

        public static double[] GetClientRanks(IEnumerable<Client> clients)
        {
            return clients.OrderBy(clnt => clnt.Id).Select(clnt => (double)clnt.Rank).ToArray();
        }

        override protected void PrepareData(ref double[] x)
        {
            double[] ranks = x.Take(ClientsToAssign.Length).ToArray();
            var clientEnum = ClientsToAssign.OrderBy(clnt => clnt.Id).GetEnumerator();
            int i = 0;
            while (clientEnum.MoveNext())
            {
                clientRanks[clientEnum.Current.Id] = (decimal)ranks[i++];
            }
            x = x.Skip(ClientsToAssign.Length).ToArray();
            base.PrepareData(ref x);
        }

        override public int Dimension
        {
            get { return ClientsToAssign.Length + base.Dimension; ; }
        }
    }
}
