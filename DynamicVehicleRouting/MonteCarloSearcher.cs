﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using ParticleSwarmOptimization;

namespace DynamicVehicleRouting
{
    public class MonteCarloSearcher
    {
        DVRPInstance instance;
        DVRPInstance simulationInstance;
        double minX;
        double minY;
        double maxX;
        double maxY;
        double minRequestSize;
        double maxRequestSize;
        int requestsNumber;
        int iterations;

        public class ActionResults
        {
            public int visits = 0;
            public double meanResult = 0.0;
        }

        enum VehicleFillness
        {
//            LessThen0_1 = 10,
            LessThen0_2 = 20,
//            LessThen0_3 = 30,
            LessThen0_4 = 40,
//            LessThen0_5 = 50,
            LessThen0_6 = 60,
//            LessThen0_7 = 70,
            LessThen0_8 = 80,
//            LessThen0_9 = 90,
            LessThen1_0 = 100,
            NA = -1
        }

        enum RequestDistance
        {
//            Ten = 10,
            Fifteen = 15,
            Thirty = 30,
            FortyFive = 45,
//            Fifty = 50,
            Sixty = 60,
//            Seventy = 70,
            Hundred = 100,
            NA = -1
        }

        /// <summary>
        /// Upper bound of requests assignement state (proposition: in capacities?)
        /// </summary>
        enum AssignmentState
        {
            _5P = 5,
            _15P = 15,
            _25P = 25,
            _35P = 35,
            _45P = 45,
            _55P = 55,
            _65P = 65,
            _75P = 75,
            _85P = 85,
            _95P = 95,
            _10P = 10,
            _20P = 20,
            _30P = 30,
            _40P = 40,
            _50P = 50,
            _60P = 60,
            _70P = 70,
            _80P = 80,
            _90P = 90,
            _100P = 100
        }

        enum Action
        {
            Action
            //Skip
        }

        
        string[] Plays
        {
            get
            {
                return bandits.SelectMany(bndt => bndt.Value.SelectMany(bndt2 => bndt2.Value.Select(bndt3 => string.Format("{0} {1} {2} {3} {4:0}", bndt.Key, bndt2.Key, bndt3.Key, bndt3.Value[0].visits, bndt3.Value.Count > 0 ? bndt3.Value[0].meanResult : double.NaN)))).ToArray();
            }
        }

        int VisitsCount
        {
            get
            {
                return bandits.Sum(bndt => bndt.Value.Sum(bndt2 => bndt2.Value.Sum(bndt3 => bndt3.Value[0].visits)));
            }
        }

        internal object States
        {
            get
            {
                return this.states;
            }
        }

        struct ConsideredAction: IComparable
        {
            public AssignmentState globalState;
            public RequestDistance distance;
            public VehicleFillness fillness;
            public int order;

            public int CompareTo(object obj)
            {
                if (!(obj is ConsideredAction))
                {
                    return int.MinValue;
                }
                ConsideredAction second = (ConsideredAction)obj;
                int states = this.globalState.CompareTo(second.globalState);
                int fillness = this.fillness.CompareTo(second.fillness);
                int distance = this.distance.CompareTo(second.distance);
                int order = this.order.CompareTo(second.order);
                if (states != 0)
                    return states;
                if (distance != 0)
                    return distance;
                if (fillness != 0)
                    return fillness;
                return order;
            }

            public override bool Equals(object obj)
            {
                return CompareTo(obj) == 0;
            }
        }

        class ConsideredActionSortedSetCompares : IEqualityComparer<SortedSet<ConsideredAction>>
        {
            public bool Equals(SortedSet<ConsideredAction> x, SortedSet<ConsideredAction> y)
            {
                if (GetHashCode(x)!=GetHashCode(y))
                    return false;
                ConsideredAction[] xarray = x.ToArray();
                ConsideredAction[] yarray = y.ToArray();
                for (int i = 0; i < xarray.Length; i++ )
                {
                    if (xarray[i].CompareTo(yarray[i]) != 0)
                        return false;
                }
                return true;

            }

            public int GetHashCode(SortedSet<ConsideredAction> obj)
            {
                return obj.Count;
            }
        }

        Dictionary<SortedSet<ConsideredAction>, List<ActionResults>[]> states = new Dictionary<SortedSet<ConsideredAction>, List<ActionResults>[]>(new ConsideredActionSortedSetCompares());

        Dictionary<AssignmentState, Dictionary<VehicleFillness, Dictionary<RequestDistance, List<ActionResults>>>> bandits;
        List<KeyValuePair<DVRPInstance,double>> solution = new List<KeyValuePair<DVRPInstance,double>>();
        List<List<ActionResults>> currentNodes;
        private bool useUCB;
        private double normalizationValue;
        private bool useGreedyInMC;

        public MonteCarloSearcher(
            DVRPInstance instance,
            out ParticleSwarmOptimization.IFunction clusterFunction,
            decimal initialPartOfTheDay,
            int resampleIterations,
            int totalSimulations,
            bool generateDataFromModel = true,
            bool useUCB = true,
            bool useGreedyInMC = false,
            List<object> states = null)
            : this(states)
        {
            //The problem of the locality i decide on the basis of incomplete information,
            //without the knowledge of the whole solution, the overall state might not be enough
            clusterFunction = null;
            this.instance = instance;
            this.useUCB = useUCB;
            this.useGreedyInMC = useGreedyInMC;
            minX = Math.Min(instance.Clients.Min(clt => clt.X), instance.Depots.Min(dpt => dpt.X));
            minY = Math.Min(instance.Clients.Min(clt => clt.Y), instance.Depots.Min(dpt => dpt.Y));
            maxX = Math.Max(instance.Clients.Max(clt => clt.X), instance.Depots.Max(dpt => dpt.X));
            maxY = Math.Max(instance.Clients.Max(clt => clt.Y), instance.Depots.Max(dpt => dpt.Y));
            minRequestSize = instance.Clients.Max(clnt => clnt.Need);
            maxRequestSize = instance.Clients.Min(clnt => clnt.Need);
            requestsNumber = instance.Clients.Length;
            DVRPInstance chosenInstance = null;
            this.iterations = totalSimulations;
            //File.WriteAllLines("count_states.csv", new string[] { "Iteration\tStates\tTime" });

            for (int i = 0; i < totalSimulations; ++i)
            {
                //File.AppendAllLines("count_states.csv", new string[] { string.Format("{0}\t{1}\t{2}", i, this.states.Count, DateTime.Now) });
                currentNodes = new List<List<ActionResults>>();
                if (i % (resampleIterations) == 0)
                {
                    normalizationValue = 1.0;
                    GenerateFakeRequests(generateDataFromModel);
                    //Takes median as normalization value
                    int s = solution.Count;
                    List<double> estimates = new List<double>() {
                        SolvePartialProblem(true, true), SolvePartialProblem(true, true),
                        SolvePartialProblem(true, true), SolvePartialProblem(true, true),
                        SolvePartialProblem(true, true), SolvePartialProblem(true, true),
                        SolvePartialProblem(true, true), SolvePartialProblem(true, true),
                        SolvePartialProblem(true, true), SolvePartialProblem(true, true),
                        SolvePartialProblem(true, true)};
                    normalizationValue = estimates.OrderBy(val => val).Take(6).Last();
                    for (; s < solution.Count; ++s)
                    {
                        DVRPInstance temp = solution[s].Key;
                        solution.Insert(s, new KeyValuePair<DVRPInstance, double>(temp, solution[s].Value / normalizationValue));
                        solution.RemoveAt(s+1);
                    }
                }
                double value = SolvePartialProblem(true, useGreedyInMC);
                foreach (List<ActionResults> nodes in currentNodes)
                    {
                        nodes.Add(new ActionResults(){ meanResult = value, visits = 1});
                    }
                for (int cn = 0; cn < currentNodes.Count; ++cn )
                {
                    List<ActionResults> actions = currentNodes[cn];
                    int visits = actions.Sum(act => act.visits);
                    double meanResult = visits == 0 ? 0 : actions.Sum(act => act.visits * act.meanResult) / visits;
                    actions.Clear();
                    actions.Add(new ActionResults() { meanResult = meanResult, visits = visits });
                }
                DVRPInstance minSolution = solution.First(slt=>slt.Value == solution.Min(slt2 => slt2.Value)).Key;
                solution.RemoveAll(slt => slt.Key != minSolution);

            }
            if (useUCB)
            {
                solution.Clear();
                for (int toss = 0; toss < 25; ++toss )
                    SolvePartialProblem(false, false);
                DVRPInstance minSolution = solution.First(slt => slt.Value == solution.Min(slt2 => slt2.Value)).Key;
                solution.RemoveAll(slt => slt.Key != minSolution);
                chosenInstance = solution.Last().Key;
            }
            else
            {
                chosenInstance = solution.First().Key;
            }

            for (int i = 0; i < this.instance.Vehicles.Length; ++i)
            {
                this.instance.Vehicles[i].X = chosenInstance.Vehicles[i].X;
                this.instance.Vehicles[i].Y = chosenInstance.Vehicles[i].Y;
            }
            for (int i = 0; i < this.instance.ClientsToAssign.Count; ++i)
            {
                this.instance.ClientsToAssign[i] = (Client)chosenInstance.Clients.First(clnt => clnt.Id == this.instance.ClientsToAssign[i].Id).Clone();
            }
            instance.PrepareVehicles();
            double[] center = null;
            int[] discreteCenter = null;

            instance.InitializeForClientAssignment(out clusterFunction, out center, out discreteCenter);
            (clusterFunction as DiscreteParticleSwarmOptimization.IFunction).Value(discreteCenter);
            //File.WriteAllLines("states.csv", new string[] {"Visits\tActions\tBestResult" }); 
            //File.AppendAllLines("states.csv", 
            //    this.states.Select(pair => string.Format(
            //        "{0}\t{1}\t{2}",
            //        pair.Value.Sum(val => val.Sum(val2 => val2.visits)),
            //        pair.Key.Count(),
            //        pair.Value.Min(val => val.Min(val2 => val2.visits > 0 ? val2.meanResult : double.MaxValue))
            //        ))
            //        .ToArray());
            ////TODO: Pokazac podzial w top 10 akcjach
            var temp_states = this
                .states
                .OrderByDescending(st => st.Value.Sum(val => val.Sum(val2 => val2.visits)))
                .Take(5000)
                .ToList();

            //File.WriteAllLines("often_states.csv", new string[] { "" });
            //foreach (var temp_state in temp_states.Take(10))
            //{
            //    File.AppendAllLines("often_states.csv", new string[] {
            //    string.Join("\t", temp_state.Key.Select(key => string.Join(",", key.globalState, key.fillness, key.distance))) });
            //    File.AppendAllLines("often_states.csv", new string[] {
            //    string.Join("\t", temp_state.Value.Select(val => val.Sum(vl => vl.visits).ToString())) });
            //    File.AppendAllLines("often_states.csv", new string[] {
            //    string.Join("\t", temp_state.Value.Select(val => val.Min(vl => vl.meanResult).ToString())) });
            //}

            //File.WriteAllLines("interesting_states.csv", new string[] { "" });
            //foreach (var temp_state in this.states.OrderBy(st => st.Value.Min(val => val.Min(val2 => val2.visits > 0 ? val2.meanResult : double.MaxValue))).Take(10))
            //{
            //    File.AppendAllLines("interesting_states.csv", new string[] {
            //    string.Join("\t", temp_state.Key.Select(key => string.Join(",", key.globalState, key.fillness, key.distance))) });
            //    File.AppendAllLines("interesting_states.csv", new string[] {
            //    string.Join("\t", temp_state.Value.Select(val => val.Sum(vl => vl.visits).ToString())) });
            //    File.AppendAllLines("interesting_states.csv", new string[] {
            //    string.Join("\t", temp_state.Value.Select(val => val.Min(vl => vl.meanResult).ToString())) });
            //}

            this.states.Clear();
            foreach (var temp_state in temp_states)
            {
                this.states.Add(temp_state.Key, temp_state.Value);
            }
            //TODO: lock file
            //File.WriteAllLines(string.Format("Bandits-{0:000}.csv", System.Threading.Thread.CurrentThread.ManagedThreadId), Plays);
        }

        private MonteCarloSearcher(List<object> _states = null)
        {
            if (_states == null)
            {
                states = new Dictionary<SortedSet<ConsideredAction>, List<ActionResults>[]>(new ConsideredActionSortedSetCompares());
            }
            else
            {
                states = new Dictionary<SortedSet<ConsideredAction>, List<ActionResults>[]>(new ConsideredActionSortedSetCompares());
                foreach (Dictionary<SortedSet<ConsideredAction>, List<ActionResults>[]> substate in _states)
                {
                    foreach (SortedSet<ConsideredAction> actions in substate.Keys)
                    {
                        if (states.ContainsKey(actions))
                        {
                            for (int i = 0; i < states[actions].Length; ++i)
                            {
                                states[actions][i].Add(substate[actions][i][0]);
                            }
                        }
                        else
                        {
                            states.Add(actions, substate[actions]);
                        }
                    }
                }
            }
        }

        private ConsideredAction GetBandit(Vehicle vehicle, Client client, AssignmentState assignmentState, ref SortedSet<ConsideredAction> sortedActions)
        {
            VehicleFillness fillness;
            RequestDistance requestDistance;
            if (vehicle.assignedClients.Count(clnt => !(clnt is Depot)) + vehicle.clientsToAssign.Count(clnt => !(clnt is Depot)) == 0)
            {
                ConsideredAction action = new ConsideredAction()
                    {
                        globalState = assignmentState,
                        distance = RequestDistance.NA,
                        fillness = VehicleFillness.NA,
                        order = sortedActions.Count
                    };
                if (sortedActions != null)
                {
                    sortedActions.Add(action);
                }
                return action;
            }

            double usage = 100*(double)(vehicle.Need + client.Need) / -vehicle.Capacity;
            fillness = VehicleFillness.NA;
            foreach (VehicleFillness fillnessKey in Enum.GetValues(typeof(VehicleFillness)).Cast<VehicleFillness>().OrderBy(key => (int)key))
            {
                if (usage <= (int)fillnessKey)
                {
                    fillness = fillnessKey;
                    break;
                }
            }

            double distance = 100.0 * GetAssignmentCost(client, vehicle) / Math.Sqrt((minX - maxX) * (minX - maxX) + (minY - maxY) * (minY - maxY));
            //double distance = 100 * vehicle.GetDistanceFromVehicle(client, Math.Sqrt((minX - maxX) * (minX - maxX) + (minY - maxY) * (minY - maxY)));
            requestDistance = RequestDistance.NA;
            foreach (RequestDistance distanceKey in Enum.GetValues(typeof(RequestDistance)).Cast<RequestDistance>().OrderBy(key => (int)key))
            {
                if (distance <= (int)distanceKey)
                {
                    requestDistance = distanceKey;
                    break;
                }
            }

            ConsideredAction addAction = new ConsideredAction()
            {
                globalState = assignmentState,
                distance = requestDistance,
                fillness = fillness,
                order = sortedActions.Count
            };
            if (sortedActions != null && requestDistance != RequestDistance.NA)
            {
                sortedActions.Add(addAction);
            }
            return addAction;

        }

        private void ObserveState(SortedSet<ConsideredAction> sortedActions)
        {
            if (states.ContainsKey(sortedActions))
            {
                if (states[sortedActions].Length != sortedActions.Count)
                    throw new ArgumentOutOfRangeException("Dictionary is longer then its values");
                return;
            }
            List<ActionResults>[] results = new List<ActionResults>[sortedActions.Count()];
            for (int i = 0; i < results.Length; ++i)
            {
                results[i] = new List<ActionResults>() { new ActionResults() { meanResult = 0.0, visits = 0}};
            }
            states.Add(sortedActions, results);
        }

        private void GenerateFakeRequests(bool generateDataFromModel)
        {
            simulationInstance = instance.Clone();
            int countExpected = requestsNumber;
            foreach (Client client in simulationInstance.Clients.Where(clnt => clnt.StartAvailable > simulationInstance.NextTime * (decimal)simulationInstance.MaxTime))
            {
                if (generateDataFromModel)
                {
                    double x = 0.0;
                    double y = 0.0;
                    do
                    {
                        bool cont = false;
                        x = Utils.Instance.random.NextDouble() * (maxX - minX) + minX;
                        y = Utils.Instance.random.NextDouble() * (maxY - minY) + minY;
                        foreach (Client setClient in simulationInstance.Clients.Where(clnt => clnt.StartAvailable <= simulationInstance.NextTime * (decimal)simulationInstance.MaxTime))
                        {
                            double dist = 1 - Math.Exp(-0.5 * ((x - setClient.X) * (x - setClient.X) + (y - setClient.Y) * (y - setClient.Y)) / ((maxX - minX) * (maxY - minY) / countExpected));
                            if (Utils.Instance.random.NextDouble() > dist)
                            {
                                cont = true;
                                break;
                            }
                        }
                        if (!cont)
                            break;
                    } while (true);
                    client.X = x;
                    client.Y = y;
                    client.Need = simulationInstance.ClientsToAssign[Utils.Instance.random.Next(simulationInstance.ClientsToAssign.Count)].Need;
                    client.FakeVehicleId = 1000;
                }
                client.StartAvailable = (int)Math.Floor(simulationInstance.NextTime * (decimal)simulationInstance.MaxTime);
                simulationInstance.ClientsToAssign.Add(client);
            }
            simulationInstance.CacheDistances();

        }

        /// <summary>
        /// This method generates a new solution and stores it in the solutions collection
        /// </summary>
        /// <param name="simulation">If it is not a simulation the bandits are used but not explored</param>
        /// <returns>The value of the found solution</returns>
        private double SolvePartialProblem(bool simulation, bool greedy)
        {

            simulationInstance.ConditionallyCopyFromStartVehicles();
            double offset = Utils.Instance.random.NextDouble() * 2 * Math.PI;
            List<Client> clientsToAnalyze = simulationInstance
                .ClientsToAssign
                .OrderBy(clnt => clnt.Id) //for simple test only
                //.OrderBy(clnt => Utils.random.NextDouble())
                //.OrderBy(clnt => clnt.FakeVehicleId)
                //.OrderBy(clnt => clnt.StartAvailable)
                //.OrderBy(clnt => GetClientAngle(clnt, offset, simulationInstance.Depots.First()))
                .ToList();
            decimal sumOfConsideredRequests = simulationInstance.Vehicles.Sum(vhcl => vhcl.assignedClients.Any() ? vhcl.assignedClients.Sum(clnt => -clnt.Need) : 0.0M);
            decimal sumOfRequests = simulationInstance.Clients.Sum(clnt => -clnt.Need);
            foreach (Client newClient in clientsToAnalyze)
            {
                sumOfConsideredRequests -= newClient.Need;
                List<Vehicle> consideredVehicles = simulationInstance.AvailableVehicles.Where(
                    vhcl => vhcl.CountAssignedClients + vhcl.CountClientsToAssign > 0 && -vhcl.Need - newClient.Need <= vhcl.Capacity ).ToList();
                if (consideredVehicles.Any())
                    consideredVehicles = consideredVehicles.OrderBy(vhcl => GetAssignmentCost(newClient, vhcl)).ToList();
                consideredVehicles.AddRange(simulationInstance.AvailableVehicles.Where(
                    vhcl => vhcl.CountAssignedClients + vhcl.CountClientsToAssign == 0).Take(1));
                int bestVehicleId = -1;
                List<List<ActionResults>> markedNodes = new List<List<ActionResults>>();
                while (bestVehicleId == -1)
                {
                    //Choose from UCB
                    Vehicle vehicle = null;
                    if (greedy)
                    {
                        vehicle = consideredVehicles.OrderBy(vhcl => GetAssignmentCost(newClient, vhcl)).FirstOrDefault();
                    }
                    else if (useUCB)
                    {
                        vehicle = ChooseVehicle(consideredVehicles, newClient, out markedNodes, sumOfConsideredRequests / sumOfRequests * 100.0M, simulation);
                    }
                    else
                    {
                        vehicle = consideredVehicles[Utils.Instance.random.Next(consideredVehicles.Count)];
                    }
                    decimal bestRank = 0;
                    double smallestIncrease = double.PositiveInfinity;
                    int bestVehicleNeed = int.MinValue;
                    vehicle.CheckPossibilityAndCostOfAssigningClientToVehicle(
                        newClient,
                        ref bestVehicleId,
                        ref bestRank,
                        ref smallestIncrease,
                        
                        simulationInstance.Depots,
                        simulationInstance.NextTime * (decimal)simulationInstance.MaxTime,
                        simulationInstance.distances,
                        ref bestVehicleNeed);
                    if (bestVehicleId != -1)
                    {
                        Vehicle chosenVehicle = simulationInstance.Vehicles.First(vhcl => vhcl.Id == bestVehicleId);
                        chosenVehicle.clientsToAssign.Add(newClient);
                        newClient.Rank = bestRank;
                        newClient.FakeVehicleId = bestVehicleId;
                        currentNodes.AddRange(markedNodes);
                    }
                    else
                    {
                        //about 1/5 is missed actions - what impact does it have on the bandits?
                        consideredVehicles.Remove(vehicle);
                    }
                }
            }
            double[] center = null;
            int[] discreteCenter = null;
            ParticleSwarmOptimization.IFunction function = null;
            simulationInstance.InitializeForClientAssignment(out function, out center, out discreteCenter);
            simulationInstance.PrepareVehicles();
            double value = (function as DiscreteParticleSwarmOptimization.IFunction).Value(discreteCenter) /  normalizationValue;
            if (!double.IsInfinity(value))
            {
                for (int i = 0; i < this.instance.ClientsToAssign.Count; ++i)
                {
                    this.instance.ClientsToAssign[i] = (Client)(function as ClientAssignmentFunction).ClientsToAssign.First(clnt => clnt.Id == this.instance.ClientsToAssign[i].Id).Clone();
                }
                solution.Add(new KeyValuePair<DVRPInstance, double>(simulationInstance.Clone(), value));
            }
            foreach (Client newClient in simulationInstance.ClientsToAssign)
            {
                Vehicle chosenVehicle = simulationInstance.Vehicles.First(vhcl => vhcl.Id == newClient.FakeVehicleId);
                chosenVehicle.clientsToAssign.Remove(newClient);
            }
            return value;
        }

        private static double GetClientAngle(Client clnt, double offset, Depot depot)
        {
            double fi = 0;
            if (clnt.Y - depot.Y >= 0)
                fi = (Math.Atan2(clnt.X - depot.X, clnt.Y - depot.Y + 1e-12) + offset);
            else
                fi = Math.PI + (Math.Atan2(clnt.X - depot.X, clnt.Y - depot.Y + 1e-12) + offset);
            if (fi > 2 * Math.PI)
                fi -= 2 * Math.PI;
            return fi;
        }

        private Vehicle ChooseVehicle(List<Vehicle> consideredVehicles, Client client, out List<List<ActionResults>> markedNodes, decimal assignmentPercentage, bool simulation)
        {
            Dictionary<Vehicle, ConsideredAction> actions = new Dictionary<Vehicle, ConsideredAction>();
            SortedSet<ConsideredAction> bandit;
            AnalyzeVehicles(consideredVehicles, client, actions, assignmentPercentage, out bandit);
            List<ActionResults>[] results = null;
            try
            {
                results = states.ContainsKey(bandit) ? states[bandit] : null;
            }
            catch
            {
                results = null;
            }
            return SelectVehicleAndMarkVisitedNodes(actions, out markedNodes, client, simulation, bandit.ToList(), results);
        }

        private double GetAssignmentCost(Client newClient, Vehicle vehicle)
        {
            int bestVehicleId = -1;
            decimal bestRank = 0;
            double smallestIncrease = double.PositiveInfinity;
            int bestVehicleNeed = int.MinValue;
            vehicle.CheckPossibilityAndCostOfAssigningClientToVehicle(
                newClient,
                ref bestVehicleId,
                ref bestRank,
                ref smallestIncrease,
                
                simulationInstance.Depots,
                simulationInstance.NextTime * (decimal)simulationInstance.MaxTime,
                simulationInstance.distances,
                ref bestVehicleNeed);
            return ( (bestVehicleId != -1) ? smallestIncrease : double.MaxValue);
        }

        private Vehicle SelectVehicleAndMarkVisitedNodes(
            Dictionary<Vehicle, ConsideredAction> actions,
            out List<List<ActionResults>> markedNodes,
            Client client,
            bool simulation,
            List<ConsideredAction> bandit,
            List<ActionResults>[] actionResults
            )
        {
            markedNodes = new List<List<ActionResults>>();
            if (actions.Keys.Count == 1)
            {
                return actions.Keys.FirstOrDefault();
            }
            if (actionResults == null)
            {
                return actions.Keys.OrderBy(vhcl => GetAssignmentCost(client, vhcl)).FirstOrDefault();
            }
            //TODO: Parametr albo odchudzanie zbioru
            if (!simulation && actionResults.Sum(results => results.Sum(rslt => rslt.visits)) < 30)
                return actions.Keys.OrderBy(vhcl => GetAssignmentCost(client, vhcl)).FirstOrDefault();
            Vehicle nonTested = actions.Keys.OrderBy(vhcl => GetAssignmentCost(client, vhcl)).FirstOrDefault(vhcl => actions.ContainsKey(vhcl) && actionResults[bandit.IndexOf(actions[vhcl])][0].visits == 0);
            if (nonTested != null)
            {
                markedNodes.Add(actionResults[bandit.IndexOf(actions[nonTested])]);
                return nonTested;
            }
            else
            {
                IEnumerable<Vehicle> selectedCandidates = actions.Keys.Where(vhcl => actions.ContainsKey(vhcl));
                Vehicle best = null;
                if (selectedCandidates.Any())
                {
                    double worstResult = actionResults.Max(results => results.Max(rslt => rslt.meanResult));
                    double bestResult = actionResults.Min(results => results.Min(rslt => rslt.meanResult));
                    if (bestResult == worstResult)
                        worstResult += 1.0;
                    int sum = actionResults.Sum(results => results.Sum(rslt => rslt.visits));
                    double max = selectedCandidates.Max(
                        vhcl => 1 - (actionResults[bandit.IndexOf(actions[vhcl])][0].meanResult - bestResult) / (worstResult - bestResult) + (simulation ? GetQuantityCoefficent(sum, actionResults[bandit.IndexOf(actions[vhcl])][0].visits) : 0.0)
                            );
                    IEnumerable<Vehicle> chosen = selectedCandidates.Where(
                        vhcl => (1 - (actionResults[bandit.IndexOf(actions[vhcl])][0].meanResult - bestResult) / (worstResult - bestResult) + (simulation ? GetQuantityCoefficent(sum, actionResults[bandit.IndexOf(actions[vhcl])][0].visits) : 0.0)) == max);
                    if (chosen.Any())
                    {
                        best = chosen.OrderBy(vhcl => GetAssignmentCost(client, vhcl)).FirstOrDefault();
                    }
                    markedNodes.Add(actionResults[bandit.IndexOf(actions[best])]);
                }
                return best;
                //if (best != null)
                //{
                //    Vehicle spare = candidates.FirstOrDefault(vhcl => vhcl.CountAssignedClients + vhcl.CountClientsToAssign == 0);
                //    if (spare != null)
                //    {
                //        if (skipActions[best][0].visits == 0)
                //        {
                //            markedNodes.Add(skipActions[best]);
                //            return spare;
                //        }
                //        double addValue = 1 - actions[best][0].meanResult / Math.Max(actions[best][0].meanResult, skipActions[best][0].meanResult) + GetQuantityCoefficent(actions[best][0].visits + skipActions[best][0].visits, actions[best][0].visits);
                //        double skipValue = 1 - skipActions[best][0].meanResult / Math.Max(actions[best][0].meanResult, skipActions[best][0].meanResult) + GetQuantityCoefficent(actions[best][0].visits + skipActions[best][0].visits, skipActions[best][0].visits);
                //        if (skipValue >= addValue)
                //        {
                //            markedNodes.Add(skipActions[best]);
                //            return spare;
                //        }
                //    }
                //    markedNodes.Add(actions[best]);
                //    return best;

                //}
                //else
                //{
                //    best = candidates.FirstOrDefault(vhcl => vhcl.CountAssignedClients + vhcl.CountClientsToAssign == 0);
                //    if (best != null)
                //    {
                //        //Adding new vehicle - should it generate results as we had no choice?
                //        //markedNodes.AddRange(skipActions.Select(skp => skp.Value));
                //        return best;
                //    }
                //    else
                //    {
                //        double worstResult = actions.Max(act => act.Value[0].meanResult);
                //        double bestResult = actions.Min(act => act.Value[0].meanResult);
                //        if (bestResult == worstResult)
                //            worstResult += 1.0;
                //        int sum = actions.Sum(act => act.Value[0].visits);
                //        double max = actions.Max(
                //                act2 => 1 - (act2.Value[0].meanResult - bestResult) / (worstResult - bestResult) + GetQuantityCoefficent(sum, act2.Value[0].visits)
                //        );
                //        List<KeyValuePair<Vehicle, List<ActionResults>>> chosen = actions.Where(
                //            act => (1 - (act.Value[0].meanResult - bestResult) / (worstResult - bestResult) + GetQuantityCoefficent(sum, act.Value[0].visits)) == max).ToList();
                //        if (chosen.Any())
                //            best = chosen.OrderBy(vhcl => Utils.random.NextDouble()).First().Key;

                        
                //        markedNodes.Add(actions[best]);
                //        return best;
                //    }
                //}
            }
        }

        private void AnalyzeVehicles(
            List<Vehicle> consideredVehicles,
            Client client,
            Dictionary<Vehicle, ConsideredAction> actions,
            decimal assignmentPercentage,
            out SortedSet<ConsideredAction> sortedActions)
        {
            AssignmentState assignmentState = AssignmentState._100P;
            foreach (AssignmentState state in Enum.GetValues(typeof(AssignmentState)).Cast<AssignmentState>().OrderBy(key => (int)key))
            {
                if (assignmentPercentage < (int)state)
                {
                    assignmentState = state;
                    break;
                }
            }

            sortedActions = new SortedSet<ConsideredAction>();
            foreach (Vehicle vehicle in consideredVehicles)
            {
                ConsideredAction action = GetBandit(vehicle, client, assignmentState, ref sortedActions);
                if (action.distance != RequestDistance.NA || action.fillness == VehicleFillness.NA)
                    actions.Add(vehicle, action);
            }
            if (sortedActions.Count() > 1)
            {
                ObserveState(sortedActions);
            }
        }

        private double GetQuantityCoefficent(int sum, int runs)
        {
            return Math.Sqrt(2 * Math.Log(sum) / runs);
        }

    }
}
