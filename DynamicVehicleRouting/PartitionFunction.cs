﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRouting
{
    /// <summary>
    /// Funkcja optymalizująca centra pojazdów licząca "optymalne" załadowanie.
    /// </summary>
    class PartitionFunction : ClusterFunction
    {
        public PartitionFunction(Vehicle[] vehicles, Client[] clients, Depot[] depots, decimal currentProblemTime, double[,] distances, double weightPowerParameter, int classesForVehicle,
            bool fullMulticlustering, bool penalizeInsufficentVehicles, decimal cutoffTime, decimal bufferSize)
            : base(vehicles, clients, depots, currentProblemTime, distances, weightPowerParameter, classesForVehicle, fullMulticlustering, penalizeInsufficentVehicles, cutoffTime, bufferSize)
        {
        }

        protected override double CalculateValue()
        {
            double sum = 0.0;
            foreach (Vehicle v in Vehicles)
            {
                if (v.clientsToAssign.Count + v.assignedClients.Count > 0)
                {
                    double needsSum = v.clientsToAssign.Sum(clnt => clnt.Need) + v.assignedClients.Sum(clnt => clnt.Need);
                    sum += (needsSum - v.Capacity * 0.9) * (needsSum - v.Capacity * 0.9);
                }
            }
            return base.CalculateValue() + sum;
        }
    }
}
