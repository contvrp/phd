﻿#define USE_CACHE
#undef USE_CACHE

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRouting
{
    class DapsoClusterFunction: ClusterFunction
    {
        public DapsoClusterFunction(Vehicle[] vehicles, Client[] clients, Depot[] depots, decimal currentProblemTime, double[,] distances, double weightPowerParameter, int classesForVehicle)
            : base(vehicles, clients, depots, currentProblemTime, distances, weightPowerParameter, classesForVehicle) {
    }
        private Dictionary<string, double> assignmentValues = new Dictionary<string, double>();

        protected override double CalculateValue()
        {
            double sum = 0.0;
            foreach (Vehicle vehicle in Vehicles.Where(vhcl => vhcl.clientsToAssign.Count + vhcl.assignedClients.Count > 0))
            {
#if USE_CACHE
                    string key = string.Join(",", vehicle.clientsToAssign.OrderBy(clnt => clnt.Rank).Select(clnt => clnt.Id));
                    if (!assignmentValues.ContainsKey(key))
                    {
                        assignmentValues.Add(key, DVRPInstance.UpdateTotalRouteDistanceWith2OptedRoutes(Depots, distances, this.currentProblemTime, vehicle));
                    }
                    sum += assignmentValues[key];
#else                
                    sum += DVRPInstance.UpdateTotalRouteDistanceWith2OptedRoutes(Depots, distances, this.currentProblemTime, vehicle);
#endif
            }
            return sum;
        }
    }
}
