﻿using ParticleSwarmOptimization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRouting
{
    class MonteCarloWithPSOSolutionProposals : MonteCarloWithSolutionProposals
    {
        private int particleSwarmIterations;
        private int particleSwarmSize;
        private bool fullMutliclustering;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance">The instance to be solved</param>
        /// <param name="models">The number of propositions</param>
        /// <param name="generateDataFromModel">Generate from oracle?</param>
        public MonteCarloWithPSOSolutionProposals(DVRPInstance instance, int models, int additionalModels, int particleSwarmSize, int particleSwarmIterations,
            bool fullMutliclustering, bool penalizeInsufficentVehicles, decimal cutoffTime, bool generateDataFromModel = true, decimal bufferSize = 0.5M)
            : base(instance, models, additionalModels, generateDataFromModel, penalizeInsufficentVehicles, bufferSize, cutoffTime)
        {
            this.particleSwarmSize = particleSwarmSize;
            this.particleSwarmIterations = particleSwarmIterations;
            this.fullMutliclustering = fullMutliclustering;
            this.penalizeInsufficentVehicles = penalizeInsufficentVehicles;
        }

        protected override void ComputeRoutes(
            DVRPInstance fakeInstance,
            out ClusterFunction clusterFunction,
            out double sum,
            bool usePartialSolution,
            bool singleModel,
            object solution = null)
        {
            base.ComputeRoutes(fakeInstance, out clusterFunction, out sum, usePartialSolution, false, solution);
        }

        protected override object GetPreviousSolution()
        {
            return instance.StartVehicles.Where(vhcl => vhcl.assignedClients.Count + vhcl.clientsToAssign.Count > 0).ToArray();
        }

        protected override object ExtractSolution(ClusterFunction clusterFunction)
        {
            return clusterFunction.Vehicles;
        }

        protected override ClusterFunction AssignClients(DVRPInstance fakeInstance, object solution)
        {
            ParticleSwarmOptimization.IFunction clusterFunction = null;
            IEnumerable<int> ids = null;
            double[] center = new double[0], heuristicSolution = new double[0], discretizedCenter = new double[0];
            int[] hansharSolution, khouadjiaSolution;
            double radius;

            fakeInstance.InitializeBySpanningTree(out clusterFunction, out ids, out heuristicSolution, out hansharSolution, out khouadjiaSolution);

            if (solution != null)
            {
                clusterFunction = new DapsoClusterFunction(
                    solution as Vehicle[],
                    fakeInstance.ClientsToAssign.ToArray(),
                    fakeInstance.Depots,
                    fakeInstance.NextTime * (decimal)fakeInstance.MaxTime,
                    fakeInstance.distances,
                    0,
                    fakeInstance.Vehicles[0].X.Length,
                    fullMutliclustering,
                    penalizeInsufficentVehicles,
                    cutoffTime,
                    0M);
               clusterFunction.Value(
                    (solution as Vehicle[])
                        .SelectMany(vhcl => vhcl.X.SelectMany((val, key) => new double[] { val, vhcl.Y[key] })).ToArray());
            }
            else
            {
                fakeInstance.SetSwarms(
                    (int)(particleSwarmSize / Math.Floor(Math.Sqrt(models))),
                    0,
                    (int)((particleSwarmIterations * particleSwarmSize - models * (models - 1) - additionalModelsCount) / particleSwarmSize / Math.Ceiling(Math.Sqrt(models))));
                fakeInstance.InitializeForDAPSOClustering(out clusterFunction, out center, out discretizedCenter, out radius, ids);
                fakeInstance.PerformContinousClusteringOptimization(clusterFunction, center, radius, discretizedCenter, heuristicSolution);
            }
            return clusterFunction as ClusterFunction;
        }
    }
}
