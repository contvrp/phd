﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicVehicleRouting
{
    class KhouadjiaFunction : DapsoClusterFunction, DiscreteParticleSwarmOptimization.IFunction
    {
        public KhouadjiaFunction(Vehicle[] vehicles, Client[] clients, Depot[] depots, decimal currentProblemTime, double[,] distances, double weightPowerParameter, int classesForVehicle, bool fullMulticlustering,
            bool penalizeInsufficentVehicles, decimal cutoffTime, decimal bufferSize)
            : base(vehicles, clients, depots, currentProblemTime, distances, weightPowerParameter, classesForVehicle, fullMulticlustering, penalizeInsufficentVehicles, cutoffTime, bufferSize)
        {
        }

        public double Value(int[] x)
        {
            CleanVehiclesRequests();
            AssignClients(x);
            double value = CalculateValue();
            return value;
        }

        private void AssignClients(int[] x)
        {
            var currentClient = ClientsToAssign.OrderBy(clnt => clnt.Id).GetEnumerator();
            int dim = 0;
            while (currentClient.MoveNext())
            {
                var vehicle = Vehicles.FirstOrDefault(vhcl => vhcl.Id == currentClient.Current.FakeVehicleId);
                if (vehicle != null)
                {
                    vehicle.clientsToAssign.Add(currentClient.Current);
                }
                currentClient.Current.FakeVehicleId = x[dim++];
            }
            currentClient = ClientsToAssign
                .Where(clnt => clnt.FakeVehicleId == -1)
                .OrderBy(clnt => ParticleSwarmOptimization.Utils.Instance.random.Next())
                .GetEnumerator();
            while (currentClient.MoveNext())
            {
                int bestVehicleId = -1;
                decimal bestRank = 0.0M;
                double smallestIncrease = double.MaxValue;
                int bestVehicleNeed = int.MinValue;
                foreach (var testVehicle in Vehicles)
                {
                    testVehicle.clientsToAssign.RemoveAll(clnt => clnt.Id == currentClient.Current.Id);
                    testVehicle.CheckPossibilityAndCostOfAssigningClientToVehicle(
                        currentClient.Current,
                        ref bestVehicleId,
                        ref bestRank,
                        ref smallestIncrease,
                        Depots,
                        currentProblemTime,
                        distances,
                        ref bestVehicleNeed);
                }
                Vehicle chosenVehicle = Vehicles.First(vhcl => vhcl.Id == bestVehicleId);
                chosenVehicle.clientsToAssign.Add(currentClient.Current);
                currentClient.Current.Rank = bestRank;
                currentClient.Current.FakeVehicleId = bestVehicleId;
            }
            var rank = 0.0M;
            var clientIterator = ClientsToAssign.OrderBy(clnt => clnt.Rank).GetEnumerator();
            while (clientIterator.MoveNext())
            {
                clientIterator.Current.Rank = rank;
                rank += 1.0M / ClientsToAssign.Length;
            }
        }

        public static int[] GetKhouadjiaEncoding(IEnumerable<Vehicle> vehicles)
        {
            return vehicles
                    .SelectMany(vhcl => vhcl.clientsToAssign)
                        .Where(clnt => !(clnt is Depot))
                        .OrderBy(clnt => clnt.Id)
                        .Select(clnt => vehicles.FirstOrDefault(vhcl => vhcl.clientsToAssign.Any(clnt2 => clnt2.Id == clnt.Id)).Id)
                        .ToArray();
        }

        public static int[] GetKhouadjiaEncoding(IEnumerable<Client> clients)
        {
            return clients
                        .Where(clnt => !(clnt is Depot))
                        .OrderBy(clnt => clnt.Id)
                        .Select(clnt => clnt.FakeVehicleId)
                        .ToArray();
        }

        public int DiscreteDimension
        {
            get { return this.ClientsToAssign.Length; }
        }

        public static List<int[]> AdaptSolutions(List<int> oldClientsList, IEnumerable<Client> currentState, List<int[]> oldSolutions)
        {
            var result = new List<int[]>();
            var orderedClientsIds = currentState.Where(clnt => !(clnt is Depot))
                        .OrderBy(clnt => clnt.Id)
                        .ToList();
            foreach (var oldSolution in oldSolutions)
            {
                var adaptedSolution = new int[orderedClientsIds.Count];
                var clientEnumerator = orderedClientsIds.GetEnumerator();
                int dim = 0;
                while (clientEnumerator.MoveNext())
                {
                    var vehicleAssignmentIndex = oldClientsList.IndexOf(clientEnumerator.Current.Id);
                    adaptedSolution[dim++] = vehicleAssignmentIndex > -1 ? oldSolution[vehicleAssignmentIndex] : -1;
                }
                result.Add(adaptedSolution);
            }
            return result;
        }
    }
}
