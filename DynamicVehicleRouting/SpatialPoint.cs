﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParticleSwarmOptimization;
using System.Runtime.Serialization;

namespace DynamicVehicleRouting
{
    [DataContract]
    public class SpatialPoint
    {
        [DataMember]
        public double X;
        [DataMember]
        public double Y;
        [DataMember]
        public int Id;

        public virtual double DistanceTo(SpatialPoint other)
        {
            return Utils.Instance.EuclideanDistance(
                new double[] { this.X, this.Y},
                new double[] {other.X, other.Y}
                );
        }


    }
}
