N2 >> N1
1. Wygeneruj N1 problemów
2. Rozwiąż każdy z nich (sprawdzić zarówno klasteryzację jak i pojedyncze przypisania)
3. Wygeneruj N2 problemów testując jako rozwiązanie bazowe każdy z N1 (prowadź eksperyment UCB np. z klasteryzacji albo zachłannego wstawiania)
4. Wskaż ten o najmniejszej średniej jako rozwiązanie (co z koniecznością dołożenia pojazdu?)
5. Wygeneruj nowe N1 problemów (rozgrywając z najlepszego albo z UCB) -> mamy w ten sposób 2N1 propozycji rozwiązań
6. Wygeneruj N2 sprawdzających to 2N1
7. Wskaż ten o najmniejszej średniej a następnie (jeżeli to konieczne) wytnij rozwiązania, które nie pasują do AssignedClients
(a w pozostałych rozwiązaniach przesuń do assigned) i dalej do 5.

UWAGA:
Warto sprawdzić, czy jak wyprostuję trasy przed wyrzuceniem sztucznych zamówień,
to nie będzie lepiej.