﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DVRPRequestsDensityAnalysis
{
    [Serializable]
    class Permute
    {
        public double[,] distances;
        public int[] availability_times;
        public int[] loading_times;
        public double[] requests;
        public long permutations = 0;
        public int id;
        public int N;
        public List<int> bestPermutation;

        private void swap(ref int a, ref int b)
        {
            if (a == b) return;
            a ^= b;
            b ^= a;
            a ^= b;
        }

        public void setper(int[] list)
        {
            int x = list.Length - 1;
            go(list, 0, x);
        }

        public double min_dist = double.MaxValue;

        private void go(int[] list, int k, int m)
        {
            if (k == m)
            {
                double time = 0;
                double dist = 0;
                double cargo = 0;
                ++permutations;
                for (int i = 0; i < list.Length; ++i)
                {
                    time = Math.Max(time, (availability_times[list[i]] > Program.TIME_MAX / 2) ? 0 : availability_times[list[i]]);
                    if (i == 0)
                    {
                        time += distances[list[i], N];
                        dist += distances[list[i], N];
                        cargo = Program.CAPACITY;
                    }
                    else if (requests[list[i]] > cargo)
                    {
                        cargo = Program.CAPACITY;
                        time += distances[list[i - 1], N] + distances[list[i], N];
                        dist += distances[list[i - 1], N] + distances[list[i], N];
                    }
                    else
                    {
                        time += distances[list[i - 1], list[i]];
                        dist += distances[list[i - 1], list[i]];
                    }
                    cargo -= requests[list[i]];
                    time += loading_times[list[i]];
                }
                time += distances[list[list.Length - 1], N];
                dist += distances[list[list.Length - 1], N];
                if (time <= Program.TIME_MAX && dist < min_dist)
                {
                    //Console.Write(String.Join(",",list));
                    bestPermutation = list.ToList();
                    min_dist = dist;
                    //Console.WriteLine(" ");
                }
            }
            else
            {
                for (int i = k; i <= m; i++)
                {
                    swap(ref list[k], ref list[i]);
                    go(list, k + 1, m);
                    swap(ref list[k], ref list[i]);
                }
            }
        }
    }

}
