﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using DCPFramework.Connection;
using DCPFramework.Thread;

namespace DVRPRequestsDensityAnalysis
{
    class Program
    {

        static Random r = new Random();

        public const int LOAD_TIME = 20;
        public const int CAPACITY = 100;
        public const int X_MIN = -100;
        public const int X_MAX = 100;
        public const int Y_MIN = -100;
        public const int Y_MAX = 100;
        public static int TIME_MAX;

        static double[,] locations = new double[2, N];
        static string locationsString = "";
        static string visitsString = "";
        static double[] requests = new double[N];
        static string requestsString = "";
        static int[] availability_times = new int[N];
        static string availability_timesString = "";
        static int[] loading_times = new int[N];
        static string loading_timesString = "";

        public static int N = 8;
        private static int NMAX = 17;
        public static int times = 6;

        private static void OneExperiment(object _id, string[] args, bool local, int k)
        {
            char id = (char)('a' + (int)_id);
            double minDist = double.MaxValue;

            int count = Environment.ProcessorCount;
            if (!local)
            {
                count = RemoteThreadManager.GetAvailableThreadsCount();
            }
            Console.WriteLine("Threads: {0}", count);
            List<DcpRemoteThread> remoteThreads = new List<DcpRemoteThread>();
            List<Thread> threads = new List<Thread>();
            string serializedLocations = "";
            for (int i = 0; i < 2 * N; i++ )
            {
                serializedLocations += locations[i % 2, i / 2] + ";";
            }
            List<ProblemInstance> instances = new List<ProblemInstance>();
            for (int i = 0; i < count; i++)
            {
                //if (1 > r.Next(4 * count))
                //    throw new Exception();

                ProblemInstance instance = new ProblemInstance()
                {
                    id = i,
                    countProc = count,
                    serializedAvailabilityTimes = string.Join(";", availability_times),
                    serializedLoadingTimes = string.Join(";", loading_times),
                    serializedRequests = string.Join(";", requests),
                    serializedLocations = serializedLocations,
                    N = N
                };
                if (local)
                {
                    instances.Add(instance);
                    Thread thread = new Thread(instance.Run);
                    thread.Start();
                    threads.Add(thread);
                }
                else
                {
                    DcpRemoteThread thread = new DcpRemoteThread(instance.Run);
                    thread.Start();
                    //count = RemoteThreadManager.GetAvailableThreadsCount();
                    remoteThreads.Add(thread);
                }
            }
            List<double> results = new List<double>();
            if (local)
            {
                foreach (Thread thread in threads)
                {
                    thread.Join();
                }
            }
            else
            {
                foreach (DcpRemoteThread thread in remoteThreads)
                {
                    //if (1 > r.Next(4 * count))
                    //    throw new Exception();
                    instances.Add((ProblemInstance)thread.Join());
                }
            }

            minDist = instances.Min(inst => inst.minDist);
            long assignments = instances.Sum(inst => inst.assignments);

            foreach (ProblemInstance inst in instances)
            {
                Console.WriteLine("Perm:{0} Assign:{1} Res:{2}", inst.permutations, inst.assignments, inst.minDist);
            }
            Console.WriteLine("Result: {0}", minDist);

            File.WriteAllText("io2_" + N + "_" + (k == 0 ? "plain" : (k == 1 ? "clustered" : "semi")) + "_" + id + "_" + "D.vrp", string.Format(Properties.Resources.FileTemplate,
                string.Format("{0:0.00}", minDist).Replace(",", "."),
                "io2_" + N + id,
                N,
                N + 1,
                CAPACITY,
                requestsString,
                locationsString,
                visitsString,
                loading_timesString,
                TIME_MAX,
                availability_timesString
                ));
            File.WriteAllLines("io2_" + N + "_" + (k == 0 ? "plain" : (k == 1 ? "clustered" : "semi")) + "_" + id + "_" + "D.solution",
                instances.First(inst => inst.minDist == minDist).bestDivision.Select(permutation => string.Join<int>(", ", permutation.Select(idx => idx + 1)))
                );
        }

       // static Random r = new Random();
        static void Main(string[] args)
        {
            bool local = true;
            foreach (string arg in args)
            {
                string[] pair = arg.Split('=');
                if (pair[0] == "MINN")
                    N = int.Parse(pair[1]);
                if (pair[0] == "MAXN")
                    NMAX = int.Parse(pair[1]);
                if (pair[0] == "L")
                    local = bool.Parse(pair[1]);
            }
            string path = System.IO.Path.GetFileName(System.Reflection.Assembly.GetEntryAssembly().Location);
            for (; N < NMAX; N++)
            {
                for (int l = 0; l < times; ++l)
                {
                    TIME_MAX = 400 + N * LOAD_TIME;
                    locations = new double[2, N];
                    locationsString = "";
                    visitsString = "";
                    requests = new double[N];
                    requestsString = "";
                    availability_times = new int[N];
                    availability_timesString = "";
                    loading_times = new int[N];
                    loading_timesString = "";
                    CreateRandomCase();
                    for (int k = 0; k < 1; k++)
                    {
                        try
                        {
                            try
                            {
                                if (!local)
                                {
                                    RemoteThreadManager.Initialize(args, "194.29.178.195", 1234, "dcpUser", "dcpUser", "DCP_C#_TSP2", path, false);
                                    local = false;
                                }
                            }
                            catch
                            {
                                local = true;
                                Console.WriteLine("DCP System is down...");
                            }

                            //if (1 > r.Next(4))
                            //{
                            //    j++;
                            //    throw new Exception();
                            //}
                            Console.Title = string.Format("{0} {1} {2}", k, !local, N);

                            DateTime start = DateTime.Now;
                            OneExperiment(l, args, local, k);
                            //if (1 > r.Next(10))
                            //    throw new Exception();
                            Console.WriteLine(DateTime.Now - start);
                            ChangeRandomCase(k + 1);
                        }
                        catch
                        {
                            Console.WriteLine("Restarting for N={0} and experiment={1}", N, k);
                            k--;
                        }
                        finally
                        {
                            if (!local)
                                RemoteThreadManager.Close();
                            foreach (string arg in args)
                            {
                                string[] pair = arg.Split('=');
                                if (pair[0] == "L")
                                    local = bool.Parse(pair[1]);
                            }

                        }
                    }
                }
            }
        }

        private static void CreateRandomCase()
        {
            for (int i = 0; i < N; i++)
            {
                requests[i] = (r.Next(CAPACITY / 2) + r.Next(CAPACITY / 2)) / 2 + 1;
                requestsString += string.Format("  {0} {1}\n", i + 1, -requests[i]);
                locations[0, i] = r.Next(X_MAX - X_MIN) + X_MIN;
                locations[1, i] = r.Next(Y_MAX - Y_MIN) + Y_MIN;
                locationsString += string.Format("  {0} {1} {2}\n", i + 1, locations[0, i], locations[1, i]);
                visitsString += string.Format("  {0} {1}\n", i + 1, i + 1);
                availability_times[i] = r.Next(TIME_MAX);
                availability_timesString += string.Format("  {0} {1}\n", i + 1, availability_times[i]);
                loading_times[i] = LOAD_TIME;
                loading_timesString += string.Format("  {0} {1}\n", i + 1, loading_times[i]);
            }
        }

        private static void ChangeRandomCase(int j)
        {
            int regions = N / 4;
            int division = (int)Math.Floor(Math.Sqrt(N));
            locationsString = "";
            List<int[]> rulette = new List<int[]>();
            if (j > 0)
            {
                while (rulette.Count < regions)
                {
                    int[] region = new int[2] { r.Next(division), r.Next(division) };
                    bool add = true;
                    foreach (int[] regionExisting in rulette)
                    {
                        if (regionExisting[0] == region[0] && regionExisting[1] == region[1])
                        {
                            add = false;
                            break;
                        }
                    }
                    if (add)
                    {
                        rulette.Add(region);
                    }
                }
            }
            if (j > 1)
            {
                for (int k = 0; k < division; ++k)
                    for (int l = 0; l < division; ++l)
                        rulette.Add(new int[] {k,l});
            }
            for (int i = 0; i < N; i++)
            {
                int[] region = rulette[r.Next(rulette.Count)];
                locations[0, i] = r.Next((X_MAX - X_MIN) / division) + X_MIN + ((X_MAX - X_MIN) * region[0]) / division;
                locations[1, i] = r.Next((Y_MAX - Y_MIN) / division) + Y_MIN + ((Y_MAX - Y_MIN) * region[1]) / division;
                locationsString += string.Format("  {0} {1} {2}\n", i + 1, locations[0, i], locations[1, i]);
            }
        }
    }
}
