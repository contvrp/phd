﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DVRPRequestsDensityAnalysis
{
    [Serializable]
    public class ProblemInstance : DCPFramework.Remote.DcpRemoteRunnable
    {
        public int countProc;
        public int id;
        public int N;
        [NonSerialized]
        private double[,] distances;
        public string serializedLocations;
        [NonSerialized]
        private double[,] locations;
        public string serializedRequests;
        [NonSerialized]
        private double[] requests;
        public string serializedAvailabilityTimes;
        [NonSerialized]
        private int[] availability_times;
        public string serializedLoadingTimes;
        [NonSerialized]
        private int[] loading_times;

        [NonSerialized]
        int[] vehicleAssignment;
        [NonSerialized]
        int[,] routeAssignemt;

        public double minDist = double.MaxValue;
        public long assignments = 0;
        public long permutations = 0;
        public long divisions = 0;
        public long counter = -1;
        List<List<int>> currentDivision;
        public List<List<int>> bestDivision = new List<List<int>>();

        private void InitializeDistanceArray()
        {
            for (int i = 0; i < N; i++)
            {
                distances[i, N] = Math.Sqrt(
                    (locations[0, i] - 0) * (locations[0, i] - 0) +
                    (locations[1, i] - 0) * (locations[1, i] - 0));
                for (int j = 0; j < N; j++)
                    distances[i, j] = Math.Sqrt(
                        (locations[0, i] - locations[0, j]) * (locations[0, i] - locations[0, j]) +
                        (locations[1, i] - locations[1, j]) * (locations[1, i] - locations[1, j]));
            }
        }


        public void Run()
        {
            loading_times = serializedLoadingTimes.Split(';').Select(a => int.Parse(a)).ToArray();
            availability_times = serializedAvailabilityTimes.Split(';').Select(a => int.Parse(a)).ToArray();
            requests = serializedRequests.Split(';').Select(a => double.Parse(a)).ToArray();
            vehicleAssignment = new int[N];
            routeAssignemt = new int[2 * N, N];
            locations = new double[2, N];
            distances = new double[N, N + 1];
            int i = 0;
            foreach (string token in serializedLocations.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                locations[i % 2, i / 2] = double.Parse(token);
                i++;
            }

            InitializeDistanceArray();
            nextDivision(0, new List<List<int>>());
            /*
            AddToVehicleAssigment(id);
            while (vehicleAssignment.Sum() <= (Program.N - 1) * Program.N)
            {
                if (++assignments % 5000000 == 0)
                {
                    Console.WriteLine("{0}:{1}", id, DateTime.Now);
                    Console.WriteLine(Math.Pow(Program.N, Program.N));
                }
                GenerateAllPermutations();
                AddToVehicleAssigment(countProc);
            }
            */
        }

        void nextDivision(int n, List<List<int>> division)
        {
            if (n == N)
            {
                ++counter;
                if (counter % countProc == id)
                {
                    assignments++;
                    currentDivision = division;
                    if (assignments % 10000000 == 0)
                    {
                        Console.WriteLine("{0}:{1}:{2}", id, assignments, DateTime.Now);
                    }
                    foreach (List<int> list in currentDivision)
                    {
                        double sum = list.Sum(elem => requests[elem]);
                        if (sum > Program.CAPACITY)
                            return;
                    }
                    GenerateAllPermutations();
                }
            }
            else
            {
                int i = 0;
                for (i = 0; i < division.Count; i++)
                {
                    division[i].Add(n);
                    nextDivision(n + 1, division);
                    division[i].Remove(n);
                }
                division.Add(new List<int>());
                division[i].Add(n);
                nextDivision(n + 1, division);
                division[i].Remove(n);
                division.RemoveAt(division.Count - 1);
            }
        }

        private void AddToVehicleAssigment(int k)
        {
            vehicleAssignment[0] += k;
            for (int i = 0; i < N; i++)
            {
                if (vehicleAssignment[i] >= N && i + 1 < N)
                {
                    vehicleAssignment[i + 1] += vehicleAssignment[i] / N;
                    vehicleAssignment[i] -= N * (vehicleAssignment[i] / N);
                }
            }
        }

        private void GenerateAllPermutations()
        {
            var test = vehicleAssignment.GroupBy(ch => ch);
            double temp_min_dist = 0.0;
            List<List<int>> currentDivisionWithPermutations = new List<List<int>>();
            foreach (List<int> indexes in currentDivision)
            {
                Permute p = new Permute() { id = id, availability_times = availability_times, distances = distances, loading_times = loading_times, requests = requests, N = N };
                int[] c2 = indexes.ToArray();
                p.setper(c2);
                temp_min_dist += p.min_dist;
                permutations += p.permutations;
                currentDivisionWithPermutations.Add(p.bestPermutation);
            }
            if (temp_min_dist < minDist)
            {
                minDist = temp_min_dist;
                bestDivision.Clear();
                foreach (List<int> indexes in currentDivisionWithPermutations)
                {
                    List<int> bestPerm = new List<int>();
                    bestPerm.AddRange(indexes);
                    bestDivision.Add(bestPerm);
                }
            }
        }



        public void ParametrizedRun(object param)
        {
            throw new NotImplementedException();
        }

    }
}
