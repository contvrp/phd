﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DynamicVehicleRouting;
using ParticleSwarmOptimization;

namespace AlgorithmsTest
{
    [TestClass]
    public class TestLambdaInterchange
    {
        private Vehicle[] vehicles;
        private Depot[] depots;
        private Client[] clients;
        private double[,] distances;

        [TestInitialize]
        public void CreateData()
        {
            clients = new Client[]
            {
                        new Client()
                        {
                            Id = 1,
                            Rank = 0.0M,
                            StartAvailable = 0,
                            TimeToUnload = 0,
                            X = -2,
                            Y = 0,
                            Need = -1,
                            FakeVehicleId = 7,
                        },
                        new Client()
                        {
                            Id = 2,
                            Rank = 0.1M,
                            StartAvailable = 0,
                            TimeToUnload = 0,
                            X = -2,
                            Y = 2,
                            Need = -1,
                            FakeVehicleId = 7,
                        },
                        new Client()
                        {
                            Id = 3,
                            Rank = 0.2M,
                            StartAvailable = 0,
                            TimeToUnload = 0,
                            X = 1,
                            Y = 2,
                            Need = -1,
                            FakeVehicleId = 7,
                        },
                        new Client()
                        {
                            Id = 4,
                            Rank = 0.0M,
                            StartAvailable = 0,
                            TimeToUnload = 0,
                            X = 2,
                            Y = 0,
                            Need = -1,
                            FakeVehicleId = 8,
                        },
                        new Client()
                        {
                            Id = 5,
                            Rank = 0.1M,
                            StartAvailable = 0,
                            TimeToUnload = 0,
                            X = 2,
                            Y = 2,
                            Need = -1,
                            FakeVehicleId = 8,
                        },
                        new Client()
                        {
                            Id = 6,
                            Rank = 0.2M,
                            StartAvailable = 0,
                            TimeToUnload = 0,
                            X = -1,
                            Y = 2,
                            Need = -1,
                            FakeVehicleId = 8,
                        },
            };
            depots = new Depot[]
            {
                new Depot()
                {
                    Id = 0,
                    X = 0,
                    Y = 0,
                    StartAvailable = 0,
                    EndAvailable = int.MaxValue
                }
            };

            distances = new double[clients.Length + depots.Length, clients.Length + depots.Length];
            foreach (Client client1 in clients.Concat(depots))
            {
                foreach (Client client2 in clients.Concat(depots))
                {
                    distances[client1.Id, client2.Id] = client1.DistanceTo(client2);
                }
            }

        }

        private void DefineTwoVehicleDivision()
        {
            vehicles = new Vehicle[]
            {
                new Vehicle() {
                    Id = 7,
                    clientsToAssign = new System.Collections.Generic.List<Client>()
                    {
                        depots[0],
                        clients[0],
                        clients[1],
                        clients[2]
                    },
                    assignedClients = new System.Collections.Generic.List<Client>(),
                    X = new double[1],
                    Y = new double[1],
                    Capacity = 3,
                    Available = true
                },
                new Vehicle() {
                    Id = 8,
                    clientsToAssign = new System.Collections.Generic.List<Client>()
                    {
                        depots[0],
                        clients[3],
                        clients[4],
                        clients[5],

                    },
                    assignedClients = new System.Collections.Generic.List<Client>(),
                    X = new double[1],
                    Y = new double[1],
                    Capacity = 3,
                    Available = true
                },
            };
        }

        /// <summary>
        /// Tests the simple entangled case (6 and 3 need to be switched)
        /// </summary>
        [TestMethod]
        public void EntanglementTest()
        {
            DefineTwoVehicleDivision();
            DVRPInstance.OptimizeWithLambdaInterchange(vehicles, 2, 2, depots, 0.0M, distances);
            Assert.IsTrue(vehicles[0].clientsToAssign.Any(clnt => clnt.Id == 1));
            Assert.IsTrue(vehicles[0].clientsToAssign.Any(clnt => clnt.Id == 2));
            Assert.IsTrue(vehicles[0].clientsToAssign.Any(clnt => clnt.Id == 6));
            Assert.IsTrue(vehicles[1].clientsToAssign.Any(clnt => clnt.Id == 4));
            Assert.IsTrue(vehicles[1].clientsToAssign.Any(clnt => clnt.Id == 5));
            Assert.IsTrue(vehicles[1].clientsToAssign.Any(clnt => clnt.Id == 3));
        }

        /// <summary>
        /// Tests the simple switch case, lonely solution must be switched from the third vehicle
        /// </summary>
        [TestMethod]
        public void SwitchTest()
        {
            DefineThreeVehicleDivision();
            DVRPInstance.OptimizeWithLambdaInterchange(vehicles, 2, 2, depots, 0.0M, distances);
            Assert.AreEqual(clients[0].FakeVehicleId, clients[1].FakeVehicleId);
            Assert.AreEqual(clients[1].FakeVehicleId, clients[5].FakeVehicleId);
            Assert.AreEqual(clients[3].FakeVehicleId, clients[4].FakeVehicleId);
            Assert.AreEqual(clients[4].FakeVehicleId, clients[2].FakeVehicleId);
            Assert.AreNotEqual(clients[1].FakeVehicleId, clients[2].FakeVehicleId);
        }

        private void DefineThreeVehicleDivision()
        {
            vehicles = new Vehicle[]
            {
                new Vehicle() {
                    Id = 7,
                    clientsToAssign = new System.Collections.Generic.List<Client>()
                    {
                        depots[0],
                        clients[0],
                        clients[1],
                        clients[5]
                    },
                    assignedClients = new System.Collections.Generic.List<Client>(),
                    X = new double[1],
                    Y = new double[1],
                    Capacity = 3,
                    Available = true
                },
                new Vehicle() {
                    Id = 8,
                    clientsToAssign = new System.Collections.Generic.List<Client>()
                    {
                        depots[0],
                        clients[3],
                        clients[2],

                    },
                    assignedClients = new System.Collections.Generic.List<Client>(),
                    X = new double[1],
                    Y = new double[1],
                    Capacity = 3,
                    Available = true
                },
                new Vehicle() {
                    Id = 9,
                    clientsToAssign = new System.Collections.Generic.List<Client>()
                    {
                        depots[0],
                        clients[4],

                    },
                    assignedClients = new System.Collections.Generic.List<Client>(),
                    X = new double[1],
                    Y = new double[1],
                    Capacity = 3,
                    Available = true
                },
            };
        }

        [TestMethod]
        public void TestRouteWithDepotGreedyInsertion()
        {
            DefineThreeVehicleDivision();
            double smallestIncrease = double.PositiveInfinity;
            int bestVehicleId = -1;
            int bestNeed = -vehicles[2].Capacity - 1;
            decimal bestRank = 0.0M;
            DVRPInstance.CheckPossibilityAndCostOfAssigningClientToVehicle(vehicles[1].clientsToAssign[2], ref bestVehicleId,
                ref bestRank, ref smallestIncrease, vehicles[2], depots, 0.0M, distances, ref bestNeed);
            Assert.AreEqual(1, vehicles[2].clientsToAssign.Count(clnt => clnt is Depot));
            vehicles[2].clientsToAssign.RemoveAt(0);
            DVRPInstance.CheckPossibilityAndCostOfAssigningClientToVehicle(vehicles[1].clientsToAssign[2], ref bestVehicleId,
                ref bestRank, ref smallestIncrease, vehicles[2], depots, 0.0M, distances, ref bestNeed);
            Assert.AreEqual(1, vehicles[2].clientsToAssign.Count(clnt => clnt is Depot));
        }

        [TestMethod]
        public void TestRouteWithAssignedClientsGreedyInsertion()
        {
            //this test did not show anything wrong with depots
            //while there exist assigned clients
            Vehicle newVehicle = new Vehicle()
            {
                assignedClients = new System.Collections.Generic.List<Client>(),
                clientsToAssign = new System.Collections.Generic.List<Client>(),
                Id = clients.Max(clnt => clnt.Id) + 1,
                Capacity = 3,
                Available = true,
                X = new double[1],
                Y = new double[1]
            };
            clients[1].Rank = 0.1M;
            clients[2].Rank = 0.2M;
            newVehicle.assignedClients.Add(depots[0]);
            newVehicle.assignedClients.Add(clients[1]);
            newVehicle.assignedClients.Add(clients[2]);

            double smallestIncrease = double.PositiveInfinity;
            int bestVehicleId = -1;
            int bestNeed = -newVehicle.Capacity - 1;
            decimal bestRank = 0.0M;
            DVRPInstance.CheckPossibilityAndCostOfAssigningClientToVehicle(clients[0], ref bestVehicleId,
                ref bestRank, ref smallestIncrease, newVehicle, depots, 0.0M, distances, ref bestNeed);
            Assert.AreEqual(0, newVehicle.clientsToAssign.Count(clnt => clnt is Depot));
        }

    }
}
