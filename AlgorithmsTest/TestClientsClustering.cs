﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DynamicVehicleRouting
{
    [TestClass]
    public class TestClientsClustering
    {
        const int DIM = 1;
        Vehicle[] vehicles;
        private Client[] clients;
        private Depot[] depots;
        private double[,] distances;

        [TestInitialize]
        public void CreateData()
        {
            vehicles = new Vehicle[] 
            {
                new Vehicle()
                {
                    Id = 6,
                    Capacity = 4,
                    Available = true,
                    clientsToAssign = new List<Client>(),
                    assignedClients = new List<Client>(),
                    X = new double[DIM],
                    Y = new double[DIM]
                },
                new Vehicle()
                {
                    Id = 7,
                    Capacity = 4,
                    Available = true,
                    clientsToAssign = new List<Client>(),
                    assignedClients = new List<Client>(),
                    X = new double[DIM],
                    Y = new double[DIM]
                }
            };

            clients = new Client[]
            {
                new Client()
                {
                    Id = 1,
                    Need = -1,
                    StartAvailable = 0,
                    TimeToUnload = 0,
                    X = 0,
                    Y = 0
                },
                new Client()
                {
                    Id = 2,
                    Need = -1,
                    StartAvailable = 0,
                    TimeToUnload = 0,
                    X = 0.9,
                    Y = 0
                },
                new Client()
                {
                    Id = 3,
                    Need = -1,
                    StartAvailable = 0,
                    TimeToUnload = 0,
                    X = 1.9,
                    Y = 0
                },
                new Client()
                {
                    Id = 4,
                    Need = -1,
                    StartAvailable = 0,
                    TimeToUnload = 0,
                    X = 3.9,
                    Y = 0
                },
                new Client()
                {
                    Id = 5,
                    Need = -1,
                    StartAvailable = 0,
                    TimeToUnload = 0,
                    X = 6.9,
                    Y = 0
                },
            };
            depots = new Depot[]
            {
                new Depot()
                {
                Id = 0,
                X = -1,
                Y = 0
                }

            };
        }

        [TestMethod]
        public void TestTreeClustering()
        {
            TreeClusterFunction<Client> treeClusterFunction = new TreeClusterFunction<Client>(vehicles, clients, depots, 0.0M, distances, 0.0, DIM, 0);
            treeClusterFunction.Value(new double[0]);
            Assert.AreEqual(clients[0].FakeVehicleId, clients[1].FakeVehicleId);
            Assert.AreEqual(clients[1].FakeVehicleId, clients[2].FakeVehicleId);
            Assert.AreEqual(clients[2].FakeVehicleId, clients[3].FakeVehicleId);
            Assert.AreNotEqual(clients[3].FakeVehicleId, clients[4].FakeVehicleId);
        }

        [TestMethod]
        public void TestMinClustering()
        {
            TreeClusterFunction<MinClusterClient> treeClusterFunction = new TreeClusterFunction<MinClusterClient>(vehicles, clients, depots, 0.0M, distances, 0.0, DIM, 0);
            treeClusterFunction.Value(new double[0]);
            Assert.AreEqual(clients[0].FakeVehicleId, clients[1].FakeVehicleId);
            Assert.AreEqual(clients[1].FakeVehicleId, clients[2].FakeVehicleId);
            Assert.AreEqual(clients[2].FakeVehicleId, clients[3].FakeVehicleId);
            Assert.AreNotEqual(clients[3].FakeVehicleId, clients[4].FakeVehicleId);
        }

        [TestMethod]
        public void TestMaxClustering()
        {
            TreeClusterFunction<MaxClusterClient> treeClusterFunction = new TreeClusterFunction<MaxClusterClient>(vehicles, clients, depots, 0.0M, distances, 0.0, DIM, 0);
            treeClusterFunction.Value(new double[0]);
            Assert.AreEqual(clients[0].FakeVehicleId, clients[1].FakeVehicleId);
            Assert.AreEqual(clients[1].FakeVehicleId, clients[2].FakeVehicleId);
            Assert.AreEqual(clients[3].FakeVehicleId, clients[4].FakeVehicleId);
            Assert.AreNotEqual(clients[2].FakeVehicleId, clients[3].FakeVehicleId);
        }

        [TestMethod]
        public void TestAvgClustering()
        {
            TreeClusterFunction<AvgClusterClient> treeClusterFunction = new TreeClusterFunction<AvgClusterClient>(vehicles, clients, depots, 0.0M, distances, 0.0, DIM, 0);
            treeClusterFunction.Value(new double[0]);
            Assert.AreEqual(clients[0].FakeVehicleId, clients[1].FakeVehicleId);
            Assert.AreEqual(clients[1].FakeVehicleId, clients[2].FakeVehicleId);
            Assert.AreEqual(clients[2].FakeVehicleId, clients[3].FakeVehicleId);
            Assert.AreNotEqual(clients[3].FakeVehicleId, clients[4].FakeVehicleId);
        }
    }
}
